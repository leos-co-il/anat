-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Sep 05, 2021 at 07:26 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anat`
--

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_actions`
--

CREATE TABLE `fmn_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_actions`
--

INSERT INTO `fmn_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(36, 'action_scheduler/migration_hook', 'complete', '2021-09-02 15:54:57', '2021-09-02 18:54:57', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630598097;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630598097;}', 1, 1, '2021-09-02 15:55:04', '2021-09-02 18:55:04', 0, NULL),
(37, 'wc-admin_import_customers', 'complete', '2021-09-03 14:50:53', '2021-09-03 17:50:53', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630680653;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630680653;}', 2, 1, '2021-09-03 14:51:45', '2021-09-03 17:51:45', 0, NULL),
(38, 'wc-admin_import_customers', 'complete', '2021-09-04 06:57:27', '2021-09-04 09:57:27', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630738647;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630738647;}', 2, 1, '2021-09-04 06:58:26', '2021-09-04 09:58:26', 0, NULL),
(39, 'wc-admin_import_customers', 'complete', '2021-09-05 00:21:43', '2021-09-05 03:21:43', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630801303;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630801303;}', 2, 1, '2021-09-05 00:21:51', '2021-09-05 03:21:51', 0, NULL),
(40, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 05:52:25', '2021-09-05 08:52:25', 0, NULL),
(41, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 06:07:42', '2021-09-05 09:07:42', 0, NULL),
(42, 'adjust_download_permissions', 'complete', '2021-09-05 06:57:25', '2021-09-05 09:57:25', '[223]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630825045;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630825045;}', 0, 1, '2021-09-05 06:57:53', '2021-09-05 09:57:53', 0, NULL),
(43, 'adjust_download_permissions', 'complete', '2021-09-05 06:58:18', '2021-09-05 09:58:18', '[223]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630825098;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630825098;}', 0, 1, '2021-09-05 06:59:34', '2021-09-05 09:59:34', 0, NULL),
(44, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 06:59:34', '2021-09-05 09:59:34', 0, NULL),
(45, 'action_scheduler/migration_hook', 'complete', '2021-09-05 10:16:47', '2021-09-05 13:16:47', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630837007;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630837007;}', 1, 1, '2021-09-05 10:17:10', '2021-09-05 13:17:10', 0, NULL),
(46, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 14:11:56', '2021-09-05 17:11:56', 0, NULL),
(47, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 14:13:00', '2021-09-05 17:13:00', 0, NULL),
(48, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 14:13:00', '2021-09-05 17:13:00', 0, NULL),
(49, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 14:13:00', '2021-09-05 17:13:00', 0, NULL),
(50, 'adjust_download_permissions', 'complete', '2021-09-05 14:40:05', '2021-09-05 17:40:05', '[246]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630852805;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630852805;}', 0, 1, '2021-09-05 14:40:36', '2021-09-05 17:40:36', 0, NULL),
(51, 'adjust_download_permissions', 'complete', '2021-09-05 14:40:48', '2021-09-05 17:40:48', '[246]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630852848;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630852848;}', 0, 1, '2021-09-05 14:40:57', '2021-09-05 17:40:57', 0, NULL),
(52, 'adjust_download_permissions', 'complete', '2021-09-05 14:40:58', '2021-09-05 17:40:58', '[246]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630852858;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630852858;}', 0, 1, '2021-09-05 14:41:44', '2021-09-05 17:41:44', 0, NULL),
(53, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 14:41:50', '2021-09-05 17:41:50', 0, NULL),
(54, 'adjust_download_permissions', 'complete', '2021-09-05 14:41:46', '2021-09-05 17:41:46', '[246]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1630852906;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1630852906;}', 0, 1, '2021-09-05 14:41:50', '2021-09-05 17:41:50', 0, NULL),
(55, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 14:42:45', '2021-09-05 17:42:45', 0, NULL),
(56, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-09-05 14:42:45', '2021-09-05 17:42:45', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_claims`
--

CREATE TABLE `fmn_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_groups`
--

CREATE TABLE `fmn_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_groups`
--

INSERT INTO `fmn_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wc-admin-data');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_logs`
--

CREATE TABLE `fmn_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_logs`
--

INSERT INTO `fmn_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 36, 'action created', '2021-09-02 15:53:57', '2021-09-02 18:53:57'),
(2, 36, 'action started via WP Cron', '2021-09-02 15:55:04', '2021-09-02 18:55:04'),
(3, 36, 'action complete via WP Cron', '2021-09-02 15:55:04', '2021-09-02 18:55:04'),
(4, 37, 'הפעולה נוצרה', '2021-09-03 14:50:48', '2021-09-03 17:50:48'),
(5, 37, 'הפעולה התחילה דרך WP Cron', '2021-09-03 14:51:45', '2021-09-03 17:51:45'),
(6, 37, 'הפעולה הושלמה דרך WP Cron', '2021-09-03 14:51:45', '2021-09-03 17:51:45'),
(7, 38, 'הפעולה נוצרה', '2021-09-04 06:57:22', '2021-09-04 09:57:22'),
(8, 38, 'הפעולה התחילה דרך WP Cron', '2021-09-04 06:58:26', '2021-09-04 09:58:26'),
(9, 38, 'הפעולה הושלמה דרך WP Cron', '2021-09-04 06:58:26', '2021-09-04 09:58:26'),
(10, 39, 'הפעולה נוצרה', '2021-09-05 00:21:38', '2021-09-05 03:21:38'),
(11, 39, 'הפעולה התחילה דרך Async Request', '2021-09-05 00:21:51', '2021-09-05 03:21:51'),
(12, 39, 'הפעולה הושלמה דרך Async Request', '2021-09-05 00:21:51', '2021-09-05 03:21:51'),
(13, 40, 'הפעולה נוצרה', '2021-09-05 05:51:42', '2021-09-05 08:51:42'),
(14, 40, 'הפעולה התחילה דרך WP Cron', '2021-09-05 05:52:25', '2021-09-05 08:52:25'),
(15, 40, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 05:52:25', '2021-09-05 08:52:25'),
(16, 41, 'הפעולה נוצרה', '2021-09-05 06:06:17', '2021-09-05 09:06:17'),
(17, 41, 'הפעולה התחילה דרך WP Cron', '2021-09-05 06:07:42', '2021-09-05 09:07:42'),
(18, 41, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 06:07:42', '2021-09-05 09:07:42'),
(19, 42, 'הפעולה נוצרה', '2021-09-05 06:57:24', '2021-09-05 09:57:24'),
(20, 42, 'הפעולה התחילה דרך Async Request', '2021-09-05 06:57:52', '2021-09-05 09:57:52'),
(21, 42, 'הפעולה הושלמה דרך Async Request', '2021-09-05 06:57:53', '2021-09-05 09:57:53'),
(22, 43, 'הפעולה נוצרה', '2021-09-05 06:58:17', '2021-09-05 09:58:17'),
(23, 44, 'הפעולה נוצרה', '2021-09-05 06:58:24', '2021-09-05 09:58:24'),
(24, 44, 'הפעולה התחילה דרך WP Cron', '2021-09-05 06:59:34', '2021-09-05 09:59:34'),
(25, 44, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 06:59:34', '2021-09-05 09:59:34'),
(26, 43, 'הפעולה התחילה דרך WP Cron', '2021-09-05 06:59:34', '2021-09-05 09:59:34'),
(27, 43, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 06:59:34', '2021-09-05 09:59:34'),
(28, 45, 'הפעולה נוצרה', '2021-09-05 10:15:47', '2021-09-05 13:15:47'),
(29, 45, 'הפעולה התחילה דרך WP Cron', '2021-09-05 10:17:10', '2021-09-05 13:17:10'),
(30, 45, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 10:17:10', '2021-09-05 13:17:10'),
(31, 46, 'הפעולה נוצרה', '2021-09-05 14:11:46', '2021-09-05 17:11:46'),
(32, 46, 'הפעולה התחילה דרך WP Cron', '2021-09-05 14:11:56', '2021-09-05 17:11:56'),
(33, 46, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 14:11:56', '2021-09-05 17:11:56'),
(34, 47, 'הפעולה נוצרה', '2021-09-05 14:12:06', '2021-09-05 17:12:06'),
(35, 48, 'הפעולה נוצרה', '2021-09-05 14:12:18', '2021-09-05 17:12:18'),
(36, 49, 'הפעולה נוצרה', '2021-09-05 14:12:40', '2021-09-05 17:12:40'),
(37, 47, 'הפעולה התחילה דרך WP Cron', '2021-09-05 14:13:00', '2021-09-05 17:13:00'),
(38, 47, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 14:13:00', '2021-09-05 17:13:00'),
(39, 48, 'הפעולה התחילה דרך WP Cron', '2021-09-05 14:13:00', '2021-09-05 17:13:00'),
(40, 48, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 14:13:00', '2021-09-05 17:13:00'),
(41, 49, 'הפעולה התחילה דרך WP Cron', '2021-09-05 14:13:00', '2021-09-05 17:13:00'),
(42, 49, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 14:13:00', '2021-09-05 17:13:00'),
(43, 50, 'הפעולה נוצרה', '2021-09-05 14:40:04', '2021-09-05 17:40:04'),
(44, 50, 'הפעולה התחילה דרך Async Request', '2021-09-05 14:40:36', '2021-09-05 17:40:36'),
(45, 50, 'הפעולה הושלמה דרך Async Request', '2021-09-05 14:40:36', '2021-09-05 17:40:36'),
(46, 51, 'הפעולה נוצרה', '2021-09-05 14:40:47', '2021-09-05 17:40:47'),
(47, 51, 'הפעולה התחילה דרך WP Cron', '2021-09-05 14:40:57', '2021-09-05 17:40:57'),
(48, 51, 'הפעולה הושלמה דרך WP Cron', '2021-09-05 14:40:57', '2021-09-05 17:40:57'),
(49, 52, 'הפעולה נוצרה', '2021-09-05 14:40:57', '2021-09-05 17:40:57'),
(50, 52, 'הפעולה התחילה דרך Async Request', '2021-09-05 14:41:44', '2021-09-05 17:41:44'),
(51, 52, 'הפעולה הושלמה דרך Async Request', '2021-09-05 14:41:44', '2021-09-05 17:41:44'),
(52, 53, 'הפעולה נוצרה', '2021-09-05 14:41:45', '2021-09-05 17:41:45'),
(53, 54, 'הפעולה נוצרה', '2021-09-05 14:41:45', '2021-09-05 17:41:45'),
(54, 53, 'הפעולה התחילה דרך Async Request', '2021-09-05 14:41:50', '2021-09-05 17:41:50'),
(55, 53, 'הפעולה הושלמה דרך Async Request', '2021-09-05 14:41:50', '2021-09-05 17:41:50'),
(56, 54, 'הפעולה התחילה דרך Async Request', '2021-09-05 14:41:50', '2021-09-05 17:41:50'),
(57, 54, 'הפעולה הושלמה דרך Async Request', '2021-09-05 14:41:50', '2021-09-05 17:41:50'),
(58, 55, 'הפעולה נוצרה', '2021-09-05 14:42:08', '2021-09-05 17:42:08'),
(59, 56, 'הפעולה נוצרה', '2021-09-05 14:42:31', '2021-09-05 17:42:31'),
(60, 55, 'הפעולה התחילה דרך Async Request', '2021-09-05 14:42:45', '2021-09-05 17:42:45'),
(61, 55, 'הפעולה הושלמה דרך Async Request', '2021-09-05 14:42:45', '2021-09-05 17:42:45'),
(62, 56, 'הפעולה התחילה דרך Async Request', '2021-09-05 14:42:45', '2021-09-05 17:42:45'),
(63, 56, 'הפעולה הושלמה דרך Async Request', '2021-09-05 14:42:45', '2021-09-05 17:42:45');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_commentmeta`
--

CREATE TABLE `fmn_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_comments`
--

CREATE TABLE `fmn_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_comments`
--

INSERT INTO `fmn_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'מגיב וורדפרס', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-08-30 20:39:27', '2021-08-30 17:39:27', 'היי, זו תגובה.\nכדי לשנות, לערוך, או למחוק תגובות, יש לגשת למסך התגובות בלוח הבקרה.\nצלמית המשתמש של המגיב מגיעה מתוך <a href=\"https://gravatar.com\">גראווטר</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_links`
--

CREATE TABLE `fmn_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_options`
--

CREATE TABLE `fmn_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_options`
--

INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://anat:8888', 'yes'),
(2, 'home', 'http://anat:8888', 'yes'),
(3, 'blogname', 'anat', 'yes'),
(4, 'blogdescription', 'אתר וורדפרס חדש', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'maxf@leos.co.il', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j בF Y', 'yes'),
(24, 'time_format', 'G:i', 'yes'),
(25, 'links_updated_date_format', 'j בF Y G:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:163:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:47:\"(([^/]+/)*wishlist)(/(.*))?/page/([0-9]{1,})/?$\";s:76:\"index.php?pagename=$matches[1]&wishlist-action=$matches[4]&paged=$matches[5]\";s:30:\"(([^/]+/)*wishlist)(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&wishlist-action=$matches[4]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=7&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:25:\"([^/]+)/wc-api(/(.*))?/?$\";s:45:\"index.php?name=$matches[1]&wc-api=$matches[3]\";s:31:\"[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:4;s:27:\"woocommerce/woocommerce.php\";i:5;s:34:\"yith-woocommerce-wishlist/init.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'anat', 'yes'),
(41, 'stylesheet', 'anat', 'yes'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '49752', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Asia/Jerusalem', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '7', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1645897167', 'yes'),
(94, 'initial_db_version', '45805', 'yes'),
(95, 'fmn_user_roles', 'a:8:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:11:\"leos_client\";a:2:{s:4:\"name\";s:11:\"Leos Client\";s:12:\"capabilities\";a:124:{s:16:\"activate_plugins\";b:0;s:19:\"delete_others_pages\";b:1;s:19:\"delete_others_posts\";b:1;s:12:\"delete_pages\";b:1;s:12:\"delete_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:14:\"edit_dashboard\";b:1;s:17:\"edit_others_pages\";b:1;s:17:\"edit_others_posts\";b:1;s:10:\"edit_pages\";b:1;s:10:\"edit_posts\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_theme_options\";b:1;s:6:\"export\";b:0;s:6:\"import\";b:0;s:10:\"list_users\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:13:\"promote_users\";b:0;s:13:\"publish_pages\";b:1;s:13:\"publish_posts\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:4:\"read\";b:1;s:12:\"remove_users\";b:0;s:13:\"switch_themes\";b:0;s:12:\"upload_files\";b:1;s:11:\"update_core\";b:0;s:14:\"update_plugins\";b:0;s:13:\"update_themes\";b:0;s:15:\"install_plugins\";b:0;s:14:\"install_themes\";b:0;s:13:\"delete_themes\";b:0;s:14:\"delete_plugins\";b:0;s:12:\"edit_plugins\";b:0;s:11:\"edit_themes\";b:0;s:10:\"edit_files\";b:0;s:10:\"edit_users\";b:0;s:9:\"add_users\";b:0;s:12:\"create_users\";b:0;s:12:\"delete_users\";b:0;s:15:\"unfiltered_html\";b:1;s:18:\"manage_woocommerce\";b:1;s:25:\"manage_woocommerce_orders\";b:1;s:26:\"manage_woocommerce_coupons\";b:1;s:27:\"manage_woocommerce_products\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:10:\"copy_posts\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:17:\"edit_shop_webhook\";b:1;s:17:\"read_shop_webhook\";b:1;s:19:\"delete_shop_webhook\";b:1;s:18:\"edit_shop_webhooks\";b:1;s:25:\"edit_others_shop_webhooks\";b:1;s:21:\"publish_shop_webhooks\";b:1;s:26:\"read_private_shop_webhooks\";b:1;s:20:\"delete_shop_webhooks\";b:1;s:28:\"delete_private_shop_webhooks\";b:1;s:30:\"delete_published_shop_webhooks\";b:1;s:27:\"delete_others_shop_webhooks\";b:1;s:26:\"edit_private_shop_webhooks\";b:1;s:28:\"edit_published_shop_webhooks\";b:1;s:25:\"manage_shop_webhook_terms\";b:1;s:23:\"edit_shop_webhook_terms\";b:1;s:25:\"delete_shop_webhook_terms\";b:1;s:25:\"assign_shop_webhook_terms\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'WPLANG', 'he_IL', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:19:{i:1630858734;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1630859775;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1630859968;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1630860836;a:1:{s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1630860843;a:1:{s:29:\"wc_admin_unsnooze_admin_notes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1630863567;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1630863568;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1630863579;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1630863581;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1630863662;a:1:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1630868035;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1630875600;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1630878835;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1630911639;a:1:{s:34:\"yith_wcwl_delete_expired_wishlists\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1630943637;a:1:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1630943645;a:2:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1631036462;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1631894095;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(123, '_site_transient_timeout_browser_164ef18bd347e2080439bfc8a27818e8', '1630949980', 'no'),
(124, '_site_transient_browser_164ef18bd347e2080439bfc8a27818e8', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"92.0.4515.159\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(125, '_site_transient_timeout_php_check_fb6df547cfb7d95cb9b49b8301cad3ab', '1630949981', 'no'),
(126, '_site_transient_php_check_fb6df547cfb7d95cb9b49b8301cad3ab', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(138, 'widget_block', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(139, 'disallowed_keys', '', 'no'),
(140, 'comment_previously_approved', '1', 'yes'),
(141, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(142, 'auto_update_core_dev', 'enabled', 'yes'),
(143, 'auto_update_core_minor', 'enabled', 'yes'),
(144, 'auto_update_core_major', 'unset', 'yes'),
(145, 'wp_force_deactivated_plugins', 'a:0:{}', 'yes'),
(146, 'finished_updating_comment_type', '1', 'yes'),
(147, 'db_upgraded', '', 'yes'),
(151, 'https_detection_errors', 'a:1:{s:20:\"https_request_failed\";a:1:{i:0;s:26:\"בקשת HTTPS נכשלה.\";}}', 'yes'),
(152, 'can_compress_scripts', '1', 'no'),
(157, 'recently_activated', 'a:1:{s:45:\"woo-smart-quick-view/wpc-smart-quick-view.php\";i:1630836946;}', 'yes'),
(169, '_transient_health-check-site-status-result', '{\"good\":12,\"recommended\":5,\"critical\":2}', 'yes'),
(170, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1630437999;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(173, 'current_theme', 'Anat', 'yes'),
(174, 'theme_mods_twentytwentyone', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1630438001;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(175, 'theme_switched', '', 'yes'),
(177, 'theme_mods_anat', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:4:{s:11:\"footer-menu\";i:16;s:11:\"header-menu\";i:17;s:19:\"footer-links-menu-1\";i:18;s:19:\"footer-links-menu-2\";i:19;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(179, 'acf_version', '5.10.2', 'yes'),
(182, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.4.2\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1630438015;s:7:\"version\";s:5:\"5.4.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(210, 'acf_pro_license', 'YToyOntzOjM6ImtleSI7czo3MjoiYjNKa1pYSmZhV1E5TmpRNE5USjhkSGx3WlQxd1pYSnpiMjVoYkh4a1lYUmxQVEl3TVRVdE1Ea3RNak1nTURVNk1qQTZNVFE9IjtzOjM6InVybCI7czoxNjoiaHR0cDovL2FuYXQ6ODg4OCI7fQ==', 'yes'),
(217, 'action_scheduler_hybrid_store_demarkation', '35', 'yes'),
(218, 'schema-ActionScheduler_StoreSchema', '4.0.1630598033', 'yes'),
(219, 'schema-ActionScheduler_LoggerSchema', '2.0.1630598034', 'yes'),
(222, 'woocommerce_schema_version', '430', 'yes'),
(223, 'woocommerce_store_address', 'חיפה', 'yes'),
(224, 'woocommerce_store_address_2', '', 'yes'),
(225, 'woocommerce_store_city', 'חיפה', 'yes'),
(226, 'woocommerce_default_country', 'IL', 'yes'),
(227, 'woocommerce_store_postcode', '12345', 'yes'),
(228, 'woocommerce_allowed_countries', 'all', 'yes'),
(229, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(230, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(231, 'woocommerce_ship_to_countries', '', 'yes'),
(232, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(233, 'woocommerce_default_customer_address', 'base', 'yes'),
(234, 'woocommerce_calc_taxes', 'no', 'yes'),
(235, 'woocommerce_enable_coupons', 'yes', 'yes'),
(236, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(237, 'woocommerce_currency', 'ILS', 'yes'),
(238, 'woocommerce_currency_pos', 'left', 'yes'),
(239, 'woocommerce_price_thousand_sep', '', 'yes'),
(240, 'woocommerce_price_decimal_sep', '', 'yes'),
(241, 'woocommerce_price_num_decimals', '0', 'yes'),
(242, 'woocommerce_shop_page_id', '36', 'yes'),
(243, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(244, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(245, 'woocommerce_placeholder_image', '35', 'yes'),
(246, 'woocommerce_weight_unit', 'kg', 'yes'),
(247, 'woocommerce_dimension_unit', 'cm', 'yes'),
(248, 'woocommerce_enable_reviews', 'yes', 'yes'),
(249, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(250, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(251, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(252, 'woocommerce_review_rating_required', 'yes', 'no'),
(253, 'woocommerce_manage_stock', 'yes', 'yes'),
(254, 'woocommerce_hold_stock_minutes', '60', 'no'),
(255, 'woocommerce_notify_low_stock', 'yes', 'no'),
(256, 'woocommerce_notify_no_stock', 'yes', 'no'),
(257, 'woocommerce_stock_email_recipient', 'maxf@leos.co.il', 'no'),
(258, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(259, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(260, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(261, 'woocommerce_stock_format', '', 'yes'),
(262, 'woocommerce_file_download_method', 'force', 'no'),
(263, 'woocommerce_downloads_redirect_fallback_allowed', 'no', 'no'),
(264, 'woocommerce_downloads_require_login', 'no', 'no'),
(265, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(266, 'woocommerce_downloads_add_hash_to_filename', 'yes', 'yes'),
(267, 'woocommerce_prices_include_tax', 'no', 'yes'),
(268, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(269, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(270, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(271, 'woocommerce_tax_classes', '', 'yes'),
(272, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(273, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(274, 'woocommerce_price_display_suffix', '', 'yes'),
(275, 'woocommerce_tax_total_display', 'itemized', 'no'),
(276, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(277, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(278, 'woocommerce_ship_to_destination', 'billing', 'no'),
(279, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(280, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(281, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(282, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(283, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(284, 'woocommerce_registration_generate_username', 'yes', 'no'),
(285, 'woocommerce_registration_generate_password', 'yes', 'no'),
(286, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(287, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(288, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(289, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(290, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(291, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(292, 'woocommerce_trash_pending_orders', '', 'no'),
(293, 'woocommerce_trash_failed_orders', '', 'no'),
(294, 'woocommerce_trash_cancelled_orders', '', 'no'),
(295, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(296, 'woocommerce_email_from_name', 'anat', 'no'),
(297, 'woocommerce_email_from_address', 'maxf@leos.co.il', 'no'),
(298, 'woocommerce_email_header_image', '', 'no'),
(299, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(300, 'woocommerce_email_base_color', '#96588a', 'no'),
(301, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(302, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(303, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(304, 'woocommerce_merchant_email_notifications', 'no', 'no'),
(305, 'woocommerce_cart_page_id', '37', 'no'),
(306, 'woocommerce_checkout_page_id', '38', 'no'),
(307, 'woocommerce_myaccount_page_id', '39', 'no'),
(308, 'woocommerce_terms_page_id', '', 'no'),
(309, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(310, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(311, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(312, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(313, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(314, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(315, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(316, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(317, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(318, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(319, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(320, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(321, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(322, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(323, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(324, 'woocommerce_api_enabled', 'no', 'yes'),
(325, 'woocommerce_allow_tracking', 'no', 'no'),
(326, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(327, 'woocommerce_single_image_width', '600', 'yes'),
(328, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(329, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(330, 'woocommerce_demo_store', 'no', 'no'),
(331, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(332, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(333, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(336, 'default_product_cat', '15', 'yes'),
(338, 'woocommerce_refund_returns_page_id', '40', 'yes'),
(341, 'woocommerce_paypal_settings', 'a:23:{s:7:\"enabled\";s:2:\"no\";s:5:\"title\";s:6:\"PayPal\";s:11:\"description\";s:85:\"Pay via PayPal; you can pay with your credit card if you don\'t have a PayPal account.\";s:5:\"email\";s:15:\"maxf@leos.co.il\";s:8:\"advanced\";s:0:\"\";s:8:\"testmode\";s:2:\"no\";s:5:\"debug\";s:2:\"no\";s:16:\"ipn_notification\";s:3:\"yes\";s:14:\"receiver_email\";s:15:\"maxf@leos.co.il\";s:14:\"identity_token\";s:0:\"\";s:14:\"invoice_prefix\";s:3:\"WC-\";s:13:\"send_shipping\";s:3:\"yes\";s:16:\"address_override\";s:2:\"no\";s:13:\"paymentaction\";s:4:\"sale\";s:9:\"image_url\";s:0:\"\";s:11:\"api_details\";s:0:\"\";s:12:\"api_username\";s:0:\"\";s:12:\"api_password\";s:0:\"\";s:13:\"api_signature\";s:0:\"\";s:20:\"sandbox_api_username\";s:0:\"\";s:20:\"sandbox_api_password\";s:0:\"\";s:21:\"sandbox_api_signature\";s:0:\"\";s:12:\"_should_load\";s:2:\"no\";}', 'yes'),
(342, 'woocommerce_version', '5.6.0', 'yes'),
(343, 'woocommerce_db_version', '5.6.0', 'yes'),
(344, 'woocommerce_inbox_variant_assignment', '12', 'yes');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(348, '_transient_jetpack_autoloader_plugin_paths', 'a:1:{i:0;s:29:\"{{WP_PLUGIN_DIR}}/woocommerce\";}', 'yes'),
(349, 'action_scheduler_lock_async-request-runner', '1630858789', 'yes'),
(350, 'woocommerce_admin_notices', 'a:1:{i:0;s:20:\"no_secure_connection\";}', 'yes'),
(351, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"ScvuurDqSLjl2VbXn4Tb7YhTcg2xjUJh\";}', 'yes'),
(352, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(353, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(354, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(355, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(356, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(357, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(358, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(359, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(360, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(361, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(362, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(363, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(364, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(367, 'woocommerce_admin_version', '2.5.1', 'yes'),
(368, 'woocommerce_admin_install_timestamp', '1630598037', 'yes'),
(369, 'wc_remote_inbox_notifications_wca_updated', '', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(370, 'wc_remote_inbox_notifications_specs', 'a:34:{s:22:\"mercadopago_q3_2021_EN\";O:8:\"stdClass\":8:{s:4:\"slug\";s:22:\"mercadopago_q3_2021_EN\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:35:\"Get paid with Mercado Pago Checkout\";s:7:\"content\";s:217:\"Latin America\'s leading payment processor is now available for WooCommerce stores. Securely accept debit and credit cards, cash, bank transfers, and installment payments – backed by exclusive fraud prevention tools.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:22:\"mercadopago_q3_2021_EN\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:13:\"Free download\";}}s:3:\"url\";s:127:\"https://woocommerce.com/products/mercado-pago-checkout/?utm_source=inbox&utm_medium=product&utm_campaign=mercadopago_q3_2021_EN\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:5:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-08-30 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-09-10 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:26:\"woocommerce-gateway-stripe\";}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:7:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"MX\";}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"AR\";}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"CO\";}i:3;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"CL\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"UY\";}i:5;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"PE\";}i:6;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"BR\";}}}i:4;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:6:\"WPLANG\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:5:\"en_US\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:6:\"WPLANG\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:0:\"\";}}}}}s:16:\"wayflyer_q3_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:16:\"wayflyer_q3_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:55:\"Grow your revenue with Wayflyer financing and analytics\";s:7:\"content\";s:322:\"Flexible financing tailored to your needs by <a href=\"https://woocommerce.com/products/wayflyer/\">Wayflyer</a> – one fee, no interest rates, penalties, equity, or personal guarantees. Based on your store\'s performance, Wayflyer can provide the financing you need to grow and the analytical insights to help you spend it.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:16:\"wayflyer_q3_2021\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Get funded\";}}s:3:\"url\";s:42:\"https://woocommerce.com/products/wayflyer/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:5:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-07-19 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-07-31 00:00:00\";}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:13:\"product_count\";s:9:\"operation\";s:2:\"<=\";s:5:\"value\";s:3:\"200\";}i:3;O:8:\"stdClass\":3:{s:4:\"type\";s:11:\"order_count\";s:9:\"operation\";s:1:\">\";s:5:\"value\";s:3:\"100\";}i:4;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:7:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"AU\";}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"BE\";}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"CA\";}i:3;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"IE\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"NL\";}i:5;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"UK\";}i:6;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:2:\"US\";}}}}}s:19:\"eu_vat_changes_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:19:\"eu_vat_changes_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:54:\"Get your business ready for the new EU tax regulations\";s:7:\"content\";s:617:\"On July 1, 2021, new taxation rules will come into play when the <a href=\"https://ec.europa.eu/taxation_customs/business/vat/modernising-vat-cross-border-ecommerce_en\">European Union (EU) Value-Added Tax (VAT) eCommerce package</a> takes effect.<br/><br/>The new regulations will impact virtually every B2C business involved in cross-border eCommerce trade with the EU.<br/><br/>We therefore recommend <a href=\"https://woocommerce.com/posts/new-eu-vat-regulations\">familiarizing yourself with the new updates</a>, and consult with a tax professional to ensure your business is following regulations and best practice.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:19:\"eu_vat_changes_2021\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:39:\"Learn more about the EU tax regulations\";}}s:3:\"url\";s:52:\"https://woocommerce.com/posts/new-eu-vat-regulations\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-06-24 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-07-11 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:3:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:3:\"all\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;a:2:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:10:\"all_except\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:27:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"BE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"BG\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"CZ\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"DK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"DE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"EE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:6;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"IE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"EL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:8;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"ES\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:9;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"FR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:10;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"HR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:11;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"IT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:12;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"CY\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:13;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"LV\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:14;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"LT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:15;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"LU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:16;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"HU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:17;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"MT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:18;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"NL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:19;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"AT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:20;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"PL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:21;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"PT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:22;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"RO\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:23;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"SI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:24;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"SK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:25;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"FI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:26;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"SE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}}}}i:2;a:3:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:3:\"all\";s:7:\"default\";b:0;s:9:\"operation\";s:2:\"!=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:10:\"all_except\";s:7:\"default\";b:0;s:9:\"operation\";s:2:\"!=\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:27:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"BE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"BG\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"CZ\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"DK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"DE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"EE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:6;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"IE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"EL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:8;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"ES\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:9;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"FR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:10;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"HR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:11;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"IT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:12;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"CY\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:13;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"LV\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:14;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"LT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:15;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"LU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:16;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"HU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:17;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"MT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:18;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"NL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:19;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"AT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:20;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"PL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:21;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"PT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:22;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"RO\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:23;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"SI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:24;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"SK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:25;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"FI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:26;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"SE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}}}}}}}}s:20:\"paypal_ppcp_gtm_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:20:\"paypal_ppcp_gtm_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:38:\"Offer more options with the new PayPal\";s:7:\"content\";s:113:\"Get the latest PayPal extension for a full suite of payment methods with extensive currency and country coverage.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:36:\"open_wc_paypal_payments_product_page\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:61:\"https://woocommerce.com/products/woocommerce-paypal-payments/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-04-05 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-04-21 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:7:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:30:\"woocommerce-gateway-paypal-pro\";}}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:37:\"woocommerce-gateway-paypal-pro-hosted\";}}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:35:\"woocommerce-gateway-paypal-advanced\";}}i:4;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:40:\"woocommerce-gateway-paypal-digital-goods\";}}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:31:\"woocommerce-paypal-here-gateway\";}}i:6;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:44:\"woocommerce-gateway-paypal-adaptive-payments\";}}}}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:27:\"woocommerce-paypal-payments\";s:7:\"version\";s:5:\"1.2.1\";s:8:\"operator\";s:1:\"<\";}}}}}s:23:\"facebook_pixel_api_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:23:\"facebook_pixel_api_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:44:\"Improve the performance of your Facebook ads\";s:7:\"content\";s:152:\"Enable Facebook Pixel and Conversions API through the latest version of Facebook for WooCommerce for improved measurement and ad targeting capabilities.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:30:\"upgrade_now_facebook_pixel_api\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:11:\"Upgrade now\";}}s:3:\"url\";s:67:\"plugin-install.php?tab=plugin-information&plugin=&section=changelog\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-05-17 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-06-14 00:00:00\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:24:\"facebook-for-woocommerce\";s:7:\"version\";s:5:\"2.4.0\";s:8:\"operator\";s:2:\"<=\";}}}s:16:\"facebook_ec_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:16:\"facebook_ec_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:59:\"Sync your product catalog with Facebook to help boost sales\";s:7:\"content\";s:170:\"A single click adds all products to your Facebook Business Page shop. Product changes are automatically synced, with the flexibility to control which products are listed.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:22:\"learn_more_facebook_ec\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:42:\"https://woocommerce.com/products/facebook/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-03-01 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-03-15 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:24:\"facebook-for-woocommerce\";}}}}s:37:\"ecomm-need-help-setting-up-your-store\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"ecomm-need-help-setting-up-your-store\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:32:\"Need help setting up your Store?\";s:7:\"content\";s:350:\"Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:16:\"set-up-concierge\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:21:\"Schedule free session\";}}s:3:\"url\";s:34:\"https://wordpress.com/me/concierge\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:3:{i:0;s:35:\"woocommerce-shipping-australia-post\";i:1;s:32:\"woocommerce-shipping-canada-post\";i:2;s:30:\"woocommerce-shipping-royalmail\";}}}}s:20:\"woocommerce-services\";O:8:\"stdClass\":8:{s:4:\"slug\";s:20:\"woocommerce-services\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"WooCommerce Shipping & Tax\";s:7:\"content\";s:255:\"WooCommerce Shipping & Tax helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:84:\"https://docs.woocommerce.com/document/woocommerce-shipping-and-tax/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-services\";}}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\"<\";s:4:\"days\";i:2;}}}s:32:\"ecomm-unique-shopping-experience\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"ecomm-unique-shopping-experience\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:53:\"For a shopping experience as unique as your customers\";s:7:\"content\";s:274:\"Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:43:\"learn-more-ecomm-unique-shopping-experience\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:71:\"https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:3:{i:0;s:35:\"woocommerce-shipping-australia-post\";i:1;s:32:\"woocommerce-shipping-canada-post\";i:2;s:30:\"woocommerce-shipping-royalmail\";}}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\"<\";s:4:\"days\";i:2;}}}s:37:\"wc-admin-getting-started-in-ecommerce\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"wc-admin-getting-started-in-ecommerce\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:38:\"Getting Started in eCommerce - webinar\";s:7:\"content\";s:174:\"We want to make eCommerce and this process of getting started as easy as possible for you. Watch this webinar to get tips on how to have our store up and running in a breeze.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:17:\"watch-the-webinar\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:17:\"Watch the webinar\";}}s:3:\"url\";s:28:\"https://youtu.be/V_2XtCOyZ7o\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:12:\"setup_client\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:1;}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:3:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:13:\"product_count\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:1:\"0\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:7:\"revenue\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:4:\"none\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:7:\"revenue\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:10:\"up-to-2500\";}}}}}s:18:\"your-first-product\";O:8:\"stdClass\":8:{s:4:\"slug\";s:18:\"your-first-product\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:18:\"Your first product\";s:7:\"content\";s:461:\"That\'s huge! You\'re well on your way to building a successful online store — now it’s time to think about how you\'ll fulfill your orders.<br/><br/>Read our shipping guide to learn best practices and options for putting together your shipping strategy. And for WooCommerce stores in the United States, you can print discounted shipping labels via USPS with <a href=\"https://href.li/?https://woocommerce.com/shipping\" target=\"_blank\">WooCommerce Shipping</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:82:\"https://woocommerce.com/posts/ecommerce-shipping-solutions-guide/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:12:\"stored_state\";s:5:\"index\";s:22:\"there_were_no_products\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";b:1;}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:12:\"stored_state\";s:5:\"index\";s:22:\"there_are_now_products\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";b:1;}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:13:\"product_count\";s:9:\"operation\";s:2:\">=\";s:5:\"value\";i:1;}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:13:\"product_types\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:8:\"physical\";s:7:\"default\";a:0:{}}}}s:31:\"wc-square-apple-pay-boost-sales\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"wc-square-apple-pay-boost-sales\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"Boost sales with Apple Pay\";s:7:\"content\";s:191:\"Now that you accept Apple Pay® with Square you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:27:\"boost-sales-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:97:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-boost-sales\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:9:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:18:\"woocommerce-square\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"2.3\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"wc_square_apple_pay_enabled\";s:5:\"value\";i:1;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:38:\"wc-square-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:38:\"wc-square-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:38:\"wc-square-apple-pay-grow-your-business\";O:8:\"stdClass\":8:{s:4:\"slug\";s:38:\"wc-square-apple-pay-grow-your-business\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:45:\"Grow your business with Square and Apple Pay \";s:7:\"content\";s:178:\"Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"grow-your-business-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:104:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-grow-your-business\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:9:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:18:\"woocommerce-square\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"2.3\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"wc_square_apple_pay_enabled\";s:5:\"value\";i:2;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:31:\"wc-square-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:31:\"wc-square-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:32:\"wcpay-apple-pay-is-now-available\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"wcpay-apple-pay-is-now-available\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:53:\"Apple Pay is now available with WooCommerce Payments!\";s:7:\"content\";s:397:\"Increase your conversion rate by offering a fast and secure checkout with <a href=\"https://woocommerce.com/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\" target=\"_blank\">Apple Pay</a>®. It’s free to get started with <a href=\"https://woocommerce.com/payments/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\" target=\"_blank\">WooCommerce Payments</a>.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:13:\"add-apple-pay\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:13:\"Add Apple Pay\";}}s:3:\"url\";s:69:\"/admin.php?page=wc-settings&tab=checkout&section=woocommerce_payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:121:\"https://docs.woocommerce.com/document/payments/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:20:\"woocommerce-payments\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:5:\"2.3.0\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";b:0;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}s:27:\"wcpay-apple-pay-boost-sales\";O:8:\"stdClass\":8:{s:4:\"slug\";s:27:\"wcpay-apple-pay-boost-sales\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"Boost sales with Apple Pay\";s:7:\"content\";s:205:\"Now that you accept Apple Pay® with WooCommerce Payments you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:27:\"boost-sales-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:96:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-boost-sales\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";i:1;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:34:\"wcpay-apple-pay-grow-your-business\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"wcpay-apple-pay-grow-your-business\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:58:\"Grow your business with WooCommerce Payments and Apple Pay\";s:7:\"content\";s:178:\"Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"grow-your-business-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:103:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-grow-your-business\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";i:2;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:37:\"wc-admin-optimizing-the-checkout-flow\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"wc-admin-optimizing-the-checkout-flow\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:28:\"Optimizing the checkout flow\";s:7:\"content\";s:171:\"It\'s crucial to get your store\'s checkout as smooth as possible to avoid losing sales. Let\'s take a look at how you can optimize the checkout experience for your shoppers.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:28:\"optimizing-the-checkout-flow\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:78:\"https://woocommerce.com/posts/optimizing-woocommerce-checkout?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\">\";s:4:\"days\";i:3;}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:8:\"payments\";s:7:\"default\";a:0:{}}}}s:39:\"wc-admin-first-five-things-to-customize\";O:8:\"stdClass\":8:{s:4:\"slug\";s:39:\"wc-admin-first-five-things-to-customize\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:45:\"The first 5 things to customize in your store\";s:7:\"content\";s:173:\"Deciding what to start with first is tricky. To help you properly prioritize, we\'ve put together this short list of the first few things you should customize in WooCommerce.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:82:\"https://woocommerce.com/posts/first-things-customize-woocommerce/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\">\";s:4:\"days\";i:2;}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:5:\"value\";s:9:\"NOT EMPTY\";s:7:\"default\";s:9:\"NOT EMPTY\";s:9:\"operation\";s:2:\"!=\";}}}s:32:\"wc-payments-qualitative-feedback\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"wc-payments-qualitative-feedback\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:55:\"WooCommerce Payments setup - let us know what you think\";s:7:\"content\";s:146:\"Congrats on enabling WooCommerce Payments for your store. Please share your feedback in this 2 minute survey to help us improve the setup process.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:35:\"qualitative-feedback-from-new-users\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:14:\"Share feedback\";}}s:3:\"url\";s:39:\"https://automattic.survey.fm/wc-pay-new\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:20:\"woocommerce-payments\";s:7:\"default\";a:0:{}}}}s:29:\"share-your-feedback-on-paypal\";O:8:\"stdClass\":8:{s:4:\"slug\";s:29:\"share-your-feedback-on-paypal\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:29:\"Share your feedback on PayPal\";s:7:\"content\";s:127:\"Share your feedback in this 2 minute survey about how we can make the process of accepting payments more useful for your store.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:14:\"share-feedback\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:14:\"Share feedback\";}}s:3:\"url\";s:43:\"http://automattic.survey.fm/paypal-feedback\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:26:\"woocommerce-gateway-stripe\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}}}s:31:\"wcpay_instant_deposits_gtm_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"wcpay_instant_deposits_gtm_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:69:\"Get paid within minutes – Instant Deposits for WooCommerce Payments\";s:7:\"content\";s:384:\"Stay flexible with immediate access to your funds when you need them – including nights, weekends, and holidays. With <a href=\"https://woocommerce.com/products/woocommerce-payments/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits\">WooCommerce Payments\'</a> new Instant Deposits feature, you’re able to transfer your earnings to a debit card within minutes.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:40:\"Learn about Instant Deposits eligibility\";}}s:3:\"url\";s:136:\"https://docs.woocommerce.com/document/payments/instant-deposits/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-05-18 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-06-01 00:00:00\";}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}s:31:\"google_listings_and_ads_install\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"google_listings_and_ads_install\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:35:\"Drive traffic and sales with Google\";s:7:\"content\";s:123:\"Reach online shoppers to drive traffic and sales for your store by showcasing products across Google, for free or with ads.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:11:\"get-started\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:11:\"Get started\";}}s:3:\"url\";s:56:\"https://woocommerce.com/products/google-listings-and-ads\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-06-09 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:23:\"google_listings_and_ads\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:11:\"order_count\";s:9:\"operation\";s:1:\">\";s:5:\"value\";i:10;}}}s:39:\"wc-subscriptions-security-update-3-0-15\";O:8:\"stdClass\":8:{s:4:\"slug\";s:39:\"wc-subscriptions-security-update-3-0-15\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:42:\"WooCommerce Subscriptions security update!\";s:7:\"content\";s:736:\"We recently released an important security update to WooCommerce Subscriptions. To ensure your site\'s data is protected, please upgrade <strong>WooCommerce Subscriptions to version 3.0.15</strong> or later.<br/><br/>Click the button below to view and update to the latest Subscriptions version, or log in to <a href=\"https://woocommerce.com/my-dashboard\">WooCommerce.com Dashboard</a> and navigate to your <strong>Downloads</strong> page.<br/><br/>We recommend always using the latest version of WooCommerce Subscriptions, and other software running on your site, to ensure maximum security.<br/><br/>If you have any questions we are here to help — just <a href=\"https://woocommerce.com/my-account/create-a-ticket/\">open a ticket</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:30:\"update-wc-subscriptions-3-0-15\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"View latest version\";}}s:3:\"url\";s:30:\"&page=wc-addons&section=helper\";s:18:\"url_is_admin_query\";b:1;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:25:\"woocommerce-subscriptions\";s:8:\"operator\";s:1:\"<\";s:7:\"version\";s:6:\"3.0.15\";}}}s:29:\"woocommerce-core-update-5-4-0\";O:8:\"stdClass\":8:{s:4:\"slug\";s:29:\"woocommerce-core-update-5-4-0\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:31:\"Update to WooCommerce 5.4.1 now\";s:7:\"content\";s:140:\"WooCommerce 5.4.1 addresses a checkout issue discovered in WooCommerce 5.4. We recommend upgrading to WooCommerce 5.4.1 as soon as possible.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:20:\"update-wc-core-5-4-0\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:25:\"How to update WooCommerce\";}}s:3:\"url\";s:64:\"https://docs.woocommerce.com/document/how-to-update-woocommerce/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.4.0\";}}}s:19:\"wcpay-promo-2020-11\";O:8:\"stdClass\":7:{s:4:\"slug\";s:19:\"wcpay-promo-2020-11\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:19:\"wcpay-promo-2020-11\";s:7:\"content\";s:19:\"wcpay-promo-2020-11\";}}s:5:\"rules\";a:0:{}}s:19:\"wcpay-promo-2020-12\";O:8:\"stdClass\":7:{s:4:\"slug\";s:19:\"wcpay-promo-2020-12\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:19:\"wcpay-promo-2020-12\";s:7:\"content\";s:19:\"wcpay-promo-2020-12\";}}s:5:\"rules\";a:0:{}}s:30:\"wcpay-promo-2021-6-incentive-1\";O:8:\"stdClass\":8:{s:4:\"slug\";s:30:\"wcpay-promo-2021-6-incentive-1\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:82:\"Simplify the payments process for you and your customers with WooCommerce Payments\";s:7:\"content\";s:702:\"With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&utm_source=product&utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies.\n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br/><br/>\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&utm_source=product&utm_campaign=wcpay601\">Terms of Service</a>\n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:25:\"get-woo-commerce-payments\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:24:\"Get WooCommerce Payments\";}}s:3:\"url\";s:57:\"admin.php?page=wc-admin&action=setup-woocommerce-payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:12:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:6:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"1\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"3\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"5\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"7\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"9\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"11\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:4:{i:0;s:17:\"crowdsignal-forms\";i:1;s:11:\"layout-grid\";i:2;s:17:\"full-site-editing\";i:3;s:13:\"page-optimize\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"woocommerce_allow_tracking\";s:5:\"value\";s:3:\"yes\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:2:\">=\";s:4:\"days\";i:31;}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.0\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:10:\"db_version\";s:5:\"value\";s:5:\"45805\";s:7:\"default\";i:0;s:9:\"operation\";s:2:\">=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:30:\"wcpay-promo-2021-6-incentive-2\";O:8:\"stdClass\":8:{s:4:\"slug\";s:30:\"wcpay-promo-2021-6-incentive-2\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:82:\"Simplify the payments process for you and your customers with WooCommerce Payments\";s:7:\"content\";s:702:\"With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&utm_source=product&utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies.\n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br/><br/>\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&utm_source=product&utm_campaign=wcpay601\">Terms of Service</a>\n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:24:\"get-woocommerce-payments\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:24:\"Get WooCommerce Payments\";}}s:3:\"url\";s:57:\"admin.php?page=wc-admin&action=setup-woocommerce-payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:12:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:6:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"2\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"4\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"6\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"8\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"10\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"12\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:4:{i:0;s:17:\"crowdsignal-forms\";i:1;s:11:\"layout-grid\";i:2;s:17:\"full-site-editing\";i:3;s:13:\"page-optimize\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"woocommerce_allow_tracking\";s:5:\"value\";s:3:\"yes\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:2:\">=\";s:4:\"days\";i:31;}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.0\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:10:\"db_version\";s:5:\"value\";s:5:\"45805\";s:7:\"default\";i:0;s:9:\"operation\";s:2:\">=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:34:\"ppxo-pps-upgrade-paypal-payments-1\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"ppxo-pps-upgrade-paypal-payments-1\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:47:\"Get the latest PayPal extension for WooCommerce\";s:7:\"content\";s:440:\"Heads up! There\'s a new PayPal on the block!<br/><br/>Now is a great time to upgrade to our latest <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension</a> to continue to receive support and updates with PayPal.<br/><br/>Get access to a full suite of PayPal payment methods, extensive currency and country coverage, and pay later options with the all-new PayPal extension for WooCommerce.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"ppxo-pps-install-paypal-payments-1\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:18:\"View upgrade guide\";}}s:3:\"url\";s:96:\"https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"woocommerce_paypal_settings\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:0;s:7:\"default\";b:0;}}}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";i:7;s:7:\"default\";i:1;s:9:\"operation\";s:1:\"<\";}}}s:34:\"ppxo-pps-upgrade-paypal-payments-2\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"ppxo-pps-upgrade-paypal-payments-2\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:31:\"Upgrade your PayPal experience!\";s:7:\"content\";s:513:\"We\'ve developed a whole new <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension for WooCommerce</a> that combines the best features of our many PayPal extensions into just one extension.<br/><br/>Get access to a full suite of PayPal payment methods, extensive currency and country coverage, offer subscription and recurring payments, and the new PayPal pay later options.<br/><br/>Start using our latest PayPal today to continue to receive support and updates.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"ppxo-pps-install-paypal-payments-2\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:18:\"View upgrade guide\";}}s:3:\"url\";s:96:\"https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"woocommerce_paypal_settings\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:0;s:7:\"default\";b:0;}}}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";i:6;s:7:\"default\";i:1;s:9:\"operation\";s:1:\">\";}}}s:46:\"woocommerce-core-sqli-july-2021-need-to-update\";O:8:\"stdClass\":8:{s:4:\"slug\";s:46:\"woocommerce-core-sqli-july-2021-need-to-update\";s:4:\"type\";s:6:\"update\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:56:\"Action required: Critical vulnerabilities in WooCommerce\";s:7:\"content\";s:570:\"In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br/><br/>Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br/><br/>For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:146:\"https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:7:\"dismiss\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:7:\"Dismiss\";}}s:3:\"url\";b:0;s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:0;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:23:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.3.6\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.4.8\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.5.9\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.6.6\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.7.2\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.8.2\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.9.4\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.0.2\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.1.2\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.2.3\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.3.4\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.4.2\";}i:12;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.5.3\";}i:13;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.6.3\";}i:14;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.7.2\";}i:15;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.8.1\";}i:16;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.9.3\";}i:17;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.0.1\";}i:18;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.1.1\";}i:19;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.2.3\";}i:20;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.3.1\";}i:21;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.4.2\";}i:22;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"<\";s:7:\"version\";s:5:\"5.5.1\";}}}s:48:\"woocommerce-blocks-sqli-july-2021-need-to-update\";O:8:\"stdClass\":8:{s:4:\"slug\";s:48:\"woocommerce-blocks-sqli-july-2021-need-to-update\";s:4:\"type\";s:6:\"update\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:63:\"Action required: Critical vulnerabilities in WooCommerce Blocks\";s:7:\"content\";s:570:\"In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br/><br/>Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br/><br/>For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:146:\"https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:7:\"dismiss\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:7:\"Dismiss\";}}s:3:\"url\";b:0;s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:0;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:31:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:6:\"2.5.16\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"2.6.2\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"2.7.2\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"2.8.1\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"2.9.1\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.0.1\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.1.1\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.2.1\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.3.1\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.4.1\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.5.1\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.6.1\";}i:12;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.7.2\";}i:13;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.8.1\";}i:14;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"3.9.1\";}i:15;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.0.1\";}i:16;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.1.1\";}i:17;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.2.1\";}i:18;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.3.1\";}i:19;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.4.3\";}i:20;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.5.3\";}i:21;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.6.1\";}i:22;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.7.1\";}i:23;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.8.1\";}i:24;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"4.9.2\";}i:25;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.0.1\";}i:26;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.1.1\";}i:27;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.2.1\";}i:28;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.3.2\";}i:29;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\"!=\";s:7:\"version\";s:5:\"5.4.1\";}i:30;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"<\";s:7:\"version\";s:5:\"5.5.1\";}}}s:45:\"woocommerce-core-sqli-july-2021-store-patched\";O:8:\"stdClass\":8:{s:4:\"slug\";s:45:\"woocommerce-core-sqli-july-2021-store-patched\";s:4:\"type\";s:6:\"update\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:55:\"Solved: Critical vulnerabilities patched in WooCommerce\";s:7:\"content\";s:433:\"In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br/><br/><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:146:\"https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:7:\"dismiss\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:7:\"Dismiss\";}}s:3:\"url\";b:0;s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:0;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:46:\"woocommerce-core-sqli-july-2021-need-to-update\";s:6:\"status\";s:7:\"pending\";s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:48:\"woocommerce-blocks-sqli-july-2021-need-to-update\";s:6:\"status\";s:7:\"pending\";s:9:\"operation\";s:1:\"=\";}}}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:23:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.3.6\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.4.8\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.5.9\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.6.6\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.7.2\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.8.2\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.9.4\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.0.2\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.1.2\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.2.3\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.3.4\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.4.2\";}i:12;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.5.3\";}i:13;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.6.3\";}i:14;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.7.2\";}i:15;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.8.1\";}i:16;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.9.3\";}i:17;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.0.1\";}i:18;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.1.1\";}i:19;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.2.3\";}i:20;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.3.1\";}i:21;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.4.2\";}i:22;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:5:\"5.5.1\";}}}}}s:47:\"woocommerce-blocks-sqli-july-2021-store-patched\";O:8:\"stdClass\":8:{s:4:\"slug\";s:47:\"woocommerce-blocks-sqli-july-2021-store-patched\";s:4:\"type\";s:6:\"update\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:62:\"Solved: Critical vulnerabilities patched in WooCommerce Blocks\";s:7:\"content\";s:433:\"In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br/><br/><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:146:\"https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:7:\"dismiss\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:7:\"Dismiss\";}}s:3:\"url\";b:0;s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:0;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:46:\"woocommerce-core-sqli-july-2021-need-to-update\";s:6:\"status\";s:7:\"pending\";s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:48:\"woocommerce-blocks-sqli-july-2021-need-to-update\";s:6:\"status\";s:7:\"pending\";s:9:\"operation\";s:1:\"=\";}}}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:31:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:6:\"2.5.16\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"2.6.2\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"2.7.2\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"2.8.1\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"2.9.1\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.0.1\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.1.1\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.2.1\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.3.1\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.4.1\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.5.1\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.6.1\";}i:12;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.7.2\";}i:13;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.8.1\";}i:14;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"3.9.1\";}i:15;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.0.1\";}i:16;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.1.1\";}i:17;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.2.1\";}i:18;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.3.1\";}i:19;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.4.3\";}i:20;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.5.3\";}i:21;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.6.1\";}i:22;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.7.1\";}i:23;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.8.1\";}i:24;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"4.9.2\";}i:25;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.0.1\";}i:26;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.1.1\";}i:27;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.2.1\";}i:28;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.3.2\";}i:29;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.4.1\";}i:30;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:28:\"woo-gutenberg-products-block\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:5:\"5.5.1\";}}}}}}', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(371, 'wc_remote_inbox_notifications_stored_state', 'O:8:\"stdClass\":2:{s:22:\"there_were_no_products\";b:1;s:22:\"there_are_now_products\";b:1;}', 'no'),
(375, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(376, 'wc_blocks_db_schema_version', '260', 'yes'),
(377, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(381, '_transient_woocommerce_reports-transient-version', '1630598070', 'yes'),
(382, '_transient_timeout_orders-all-statuses', '1631203070', 'no'),
(383, '_transient_orders-all-statuses', 'a:2:{s:7:\"version\";s:10:\"1630598070\";s:5:\"value\";a:0:{}}', 'no'),
(391, 'woocommerce_onboarding_profile', 'a:8:{s:12:\"setup_client\";b:1;s:8:\"industry\";a:1:{i:0;a:1:{s:4:\"slug\";s:13:\"health-beauty\";}}s:13:\"product_types\";a:1:{i:0;s:8:\"physical\";}s:13:\"product_count\";s:5:\"1000+\";s:14:\"selling_venues\";s:2:\"no\";s:19:\"business_extensions\";a:0:{}s:5:\"theme\";s:4:\"anat\";s:9:\"completed\";b:1;}', 'yes'),
(396, 'woocommerce_task_list_tracked_completed_tasks', 'a:1:{i:0;s:13:\"store_details\";}', 'yes'),
(397, '_transient_timeout_wc_report_orders_stats_b412091a9542b4e1121a419da36adf16', '1631203073', 'no'),
(398, '_transient_timeout_wc_report_orders_stats_3ecce004ad83d268c1f8968cf508ea57', '1631203073', 'no'),
(399, '_transient_wc_report_orders_stats_b412091a9542b4e1121a419da36adf16', 'a:2:{s:7:\"version\";s:10:\"1630598070\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-36\";s:10:\"date_start\";s:19:\"2021-09-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-09-01 21:00:00\";s:8:\"date_end\";s:19:\"2021-09-02 18:57:51\";s:12:\"date_end_gmt\";s:19:\"2021-09-02 15:57:51\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(400, '_transient_wc_report_orders_stats_3ecce004ad83d268c1f8968cf508ea57', 'a:2:{s:7:\"version\";s:10:\"1630598070\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-36\";s:10:\"date_start\";s:19:\"2021-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-08-31 21:00:00\";s:8:\"date_end\";s:19:\"2021-09-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-09-01 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(401, '_transient_timeout_wc_report_orders_stats_93feb57e1aa7af24fa7572a6cfe09c62', '1631203073', 'no'),
(402, '_transient_timeout_wc_report_orders_stats_8fcfa9830e2e2d599422f48a1c41aa67', '1631203073', 'no'),
(403, '_transient_wc_report_orders_stats_93feb57e1aa7af24fa7572a6cfe09c62', 'a:2:{s:7:\"version\";s:10:\"1630598070\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-36\";s:10:\"date_start\";s:19:\"2021-09-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-09-01 21:00:00\";s:8:\"date_end\";s:19:\"2021-09-02 18:57:51\";s:12:\"date_end_gmt\";s:19:\"2021-09-02 15:57:51\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(404, '_transient_wc_report_orders_stats_8fcfa9830e2e2d599422f48a1c41aa67', 'a:2:{s:7:\"version\";s:10:\"1630598070\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-36\";s:10:\"date_start\";s:19:\"2021-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-08-31 21:00:00\";s:8:\"date_end\";s:19:\"2021-09-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-09-01 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(406, 'woocommerce_task_list_welcome_modal_dismissed', 'yes', 'yes'),
(438, '_transient_product_query-transient-version', '1630852951', 'yes'),
(548, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(572, 'options_tel', 'טלפון 052-4766891', 'no'),
(573, '_options_tel', 'field_5dd3a33f42e0c', 'no'),
(574, 'options_mail', 'info@anatboutique.com', 'no'),
(575, '_options_mail', 'field_5dd3a35642e0d', 'no'),
(576, 'options_fax', '', 'no'),
(577, '_options_fax', 'field_5dd3a36942e0e', 'no'),
(578, 'options_address', 'ש”י עגנון 15, יבנה', 'no'),
(579, '_options_address', 'field_5dd3a4d142e0f', 'no'),
(580, 'options_map_image', '', 'no'),
(581, '_options_map_image', 'field_5ddbd67734310', 'no'),
(582, 'options_facebook', 'https://www.facebook.com/facebook', 'no'),
(583, '_options_facebook', 'field_5ddbd6b3cc0cd', 'no'),
(584, 'options_whatsapp', 'טלפון 052-4766891', 'no'),
(585, '_options_whatsapp', 'field_5ddbd6cdcc0ce', 'no'),
(586, 'options_instagram', 'https://www.instagram.com/', 'no'),
(587, '_options_instagram', 'field_612f5d7b40554', 'no'),
(588, 'options_post_form_title', 'משפחה מרוויחה יותר!', 'no'),
(589, '_options_post_form_title', 'field_6130fc2233f77', 'no'),
(590, 'options_post_form_text', 'הצטרפי למשפחת ויקטוריה סיקרט ישראל ותהני מהנחות \r\nאישיות, מידע על מוצרים חדשים וקופונים הכי שווים', 'no'),
(591, '_options_post_form_text', 'field_6130fc3133f78', 'no'),
(592, 'options_to_blog_link', 'a:3:{s:5:\"title\";s:21:\"לכל המאמרים\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}', 'no'),
(593, '_options_to_blog_link', 'field_6130fc4933f79', 'no'),
(594, 'options_header_brand_logo', '131', 'no'),
(595, '_options_header_brand_logo', 'field_6130fcce2f192', 'no'),
(596, 'options_header_brand_link', 'http://www.lorem-ipsum.co.il/lorem-ipsum.php', 'no'),
(597, '_options_header_brand_link', 'field_6130fcf92f193', 'no'),
(598, 'options_news_slider_0_news_text', 'משלוח חינם למשלמות מעל 250 ש”ח!', 'no'),
(599, '_options_news_slider_0_news_text', 'field_6130fd242f195', 'no'),
(600, 'options_news_slider_0_news_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}', 'no'),
(601, '_options_news_slider_0_news_link', 'field_6130fd2f2f196', 'no'),
(602, 'options_news_slider_1_news_text', 'לורם איפסום דולור סיט אמט 1', 'no'),
(603, '_options_news_slider_1_news_text', 'field_6130fd242f195', 'no'),
(604, 'options_news_slider_1_news_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:44:\"http://www.lorem-ipsum.co.il/lorem-ipsum.php\";s:6:\"target\";s:0:\"\";}', 'no'),
(605, '_options_news_slider_1_news_link', 'field_6130fd2f2f196', 'no'),
(606, 'options_news_slider_2_news_text', 'לורם איפסום דולור סיט אמט 2', 'no'),
(607, '_options_news_slider_2_news_text', 'field_6130fd242f195', 'no'),
(608, 'options_news_slider_2_news_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:44:\"http://www.lorem-ipsum.co.il/lorem-ipsum.php\";s:6:\"target\";s:0:\"\";}', 'no'),
(609, '_options_news_slider_2_news_link', 'field_6130fd2f2f196', 'no'),
(610, 'options_news_slider', '3', 'no'),
(611, '_options_news_slider', 'field_6130fd0e2f194', 'no'),
(612, 'options_pop_form_title', 'משפחה מרוויחה יותר!', 'no'),
(613, '_options_pop_form_title', 'field_6130fd67974ae', 'no'),
(614, 'options_pop_form_text', 'הצטרפי למשפחת ויקטוריה סיקרט ישראל ותהני מהנחות אישיות, \r\nמידע על מוצרים חדשים וקופונים הכי שווים', 'no'),
(615, '_options_pop_form_text', 'field_6130fd78974af', 'no'),
(616, 'options_foo_form_title', 'בואו נשמור על קשר!', 'no'),
(617, '_options_foo_form_title', 'field_6130fd9a974b1', 'no'),
(618, 'options_foo_form_subtitle', 'מלאו את כתובת המייל ותתעדכנו בכל המבצעים!', 'no'),
(619, '_options_foo_form_subtitle', 'field_6130fdad974b2', 'no'),
(620, 'options_menu_title_1', 'כאן תופיע כותרת לורם 1', 'no'),
(621, '_options_menu_title_1', 'field_6130fdbb974b3', 'no'),
(622, 'options_menu_title_2', 'כאן תופיע כותרת לורם 2', 'no'),
(623, '_options_menu_title_2', 'field_6130fddd2e53d', 'no'),
(624, 'options_payments', 'a:5:{i:0;s:3:\"132\";i:1;s:3:\"133\";i:2;s:3:\"134\";i:3;s:3:\"135\";i:4;s:3:\"136\";}', 'no'),
(625, '_options_payments', 'field_613128dbc0b8c', 'no'),
(626, 'options_logo', '130', 'no'),
(627, '_options_logo', 'field_5dd3a310760b9', 'no'),
(628, 'options_logo_foo', '129', 'no'),
(629, '_options_logo_foo', 'field_6130fe1a32ded', 'no'),
(698, '_transient_timeout_acf_plugin_updates', '1630950025', 'no'),
(699, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.10.2\";}}', 'no'),
(711, 'options_base_form_title', 'משפחה מרוויחה יותר!', 'no'),
(712, '_options_base_form_title', 'field_6133ac34910fc', 'no'),
(713, 'options_base_form_text', 'הצטרפי למשפחת ויקטוריה סיקרט ישראל ותהני מהנחות אישיות, מידע על מוצרים חדשים וקופונים הכי שווים', 'no'),
(714, '_options_base_form_text', 'field_6133ac38910fd', 'no'),
(728, 'category_children', 'a:0:{}', 'yes'),
(757, '_transient_timeout__woocommerce_helper_updates', '1630863590', 'no'),
(758, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1630820390;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(759, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:63:\"https://downloads.wordpress.org/release/he_IL/wordpress-5.8.zip\";s:6:\"locale\";s:5:\"he_IL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:63:\"https://downloads.wordpress.org/release/he_IL/wordpress-5.8.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:3:\"5.8\";s:7:\"version\";s:3:\"5.8\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1630834776;s:15:\"version_checked\";s:3:\"5.8\";s:12:\"translations\";a:0:{}}', 'no'),
(760, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1630834778;s:7:\"checked\";a:2:{s:4:\"anat\";s:3:\"1.0\";s:15:\"twentytwentyone\";s:3:\"1.4\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:1:{s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.4.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:12:\"translations\";a:0:{}}', 'no'),
(772, '_transient_product-transient-version', '1630852951', 'yes'),
(786, 'options_prod_delivery', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'no'),
(787, '_options_prod_delivery', 'field_61345d9912b94', 'no'),
(811, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1630834937', 'no'),
(812, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:5118;}s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4767;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2725;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2605;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:2012;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1866;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1852;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1527;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1514;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1506;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1501;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1500;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1475;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1311;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1280;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:1275;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1238;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1160;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1129;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:1063;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:974;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:938;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:916;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:901;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:888;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:841;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:823;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:817;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:813;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:797;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:762;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:752;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:733;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:729;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:729;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:717;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:712;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:685;}s:9:\"gutenberg\";a:3:{s:4:\"name\";s:9:\"gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:5:\"count\";i:669;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:668;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:667;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:665;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:664;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:662;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:652;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:636;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:602;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:601;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:593;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:592;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:577;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:575;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:568;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:567;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:565;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:557;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:556;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:553;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:550;}s:9:\"elementor\";a:3:{s:4:\"name\";s:9:\"elementor\";s:4:\"slug\";s:9:\"elementor\";s:5:\"count\";i:543;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:538;}s:5:\"block\";a:3:{s:4:\"name\";s:5:\"block\";s:4:\"slug\";s:5:\"block\";s:5:\"count\";i:537;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:533;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:523;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:514;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:510;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:500;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:499;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:499;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:487;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:485;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:474;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:468;}s:8:\"shipping\";a:3:{s:4:\"name\";s:8:\"shipping\";s:4:\"slug\";s:8:\"shipping\";s:5:\"count\";i:466;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:464;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:464;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:454;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:448;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:446;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:444;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:434;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:427;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:424;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:419;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:411;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:411;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:406;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:401;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:401;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:391;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:390;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:384;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:384;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:380;}s:6:\"import\";a:3:{s:4:\"name\";s:6:\"import\";s:4:\"slug\";s:6:\"import\";s:5:\"count\";i:377;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:373;}s:6:\"blocks\";a:3:{s:4:\"name\";s:6:\"blocks\";s:4:\"slug\";s:6:\"blocks\";s:5:\"count\";i:369;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:369;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:360;}s:5:\"cache\";a:3:{s:4:\"name\";s:5:\"cache\";s:4:\"slug\";s:5:\"cache\";s:5:\"count\";i:357;}}', 'no'),
(841, '_transient_timeout_wc_onboarding_product_data', '1630911431', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(842, '_transient_wc_onboarding_product_data', 'a:6:{s:7:\"headers\";O:42:\"Requests_Utility_CaseInsensitiveDictionary\":1:{s:7:\"\0*\0data\";a:18:{s:6:\"server\";s:5:\"nginx\";s:4:\"date\";s:29:\"Sun, 05 Sep 2021 06:57:11 GMT\";s:12:\"content-type\";s:31:\"application/json; charset=UTF-8\";s:14:\"content-length\";s:5:\"11752\";s:12:\"x-robots-tag\";s:7:\"noindex\";s:4:\"link\";s:60:\"<https://woocommerce.com/wp-json/>; rel=\"https://api.w.org/\"\";s:22:\"x-content-type-options\";s:7:\"nosniff\";s:29:\"access-control-expose-headers\";s:33:\"X-WP-Total, X-WP-TotalPages, Link\";s:28:\"access-control-allow-headers\";s:73:\"Authorization, X-WP-Nonce, Content-Disposition, Content-MD5, Content-Type\";s:13:\"x-wccom-cache\";s:3:\"HIT\";s:13:\"cache-control\";s:10:\"max-age=60\";s:5:\"allow\";s:3:\"GET\";s:16:\"content-encoding\";s:4:\"gzip\";s:4:\"x-rq\";s:13:\"vie1 0 4 9980\";s:3:\"age\";s:2:\"21\";s:7:\"x-cache\";s:3:\"hit\";s:4:\"vary\";s:23:\"Accept-Encoding, Origin\";s:13:\"accept-ranges\";s:5:\"bytes\";}}s:4:\"body\";s:49598:\"{\"products\":[{\"title\":\"WooCommerce Google Analytics\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/GA-Dark.png\",\"excerpt\":\"Understand your customers and increase revenue with world\\u2019s leading analytics platform - integrated with WooCommerce for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2d21f7de14dfb8e9885a4622be701ddf\",\"slug\":\"woocommerce-google-analytics-integration\",\"id\":1442927},{\"title\":\"WooCommerce Tax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Tax-Dark.png\",\"excerpt\":\"Automatically calculate how much sales tax should be collected for WooCommerce orders - by city, country, or state - at checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/tax\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":3220291},{\"title\":\"Stripe\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Stripe-Dark-1.png\",\"excerpt\":\"Accept all major debit and credit cards as well as local payment methods with Stripe.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/stripe\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"50bb7a985c691bb943a9da4d2c8b5efd\",\"slug\":\"woocommerce-gateway-stripe\",\"id\":18627},{\"title\":\"Jetpack\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Jetpack-Dark.png\",\"excerpt\":\"Security, performance, and marketing tools made for WooCommerce stores by the WordPress experts. Get started with basic security and speed tools for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jetpack\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"d5bfef9700b62b2b132c74c74c3193eb\",\"slug\":\"jetpack\",\"id\":2725249},{\"title\":\"Facebook for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Facebook-Dark.png\",\"excerpt\":\"Get the Official Facebook for WooCommerce plugin for three powerful ways to help grow your business.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/facebook\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"0ea4fe4c2d7ca6338f8a322fb3e4e187\",\"slug\":\"facebook-for-woocommerce\",\"id\":2127297},{\"title\":\"WooCommerce Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Pay-Dark.png\",\"excerpt\":\"Securely accept payments, track cash flow, and manage recurring revenue from your dashboard \\u2014 all without setup costs or monthly fees.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"8c6319ca-8f41-4e69-be63-6b15ee37773b\",\"slug\":\"woocommerce-payments\",\"id\":5278104},{\"title\":\"Amazon Pay\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Amazon-Pay-Dark.png\",\"excerpt\":\"Amazon Pay is embedded in your WooCommerce store. Transactions take place via\\u00a0Amazon widgets, so the buyer never leaves your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/pay-with-amazon\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9865e043bbbe4f8c9735af31cb509b53\",\"slug\":\"woocommerce-gateway-amazon-payments-advanced\",\"id\":238816},{\"title\":\"Square for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Square-Dark.png\",\"excerpt\":\"Accepting payments is easy with Square. Clear rates, fast deposits (1-2 business days). Sell online and in person, and sync all payments, items and inventory.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/square\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e907be8b86d7df0c8f8e0d0020b52638\",\"slug\":\"woocommerce-square\",\"id\":1770503},{\"title\":\"WooCommerce Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Dark-1.png\",\"excerpt\":\"Print USPS and DHL labels right from your WooCommerce dashboard and instantly save up to 90%. WooCommerce Shipping is free to use and saves you time and money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":2165910},{\"title\":\"WooCommerce Subscriptions\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Subscriptions-Dark.png\",\"excerpt\":\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscriptions\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"6115e6d7e297b623a169fdcf5728b224\",\"slug\":\"woocommerce-subscriptions\",\"id\":27147},{\"title\":\"Mailchimp for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/09\\/logo-mailchimp-dark-v2.png\",\"excerpt\":\"Increase traffic, drive repeat purchases, and personalize your marketing when you connect to Mailchimp.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/mailchimp-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b4481616ebece8b1ff68fc59b90c1a91\",\"slug\":\"mailchimp-for-woocommerce\",\"id\":2545166},{\"title\":\"Product Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-Add-Ons-Dark.png\",\"excerpt\":\"Offer add-ons like gift wrapping, special messages or other special options for your products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"147d0077e591e16db9d0d67daeb8c484\",\"slug\":\"woocommerce-product-addons\",\"id\":18618},{\"title\":\"ShipStation Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Shipstation-Dark.png\",\"excerpt\":\"Fulfill all your Woo orders (and wherever else you sell) quickly and easily using ShipStation. Try it free for 30 days today!\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipstation-integration\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9de8640767ba64237808ed7f245a49bb\",\"slug\":\"woocommerce-shipstation-integration\",\"id\":18734},{\"title\":\"PayFast Payment Gateway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Payfast-Dark-1.png\",\"excerpt\":\"Take payments on your WooCommerce store via PayFast (redirect method).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/payfast-payment-gateway\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"557bf07293ad916f20c207c6c9cd15ff\",\"slug\":\"woocommerce-payfast-gateway\",\"id\":18596},{\"title\":\"USPS Shipping Method\",\"image\":\"\",\"excerpt\":\"Get shipping rates from the USPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/usps-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"83d1524e8f5f1913e58889f83d442c32\",\"slug\":\"woocommerce-shipping-usps\",\"id\":18657},{\"title\":\"Google Listings &#038; Ads\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2021\\/06\\/marketplace-card.png\",\"excerpt\":\"Reach millions of engaged shoppers across Google with free product listings and ads. Built in partnership with Google.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-listings-and-ads\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"118f4d86-f126-4c3a-8525-644e3170d161\",\"slug\":\"google-listings-and-ads\",\"id\":7623964},{\"title\":\"Google Ads &amp; Marketing by Kliken\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/02\\/GA-for-Woo-Logo-374x192px-qu3duk.png\",\"excerpt\":\"Get in front of shoppers and drive traffic to your store so you can grow your business with Smart Shopping Campaigns and free listings.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-ads-and-marketing\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"bf66e173-a220-4da7-9512-b5728c20fc16\",\"slug\":\"kliken-marketing-for-google\",\"id\":3866145},{\"title\":\"UPS Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/UPS-Shipping-Method-Dark.png\",\"excerpt\":\"Get shipping rates from the UPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ups-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8dae58502913bac0fbcdcaba515ea998\",\"slug\":\"woocommerce-shipping-ups\",\"id\":18665},{\"title\":\"Shipment Tracking\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Tracking-Dark-1.png\",\"excerpt\":\"Add shipment tracking information to your orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipment-tracking\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"1968e199038a8a001c9f9966fd06bf88\",\"slug\":\"woocommerce-shipment-tracking\",\"id\":18693},{\"title\":\"Table Rate Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Product-Table-Rate-Shipping-Dark.png\",\"excerpt\":\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/table-rate-shipping\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"3034ed8aff427b0f635fe4c86bbf008a\",\"slug\":\"woocommerce-table-rate-shipping\",\"id\":18718},{\"title\":\"Checkout Field Editor\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Checkout-Field-Editor-Dark.png\",\"excerpt\":\"Optimize your checkout process by adding, removing or editing fields to suit your needs.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-field-editor\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"2b8029f0d7cdd1118f4d843eb3ab43ff\",\"slug\":\"woocommerce-checkout-field-editor\",\"id\":184594},{\"title\":\"WooCommerce Bookings\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Dark.png\",\"excerpt\":\"Allow customers to book appointments, make reservations or rent equipment without leaving your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-bookings\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/hotel\\/\",\"price\":\"&#36;249.00\",\"hash\":\"911c438934af094c2b38d5560b9f50f3\",\"slug\":\"WooCommerce Bookings\",\"id\":390890},{\"title\":\"Braintree for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/02\\/braintree-black-copy.png\",\"excerpt\":\"Accept PayPal, credit cards and debit cards with a single payment gateway solution \\u2014 PayPal Powered by Braintree.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-powered-by-braintree\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"27f010c8e34ca65b205ddec88ad14536\",\"slug\":\"woocommerce-gateway-paypal-powered-by-braintree\",\"id\":1489837},{\"title\":\"WooCommerce Memberships\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/06\\/Thumbnail-Memberships-updated.png\",\"excerpt\":\"Power your membership association, online magazine, elearning sites, and more with access control to content\\/products and member discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-memberships\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"9288e7609ad0b487b81ef6232efa5cfc\",\"slug\":\"woocommerce-memberships\",\"id\":958589},{\"title\":\"Min\\/Max Quantities\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Min-Max-Qua-Dark.png\",\"excerpt\":\"Specify minimum and maximum allowed product quantities for orders to be completed.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/minmax-quantities\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"2b5188d90baecfb781a5aa2d6abb900a\",\"slug\":\"woocommerce-min-max-quantities\",\"id\":18616},{\"title\":\"Product Bundles\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-PB.png?v=1\",\"excerpt\":\"Offer personalized product bundles, bulk discount packages, and assembled\\u00a0products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-bundles\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa2518b5-ab19-4b75-bde9-60ca51e20f28\",\"slug\":\"woocommerce-product-bundles\",\"id\":18716},{\"title\":\"Multichannel for WooCommerce: Google, Amazon, eBay &#038; Walmart Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/10\\/Woo-Extension-Store-Logo-v2.png\",\"excerpt\":\"Get the official Google, Amazon, eBay and Walmart extension and create, sync and manage multichannel listings directly from WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-ebay-integration\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e4000666-9275-4c71-8619-be61fb41c9f9\",\"slug\":\"woocommerce-amazon-ebay-integration\",\"id\":3545890},{\"title\":\"FedEx Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/01\\/FedEx_Logo_Wallpaper.jpeg\",\"excerpt\":\"Get shipping rates from the FedEx API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/fedex-shipping-module\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1a48b598b47a81559baadef15e320f64\",\"slug\":\"woocommerce-shipping-fedex\",\"id\":18620},{\"title\":\"Product CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-CSV-Import-Dark.png\",\"excerpt\":\"Import, merge, and export products and variations to and from WooCommerce using a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-csv-import-suite\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"7ac9b00a1fe980fb61d28ab54d167d0d\",\"slug\":\"woocommerce-product-csv-import-suite\",\"id\":18680},{\"title\":\"Australia Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/australia-post.gif\",\"excerpt\":\"Get shipping rates for your WooCommerce store from the Australia Post API, which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/australia-post-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1dbd4dc6bd91a9cda1bd6b9e7a5e4f43\",\"slug\":\"woocommerce-shipping-australia-post\",\"id\":18622},{\"title\":\"Canada Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/canada-post.png\",\"excerpt\":\"Get shipping rates from the Canada Post Ratings API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/canada-post-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ac029cdf3daba20b20c7b9be7dc00e0e\",\"slug\":\"woocommerce-shipping-canada-post\",\"id\":18623},{\"title\":\"Follow-Ups\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Follow-Ups-Dark.png\",\"excerpt\":\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/follow-up-emails\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"05ece68fe94558e65278fe54d9ec84d2\",\"slug\":\"woocommerce-follow-up-emails\",\"id\":18686},{\"title\":\"LiveChat for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2015\\/11\\/LC_woo_regular-zmiaym.png\",\"excerpt\":\"Live Chat and messaging platform for sales and support -- increase average order value and overall sales through live conversations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/livechat\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.livechat.com\\/livechat-for-ecommerce\\/?a=woocommerce&amp;utm_source=woocommerce.com&amp;utm_medium=integration&amp;utm_campaign=woocommerce.com\",\"price\":\"&#36;0.00\",\"hash\":\"5344cc1f-ed4a-4d00-beff-9d67f6d372f3\",\"slug\":\"livechat-woocommerce\",\"id\":1348888},{\"title\":\"Authorize.Net\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2013\\/04\\/Thumbnail-Authorize.net-updated.png\",\"excerpt\":\"Authorize.Net gateway with support for pre-orders and subscriptions.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/authorize-net\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8b61524fe53add7fdd1a8d1b00b9327d\",\"slug\":\"woocommerce-gateway-authorize-net-cim\",\"id\":178481},{\"title\":\"Royal Mail\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/04\\/royalmail.png\",\"excerpt\":\"Offer Royal Mail shipping rates to your customers\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/royal-mail\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"03839cca1a16c4488fcb669aeb91a056\",\"slug\":\"woocommerce-shipping-royalmail\",\"id\":182719},{\"title\":\"Product Vendors\",\"image\":\"\",\"excerpt\":\"Turn your store into a multi-vendor marketplace\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-vendors\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"a97d99fccd651bbdd728f4d67d492c31\",\"slug\":\"woocommerce-product-vendors\",\"id\":219982},{\"title\":\"WooCommerce Accommodation Bookings\",\"image\":\"\",\"excerpt\":\"Book accommodation using WooCommerce and the WooCommerce Bookings extension.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-accommodation-bookings\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"99b2a7a4af90b6cefd2a733b3b1f78e7\",\"slug\":\"woocommerce-accommodation-bookings\",\"id\":1412069},{\"title\":\"Xero\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/08\\/xero2.png\",\"excerpt\":\"Save time with automated sync between WooCommerce and your Xero account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/xero\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"f0dd29d338d3c67cf6cee88eddf6869b\",\"slug\":\"woocommerce-xero\",\"id\":18733},{\"title\":\"WooCommerce Brands\",\"image\":\"\",\"excerpt\":\"Create, assign and list brands for products, and allow customers to view by brand.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/brands\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"8a88c7cbd2f1e73636c331c7a86f818c\",\"slug\":\"woocommerce-brands\",\"id\":18737},{\"title\":\"WooCommerce Customer \\/ Order \\/ Coupon Export\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-Customer-Order-Coupon-Export-updated.png\",\"excerpt\":\"Export customers, orders, and coupons from WooCommerce manually or on an automated schedule.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ordercustomer-csv-export\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"914de15813a903c767b55445608bf290\",\"slug\":\"woocommerce-customer-order-csv-export\",\"id\":18652},{\"title\":\"AutomateWoo\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-AutomateWoo-Dark-1.png\",\"excerpt\":\"Powerful marketing automation for WooCommerce. AutomateWoo has the tools you need to grow your store and make more money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/automatewoo\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"ba9299b8-1dba-4aa0-a313-28bc1755cb88\",\"slug\":\"automatewoo\",\"id\":4652610},{\"title\":\"Advanced Notifications\",\"image\":\"\",\"excerpt\":\"Easily setup \\\"new order\\\" and stock email notifications for multiple recipients of your choosing.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/advanced-notifications\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"112372c44b002fea2640bd6bfafbca27\",\"slug\":\"woocommerce-advanced-notifications\",\"id\":18740},{\"title\":\"WooCommerce Points and Rewards\",\"image\":\"\",\"excerpt\":\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-points-and-rewards\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"1649b6cca5da8b923b01ca56b5cdd246\",\"slug\":\"woocommerce-points-and-rewards\",\"id\":210259},{\"title\":\"Smart Coupons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/10\\/wc-product-smart-coupons.png\",\"excerpt\":\"Everything you need for discounts, coupons, credits, gift cards, product giveaways, offers, and promotions. Most popular and complete coupons plugin for WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/smart-coupons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demo.storeapps.org\\/?demo=sc\",\"price\":\"&#36;99.00\",\"hash\":\"05c45f2aa466106a466de4402fff9dde\",\"slug\":\"woocommerce-smart-coupons\",\"id\":18729},{\"title\":\"WooCommerce Pre-Orders\",\"image\":\"\",\"excerpt\":\"Allow customers to order products before they are available.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-pre-orders\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"b2dc75e7d55e6f5bbfaccb59830f66b7\",\"slug\":\"woocommerce-pre-orders\",\"id\":178477},{\"title\":\"PayPal Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/10\\/PPCP-Tile-PayPal-Logo-and-Cart-Art-2x-2-uozwz8.jpg\",\"excerpt\":\"PayPal\'s latest, all-in-one checkout solution. Securely accept PayPal Digital Payments, credit\\/debit cards and local payment methods.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paypal-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"934115ab-e3f3-4435-9580-345b1ce21899\",\"slug\":\"woocommerce-paypal-payments\",\"id\":6410731},{\"title\":\"WooCommerce Subscription Downloads\",\"image\":\"\",\"excerpt\":\"Offer additional downloads to your subscribers, via downloadable products listed in your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscription-downloads\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5be9e21c13953253e4406d2a700382ec\",\"slug\":\"woocommerce-subscription-downloads\",\"id\":420458},{\"title\":\"WooCommerce Additional Variation Images\",\"image\":\"\",\"excerpt\":\"Add gallery images per variation on variable products within WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-additional-variation-images\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/storefront\\/product\\/woo-single-1\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c61dd6de57dcecb32bd7358866de4539\",\"slug\":\"woocommerce-additional-variation-images\",\"id\":477384},{\"title\":\"WooCommerce Zapier\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/woocommerce-zapier-logo.png\",\"excerpt\":\"Integrate your WooCommerce store with 3000+ cloud apps and services today. Trusted by 10,000+ users.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-zapier\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;59.00\",\"hash\":\"0782bdbe932c00f4978850268c6cfe40\",\"slug\":\"woocommerce-zapier\",\"id\":243589},{\"title\":\"WooCommerce Deposits\",\"image\":\"\",\"excerpt\":\"Enable customers to pay for products using a deposit or a payment plan.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-deposits\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;179.00\",\"hash\":\"de192a6cf12c4fd803248da5db700762\",\"slug\":\"woocommerce-deposits\",\"id\":977087},{\"title\":\"Amazon S3 Storage\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/amazon.png\",\"excerpt\":\"Serve digital products via Amazon S3\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-s3-storage\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"473bf6f221b865eff165c97881b473bb\",\"slug\":\"woocommerce-amazon-s3-storage\",\"id\":18663},{\"title\":\"Cart Add-ons\",\"image\":\"\",\"excerpt\":\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/cart-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"3a8ef25334396206f5da4cf208adeda3\",\"slug\":\"woocommerce-cart-add-ons\",\"id\":18717},{\"title\":\"Shipping Multiple Addresses\",\"image\":\"\",\"excerpt\":\"Allow your customers to ship individual items in a single order to multiple addresses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping-multiple-addresses\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa0eb6f777846d329952d5b891d6f8cc\",\"slug\":\"woocommerce-shipping-multiple-addresses\",\"id\":18741},{\"title\":\"Dynamic Pricing\",\"image\":\"\",\"excerpt\":\"Bulk discounts, role-based pricing and much more\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/dynamic-pricing\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"9a41775bb33843f52c93c922b0053986\",\"slug\":\"woocommerce-dynamic-pricing\",\"id\":18643},{\"title\":\"Name Your Price\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/09\\/nyp-icon-dark-v83owf.png\",\"excerpt\":\"Allow customers to define the product price. Also useful for accepting user-set donations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/name-your-price\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"31b4e11696cd99a3c0572975a84f1c08\",\"slug\":\"woocommerce-name-your-price\",\"id\":18738},{\"title\":\"TaxJar\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/10\\/taxjar-logotype.png\",\"excerpt\":\"Save hours every month by putting your sales tax on autopilot. Automated, multi-state sales tax calculation, reporting, and filing for your WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/taxjar\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"12072d8e-e933-4561-97b1-9db3c7eeed91\",\"slug\":\"taxjar-simplified-taxes-for-woocommerce\",\"id\":514914},{\"title\":\"Klarna Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Checkout_Black-1.png\",\"excerpt\":\"Klarna Checkout is a full checkout experience embedded on your site that includes all popular payment methods (Pay Now, Pay Later, Financing, Installments).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-checkout\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnacheckout\\/\",\"price\":\"&#36;0.00\",\"hash\":\"90f8ce584e785fcd8c2d739fd4f40d78\",\"slug\":\"klarna-checkout-for-woocommerce\",\"id\":2754152},{\"title\":\"Bulk Stock Management\",\"image\":\"\",\"excerpt\":\"Edit product and variation stock levels in bulk via this handy interface\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bulk-stock-management\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"02f4328d52f324ebe06a78eaaae7934f\",\"slug\":\"woocommerce-bulk-stock-management\",\"id\":18670},{\"title\":\"WooCommerce Print Invoices &amp; Packing lists\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/03\\/Thumbnail-Print-Invoices-Packing-lists-updated.png\",\"excerpt\":\"Generate invoices, packing slips, and pick lists for your WooCommerce orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/print-invoices-packing-lists\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"465de1126817cdfb42d97ebca7eea717\",\"slug\":\"woocommerce-pip\",\"id\":18666},{\"title\":\"WooCommerce Email Customizer\",\"image\":\"\",\"excerpt\":\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-email-customizer\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"bd909fa97874d431f203b5336c7e8873\",\"slug\":\"woocommerce-email-customizer\",\"id\":853277},{\"title\":\"Force Sells\",\"image\":\"\",\"excerpt\":\"Force products to be added to the cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/force-sells\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"3ebddfc491ca168a4ea4800b893302b0\",\"slug\":\"woocommerce-force-sells\",\"id\":18678},{\"title\":\"WooCommerce Quick View\",\"image\":\"\",\"excerpt\":\"Show a quick-view button to view product details and add to cart via lightbox popup\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-quick-view\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"619c6e57ce72c49c4b57e15b06eddb65\",\"slug\":\"woocommerce-quick-view\",\"id\":187509},{\"title\":\"WooCommerce Purchase Order Gateway\",\"image\":\"\",\"excerpt\":\"Receive purchase orders via your WooCommerce-powered online store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-purchase-order\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"573a92318244ece5facb449d63e74874\",\"slug\":\"woocommerce-gateway-purchase-order\",\"id\":478542},{\"title\":\"Returns and Warranty Requests\",\"image\":\"\",\"excerpt\":\"Manage the RMA process, add warranties to products &amp; let customers request &amp; manage returns \\/ exchanges from their account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/warranty-requests\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"9b4c41102e6b61ea5f558e16f9b63e25\",\"slug\":\"woocommerce-warranty\",\"id\":228315},{\"title\":\"WooCommerce AvaTax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-Avalara-updated.png\",\"excerpt\":\"Get 100% accurate sales tax calculations and on-time tax filing. No more tracking sales tax rates and rules.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-avatax\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"57077a4b28ba71cacf692bcf4a1a7f60\",\"slug\":\"woocommerce-avatax\",\"id\":1389326},{\"title\":\"WooCommerce Box Office\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-BO-Dark.png\",\"excerpt\":\"Sell tickets for your next event, concert, function, fundraiser or conference directly on your own site\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-box-office\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"e704c9160de318216a8fa657404b9131\",\"slug\":\"woocommerce-box-office\",\"id\":1628717},{\"title\":\"Product Enquiry Form\",\"image\":\"\",\"excerpt\":\"Allow visitors to contact you directly from the product details page via a reCAPTCHA protected form to enquire about a product.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-enquiry-form\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5a0f5d72519a8ffcc86669f042296937\",\"slug\":\"woocommerce-product-enquiry-form\",\"id\":18601},{\"title\":\"Google Product Feed\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2011\\/11\\/logo-regular-lscryp.png\",\"excerpt\":\"Feed rich product data to Google Merchant Center for setting up free product listings, product ads, and local inventory campaigns. Full control over your field mappings, and feed content so you can maximize campaign performance and ad spend.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-product-feed\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d55b4f852872025741312839f142447e\",\"slug\":\"woocommerce-product-feeds\",\"id\":18619},{\"title\":\"WooCommerce Order Barcodes\",\"image\":\"\",\"excerpt\":\"Generates a unique barcode for each order on your site - perfect for e-tickets, packing slips, reservations and a variety of other uses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-barcodes\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"889835bb29ee3400923653e1e44a3779\",\"slug\":\"woocommerce-order-barcodes\",\"id\":391708},{\"title\":\"WooCommerce 360\\u00ba Image\",\"image\":\"\",\"excerpt\":\"An easy way to add a dynamic, controllable 360\\u00ba image rotation to your WooCommerce site, by adding a group of images to a product\\u2019s gallery.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-360-image\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"24eb2cfa3738a66bf3b2587876668cd2\",\"slug\":\"woocommerce-360-image\",\"id\":512186},{\"title\":\"WooCommerce Photography\",\"image\":\"\",\"excerpt\":\"Sell photos in the blink of an eye using this simple as dragging &amp; dropping interface.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-photography\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ee76e8b9daf1d97ca4d3874cc9e35687\",\"slug\":\"woocommerce-photography\",\"id\":583602},{\"title\":\"WooCommerce Bookings Availability\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Aval-Dark.png\",\"excerpt\":\"Sell more bookings by presenting a calendar or schedule of available slots in a page or post.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bookings-availability\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"30770d2a-e392-4e82-baaa-76cfc7d02ae3\",\"slug\":\"woocommerce-bookings-availability\",\"id\":4228225},{\"title\":\"Software Add-on\",\"image\":\"\",\"excerpt\":\"Sell License Keys for Software\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/software-add-on\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"79f6dbfe1f1d3a56a86f0509b6d6b04b\",\"slug\":\"woocommerce-software-add-on\",\"id\":18683},{\"title\":\"WooCommerce Products Compare\",\"image\":\"\",\"excerpt\":\"WooCommerce Products Compare will allow your potential customers to easily compare products within your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-products-compare\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"c3ba0a4a3199a0cc7a6112eb24414548\",\"slug\":\"woocommerce-products-compare\",\"id\":853117},{\"title\":\"WooCommerce Store Catalog PDF Download\",\"image\":\"\",\"excerpt\":\"Offer your customers a PDF download of your product catalog, generated by WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-store-catalog-pdf-download\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"79ca7aadafe706364e2d738b7c1090c4\",\"slug\":\"woocommerce-store-catalog-pdf-download\",\"id\":675790},{\"title\":\"Gravity Forms Product Add-ons\",\"image\":\"\",\"excerpt\":\"Powerful product add-ons, Gravity style\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/gravity-forms-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/gravity-forms\\/\",\"price\":\"&#36;99.00\",\"hash\":\"a6ac0ab1a1536e3a357ccf24c0650ed0\",\"slug\":\"woocommerce-gravityforms-product-addons\",\"id\":18633},{\"title\":\"Composite Products\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CP.png?v=1\",\"excerpt\":\"Create product kit builders and custom product configurators using existing products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/composite-products\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"0343e0115bbcb97ccd98442b8326a0af\",\"slug\":\"woocommerce-composite-products\",\"id\":216836},{\"title\":\"Eway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/51456-Eway-logo-tagline-RGB-H-yellow-_-grey.png\",\"excerpt\":\"Take credit card payments securely via Eway (SG, MY, HK, AU, and NZ) keeping customers on your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eway\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2c497769d98d025e0d340cd0b5ea5da1\",\"slug\":\"woocommerce-gateway-eway\",\"id\":18604},{\"title\":\"WooCommerce Paid Courses\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2021\\/07\\/wc-paid-courses.png\",\"excerpt\":\"Sell your online courses using the most popular eCommerce platform on the web \\u2013 WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paid-courses\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"bad2a02a063555b7e2bee59924690763\",\"slug\":\"woothemes-sensei\",\"id\":152116},{\"title\":\"QuickBooks Sync for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/04\\/woocommerce-com-logo-1-hyhzbh.png\",\"excerpt\":\"Automatic two-way sync for orders, customers, products, inventory and more between WooCommerce and QuickBooks (Online, Desktop, or POS).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/quickbooks-sync-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c5e32e20-7c1f-4585-8b15-d930c2d842ac\",\"slug\":\"myworks-woo-sync-for-quickbooks-online\",\"id\":4065824},{\"title\":\"Catalog Visibility Options\",\"image\":\"\",\"excerpt\":\"Transform WooCommerce into an online catalog by removing eCommerce functionality\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/catalog-visibility-options\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"12e791110365fdbb5865c8658907967e\",\"slug\":\"woocommerce-catalog-visibility-options\",\"id\":18648},{\"title\":\"WooCommerce Blocks\",\"image\":\"\",\"excerpt\":\"WooCommerce Blocks offers a range of Gutenberg blocks you can use to build and customise your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gutenberg-products-block\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c2e9f13a-f90c-4ffe-a8a5-b432399ec263\",\"slug\":\"woo-gutenberg-products-block\",\"id\":3076677},{\"title\":\"Conditional Shipping and Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CSP.png?v=1\",\"excerpt\":\"Use conditional logic to restrict the shipping and payment options available on your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/conditional-shipping-and-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1f56ff002fa830b77017b0107505211a\",\"slug\":\"woocommerce-conditional-shipping-and-payments\",\"id\":680253},{\"title\":\"WooCommerce Order Status Manager\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/02\\/Thumbnail-Order-Status-Manager-updated.png\",\"excerpt\":\"Create, edit, and delete completely custom order statuses and integrate them seamlessly into your order management flow.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-manager\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"51fd9ab45394b4cad5a0ebf58d012342\",\"slug\":\"woocommerce-order-status-manager\",\"id\":588398},{\"title\":\"Sequential Order Numbers Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/05\\/Thumbnail-Sequential-Order-Numbers-Pro-updated.png\",\"excerpt\":\"Tame your order numbers! Upgrade from Sequential Order Numbers with advanced features and with optional prefixes\\/suffixes.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sequential-order-numbers-pro\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"0b18a2816e016ba9988b93b1cd8fe766\",\"slug\":\"woocommerce-sequential-order-numbers-pro\",\"id\":18688},{\"title\":\"Coupon Shortcodes\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/09\\/woocommerce-coupon-shortcodes-product-image-1870x960-1-vc5gux.png\",\"excerpt\":\"Show coupon discount info using shortcodes. Allows to render coupon information and content conditionally, based on the validity of coupons.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/coupon-shortcodes\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"ac5d9d51-70b2-4d8f-8b89-24200eea1394\",\"slug\":\"woocommerce-coupon-shortcodes\",\"id\":244762},{\"title\":\"WooCommerce Checkout Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/07\\/Thumbnail-Checkout-Add-Ons-updated.png\",\"excerpt\":\"Highlight relevant products, offers like free shipping and other up-sells during checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8fdca00b4000b7a8cc26371d0e470a8f\",\"slug\":\"woocommerce-checkout-add-ons\",\"id\":466854},{\"title\":\"WooCommerce Google Analytics Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-GAPro-updated.png\",\"excerpt\":\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics-pro\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d8aed8b7306b509eec1589e59abe319f\",\"slug\":\"woocommerce-google-analytics-pro\",\"id\":1312497},{\"title\":\"WooCommerce One Page Checkout\",\"image\":\"\",\"excerpt\":\"Create special pages where customers can choose products, checkout &amp; pay all on the one page.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-one-page-checkout\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"c9ba8f8352cd71b5508af5161268619a\",\"slug\":\"woocommerce-one-page-checkout\",\"id\":527886},{\"title\":\"WooCommerce Product Search\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2014\\/10\\/woocommerce-product-search-product-image-1870x960-1-jvsljj.png\",\"excerpt\":\"The perfect search engine helps customers to find and buy products quickly \\u2013 essential for every WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-product-search\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.itthinx.com\\/wps\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c84cc8ca16ddac3408e6b6c5871133a8\",\"slug\":\"woocommerce-product-search\",\"id\":512174},{\"title\":\"First Data\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-FirstData-updated.png\",\"excerpt\":\"Accept payment with First Data using the Payeezy Gateway, Payeezy, or legacy Global Gateway payment gateways.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/firstdata\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb3e32663ec0810592eaf0d097796230\",\"slug\":\"woocommerce-gateway-firstdata\",\"id\":18645},{\"title\":\"Klarna Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Payments_Pink.png\",\"excerpt\":\"With Klarna Payments\\u00a0you can choose the payment that you want, Pay Now, Pay Later or Slice It. No credit card numbers, no passwords, no worries.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnapayments\\/\",\"price\":\"&#36;0.00\",\"hash\":\"a19c689325bc8ea63c620765dd54b33a\",\"slug\":\"klarna-payments-for-woocommerce\",\"id\":2754217},{\"title\":\"WooSlider\",\"image\":\"\",\"excerpt\":\"WooSlider is the ultimate responsive slideshow WordPress slider plugin\\r\\n\\r\\n\\u00a0\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/wooslider\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/www.wooslider.com\\/\",\"price\":\"&#36;49.00\",\"hash\":\"209d98f3ccde6cc3de7e8732a2b20b6a\",\"slug\":\"wooslider\",\"id\":46506},{\"title\":\"WooCommerce Order Status Control\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/06\\/Thumbnail-Order-Status-Control-updated.png\",\"excerpt\":\"Use this extension to automatically change the order status to \\\"completed\\\" after successful payment.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-control\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"32400e509c7c36dcc1cd368e8267d981\",\"slug\":\"woocommerce-order-status-control\",\"id\":439037},{\"title\":\"HubSpot for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/04\\/hubspotlogo-web-color-pxebeq.png\",\"excerpt\":\"Connect your WooCommerce store to HubSpot. Sync, automate &amp; analyze data with HubSpot WooCommerce Integration\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/hubspot-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e50acec8-3a6c-454c-8562-2da4898fa6c1\",\"slug\":\"hubspot-for-woocommerce\",\"id\":5785079},{\"title\":\"WooCommerce Social Login\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/08\\/Thumbnail-Social-Login-updated.png\",\"excerpt\":\"Enable Social Login for seamless checkout and account creation.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-social-login\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demos.skyverge.com\\/woocommerce-social-login\\/\",\"price\":\"&#36;79.00\",\"hash\":\"b231cd6367a79cc8a53b7d992d77525d\",\"slug\":\"woocommerce-social-login\",\"id\":473617},{\"title\":\"Variation Swatches and Photos\",\"image\":\"\",\"excerpt\":\"Show color and image swatches instead of dropdowns for variable products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/variation-swatches-and-photos\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/swatches-and-photos\\/\",\"price\":\"&#36;99.00\",\"hash\":\"37bea8d549df279c8278878d081b062f\",\"slug\":\"woocommerce-variation-swatches-and-photos\",\"id\":18697},{\"title\":\"Opayo Payment Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/Opayo_logo_RGB.png\",\"excerpt\":\"Take payments on your WooCommerce store via Opayo (formally SagePay).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sage-pay-form\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6bc0cca47d0274d8ef9b164f6fbec1cc\",\"slug\":\"woocommerce-gateway-sagepay-form\",\"id\":18599},{\"title\":\"Viva Wallet for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/04\\/Viva-Wallet-logo.png?w=374\",\"excerpt\":\"Integrate the Viva Wallet payment gateway with your WooCommerce store to process and sync your payments and help you sell more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/viva-wallet-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"7240a329-047f-4d8b-b7ec-ee3defd798bd\",\"slug\":\"viva-wallet-for-woocommerce\",\"id\":6137160},{\"title\":\"EU VAT Number\",\"image\":\"\",\"excerpt\":\"Collect VAT numbers at checkout and remove the VAT charge for eligible EU businesses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eu-vat-number\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"d2720c4b4bb8d6908e530355b7a2d734\",\"slug\":\"woocommerce-eu-vat-number\",\"id\":18592}]}\";s:8:\"response\";a:2:{s:4:\"code\";i:200;s:7:\"message\";s:2:\"OK\";}s:7:\"cookies\";a:0:{}s:8:\"filename\";N;s:13:\"http_response\";O:25:\"WP_HTTP_Requests_Response\":5:{s:11:\"\0*\0response\";O:17:\"Requests_Response\":10:{s:4:\"body\";s:49598:\"{\"products\":[{\"title\":\"WooCommerce Google Analytics\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/GA-Dark.png\",\"excerpt\":\"Understand your customers and increase revenue with world\\u2019s leading analytics platform - integrated with WooCommerce for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2d21f7de14dfb8e9885a4622be701ddf\",\"slug\":\"woocommerce-google-analytics-integration\",\"id\":1442927},{\"title\":\"WooCommerce Tax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Tax-Dark.png\",\"excerpt\":\"Automatically calculate how much sales tax should be collected for WooCommerce orders - by city, country, or state - at checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/tax\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":3220291},{\"title\":\"Stripe\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Stripe-Dark-1.png\",\"excerpt\":\"Accept all major debit and credit cards as well as local payment methods with Stripe.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/stripe\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"50bb7a985c691bb943a9da4d2c8b5efd\",\"slug\":\"woocommerce-gateway-stripe\",\"id\":18627},{\"title\":\"Jetpack\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Jetpack-Dark.png\",\"excerpt\":\"Security, performance, and marketing tools made for WooCommerce stores by the WordPress experts. Get started with basic security and speed tools for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jetpack\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"d5bfef9700b62b2b132c74c74c3193eb\",\"slug\":\"jetpack\",\"id\":2725249},{\"title\":\"Facebook for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Facebook-Dark.png\",\"excerpt\":\"Get the Official Facebook for WooCommerce plugin for three powerful ways to help grow your business.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/facebook\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"0ea4fe4c2d7ca6338f8a322fb3e4e187\",\"slug\":\"facebook-for-woocommerce\",\"id\":2127297},{\"title\":\"WooCommerce Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Pay-Dark.png\",\"excerpt\":\"Securely accept payments, track cash flow, and manage recurring revenue from your dashboard \\u2014 all without setup costs or monthly fees.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"8c6319ca-8f41-4e69-be63-6b15ee37773b\",\"slug\":\"woocommerce-payments\",\"id\":5278104},{\"title\":\"Amazon Pay\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Amazon-Pay-Dark.png\",\"excerpt\":\"Amazon Pay is embedded in your WooCommerce store. Transactions take place via\\u00a0Amazon widgets, so the buyer never leaves your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/pay-with-amazon\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9865e043bbbe4f8c9735af31cb509b53\",\"slug\":\"woocommerce-gateway-amazon-payments-advanced\",\"id\":238816},{\"title\":\"Square for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Square-Dark.png\",\"excerpt\":\"Accepting payments is easy with Square. Clear rates, fast deposits (1-2 business days). Sell online and in person, and sync all payments, items and inventory.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/square\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e907be8b86d7df0c8f8e0d0020b52638\",\"slug\":\"woocommerce-square\",\"id\":1770503},{\"title\":\"WooCommerce Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Dark-1.png\",\"excerpt\":\"Print USPS and DHL labels right from your WooCommerce dashboard and instantly save up to 90%. WooCommerce Shipping is free to use and saves you time and money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":2165910},{\"title\":\"WooCommerce Subscriptions\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Subscriptions-Dark.png\",\"excerpt\":\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscriptions\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"6115e6d7e297b623a169fdcf5728b224\",\"slug\":\"woocommerce-subscriptions\",\"id\":27147},{\"title\":\"Mailchimp for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/09\\/logo-mailchimp-dark-v2.png\",\"excerpt\":\"Increase traffic, drive repeat purchases, and personalize your marketing when you connect to Mailchimp.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/mailchimp-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b4481616ebece8b1ff68fc59b90c1a91\",\"slug\":\"mailchimp-for-woocommerce\",\"id\":2545166},{\"title\":\"Product Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-Add-Ons-Dark.png\",\"excerpt\":\"Offer add-ons like gift wrapping, special messages or other special options for your products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"147d0077e591e16db9d0d67daeb8c484\",\"slug\":\"woocommerce-product-addons\",\"id\":18618},{\"title\":\"ShipStation Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Shipstation-Dark.png\",\"excerpt\":\"Fulfill all your Woo orders (and wherever else you sell) quickly and easily using ShipStation. Try it free for 30 days today!\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipstation-integration\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9de8640767ba64237808ed7f245a49bb\",\"slug\":\"woocommerce-shipstation-integration\",\"id\":18734},{\"title\":\"PayFast Payment Gateway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Payfast-Dark-1.png\",\"excerpt\":\"Take payments on your WooCommerce store via PayFast (redirect method).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/payfast-payment-gateway\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"557bf07293ad916f20c207c6c9cd15ff\",\"slug\":\"woocommerce-payfast-gateway\",\"id\":18596},{\"title\":\"USPS Shipping Method\",\"image\":\"\",\"excerpt\":\"Get shipping rates from the USPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/usps-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"83d1524e8f5f1913e58889f83d442c32\",\"slug\":\"woocommerce-shipping-usps\",\"id\":18657},{\"title\":\"Google Listings &#038; Ads\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2021\\/06\\/marketplace-card.png\",\"excerpt\":\"Reach millions of engaged shoppers across Google with free product listings and ads. Built in partnership with Google.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-listings-and-ads\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"118f4d86-f126-4c3a-8525-644e3170d161\",\"slug\":\"google-listings-and-ads\",\"id\":7623964},{\"title\":\"Google Ads &amp; Marketing by Kliken\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/02\\/GA-for-Woo-Logo-374x192px-qu3duk.png\",\"excerpt\":\"Get in front of shoppers and drive traffic to your store so you can grow your business with Smart Shopping Campaigns and free listings.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-ads-and-marketing\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"bf66e173-a220-4da7-9512-b5728c20fc16\",\"slug\":\"kliken-marketing-for-google\",\"id\":3866145},{\"title\":\"UPS Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/UPS-Shipping-Method-Dark.png\",\"excerpt\":\"Get shipping rates from the UPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ups-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8dae58502913bac0fbcdcaba515ea998\",\"slug\":\"woocommerce-shipping-ups\",\"id\":18665},{\"title\":\"Shipment Tracking\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Tracking-Dark-1.png\",\"excerpt\":\"Add shipment tracking information to your orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipment-tracking\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"1968e199038a8a001c9f9966fd06bf88\",\"slug\":\"woocommerce-shipment-tracking\",\"id\":18693},{\"title\":\"Table Rate Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Product-Table-Rate-Shipping-Dark.png\",\"excerpt\":\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/table-rate-shipping\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"3034ed8aff427b0f635fe4c86bbf008a\",\"slug\":\"woocommerce-table-rate-shipping\",\"id\":18718},{\"title\":\"Checkout Field Editor\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Checkout-Field-Editor-Dark.png\",\"excerpt\":\"Optimize your checkout process by adding, removing or editing fields to suit your needs.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-field-editor\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"2b8029f0d7cdd1118f4d843eb3ab43ff\",\"slug\":\"woocommerce-checkout-field-editor\",\"id\":184594},{\"title\":\"WooCommerce Bookings\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Dark.png\",\"excerpt\":\"Allow customers to book appointments, make reservations or rent equipment without leaving your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-bookings\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/hotel\\/\",\"price\":\"&#36;249.00\",\"hash\":\"911c438934af094c2b38d5560b9f50f3\",\"slug\":\"WooCommerce Bookings\",\"id\":390890},{\"title\":\"Braintree for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/02\\/braintree-black-copy.png\",\"excerpt\":\"Accept PayPal, credit cards and debit cards with a single payment gateway solution \\u2014 PayPal Powered by Braintree.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-powered-by-braintree\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"27f010c8e34ca65b205ddec88ad14536\",\"slug\":\"woocommerce-gateway-paypal-powered-by-braintree\",\"id\":1489837},{\"title\":\"WooCommerce Memberships\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/06\\/Thumbnail-Memberships-updated.png\",\"excerpt\":\"Power your membership association, online magazine, elearning sites, and more with access control to content\\/products and member discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-memberships\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"9288e7609ad0b487b81ef6232efa5cfc\",\"slug\":\"woocommerce-memberships\",\"id\":958589},{\"title\":\"Min\\/Max Quantities\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Min-Max-Qua-Dark.png\",\"excerpt\":\"Specify minimum and maximum allowed product quantities for orders to be completed.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/minmax-quantities\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"2b5188d90baecfb781a5aa2d6abb900a\",\"slug\":\"woocommerce-min-max-quantities\",\"id\":18616},{\"title\":\"Product Bundles\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-PB.png?v=1\",\"excerpt\":\"Offer personalized product bundles, bulk discount packages, and assembled\\u00a0products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-bundles\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa2518b5-ab19-4b75-bde9-60ca51e20f28\",\"slug\":\"woocommerce-product-bundles\",\"id\":18716},{\"title\":\"Multichannel for WooCommerce: Google, Amazon, eBay &#038; Walmart Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/10\\/Woo-Extension-Store-Logo-v2.png\",\"excerpt\":\"Get the official Google, Amazon, eBay and Walmart extension and create, sync and manage multichannel listings directly from WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-ebay-integration\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e4000666-9275-4c71-8619-be61fb41c9f9\",\"slug\":\"woocommerce-amazon-ebay-integration\",\"id\":3545890},{\"title\":\"FedEx Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/01\\/FedEx_Logo_Wallpaper.jpeg\",\"excerpt\":\"Get shipping rates from the FedEx API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/fedex-shipping-module\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1a48b598b47a81559baadef15e320f64\",\"slug\":\"woocommerce-shipping-fedex\",\"id\":18620},{\"title\":\"Product CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-CSV-Import-Dark.png\",\"excerpt\":\"Import, merge, and export products and variations to and from WooCommerce using a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-csv-import-suite\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"7ac9b00a1fe980fb61d28ab54d167d0d\",\"slug\":\"woocommerce-product-csv-import-suite\",\"id\":18680},{\"title\":\"Australia Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/australia-post.gif\",\"excerpt\":\"Get shipping rates for your WooCommerce store from the Australia Post API, which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/australia-post-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1dbd4dc6bd91a9cda1bd6b9e7a5e4f43\",\"slug\":\"woocommerce-shipping-australia-post\",\"id\":18622},{\"title\":\"Canada Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/canada-post.png\",\"excerpt\":\"Get shipping rates from the Canada Post Ratings API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/canada-post-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ac029cdf3daba20b20c7b9be7dc00e0e\",\"slug\":\"woocommerce-shipping-canada-post\",\"id\":18623},{\"title\":\"Follow-Ups\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Follow-Ups-Dark.png\",\"excerpt\":\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/follow-up-emails\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"05ece68fe94558e65278fe54d9ec84d2\",\"slug\":\"woocommerce-follow-up-emails\",\"id\":18686},{\"title\":\"LiveChat for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2015\\/11\\/LC_woo_regular-zmiaym.png\",\"excerpt\":\"Live Chat and messaging platform for sales and support -- increase average order value and overall sales through live conversations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/livechat\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.livechat.com\\/livechat-for-ecommerce\\/?a=woocommerce&amp;utm_source=woocommerce.com&amp;utm_medium=integration&amp;utm_campaign=woocommerce.com\",\"price\":\"&#36;0.00\",\"hash\":\"5344cc1f-ed4a-4d00-beff-9d67f6d372f3\",\"slug\":\"livechat-woocommerce\",\"id\":1348888},{\"title\":\"Authorize.Net\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2013\\/04\\/Thumbnail-Authorize.net-updated.png\",\"excerpt\":\"Authorize.Net gateway with support for pre-orders and subscriptions.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/authorize-net\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8b61524fe53add7fdd1a8d1b00b9327d\",\"slug\":\"woocommerce-gateway-authorize-net-cim\",\"id\":178481},{\"title\":\"Royal Mail\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/04\\/royalmail.png\",\"excerpt\":\"Offer Royal Mail shipping rates to your customers\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/royal-mail\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"03839cca1a16c4488fcb669aeb91a056\",\"slug\":\"woocommerce-shipping-royalmail\",\"id\":182719},{\"title\":\"Product Vendors\",\"image\":\"\",\"excerpt\":\"Turn your store into a multi-vendor marketplace\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-vendors\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"a97d99fccd651bbdd728f4d67d492c31\",\"slug\":\"woocommerce-product-vendors\",\"id\":219982},{\"title\":\"WooCommerce Accommodation Bookings\",\"image\":\"\",\"excerpt\":\"Book accommodation using WooCommerce and the WooCommerce Bookings extension.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-accommodation-bookings\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"99b2a7a4af90b6cefd2a733b3b1f78e7\",\"slug\":\"woocommerce-accommodation-bookings\",\"id\":1412069},{\"title\":\"Xero\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/08\\/xero2.png\",\"excerpt\":\"Save time with automated sync between WooCommerce and your Xero account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/xero\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"f0dd29d338d3c67cf6cee88eddf6869b\",\"slug\":\"woocommerce-xero\",\"id\":18733},{\"title\":\"WooCommerce Brands\",\"image\":\"\",\"excerpt\":\"Create, assign and list brands for products, and allow customers to view by brand.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/brands\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"8a88c7cbd2f1e73636c331c7a86f818c\",\"slug\":\"woocommerce-brands\",\"id\":18737},{\"title\":\"WooCommerce Customer \\/ Order \\/ Coupon Export\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-Customer-Order-Coupon-Export-updated.png\",\"excerpt\":\"Export customers, orders, and coupons from WooCommerce manually or on an automated schedule.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ordercustomer-csv-export\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"914de15813a903c767b55445608bf290\",\"slug\":\"woocommerce-customer-order-csv-export\",\"id\":18652},{\"title\":\"AutomateWoo\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-AutomateWoo-Dark-1.png\",\"excerpt\":\"Powerful marketing automation for WooCommerce. AutomateWoo has the tools you need to grow your store and make more money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/automatewoo\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"ba9299b8-1dba-4aa0-a313-28bc1755cb88\",\"slug\":\"automatewoo\",\"id\":4652610},{\"title\":\"Advanced Notifications\",\"image\":\"\",\"excerpt\":\"Easily setup \\\"new order\\\" and stock email notifications for multiple recipients of your choosing.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/advanced-notifications\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"112372c44b002fea2640bd6bfafbca27\",\"slug\":\"woocommerce-advanced-notifications\",\"id\":18740},{\"title\":\"WooCommerce Points and Rewards\",\"image\":\"\",\"excerpt\":\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-points-and-rewards\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"1649b6cca5da8b923b01ca56b5cdd246\",\"slug\":\"woocommerce-points-and-rewards\",\"id\":210259},{\"title\":\"Smart Coupons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/10\\/wc-product-smart-coupons.png\",\"excerpt\":\"Everything you need for discounts, coupons, credits, gift cards, product giveaways, offers, and promotions. Most popular and complete coupons plugin for WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/smart-coupons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demo.storeapps.org\\/?demo=sc\",\"price\":\"&#36;99.00\",\"hash\":\"05c45f2aa466106a466de4402fff9dde\",\"slug\":\"woocommerce-smart-coupons\",\"id\":18729},{\"title\":\"WooCommerce Pre-Orders\",\"image\":\"\",\"excerpt\":\"Allow customers to order products before they are available.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-pre-orders\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"b2dc75e7d55e6f5bbfaccb59830f66b7\",\"slug\":\"woocommerce-pre-orders\",\"id\":178477},{\"title\":\"PayPal Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/10\\/PPCP-Tile-PayPal-Logo-and-Cart-Art-2x-2-uozwz8.jpg\",\"excerpt\":\"PayPal\'s latest, all-in-one checkout solution. Securely accept PayPal Digital Payments, credit\\/debit cards and local payment methods.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paypal-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"934115ab-e3f3-4435-9580-345b1ce21899\",\"slug\":\"woocommerce-paypal-payments\",\"id\":6410731},{\"title\":\"WooCommerce Subscription Downloads\",\"image\":\"\",\"excerpt\":\"Offer additional downloads to your subscribers, via downloadable products listed in your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscription-downloads\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5be9e21c13953253e4406d2a700382ec\",\"slug\":\"woocommerce-subscription-downloads\",\"id\":420458},{\"title\":\"WooCommerce Additional Variation Images\",\"image\":\"\",\"excerpt\":\"Add gallery images per variation on variable products within WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-additional-variation-images\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/storefront\\/product\\/woo-single-1\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c61dd6de57dcecb32bd7358866de4539\",\"slug\":\"woocommerce-additional-variation-images\",\"id\":477384},{\"title\":\"WooCommerce Zapier\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/woocommerce-zapier-logo.png\",\"excerpt\":\"Integrate your WooCommerce store with 3000+ cloud apps and services today. Trusted by 10,000+ users.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-zapier\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;59.00\",\"hash\":\"0782bdbe932c00f4978850268c6cfe40\",\"slug\":\"woocommerce-zapier\",\"id\":243589},{\"title\":\"WooCommerce Deposits\",\"image\":\"\",\"excerpt\":\"Enable customers to pay for products using a deposit or a payment plan.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-deposits\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;179.00\",\"hash\":\"de192a6cf12c4fd803248da5db700762\",\"slug\":\"woocommerce-deposits\",\"id\":977087},{\"title\":\"Amazon S3 Storage\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/amazon.png\",\"excerpt\":\"Serve digital products via Amazon S3\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-s3-storage\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"473bf6f221b865eff165c97881b473bb\",\"slug\":\"woocommerce-amazon-s3-storage\",\"id\":18663},{\"title\":\"Cart Add-ons\",\"image\":\"\",\"excerpt\":\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/cart-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"3a8ef25334396206f5da4cf208adeda3\",\"slug\":\"woocommerce-cart-add-ons\",\"id\":18717},{\"title\":\"Shipping Multiple Addresses\",\"image\":\"\",\"excerpt\":\"Allow your customers to ship individual items in a single order to multiple addresses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping-multiple-addresses\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa0eb6f777846d329952d5b891d6f8cc\",\"slug\":\"woocommerce-shipping-multiple-addresses\",\"id\":18741},{\"title\":\"Dynamic Pricing\",\"image\":\"\",\"excerpt\":\"Bulk discounts, role-based pricing and much more\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/dynamic-pricing\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"9a41775bb33843f52c93c922b0053986\",\"slug\":\"woocommerce-dynamic-pricing\",\"id\":18643},{\"title\":\"Name Your Price\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/09\\/nyp-icon-dark-v83owf.png\",\"excerpt\":\"Allow customers to define the product price. Also useful for accepting user-set donations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/name-your-price\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"31b4e11696cd99a3c0572975a84f1c08\",\"slug\":\"woocommerce-name-your-price\",\"id\":18738},{\"title\":\"TaxJar\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/10\\/taxjar-logotype.png\",\"excerpt\":\"Save hours every month by putting your sales tax on autopilot. Automated, multi-state sales tax calculation, reporting, and filing for your WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/taxjar\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"12072d8e-e933-4561-97b1-9db3c7eeed91\",\"slug\":\"taxjar-simplified-taxes-for-woocommerce\",\"id\":514914},{\"title\":\"Klarna Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Checkout_Black-1.png\",\"excerpt\":\"Klarna Checkout is a full checkout experience embedded on your site that includes all popular payment methods (Pay Now, Pay Later, Financing, Installments).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-checkout\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnacheckout\\/\",\"price\":\"&#36;0.00\",\"hash\":\"90f8ce584e785fcd8c2d739fd4f40d78\",\"slug\":\"klarna-checkout-for-woocommerce\",\"id\":2754152},{\"title\":\"Bulk Stock Management\",\"image\":\"\",\"excerpt\":\"Edit product and variation stock levels in bulk via this handy interface\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bulk-stock-management\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"02f4328d52f324ebe06a78eaaae7934f\",\"slug\":\"woocommerce-bulk-stock-management\",\"id\":18670},{\"title\":\"WooCommerce Print Invoices &amp; Packing lists\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/03\\/Thumbnail-Print-Invoices-Packing-lists-updated.png\",\"excerpt\":\"Generate invoices, packing slips, and pick lists for your WooCommerce orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/print-invoices-packing-lists\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"465de1126817cdfb42d97ebca7eea717\",\"slug\":\"woocommerce-pip\",\"id\":18666},{\"title\":\"WooCommerce Email Customizer\",\"image\":\"\",\"excerpt\":\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-email-customizer\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"bd909fa97874d431f203b5336c7e8873\",\"slug\":\"woocommerce-email-customizer\",\"id\":853277},{\"title\":\"Force Sells\",\"image\":\"\",\"excerpt\":\"Force products to be added to the cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/force-sells\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"3ebddfc491ca168a4ea4800b893302b0\",\"slug\":\"woocommerce-force-sells\",\"id\":18678},{\"title\":\"WooCommerce Quick View\",\"image\":\"\",\"excerpt\":\"Show a quick-view button to view product details and add to cart via lightbox popup\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-quick-view\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"619c6e57ce72c49c4b57e15b06eddb65\",\"slug\":\"woocommerce-quick-view\",\"id\":187509},{\"title\":\"WooCommerce Purchase Order Gateway\",\"image\":\"\",\"excerpt\":\"Receive purchase orders via your WooCommerce-powered online store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-purchase-order\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"573a92318244ece5facb449d63e74874\",\"slug\":\"woocommerce-gateway-purchase-order\",\"id\":478542},{\"title\":\"Returns and Warranty Requests\",\"image\":\"\",\"excerpt\":\"Manage the RMA process, add warranties to products &amp; let customers request &amp; manage returns \\/ exchanges from their account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/warranty-requests\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"9b4c41102e6b61ea5f558e16f9b63e25\",\"slug\":\"woocommerce-warranty\",\"id\":228315},{\"title\":\"WooCommerce AvaTax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-Avalara-updated.png\",\"excerpt\":\"Get 100% accurate sales tax calculations and on-time tax filing. No more tracking sales tax rates and rules.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-avatax\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"57077a4b28ba71cacf692bcf4a1a7f60\",\"slug\":\"woocommerce-avatax\",\"id\":1389326},{\"title\":\"WooCommerce Box Office\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-BO-Dark.png\",\"excerpt\":\"Sell tickets for your next event, concert, function, fundraiser or conference directly on your own site\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-box-office\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"e704c9160de318216a8fa657404b9131\",\"slug\":\"woocommerce-box-office\",\"id\":1628717},{\"title\":\"Product Enquiry Form\",\"image\":\"\",\"excerpt\":\"Allow visitors to contact you directly from the product details page via a reCAPTCHA protected form to enquire about a product.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-enquiry-form\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5a0f5d72519a8ffcc86669f042296937\",\"slug\":\"woocommerce-product-enquiry-form\",\"id\":18601},{\"title\":\"Google Product Feed\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2011\\/11\\/logo-regular-lscryp.png\",\"excerpt\":\"Feed rich product data to Google Merchant Center for setting up free product listings, product ads, and local inventory campaigns. Full control over your field mappings, and feed content so you can maximize campaign performance and ad spend.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-product-feed\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d55b4f852872025741312839f142447e\",\"slug\":\"woocommerce-product-feeds\",\"id\":18619},{\"title\":\"WooCommerce Order Barcodes\",\"image\":\"\",\"excerpt\":\"Generates a unique barcode for each order on your site - perfect for e-tickets, packing slips, reservations and a variety of other uses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-barcodes\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"889835bb29ee3400923653e1e44a3779\",\"slug\":\"woocommerce-order-barcodes\",\"id\":391708},{\"title\":\"WooCommerce 360\\u00ba Image\",\"image\":\"\",\"excerpt\":\"An easy way to add a dynamic, controllable 360\\u00ba image rotation to your WooCommerce site, by adding a group of images to a product\\u2019s gallery.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-360-image\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"24eb2cfa3738a66bf3b2587876668cd2\",\"slug\":\"woocommerce-360-image\",\"id\":512186},{\"title\":\"WooCommerce Photography\",\"image\":\"\",\"excerpt\":\"Sell photos in the blink of an eye using this simple as dragging &amp; dropping interface.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-photography\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ee76e8b9daf1d97ca4d3874cc9e35687\",\"slug\":\"woocommerce-photography\",\"id\":583602},{\"title\":\"WooCommerce Bookings Availability\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Aval-Dark.png\",\"excerpt\":\"Sell more bookings by presenting a calendar or schedule of available slots in a page or post.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bookings-availability\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"30770d2a-e392-4e82-baaa-76cfc7d02ae3\",\"slug\":\"woocommerce-bookings-availability\",\"id\":4228225},{\"title\":\"Software Add-on\",\"image\":\"\",\"excerpt\":\"Sell License Keys for Software\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/software-add-on\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"79f6dbfe1f1d3a56a86f0509b6d6b04b\",\"slug\":\"woocommerce-software-add-on\",\"id\":18683},{\"title\":\"WooCommerce Products Compare\",\"image\":\"\",\"excerpt\":\"WooCommerce Products Compare will allow your potential customers to easily compare products within your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-products-compare\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"c3ba0a4a3199a0cc7a6112eb24414548\",\"slug\":\"woocommerce-products-compare\",\"id\":853117},{\"title\":\"WooCommerce Store Catalog PDF Download\",\"image\":\"\",\"excerpt\":\"Offer your customers a PDF download of your product catalog, generated by WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-store-catalog-pdf-download\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"79ca7aadafe706364e2d738b7c1090c4\",\"slug\":\"woocommerce-store-catalog-pdf-download\",\"id\":675790},{\"title\":\"Gravity Forms Product Add-ons\",\"image\":\"\",\"excerpt\":\"Powerful product add-ons, Gravity style\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/gravity-forms-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/gravity-forms\\/\",\"price\":\"&#36;99.00\",\"hash\":\"a6ac0ab1a1536e3a357ccf24c0650ed0\",\"slug\":\"woocommerce-gravityforms-product-addons\",\"id\":18633},{\"title\":\"Composite Products\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CP.png?v=1\",\"excerpt\":\"Create product kit builders and custom product configurators using existing products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/composite-products\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"0343e0115bbcb97ccd98442b8326a0af\",\"slug\":\"woocommerce-composite-products\",\"id\":216836},{\"title\":\"Eway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/51456-Eway-logo-tagline-RGB-H-yellow-_-grey.png\",\"excerpt\":\"Take credit card payments securely via Eway (SG, MY, HK, AU, and NZ) keeping customers on your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eway\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2c497769d98d025e0d340cd0b5ea5da1\",\"slug\":\"woocommerce-gateway-eway\",\"id\":18604},{\"title\":\"WooCommerce Paid Courses\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2021\\/07\\/wc-paid-courses.png\",\"excerpt\":\"Sell your online courses using the most popular eCommerce platform on the web \\u2013 WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paid-courses\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"bad2a02a063555b7e2bee59924690763\",\"slug\":\"woothemes-sensei\",\"id\":152116},{\"title\":\"QuickBooks Sync for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/04\\/woocommerce-com-logo-1-hyhzbh.png\",\"excerpt\":\"Automatic two-way sync for orders, customers, products, inventory and more between WooCommerce and QuickBooks (Online, Desktop, or POS).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/quickbooks-sync-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c5e32e20-7c1f-4585-8b15-d930c2d842ac\",\"slug\":\"myworks-woo-sync-for-quickbooks-online\",\"id\":4065824},{\"title\":\"Catalog Visibility Options\",\"image\":\"\",\"excerpt\":\"Transform WooCommerce into an online catalog by removing eCommerce functionality\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/catalog-visibility-options\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"12e791110365fdbb5865c8658907967e\",\"slug\":\"woocommerce-catalog-visibility-options\",\"id\":18648},{\"title\":\"WooCommerce Blocks\",\"image\":\"\",\"excerpt\":\"WooCommerce Blocks offers a range of Gutenberg blocks you can use to build and customise your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gutenberg-products-block\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c2e9f13a-f90c-4ffe-a8a5-b432399ec263\",\"slug\":\"woo-gutenberg-products-block\",\"id\":3076677},{\"title\":\"Conditional Shipping and Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CSP.png?v=1\",\"excerpt\":\"Use conditional logic to restrict the shipping and payment options available on your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/conditional-shipping-and-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1f56ff002fa830b77017b0107505211a\",\"slug\":\"woocommerce-conditional-shipping-and-payments\",\"id\":680253},{\"title\":\"WooCommerce Order Status Manager\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/02\\/Thumbnail-Order-Status-Manager-updated.png\",\"excerpt\":\"Create, edit, and delete completely custom order statuses and integrate them seamlessly into your order management flow.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-manager\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"51fd9ab45394b4cad5a0ebf58d012342\",\"slug\":\"woocommerce-order-status-manager\",\"id\":588398},{\"title\":\"Sequential Order Numbers Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/05\\/Thumbnail-Sequential-Order-Numbers-Pro-updated.png\",\"excerpt\":\"Tame your order numbers! Upgrade from Sequential Order Numbers with advanced features and with optional prefixes\\/suffixes.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sequential-order-numbers-pro\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"0b18a2816e016ba9988b93b1cd8fe766\",\"slug\":\"woocommerce-sequential-order-numbers-pro\",\"id\":18688},{\"title\":\"Coupon Shortcodes\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/09\\/woocommerce-coupon-shortcodes-product-image-1870x960-1-vc5gux.png\",\"excerpt\":\"Show coupon discount info using shortcodes. Allows to render coupon information and content conditionally, based on the validity of coupons.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/coupon-shortcodes\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"ac5d9d51-70b2-4d8f-8b89-24200eea1394\",\"slug\":\"woocommerce-coupon-shortcodes\",\"id\":244762},{\"title\":\"WooCommerce Checkout Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/07\\/Thumbnail-Checkout-Add-Ons-updated.png\",\"excerpt\":\"Highlight relevant products, offers like free shipping and other up-sells during checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8fdca00b4000b7a8cc26371d0e470a8f\",\"slug\":\"woocommerce-checkout-add-ons\",\"id\":466854},{\"title\":\"WooCommerce Google Analytics Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-GAPro-updated.png\",\"excerpt\":\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics-pro\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d8aed8b7306b509eec1589e59abe319f\",\"slug\":\"woocommerce-google-analytics-pro\",\"id\":1312497},{\"title\":\"WooCommerce One Page Checkout\",\"image\":\"\",\"excerpt\":\"Create special pages where customers can choose products, checkout &amp; pay all on the one page.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-one-page-checkout\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"c9ba8f8352cd71b5508af5161268619a\",\"slug\":\"woocommerce-one-page-checkout\",\"id\":527886},{\"title\":\"WooCommerce Product Search\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2014\\/10\\/woocommerce-product-search-product-image-1870x960-1-jvsljj.png\",\"excerpt\":\"The perfect search engine helps customers to find and buy products quickly \\u2013 essential for every WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-product-search\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.itthinx.com\\/wps\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c84cc8ca16ddac3408e6b6c5871133a8\",\"slug\":\"woocommerce-product-search\",\"id\":512174},{\"title\":\"First Data\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-FirstData-updated.png\",\"excerpt\":\"Accept payment with First Data using the Payeezy Gateway, Payeezy, or legacy Global Gateway payment gateways.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/firstdata\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb3e32663ec0810592eaf0d097796230\",\"slug\":\"woocommerce-gateway-firstdata\",\"id\":18645},{\"title\":\"Klarna Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Payments_Pink.png\",\"excerpt\":\"With Klarna Payments\\u00a0you can choose the payment that you want, Pay Now, Pay Later or Slice It. No credit card numbers, no passwords, no worries.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnapayments\\/\",\"price\":\"&#36;0.00\",\"hash\":\"a19c689325bc8ea63c620765dd54b33a\",\"slug\":\"klarna-payments-for-woocommerce\",\"id\":2754217},{\"title\":\"WooSlider\",\"image\":\"\",\"excerpt\":\"WooSlider is the ultimate responsive slideshow WordPress slider plugin\\r\\n\\r\\n\\u00a0\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/wooslider\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/www.wooslider.com\\/\",\"price\":\"&#36;49.00\",\"hash\":\"209d98f3ccde6cc3de7e8732a2b20b6a\",\"slug\":\"wooslider\",\"id\":46506},{\"title\":\"WooCommerce Order Status Control\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/06\\/Thumbnail-Order-Status-Control-updated.png\",\"excerpt\":\"Use this extension to automatically change the order status to \\\"completed\\\" after successful payment.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-control\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"32400e509c7c36dcc1cd368e8267d981\",\"slug\":\"woocommerce-order-status-control\",\"id\":439037},{\"title\":\"HubSpot for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/04\\/hubspotlogo-web-color-pxebeq.png\",\"excerpt\":\"Connect your WooCommerce store to HubSpot. Sync, automate &amp; analyze data with HubSpot WooCommerce Integration\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/hubspot-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e50acec8-3a6c-454c-8562-2da4898fa6c1\",\"slug\":\"hubspot-for-woocommerce\",\"id\":5785079},{\"title\":\"WooCommerce Social Login\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/08\\/Thumbnail-Social-Login-updated.png\",\"excerpt\":\"Enable Social Login for seamless checkout and account creation.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-social-login\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demos.skyverge.com\\/woocommerce-social-login\\/\",\"price\":\"&#36;79.00\",\"hash\":\"b231cd6367a79cc8a53b7d992d77525d\",\"slug\":\"woocommerce-social-login\",\"id\":473617},{\"title\":\"Variation Swatches and Photos\",\"image\":\"\",\"excerpt\":\"Show color and image swatches instead of dropdowns for variable products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/variation-swatches-and-photos\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/swatches-and-photos\\/\",\"price\":\"&#36;99.00\",\"hash\":\"37bea8d549df279c8278878d081b062f\",\"slug\":\"woocommerce-variation-swatches-and-photos\",\"id\":18697},{\"title\":\"Opayo Payment Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/Opayo_logo_RGB.png\",\"excerpt\":\"Take payments on your WooCommerce store via Opayo (formally SagePay).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sage-pay-form\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6bc0cca47d0274d8ef9b164f6fbec1cc\",\"slug\":\"woocommerce-gateway-sagepay-form\",\"id\":18599},{\"title\":\"Viva Wallet for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/04\\/Viva-Wallet-logo.png?w=374\",\"excerpt\":\"Integrate the Viva Wallet payment gateway with your WooCommerce store to process and sync your payments and help you sell more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/viva-wallet-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"7240a329-047f-4d8b-b7ec-ee3defd798bd\",\"slug\":\"viva-wallet-for-woocommerce\",\"id\":6137160},{\"title\":\"EU VAT Number\",\"image\":\"\",\"excerpt\":\"Collect VAT numbers at checkout and remove the VAT charge for eligible EU businesses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eu-vat-number\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"d2720c4b4bb8d6908e530355b7a2d734\",\"slug\":\"woocommerce-eu-vat-number\",\"id\":18592}]}\";s:3:\"raw\";s:50233:\"HTTP/1.1 200 OK\r\nServer: nginx\r\nDate: Sun, 05 Sep 2021 06:57:11 GMT\r\nContent-Type: application/json; charset=UTF-8\r\nContent-Length: 11752\r\nConnection: close\r\nX-Robots-Tag: noindex\r\nLink: <https://woocommerce.com/wp-json/>; rel=\"https://api.w.org/\"\r\nX-Content-Type-Options: nosniff\r\nAccess-Control-Expose-Headers: X-WP-Total, X-WP-TotalPages, Link\r\nAccess-Control-Allow-Headers: Authorization, X-WP-Nonce, Content-Disposition, Content-MD5, Content-Type\r\nX-WCCOM-Cache: HIT\r\nCache-Control: max-age=60\r\nAllow: GET\r\nContent-Encoding: gzip\r\nX-rq: vie1 0 4 9980\r\nAge: 21\r\nX-Cache: hit\r\nVary: Accept-Encoding, Origin\r\nAccept-Ranges: bytes\r\n\r\n{\"products\":[{\"title\":\"WooCommerce Google Analytics\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/GA-Dark.png\",\"excerpt\":\"Understand your customers and increase revenue with world\\u2019s leading analytics platform - integrated with WooCommerce for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2d21f7de14dfb8e9885a4622be701ddf\",\"slug\":\"woocommerce-google-analytics-integration\",\"id\":1442927},{\"title\":\"WooCommerce Tax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Tax-Dark.png\",\"excerpt\":\"Automatically calculate how much sales tax should be collected for WooCommerce orders - by city, country, or state - at checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/tax\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":3220291},{\"title\":\"Stripe\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Stripe-Dark-1.png\",\"excerpt\":\"Accept all major debit and credit cards as well as local payment methods with Stripe.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/stripe\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"50bb7a985c691bb943a9da4d2c8b5efd\",\"slug\":\"woocommerce-gateway-stripe\",\"id\":18627},{\"title\":\"Jetpack\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Jetpack-Dark.png\",\"excerpt\":\"Security, performance, and marketing tools made for WooCommerce stores by the WordPress experts. Get started with basic security and speed tools for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jetpack\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"d5bfef9700b62b2b132c74c74c3193eb\",\"slug\":\"jetpack\",\"id\":2725249},{\"title\":\"Facebook for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Facebook-Dark.png\",\"excerpt\":\"Get the Official Facebook for WooCommerce plugin for three powerful ways to help grow your business.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/facebook\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"0ea4fe4c2d7ca6338f8a322fb3e4e187\",\"slug\":\"facebook-for-woocommerce\",\"id\":2127297},{\"title\":\"WooCommerce Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Pay-Dark.png\",\"excerpt\":\"Securely accept payments, track cash flow, and manage recurring revenue from your dashboard \\u2014 all without setup costs or monthly fees.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"8c6319ca-8f41-4e69-be63-6b15ee37773b\",\"slug\":\"woocommerce-payments\",\"id\":5278104},{\"title\":\"Amazon Pay\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Amazon-Pay-Dark.png\",\"excerpt\":\"Amazon Pay is embedded in your WooCommerce store. Transactions take place via\\u00a0Amazon widgets, so the buyer never leaves your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/pay-with-amazon\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9865e043bbbe4f8c9735af31cb509b53\",\"slug\":\"woocommerce-gateway-amazon-payments-advanced\",\"id\":238816},{\"title\":\"Square for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Square-Dark.png\",\"excerpt\":\"Accepting payments is easy with Square. Clear rates, fast deposits (1-2 business days). Sell online and in person, and sync all payments, items and inventory.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/square\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e907be8b86d7df0c8f8e0d0020b52638\",\"slug\":\"woocommerce-square\",\"id\":1770503},{\"title\":\"WooCommerce Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Dark-1.png\",\"excerpt\":\"Print USPS and DHL labels right from your WooCommerce dashboard and instantly save up to 90%. WooCommerce Shipping is free to use and saves you time and money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":2165910},{\"title\":\"WooCommerce Subscriptions\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Subscriptions-Dark.png\",\"excerpt\":\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscriptions\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"6115e6d7e297b623a169fdcf5728b224\",\"slug\":\"woocommerce-subscriptions\",\"id\":27147},{\"title\":\"Mailchimp for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/09\\/logo-mailchimp-dark-v2.png\",\"excerpt\":\"Increase traffic, drive repeat purchases, and personalize your marketing when you connect to Mailchimp.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/mailchimp-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b4481616ebece8b1ff68fc59b90c1a91\",\"slug\":\"mailchimp-for-woocommerce\",\"id\":2545166},{\"title\":\"Product Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-Add-Ons-Dark.png\",\"excerpt\":\"Offer add-ons like gift wrapping, special messages or other special options for your products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"147d0077e591e16db9d0d67daeb8c484\",\"slug\":\"woocommerce-product-addons\",\"id\":18618},{\"title\":\"ShipStation Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Shipstation-Dark.png\",\"excerpt\":\"Fulfill all your Woo orders (and wherever else you sell) quickly and easily using ShipStation. Try it free for 30 days today!\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipstation-integration\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9de8640767ba64237808ed7f245a49bb\",\"slug\":\"woocommerce-shipstation-integration\",\"id\":18734},{\"title\":\"PayFast Payment Gateway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Payfast-Dark-1.png\",\"excerpt\":\"Take payments on your WooCommerce store via PayFast (redirect method).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/payfast-payment-gateway\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"557bf07293ad916f20c207c6c9cd15ff\",\"slug\":\"woocommerce-payfast-gateway\",\"id\":18596},{\"title\":\"USPS Shipping Method\",\"image\":\"\",\"excerpt\":\"Get shipping rates from the USPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/usps-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"83d1524e8f5f1913e58889f83d442c32\",\"slug\":\"woocommerce-shipping-usps\",\"id\":18657},{\"title\":\"Google Listings &#038; Ads\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2021\\/06\\/marketplace-card.png\",\"excerpt\":\"Reach millions of engaged shoppers across Google with free product listings and ads. Built in partnership with Google.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-listings-and-ads\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"118f4d86-f126-4c3a-8525-644e3170d161\",\"slug\":\"google-listings-and-ads\",\"id\":7623964},{\"title\":\"Google Ads &amp; Marketing by Kliken\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/02\\/GA-for-Woo-Logo-374x192px-qu3duk.png\",\"excerpt\":\"Get in front of shoppers and drive traffic to your store so you can grow your business with Smart Shopping Campaigns and free listings.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-ads-and-marketing\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"bf66e173-a220-4da7-9512-b5728c20fc16\",\"slug\":\"kliken-marketing-for-google\",\"id\":3866145},{\"title\":\"UPS Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/UPS-Shipping-Method-Dark.png\",\"excerpt\":\"Get shipping rates from the UPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ups-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8dae58502913bac0fbcdcaba515ea998\",\"slug\":\"woocommerce-shipping-ups\",\"id\":18665},{\"title\":\"Shipment Tracking\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Tracking-Dark-1.png\",\"excerpt\":\"Add shipment tracking information to your orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipment-tracking\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"1968e199038a8a001c9f9966fd06bf88\",\"slug\":\"woocommerce-shipment-tracking\",\"id\":18693},{\"title\":\"Table Rate Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Product-Table-Rate-Shipping-Dark.png\",\"excerpt\":\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/table-rate-shipping\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"3034ed8aff427b0f635fe4c86bbf008a\",\"slug\":\"woocommerce-table-rate-shipping\",\"id\":18718},{\"title\":\"Checkout Field Editor\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Checkout-Field-Editor-Dark.png\",\"excerpt\":\"Optimize your checkout process by adding, removing or editing fields to suit your needs.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-field-editor\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"2b8029f0d7cdd1118f4d843eb3ab43ff\",\"slug\":\"woocommerce-checkout-field-editor\",\"id\":184594},{\"title\":\"WooCommerce Bookings\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Dark.png\",\"excerpt\":\"Allow customers to book appointments, make reservations or rent equipment without leaving your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-bookings\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/hotel\\/\",\"price\":\"&#36;249.00\",\"hash\":\"911c438934af094c2b38d5560b9f50f3\",\"slug\":\"WooCommerce Bookings\",\"id\":390890},{\"title\":\"Braintree for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/02\\/braintree-black-copy.png\",\"excerpt\":\"Accept PayPal, credit cards and debit cards with a single payment gateway solution \\u2014 PayPal Powered by Braintree.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-powered-by-braintree\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"27f010c8e34ca65b205ddec88ad14536\",\"slug\":\"woocommerce-gateway-paypal-powered-by-braintree\",\"id\":1489837},{\"title\":\"WooCommerce Memberships\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/06\\/Thumbnail-Memberships-updated.png\",\"excerpt\":\"Power your membership association, online magazine, elearning sites, and more with access control to content\\/products and member discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-memberships\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"9288e7609ad0b487b81ef6232efa5cfc\",\"slug\":\"woocommerce-memberships\",\"id\":958589},{\"title\":\"Min\\/Max Quantities\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Min-Max-Qua-Dark.png\",\"excerpt\":\"Specify minimum and maximum allowed product quantities for orders to be completed.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/minmax-quantities\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"2b5188d90baecfb781a5aa2d6abb900a\",\"slug\":\"woocommerce-min-max-quantities\",\"id\":18616},{\"title\":\"Product Bundles\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-PB.png?v=1\",\"excerpt\":\"Offer personalized product bundles, bulk discount packages, and assembled\\u00a0products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-bundles\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa2518b5-ab19-4b75-bde9-60ca51e20f28\",\"slug\":\"woocommerce-product-bundles\",\"id\":18716},{\"title\":\"Multichannel for WooCommerce: Google, Amazon, eBay &#038; Walmart Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/10\\/Woo-Extension-Store-Logo-v2.png\",\"excerpt\":\"Get the official Google, Amazon, eBay and Walmart extension and create, sync and manage multichannel listings directly from WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-ebay-integration\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e4000666-9275-4c71-8619-be61fb41c9f9\",\"slug\":\"woocommerce-amazon-ebay-integration\",\"id\":3545890},{\"title\":\"FedEx Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/01\\/FedEx_Logo_Wallpaper.jpeg\",\"excerpt\":\"Get shipping rates from the FedEx API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/fedex-shipping-module\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1a48b598b47a81559baadef15e320f64\",\"slug\":\"woocommerce-shipping-fedex\",\"id\":18620},{\"title\":\"Product CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-CSV-Import-Dark.png\",\"excerpt\":\"Import, merge, and export products and variations to and from WooCommerce using a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-csv-import-suite\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"7ac9b00a1fe980fb61d28ab54d167d0d\",\"slug\":\"woocommerce-product-csv-import-suite\",\"id\":18680},{\"title\":\"Australia Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/australia-post.gif\",\"excerpt\":\"Get shipping rates for your WooCommerce store from the Australia Post API, which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/australia-post-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1dbd4dc6bd91a9cda1bd6b9e7a5e4f43\",\"slug\":\"woocommerce-shipping-australia-post\",\"id\":18622},{\"title\":\"Canada Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/canada-post.png\",\"excerpt\":\"Get shipping rates from the Canada Post Ratings API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/canada-post-shipping-method\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ac029cdf3daba20b20c7b9be7dc00e0e\",\"slug\":\"woocommerce-shipping-canada-post\",\"id\":18623},{\"title\":\"Follow-Ups\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Follow-Ups-Dark.png\",\"excerpt\":\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/follow-up-emails\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"05ece68fe94558e65278fe54d9ec84d2\",\"slug\":\"woocommerce-follow-up-emails\",\"id\":18686},{\"title\":\"LiveChat for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2015\\/11\\/LC_woo_regular-zmiaym.png\",\"excerpt\":\"Live Chat and messaging platform for sales and support -- increase average order value and overall sales through live conversations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/livechat\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.livechat.com\\/livechat-for-ecommerce\\/?a=woocommerce&amp;utm_source=woocommerce.com&amp;utm_medium=integration&amp;utm_campaign=woocommerce.com\",\"price\":\"&#36;0.00\",\"hash\":\"5344cc1f-ed4a-4d00-beff-9d67f6d372f3\",\"slug\":\"livechat-woocommerce\",\"id\":1348888},{\"title\":\"Authorize.Net\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2013\\/04\\/Thumbnail-Authorize.net-updated.png\",\"excerpt\":\"Authorize.Net gateway with support for pre-orders and subscriptions.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/authorize-net\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8b61524fe53add7fdd1a8d1b00b9327d\",\"slug\":\"woocommerce-gateway-authorize-net-cim\",\"id\":178481},{\"title\":\"Royal Mail\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/04\\/royalmail.png\",\"excerpt\":\"Offer Royal Mail shipping rates to your customers\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/royal-mail\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"03839cca1a16c4488fcb669aeb91a056\",\"slug\":\"woocommerce-shipping-royalmail\",\"id\":182719},{\"title\":\"Product Vendors\",\"image\":\"\",\"excerpt\":\"Turn your store into a multi-vendor marketplace\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-vendors\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"a97d99fccd651bbdd728f4d67d492c31\",\"slug\":\"woocommerce-product-vendors\",\"id\":219982},{\"title\":\"WooCommerce Accommodation Bookings\",\"image\":\"\",\"excerpt\":\"Book accommodation using WooCommerce and the WooCommerce Bookings extension.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-accommodation-bookings\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"99b2a7a4af90b6cefd2a733b3b1f78e7\",\"slug\":\"woocommerce-accommodation-bookings\",\"id\":1412069},{\"title\":\"Xero\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/08\\/xero2.png\",\"excerpt\":\"Save time with automated sync between WooCommerce and your Xero account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/xero\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"f0dd29d338d3c67cf6cee88eddf6869b\",\"slug\":\"woocommerce-xero\",\"id\":18733},{\"title\":\"WooCommerce Brands\",\"image\":\"\",\"excerpt\":\"Create, assign and list brands for products, and allow customers to view by brand.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/brands\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"8a88c7cbd2f1e73636c331c7a86f818c\",\"slug\":\"woocommerce-brands\",\"id\":18737},{\"title\":\"WooCommerce Customer \\/ Order \\/ Coupon Export\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-Customer-Order-Coupon-Export-updated.png\",\"excerpt\":\"Export customers, orders, and coupons from WooCommerce manually or on an automated schedule.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ordercustomer-csv-export\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"914de15813a903c767b55445608bf290\",\"slug\":\"woocommerce-customer-order-csv-export\",\"id\":18652},{\"title\":\"AutomateWoo\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-AutomateWoo-Dark-1.png\",\"excerpt\":\"Powerful marketing automation for WooCommerce. AutomateWoo has the tools you need to grow your store and make more money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/automatewoo\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"ba9299b8-1dba-4aa0-a313-28bc1755cb88\",\"slug\":\"automatewoo\",\"id\":4652610},{\"title\":\"Advanced Notifications\",\"image\":\"\",\"excerpt\":\"Easily setup \\\"new order\\\" and stock email notifications for multiple recipients of your choosing.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/advanced-notifications\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"112372c44b002fea2640bd6bfafbca27\",\"slug\":\"woocommerce-advanced-notifications\",\"id\":18740},{\"title\":\"WooCommerce Points and Rewards\",\"image\":\"\",\"excerpt\":\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-points-and-rewards\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"1649b6cca5da8b923b01ca56b5cdd246\",\"slug\":\"woocommerce-points-and-rewards\",\"id\":210259},{\"title\":\"Smart Coupons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/10\\/wc-product-smart-coupons.png\",\"excerpt\":\"Everything you need for discounts, coupons, credits, gift cards, product giveaways, offers, and promotions. Most popular and complete coupons plugin for WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/smart-coupons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demo.storeapps.org\\/?demo=sc\",\"price\":\"&#36;99.00\",\"hash\":\"05c45f2aa466106a466de4402fff9dde\",\"slug\":\"woocommerce-smart-coupons\",\"id\":18729},{\"title\":\"WooCommerce Pre-Orders\",\"image\":\"\",\"excerpt\":\"Allow customers to order products before they are available.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-pre-orders\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"b2dc75e7d55e6f5bbfaccb59830f66b7\",\"slug\":\"woocommerce-pre-orders\",\"id\":178477},{\"title\":\"PayPal Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/10\\/PPCP-Tile-PayPal-Logo-and-Cart-Art-2x-2-uozwz8.jpg\",\"excerpt\":\"PayPal\'s latest, all-in-one checkout solution. Securely accept PayPal Digital Payments, credit\\/debit cards and local payment methods.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paypal-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"934115ab-e3f3-4435-9580-345b1ce21899\",\"slug\":\"woocommerce-paypal-payments\",\"id\":6410731},{\"title\":\"WooCommerce Subscription Downloads\",\"image\":\"\",\"excerpt\":\"Offer additional downloads to your subscribers, via downloadable products listed in your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscription-downloads\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5be9e21c13953253e4406d2a700382ec\",\"slug\":\"woocommerce-subscription-downloads\",\"id\":420458},{\"title\":\"WooCommerce Additional Variation Images\",\"image\":\"\",\"excerpt\":\"Add gallery images per variation on variable products within WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-additional-variation-images\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/storefront\\/product\\/woo-single-1\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c61dd6de57dcecb32bd7358866de4539\",\"slug\":\"woocommerce-additional-variation-images\",\"id\":477384},{\"title\":\"WooCommerce Zapier\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/woocommerce-zapier-logo.png\",\"excerpt\":\"Integrate your WooCommerce store with 3000+ cloud apps and services today. Trusted by 10,000+ users.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-zapier\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;59.00\",\"hash\":\"0782bdbe932c00f4978850268c6cfe40\",\"slug\":\"woocommerce-zapier\",\"id\":243589},{\"title\":\"WooCommerce Deposits\",\"image\":\"\",\"excerpt\":\"Enable customers to pay for products using a deposit or a payment plan.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-deposits\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;179.00\",\"hash\":\"de192a6cf12c4fd803248da5db700762\",\"slug\":\"woocommerce-deposits\",\"id\":977087},{\"title\":\"Amazon S3 Storage\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/amazon.png\",\"excerpt\":\"Serve digital products via Amazon S3\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-s3-storage\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"473bf6f221b865eff165c97881b473bb\",\"slug\":\"woocommerce-amazon-s3-storage\",\"id\":18663},{\"title\":\"Cart Add-ons\",\"image\":\"\",\"excerpt\":\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/cart-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"3a8ef25334396206f5da4cf208adeda3\",\"slug\":\"woocommerce-cart-add-ons\",\"id\":18717},{\"title\":\"Shipping Multiple Addresses\",\"image\":\"\",\"excerpt\":\"Allow your customers to ship individual items in a single order to multiple addresses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping-multiple-addresses\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa0eb6f777846d329952d5b891d6f8cc\",\"slug\":\"woocommerce-shipping-multiple-addresses\",\"id\":18741},{\"title\":\"Dynamic Pricing\",\"image\":\"\",\"excerpt\":\"Bulk discounts, role-based pricing and much more\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/dynamic-pricing\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"9a41775bb33843f52c93c922b0053986\",\"slug\":\"woocommerce-dynamic-pricing\",\"id\":18643},{\"title\":\"Name Your Price\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/09\\/nyp-icon-dark-v83owf.png\",\"excerpt\":\"Allow customers to define the product price. Also useful for accepting user-set donations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/name-your-price\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"31b4e11696cd99a3c0572975a84f1c08\",\"slug\":\"woocommerce-name-your-price\",\"id\":18738},{\"title\":\"TaxJar\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/10\\/taxjar-logotype.png\",\"excerpt\":\"Save hours every month by putting your sales tax on autopilot. Automated, multi-state sales tax calculation, reporting, and filing for your WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/taxjar\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"12072d8e-e933-4561-97b1-9db3c7eeed91\",\"slug\":\"taxjar-simplified-taxes-for-woocommerce\",\"id\":514914},{\"title\":\"Klarna Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Checkout_Black-1.png\",\"excerpt\":\"Klarna Checkout is a full checkout experience embedded on your site that includes all popular payment methods (Pay Now, Pay Later, Financing, Installments).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-checkout\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnacheckout\\/\",\"price\":\"&#36;0.00\",\"hash\":\"90f8ce584e785fcd8c2d739fd4f40d78\",\"slug\":\"klarna-checkout-for-woocommerce\",\"id\":2754152},{\"title\":\"Bulk Stock Management\",\"image\":\"\",\"excerpt\":\"Edit product and variation stock levels in bulk via this handy interface\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bulk-stock-management\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"02f4328d52f324ebe06a78eaaae7934f\",\"slug\":\"woocommerce-bulk-stock-management\",\"id\":18670},{\"title\":\"WooCommerce Print Invoices &amp; Packing lists\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/03\\/Thumbnail-Print-Invoices-Packing-lists-updated.png\",\"excerpt\":\"Generate invoices, packing slips, and pick lists for your WooCommerce orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/print-invoices-packing-lists\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"465de1126817cdfb42d97ebca7eea717\",\"slug\":\"woocommerce-pip\",\"id\":18666},{\"title\":\"WooCommerce Email Customizer\",\"image\":\"\",\"excerpt\":\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-email-customizer\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"bd909fa97874d431f203b5336c7e8873\",\"slug\":\"woocommerce-email-customizer\",\"id\":853277},{\"title\":\"Force Sells\",\"image\":\"\",\"excerpt\":\"Force products to be added to the cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/force-sells\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"3ebddfc491ca168a4ea4800b893302b0\",\"slug\":\"woocommerce-force-sells\",\"id\":18678},{\"title\":\"WooCommerce Quick View\",\"image\":\"\",\"excerpt\":\"Show a quick-view button to view product details and add to cart via lightbox popup\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-quick-view\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"619c6e57ce72c49c4b57e15b06eddb65\",\"slug\":\"woocommerce-quick-view\",\"id\":187509},{\"title\":\"WooCommerce Purchase Order Gateway\",\"image\":\"\",\"excerpt\":\"Receive purchase orders via your WooCommerce-powered online store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-purchase-order\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"573a92318244ece5facb449d63e74874\",\"slug\":\"woocommerce-gateway-purchase-order\",\"id\":478542},{\"title\":\"Returns and Warranty Requests\",\"image\":\"\",\"excerpt\":\"Manage the RMA process, add warranties to products &amp; let customers request &amp; manage returns \\/ exchanges from their account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/warranty-requests\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"9b4c41102e6b61ea5f558e16f9b63e25\",\"slug\":\"woocommerce-warranty\",\"id\":228315},{\"title\":\"WooCommerce AvaTax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-Avalara-updated.png\",\"excerpt\":\"Get 100% accurate sales tax calculations and on-time tax filing. No more tracking sales tax rates and rules.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-avatax\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"57077a4b28ba71cacf692bcf4a1a7f60\",\"slug\":\"woocommerce-avatax\",\"id\":1389326},{\"title\":\"WooCommerce Box Office\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-BO-Dark.png\",\"excerpt\":\"Sell tickets for your next event, concert, function, fundraiser or conference directly on your own site\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-box-office\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"e704c9160de318216a8fa657404b9131\",\"slug\":\"woocommerce-box-office\",\"id\":1628717},{\"title\":\"Product Enquiry Form\",\"image\":\"\",\"excerpt\":\"Allow visitors to contact you directly from the product details page via a reCAPTCHA protected form to enquire about a product.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-enquiry-form\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5a0f5d72519a8ffcc86669f042296937\",\"slug\":\"woocommerce-product-enquiry-form\",\"id\":18601},{\"title\":\"Google Product Feed\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2011\\/11\\/logo-regular-lscryp.png\",\"excerpt\":\"Feed rich product data to Google Merchant Center for setting up free product listings, product ads, and local inventory campaigns. Full control over your field mappings, and feed content so you can maximize campaign performance and ad spend.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-product-feed\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d55b4f852872025741312839f142447e\",\"slug\":\"woocommerce-product-feeds\",\"id\":18619},{\"title\":\"WooCommerce Order Barcodes\",\"image\":\"\",\"excerpt\":\"Generates a unique barcode for each order on your site - perfect for e-tickets, packing slips, reservations and a variety of other uses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-barcodes\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"889835bb29ee3400923653e1e44a3779\",\"slug\":\"woocommerce-order-barcodes\",\"id\":391708},{\"title\":\"WooCommerce 360\\u00ba Image\",\"image\":\"\",\"excerpt\":\"An easy way to add a dynamic, controllable 360\\u00ba image rotation to your WooCommerce site, by adding a group of images to a product\\u2019s gallery.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-360-image\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"24eb2cfa3738a66bf3b2587876668cd2\",\"slug\":\"woocommerce-360-image\",\"id\":512186},{\"title\":\"WooCommerce Photography\",\"image\":\"\",\"excerpt\":\"Sell photos in the blink of an eye using this simple as dragging &amp; dropping interface.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-photography\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ee76e8b9daf1d97ca4d3874cc9e35687\",\"slug\":\"woocommerce-photography\",\"id\":583602},{\"title\":\"WooCommerce Bookings Availability\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Aval-Dark.png\",\"excerpt\":\"Sell more bookings by presenting a calendar or schedule of available slots in a page or post.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bookings-availability\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"30770d2a-e392-4e82-baaa-76cfc7d02ae3\",\"slug\":\"woocommerce-bookings-availability\",\"id\":4228225},{\"title\":\"Software Add-on\",\"image\":\"\",\"excerpt\":\"Sell License Keys for Software\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/software-add-on\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"79f6dbfe1f1d3a56a86f0509b6d6b04b\",\"slug\":\"woocommerce-software-add-on\",\"id\":18683},{\"title\":\"WooCommerce Products Compare\",\"image\":\"\",\"excerpt\":\"WooCommerce Products Compare will allow your potential customers to easily compare products within your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-products-compare\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"c3ba0a4a3199a0cc7a6112eb24414548\",\"slug\":\"woocommerce-products-compare\",\"id\":853117},{\"title\":\"WooCommerce Store Catalog PDF Download\",\"image\":\"\",\"excerpt\":\"Offer your customers a PDF download of your product catalog, generated by WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-store-catalog-pdf-download\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"79ca7aadafe706364e2d738b7c1090c4\",\"slug\":\"woocommerce-store-catalog-pdf-download\",\"id\":675790},{\"title\":\"Gravity Forms Product Add-ons\",\"image\":\"\",\"excerpt\":\"Powerful product add-ons, Gravity style\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/gravity-forms-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/gravity-forms\\/\",\"price\":\"&#36;99.00\",\"hash\":\"a6ac0ab1a1536e3a357ccf24c0650ed0\",\"slug\":\"woocommerce-gravityforms-product-addons\",\"id\":18633},{\"title\":\"Composite Products\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CP.png?v=1\",\"excerpt\":\"Create product kit builders and custom product configurators using existing products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/composite-products\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"0343e0115bbcb97ccd98442b8326a0af\",\"slug\":\"woocommerce-composite-products\",\"id\":216836},{\"title\":\"Eway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/51456-Eway-logo-tagline-RGB-H-yellow-_-grey.png\",\"excerpt\":\"Take credit card payments securely via Eway (SG, MY, HK, AU, and NZ) keeping customers on your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eway\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2c497769d98d025e0d340cd0b5ea5da1\",\"slug\":\"woocommerce-gateway-eway\",\"id\":18604},{\"title\":\"WooCommerce Paid Courses\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2021\\/07\\/wc-paid-courses.png\",\"excerpt\":\"Sell your online courses using the most popular eCommerce platform on the web \\u2013 WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paid-courses\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"bad2a02a063555b7e2bee59924690763\",\"slug\":\"woothemes-sensei\",\"id\":152116},{\"title\":\"QuickBooks Sync for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/04\\/woocommerce-com-logo-1-hyhzbh.png\",\"excerpt\":\"Automatic two-way sync for orders, customers, products, inventory and more between WooCommerce and QuickBooks (Online, Desktop, or POS).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/quickbooks-sync-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c5e32e20-7c1f-4585-8b15-d930c2d842ac\",\"slug\":\"myworks-woo-sync-for-quickbooks-online\",\"id\":4065824},{\"title\":\"Catalog Visibility Options\",\"image\":\"\",\"excerpt\":\"Transform WooCommerce into an online catalog by removing eCommerce functionality\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/catalog-visibility-options\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"12e791110365fdbb5865c8658907967e\",\"slug\":\"woocommerce-catalog-visibility-options\",\"id\":18648},{\"title\":\"WooCommerce Blocks\",\"image\":\"\",\"excerpt\":\"WooCommerce Blocks offers a range of Gutenberg blocks you can use to build and customise your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gutenberg-products-block\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c2e9f13a-f90c-4ffe-a8a5-b432399ec263\",\"slug\":\"woo-gutenberg-products-block\",\"id\":3076677},{\"title\":\"Conditional Shipping and Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CSP.png?v=1\",\"excerpt\":\"Use conditional logic to restrict the shipping and payment options available on your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/conditional-shipping-and-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1f56ff002fa830b77017b0107505211a\",\"slug\":\"woocommerce-conditional-shipping-and-payments\",\"id\":680253},{\"title\":\"WooCommerce Order Status Manager\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/02\\/Thumbnail-Order-Status-Manager-updated.png\",\"excerpt\":\"Create, edit, and delete completely custom order statuses and integrate them seamlessly into your order management flow.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-manager\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"51fd9ab45394b4cad5a0ebf58d012342\",\"slug\":\"woocommerce-order-status-manager\",\"id\":588398},{\"title\":\"Sequential Order Numbers Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/05\\/Thumbnail-Sequential-Order-Numbers-Pro-updated.png\",\"excerpt\":\"Tame your order numbers! Upgrade from Sequential Order Numbers with advanced features and with optional prefixes\\/suffixes.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sequential-order-numbers-pro\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"0b18a2816e016ba9988b93b1cd8fe766\",\"slug\":\"woocommerce-sequential-order-numbers-pro\",\"id\":18688},{\"title\":\"Coupon Shortcodes\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/09\\/woocommerce-coupon-shortcodes-product-image-1870x960-1-vc5gux.png\",\"excerpt\":\"Show coupon discount info using shortcodes. Allows to render coupon information and content conditionally, based on the validity of coupons.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/coupon-shortcodes\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"ac5d9d51-70b2-4d8f-8b89-24200eea1394\",\"slug\":\"woocommerce-coupon-shortcodes\",\"id\":244762},{\"title\":\"WooCommerce Checkout Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/07\\/Thumbnail-Checkout-Add-Ons-updated.png\",\"excerpt\":\"Highlight relevant products, offers like free shipping and other up-sells during checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-add-ons\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8fdca00b4000b7a8cc26371d0e470a8f\",\"slug\":\"woocommerce-checkout-add-ons\",\"id\":466854},{\"title\":\"WooCommerce Google Analytics Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-GAPro-updated.png\",\"excerpt\":\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics-pro\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d8aed8b7306b509eec1589e59abe319f\",\"slug\":\"woocommerce-google-analytics-pro\",\"id\":1312497},{\"title\":\"WooCommerce One Page Checkout\",\"image\":\"\",\"excerpt\":\"Create special pages where customers can choose products, checkout &amp; pay all on the one page.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-one-page-checkout\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"c9ba8f8352cd71b5508af5161268619a\",\"slug\":\"woocommerce-one-page-checkout\",\"id\":527886},{\"title\":\"WooCommerce Product Search\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2014\\/10\\/woocommerce-product-search-product-image-1870x960-1-jvsljj.png\",\"excerpt\":\"The perfect search engine helps customers to find and buy products quickly \\u2013 essential for every WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-product-search\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.itthinx.com\\/wps\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c84cc8ca16ddac3408e6b6c5871133a8\",\"slug\":\"woocommerce-product-search\",\"id\":512174},{\"title\":\"First Data\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-FirstData-updated.png\",\"excerpt\":\"Accept payment with First Data using the Payeezy Gateway, Payeezy, or legacy Global Gateway payment gateways.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/firstdata\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb3e32663ec0810592eaf0d097796230\",\"slug\":\"woocommerce-gateway-firstdata\",\"id\":18645},{\"title\":\"Klarna Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Payments_Pink.png\",\"excerpt\":\"With Klarna Payments\\u00a0you can choose the payment that you want, Pay Now, Pay Later or Slice It. No credit card numbers, no passwords, no worries.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-payments\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnapayments\\/\",\"price\":\"&#36;0.00\",\"hash\":\"a19c689325bc8ea63c620765dd54b33a\",\"slug\":\"klarna-payments-for-woocommerce\",\"id\":2754217},{\"title\":\"WooSlider\",\"image\":\"\",\"excerpt\":\"WooSlider is the ultimate responsive slideshow WordPress slider plugin\\r\\n\\r\\n\\u00a0\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/wooslider\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/www.wooslider.com\\/\",\"price\":\"&#36;49.00\",\"hash\":\"209d98f3ccde6cc3de7e8732a2b20b6a\",\"slug\":\"wooslider\",\"id\":46506},{\"title\":\"WooCommerce Order Status Control\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/06\\/Thumbnail-Order-Status-Control-updated.png\",\"excerpt\":\"Use this extension to automatically change the order status to \\\"completed\\\" after successful payment.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-control\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"32400e509c7c36dcc1cd368e8267d981\",\"slug\":\"woocommerce-order-status-control\",\"id\":439037},{\"title\":\"HubSpot for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/04\\/hubspotlogo-web-color-pxebeq.png\",\"excerpt\":\"Connect your WooCommerce store to HubSpot. Sync, automate &amp; analyze data with HubSpot WooCommerce Integration\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/hubspot-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e50acec8-3a6c-454c-8562-2da4898fa6c1\",\"slug\":\"hubspot-for-woocommerce\",\"id\":5785079},{\"title\":\"WooCommerce Social Login\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/08\\/Thumbnail-Social-Login-updated.png\",\"excerpt\":\"Enable Social Login for seamless checkout and account creation.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-social-login\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demos.skyverge.com\\/woocommerce-social-login\\/\",\"price\":\"&#36;79.00\",\"hash\":\"b231cd6367a79cc8a53b7d992d77525d\",\"slug\":\"woocommerce-social-login\",\"id\":473617},{\"title\":\"Variation Swatches and Photos\",\"image\":\"\",\"excerpt\":\"Show color and image swatches instead of dropdowns for variable products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/variation-swatches-and-photos\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/swatches-and-photos\\/\",\"price\":\"&#36;99.00\",\"hash\":\"37bea8d549df279c8278878d081b062f\",\"slug\":\"woocommerce-variation-swatches-and-photos\",\"id\":18697},{\"title\":\"Opayo Payment Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/Opayo_logo_RGB.png\",\"excerpt\":\"Take payments on your WooCommerce store via Opayo (formally SagePay).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sage-pay-form\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6bc0cca47d0274d8ef9b164f6fbec1cc\",\"slug\":\"woocommerce-gateway-sagepay-form\",\"id\":18599},{\"title\":\"Viva Wallet for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/04\\/Viva-Wallet-logo.png?w=374\",\"excerpt\":\"Integrate the Viva Wallet payment gateway with your WooCommerce store to process and sync your payments and help you sell more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/viva-wallet-for-woocommerce\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"7240a329-047f-4d8b-b7ec-ee3defd798bd\",\"slug\":\"viva-wallet-for-woocommerce\",\"id\":6137160},{\"title\":\"EU VAT Number\",\"image\":\"\",\"excerpt\":\"Collect VAT numbers at checkout and remove the VAT charge for eligible EU businesses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eu-vat-number\\/?utm_source=extensionsscreen&utm_medium=product&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"d2720c4b4bb8d6908e530355b7a2d734\",\"slug\":\"woocommerce-eu-vat-number\",\"id\":18592}]}\";s:7:\"headers\";O:25:\"Requests_Response_Headers\":1:{s:7:\"\0*\0data\";a:18:{s:6:\"server\";a:1:{i:0;s:5:\"nginx\";}s:4:\"date\";a:1:{i:0;s:29:\"Sun, 05 Sep 2021 06:57:11 GMT\";}s:12:\"content-type\";a:1:{i:0;s:31:\"application/json; charset=UTF-8\";}s:14:\"content-length\";a:1:{i:0;s:5:\"11752\";}s:12:\"x-robots-tag\";a:1:{i:0;s:7:\"noindex\";}s:4:\"link\";a:1:{i:0;s:60:\"<https://woocommerce.com/wp-json/>; rel=\"https://api.w.org/\"\";}s:22:\"x-content-type-options\";a:1:{i:0;s:7:\"nosniff\";}s:29:\"access-control-expose-headers\";a:1:{i:0;s:33:\"X-WP-Total, X-WP-TotalPages, Link\";}s:28:\"access-control-allow-headers\";a:1:{i:0;s:73:\"Authorization, X-WP-Nonce, Content-Disposition, Content-MD5, Content-Type\";}s:13:\"x-wccom-cache\";a:1:{i:0;s:3:\"HIT\";}s:13:\"cache-control\";a:1:{i:0;s:10:\"max-age=60\";}s:5:\"allow\";a:1:{i:0;s:3:\"GET\";}s:16:\"content-encoding\";a:1:{i:0;s:4:\"gzip\";}s:4:\"x-rq\";a:1:{i:0;s:13:\"vie1 0 4 9980\";}s:3:\"age\";a:1:{i:0;s:2:\"21\";}s:7:\"x-cache\";a:1:{i:0;s:3:\"hit\";}s:4:\"vary\";a:1:{i:0;s:23:\"Accept-Encoding, Origin\";}s:13:\"accept-ranges\";a:1:{i:0;s:5:\"bytes\";}}}s:11:\"status_code\";i:200;s:16:\"protocol_version\";d:1.1;s:7:\"success\";b:1;s:9:\"redirects\";i:0;s:3:\"url\";s:59:\"https://woocommerce.com/wp-json/wccom-extensions/1.0/search\";s:7:\"history\";a:0:{}s:7:\"cookies\";O:19:\"Requests_Cookie_Jar\":1:{s:10:\"\0*\0cookies\";a:0:{}}}s:11:\"\0*\0filename\";N;s:4:\"data\";N;s:7:\"headers\";N;s:6:\"status\";N;}}', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(862, '_transient_timeout_wc_product_children_223', '1633417107', 'no'),
(863, '_transient_wc_product_children_223', 'a:2:{s:3:\"all\";a:2:{i:0;i:231;i:1;i:232;}s:7:\"visible\";a:2:{i:0;i:231;i:1;i:232;}}', 'no'),
(864, '_transient_timeout_wc_var_prices_223', '1633445339', 'no'),
(865, '_transient_wc_var_prices_223', '{\"version\":\"1630852951\",\"f9e544f77b7eac7add281ef28ca5559f\":{\"price\":{\"231\":\"350\",\"232\":\"290\"},\"regular_price\":{\"231\":\"460\",\"232\":\"340\"},\"sale_price\":{\"231\":\"350\",\"232\":\"290\"}}}', 'no'),
(871, 'yit_recently_activated', 'a:1:{i:0;s:34:\"yith-woocommerce-wishlist/init.php\";}', 'yes'),
(872, 'yith_wcwl_wishlist_page_id', '233', 'yes'),
(873, 'yith_wcwl_version', '3.0.25', 'yes'),
(874, 'yith_wcwl_db_version', '3.0.0', 'yes'),
(875, 'yith_wcwl_ajax_enable', 'no', 'yes'),
(876, 'yith_wfbt_enable_integration', 'yes', 'yes'),
(877, 'yith_wcwl_after_add_to_wishlist_behaviour', 'add', 'yes'),
(878, 'yith_wcwl_show_on_loop', 'no', 'yes'),
(879, 'yith_wcwl_loop_position', 'after_add_to_cart', 'yes'),
(880, 'yith_wcwl_button_position', 'add-to-cart', 'yes'),
(881, 'yith_wcwl_add_to_wishlist_text', 'הוסף לרשימת המשאלות', 'yes'),
(882, 'yith_wcwl_product_added_text', 'המוצר נשמר!', 'yes'),
(883, 'yith_wcwl_browse_wishlist_text', 'עיון ברשימת המשאלות', 'yes'),
(884, 'yith_wcwl_already_in_wishlist_text', 'המוצר כבר ברשימת המשאלות שלך!', 'yes'),
(885, 'yith_wcwl_add_to_wishlist_style', 'link', 'yes'),
(886, 'yith_wcwl_rounded_corners_radius', '16', 'yes'),
(887, 'yith_wcwl_add_to_wishlist_icon', 'fa-heart-o', 'yes'),
(888, 'yith_wcwl_add_to_wishlist_custom_icon', '', 'yes'),
(889, 'yith_wcwl_added_to_wishlist_icon', 'fa-heart', 'yes'),
(890, 'yith_wcwl_added_to_wishlist_custom_icon', '', 'yes'),
(891, 'yith_wcwl_custom_css', '', 'yes'),
(892, 'yith_wcwl_variation_show', '', 'yes'),
(893, 'yith_wcwl_price_show', 'yes', 'yes'),
(894, 'yith_wcwl_stock_show', 'yes', 'yes'),
(895, 'yith_wcwl_show_dateadded', '', 'yes'),
(896, 'yith_wcwl_add_to_cart_show', 'yes', 'yes'),
(897, 'yith_wcwl_show_remove', 'yes', 'yes'),
(898, 'yith_wcwl_repeat_remove_button', '', 'yes'),
(899, 'yith_wcwl_redirect_cart', 'no', 'yes'),
(900, 'yith_wcwl_remove_after_add_to_cart', 'yes', 'yes'),
(901, 'yith_wcwl_enable_share', 'yes', 'yes'),
(902, 'yith_wcwl_share_fb', 'yes', 'yes'),
(903, 'yith_wcwl_share_twitter', 'yes', 'yes'),
(904, 'yith_wcwl_share_pinterest', 'yes', 'yes'),
(905, 'yith_wcwl_share_email', 'yes', 'yes'),
(906, 'yith_wcwl_share_whatsapp', 'yes', 'yes'),
(907, 'yith_wcwl_share_url', 'no', 'yes'),
(908, 'yith_wcwl_socials_title', 'רשימת המשאלות שלי ב-anat', 'yes'),
(909, 'yith_wcwl_socials_text', '', 'yes'),
(910, 'yith_wcwl_socials_image_url', '', 'yes'),
(911, 'yith_wcwl_wishlist_title', 'My wishlist', 'yes'),
(912, 'yith_wcwl_add_to_cart_text', 'Add to cart', 'yes'),
(913, 'yith_wcwl_add_to_cart_style', 'link', 'yes'),
(914, 'yith_wcwl_add_to_cart_rounded_corners_radius', '16', 'yes'),
(915, 'yith_wcwl_add_to_cart_icon', 'fa-shopping-cart', 'yes'),
(916, 'yith_wcwl_add_to_cart_custom_icon', '', 'yes'),
(917, 'yith_wcwl_color_headers_background', '#F4F4F4', 'yes'),
(918, 'yith_wcwl_fb_button_icon', 'fa-facebook', 'yes'),
(919, 'yith_wcwl_fb_button_custom_icon', '', 'yes'),
(920, 'yith_wcwl_tw_button_icon', 'fa-twitter', 'yes'),
(921, 'yith_wcwl_tw_button_custom_icon', '', 'yes'),
(922, 'yith_wcwl_pr_button_icon', 'fa-pinterest', 'yes'),
(923, 'yith_wcwl_pr_button_custom_icon', '', 'yes'),
(924, 'yith_wcwl_em_button_icon', 'fa-envelope-o', 'yes'),
(925, 'yith_wcwl_em_button_custom_icon', '', 'yes'),
(926, 'yith_wcwl_wa_button_icon', 'fa-whatsapp', 'yes'),
(927, 'yith_wcwl_wa_button_custom_icon', '', 'yes'),
(928, 'yit_plugin_fw_panel_wc_default_options_set', 'a:1:{s:15:\"yith_wcwl_panel\";b:1;}', 'yes'),
(929, 'yith_plugin_fw_promo_2019_bis', '1', 'yes'),
(930, '_site_transient_timeout_yith_promo_message', '3261747558', 'no'),
(931, '_site_transient_yith_promo_message', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2019-12-10</expiry_date>\n    <promo>\n        <promo_id>yithblackfriday2019</promo_id>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid from <strong>28th November</strong> to <strong>2nd December</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n            <border_color>#272121</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-11-27 23:59:59</start_date>\n        <end_date>2019-12-03 08:00:00</end_date>\n    </promo>\n</promotions>', 'no'),
(934, 'yith_wcwl_color_add_to_wishlist', 'a:6:{s:10:\"background\";s:7:\"#333333\";s:4:\"text\";s:7:\"#FFFFFF\";s:6:\"border\";s:7:\"#333333\";s:16:\"background_hover\";s:7:\"#333333\";s:10:\"text_hover\";s:7:\"#FFFFFF\";s:12:\"border_hover\";s:7:\"#333333\";}', 'yes'),
(948, '_transient_timeout_yith_wcwl_hidden_products', '1633418065', 'no'),
(949, '_transient_yith_wcwl_hidden_products', 'a:0:{}', 'no'),
(966, '_transient_wc_attribute_taxonomies', 'a:2:{i:0;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"1\";s:14:\"attribute_name\";s:2:\"ml\";s:15:\"attribute_label\";s:8:\"גודל\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"0\";}i:1;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"2\";s:14:\"attribute_name\";s:4:\"size\";s:15:\"attribute_label\";s:8:\"מידה\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"0\";}}', 'yes'),
(1052, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1630834846;s:8:\"response\";a:0:{}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2021-02-05 18:15:06\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.4.2/he_IL.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.6.0\";s:7:\"updated\";s:19:\"2021-08-17 20:32:37\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/woocommerce/5.6.0/he_IL.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:4:\"17.0\";s:7:\"updated\";s:19:\"2021-03-22 15:34:46\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/17.0/he_IL.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:9:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:5:\"1.6.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.4.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.4.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.5\";}s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:32:\"w.org/plugins/contact-form-cfdb7\";s:4:\"slug\";s:18:\"contact-form-cfdb7\";s:6:\"plugin\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:11:\"new_version\";s:7:\"1.2.5.9\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/contact-form-cfdb7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-cfdb7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-256x256.png?rev=1619878\";s:2:\"1x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-128x128.png?rev=1619878\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/contact-form-cfdb7/assets/banner-772x250.png?rev=1619902\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.8\";}s:24:\"fancy-gallery/plugin.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:27:\"w.org/plugins/fancy-gallery\";s:4:\"slug\";s:13:\"fancy-gallery\";s:6:\"plugin\";s:24:\"fancy-gallery/plugin.php\";s:11:\"new_version\";s:6:\"1.6.56\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/fancy-gallery/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/fancy-gallery.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/fancy-gallery/assets/icon-128x128.png?rev=1723946\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/fancy-gallery/assets/banner-772x250.png?rev=1723946\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.0\";}s:33:\"jquery-updater/jquery-updater.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:28:\"w.org/plugins/jquery-updater\";s:4:\"slug\";s:14:\"jquery-updater\";s:6:\"plugin\";s:33:\"jquery-updater/jquery-updater.php\";s:11:\"new_version\";s:7:\"3.6.0.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/jquery-updater/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/jquery-updater.3.6.0.1.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:58:\"https://s.w.org/plugins/geopattern-icon/jquery-updater.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.6\";}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"5.6.0\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.5.6.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2366418\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2366418\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2366418\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2366418\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.6\";}s:45:\"woo-smart-quick-view/wpc-smart-quick-view.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:34:\"w.org/plugins/woo-smart-quick-view\";s:4:\"slug\";s:20:\"woo-smart-quick-view\";s:6:\"plugin\";s:45:\"woo-smart-quick-view/wpc-smart-quick-view.php\";s:11:\"new_version\";s:5:\"2.6.9\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/woo-smart-quick-view/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/woo-smart-quick-view.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/woo-smart-quick-view/assets/icon-128x128.png?rev=1857803\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:75:\"https://ps.w.org/woo-smart-quick-view/assets/banner-772x250.png?rev=2425219\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.0\";}s:34:\"yith-woocommerce-wishlist/init.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:39:\"w.org/plugins/yith-woocommerce-wishlist\";s:4:\"slug\";s:25:\"yith-woocommerce-wishlist\";s:6:\"plugin\";s:34:\"yith-woocommerce-wishlist/init.php\";s:11:\"new_version\";s:6:\"3.0.25\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/yith-woocommerce-wishlist/\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/plugin/yith-woocommerce-wishlist.3.0.25.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:78:\"https://ps.w.org/yith-woocommerce-wishlist/assets/icon-128x128.jpg?rev=2215573\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-1544x500.jpg?rev=2209192\";s:2:\"1x\";s:80:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-772x250.jpg?rev=2209192\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.6\";}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"17.0\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.17.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:8:\"requires\";s:3:\"5.6\";}}s:7:\"checked\";a:15:{s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.10.2\";s:33:\"classic-editor/classic-editor.php\";s:5:\"1.6.2\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.4.2\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:7:\"1.2.5.9\";s:24:\"fancy-gallery/plugin.php\";s:6:\"1.6.56\";s:33:\"jquery-updater/jquery-updater.php\";s:7:\"3.6.0.1\";s:29:\"versionpress/versionpress.php\";s:9:\"4.0-beta2\";s:17:\"wesafe/wesafe.php\";s:5:\"1.7.2\";s:27:\"woocommerce/woocommerce.php\";s:5:\"5.6.0\";s:25:\"dummy-data/dummy-data.php\";s:3:\"1.0\";s:45:\"woo-smart-quick-view/wpc-smart-quick-view.php\";s:5:\"2.6.9\";s:29:\"wp-sync-db-1.5/wp-sync-db.php\";s:3:\"1.5\";s:34:\"yith-woocommerce-wishlist/init.php\";s:6:\"3.0.25\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"17.0\";s:17:\"leosem/leosem.php\";s:3:\"2.0\";}}', 'no'),
(1055, '_transient_shipping-transient-version', '1630836010', 'yes'),
(1056, '_transient_timeout_wc_shipping_method_count_legacy', '1633428010', 'no'),
(1057, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1630836010\";s:5:\"value\";i:0;}', 'no'),
(1070, '_transient_timeout_as-post-store-dependencies-met', '1630923347', 'no'),
(1071, '_transient_as-post-store-dependencies-met', 'yes', 'no'),
(1075, '_site_transient_timeout_theme_roots', '1630838772', 'no'),
(1076, '_site_transient_theme_roots', 'a:2:{s:4:\"anat\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";}', 'no'),
(1078, 'action_scheduler_migration_status', 'complete', 'yes'),
(1088, 'product_cat_children', 'a:1:{i:23;a:2:{i:0;i:30;i:1;i:31;}}', 'yes'),
(1108, 'options_shop_form_title', 'משפחה מרוויחה יותר!', 'no'),
(1109, '_options_shop_form_title', 'field_61349fee43a8c', 'no'),
(1110, 'options_shop_form_text', 'הצטרפי למשפחת ויקטוריה סיקרט:', 'no'),
(1111, '_options_shop_form_text', 'field_61349ff443a8d', 'no'),
(1243, '_transient_timeout_wc_product_children_246', '1633444910', 'no'),
(1244, '_transient_wc_product_children_246', 'a:2:{s:3:\"all\";a:3:{i:0;i:252;i:1;i:253;i:2;i:254;}s:7:\"visible\";a:3:{i:0;i:252;i:1;i:253;i:2;i:254;}}', 'no'),
(1252, '_transient_timeout_wc_term_counts', '1633445339', 'no'),
(1253, '_transient_wc_term_counts', 'a:7:{i:15;s:1:\"5\";i:24;s:0:\"\";i:21;s:0:\"\";i:20;s:0:\"\";i:23;s:1:\"2\";i:22;s:1:\"1\";i:30;s:1:\"1\";}', 'no'),
(1254, '_transient_timeout_wc_var_prices_246', '1633445340', 'no'),
(1255, '_transient_wc_var_prices_246', '{\"version\":\"1630852951\",\"f9e544f77b7eac7add281ef28ca5559f\":{\"price\":{\"252\":\"120\",\"253\":\"130\"},\"regular_price\":{\"252\":\"140\",\"253\":\"150\"},\"sale_price\":{\"252\":\"120\",\"253\":\"130\"}}}', 'no'),
(1267, 'options_menu_item_0_icon', '262', 'no'),
(1268, '_options_menu_item_0_icon', 'field_6134e15a2ede8', 'no'),
(1269, 'options_menu_item_0_link', 'a:3:{s:5:\"title\";s:22:\"קרם גוף/שימר\";s:3:\"url\";s:91:\"/product-category/מבשמי-הגוף-שלנו/תת-קטגוריה-לורם-איפסום-1/\";s:6:\"target\";s:0:\"\";}', 'no'),
(1270, '_options_menu_item_0_link', 'field_6134e16c2ede9', 'no'),
(1271, 'options_menu_item_1_icon', '263', 'no'),
(1272, '_options_menu_item_1_icon', 'field_6134e15a2ede8', 'no'),
(1273, 'options_menu_item_1_link', 'a:3:{s:5:\"title\";s:26:\"מבשמי גוף/שימר\";s:3:\"url\";s:91:\"/product-category/מבשמי-הגוף-שלנו/תת-קטגוריה-לורם-איפסום-2/\";s:6:\"target\";s:0:\"\";}', 'no'),
(1274, '_options_menu_item_1_link', 'field_6134e16c2ede9', 'no'),
(1275, 'options_menu_item_2_icon', '264', 'no'),
(1276, '_options_menu_item_2_icon', 'field_6134e15a2ede8', 'no'),
(1277, 'options_menu_item_2_link', 'a:3:{s:5:\"title\";s:10:\"בשמים\";s:3:\"url\";s:26:\"/product-category/parfums/\";s:6:\"target\";s:0:\"\";}', 'no'),
(1278, '_options_menu_item_2_link', 'field_6134e16c2ede9', 'no'),
(1279, 'options_menu_item_3_icon', '265', 'no'),
(1280, '_options_menu_item_3_icon', 'field_6134e15a2ede8', 'no'),
(1281, 'options_menu_item_3_link', 'a:3:{s:5:\"title\";s:12:\"מארזים\";s:3:\"url\";s:31:\"/product-category/מארזים/\";s:6:\"target\";s:0:\"\";}', 'no'),
(1282, '_options_menu_item_3_link', 'field_6134e16c2ede9', 'no'),
(1283, 'options_menu_item_4_icon', '266', 'no'),
(1284, '_options_menu_item_4_icon', 'field_6134e15a2ede8', 'no'),
(1285, 'options_menu_item_4_link', 'a:3:{s:5:\"title\";s:12:\"ארנקים\";s:3:\"url\";s:55:\"/product-category/ארנקים-תיקים-קלמרים/\";s:6:\"target\";s:0:\"\";}', 'no'),
(1286, '_options_menu_item_4_link', 'field_6134e16c2ede9', 'no'),
(1287, 'options_menu_item_5_icon', '267', 'no'),
(1288, '_options_menu_item_5_icon', 'field_6134e15a2ede8', 'no'),
(1289, 'options_menu_item_5_link', 'a:3:{s:5:\"title\";s:10:\"תיקים\";s:3:\"url\";s:55:\"/product-category/ארנקים-תיקים-קלמרים/\";s:6:\"target\";s:0:\"\";}', 'no'),
(1290, '_options_menu_item_5_link', 'field_6134e16c2ede9', 'no'),
(1291, 'options_menu_item_6_icon', '268', 'no'),
(1292, '_options_menu_item_6_icon', 'field_6134e15a2ede8', 'no'),
(1293, 'options_menu_item_6_link', 'a:3:{s:5:\"title\";s:32:\"תיקי איפור/קלמרים\";s:3:\"url\";s:55:\"/product-category/ארנקים-תיקים-קלמרים/\";s:6:\"target\";s:0:\"\";}', 'no'),
(1294, '_options_menu_item_6_link', 'field_6134e16c2ede9', 'no'),
(1295, 'options_menu_item_7_icon', '269', 'no'),
(1296, '_options_menu_item_7_icon', 'field_6134e15a2ede8', 'no'),
(1297, 'options_menu_item_7_link', 'a:3:{s:5:\"title\";s:16:\"אקססוריז\";s:3:\"url\";s:35:\"/product-category/אקססוריז/\";s:6:\"target\";s:0:\"\";}', 'no'),
(1298, '_options_menu_item_7_link', 'field_6134e16c2ede9', 'no'),
(1299, 'options_menu_item', '8', 'no'),
(1300, '_options_menu_item', 'field_6134e13b2ede7', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_postmeta`
--

CREATE TABLE `fmn_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_postmeta`
--

INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_lock', '1630437851:1'),
(10, 7, '_edit_last', '1'),
(11, 7, '_edit_lock', '1630853150:1'),
(12, 7, '_wp_page_template', 'views/home.php'),
(13, 7, 'title_tag', ''),
(14, 7, '_title_tag', 'field_5ddbe7577e0e7'),
(15, 7, 'single_slider_seo', '2'),
(16, 7, '_single_slider_seo', 'field_5ddbde5499115'),
(17, 7, 'single_gallery', ''),
(18, 7, '_single_gallery', 'field_5ddbe5aea4275'),
(19, 7, 'single_video_slider', ''),
(20, 7, '_single_video_slider', 'field_5ddbe636a4276'),
(21, 8, 'title_tag', ''),
(22, 8, '_title_tag', 'field_5ddbe7577e0e7'),
(23, 8, 'single_slider_seo', ''),
(24, 8, '_single_slider_seo', 'field_5ddbde5499115'),
(25, 8, 'single_gallery', ''),
(26, 8, '_single_gallery', 'field_5ddbe5aea4275'),
(27, 8, 'single_video_slider', ''),
(28, 8, '_single_video_slider', 'field_5ddbe636a4276'),
(29, 9, '_edit_last', '1'),
(30, 9, '_edit_lock', '1630683734:1'),
(31, 9, '_wp_page_template', 'views/contact.php'),
(32, 9, 'title_tag', ''),
(33, 9, '_title_tag', 'field_5ddbe7577e0e7'),
(34, 9, 'single_slider_seo', ''),
(35, 9, '_single_slider_seo', 'field_5ddbde5499115'),
(36, 9, 'single_gallery', ''),
(37, 9, '_single_gallery', 'field_5ddbe5aea4275'),
(38, 9, 'single_video_slider', ''),
(39, 9, '_single_video_slider', 'field_5ddbe636a4276'),
(40, 10, 'title_tag', ''),
(41, 10, '_title_tag', 'field_5ddbe7577e0e7'),
(42, 10, 'single_slider_seo', ''),
(43, 10, '_single_slider_seo', 'field_5ddbde5499115'),
(44, 10, 'single_gallery', ''),
(45, 10, '_single_gallery', 'field_5ddbe5aea4275'),
(46, 10, 'single_video_slider', ''),
(47, 10, '_single_video_slider', 'field_5ddbe636a4276'),
(48, 11, '_edit_last', '1'),
(49, 11, '_edit_lock', '1630778280:1'),
(50, 11, '_wp_page_template', 'views/blog.php'),
(51, 11, 'title_tag', ''),
(52, 11, '_title_tag', 'field_5ddbe7577e0e7'),
(53, 11, 'single_slider_seo', ''),
(54, 11, '_single_slider_seo', 'field_5ddbde5499115'),
(55, 11, 'single_gallery', ''),
(56, 11, '_single_gallery', 'field_5ddbe5aea4275'),
(57, 11, 'single_video_slider', ''),
(58, 11, '_single_video_slider', 'field_5ddbe636a4276'),
(59, 12, 'title_tag', ''),
(60, 12, '_title_tag', 'field_5ddbe7577e0e7'),
(61, 12, 'single_slider_seo', ''),
(62, 12, '_single_slider_seo', 'field_5ddbde5499115'),
(63, 12, 'single_gallery', ''),
(64, 12, '_single_gallery', 'field_5ddbe5aea4275'),
(65, 12, 'single_video_slider', ''),
(66, 12, '_single_video_slider', 'field_5ddbe636a4276'),
(67, 21, '_edit_lock', '1630680592:1'),
(68, 21, '_edit_last', '1'),
(69, 13, '_edit_lock', '1630498249:1'),
(70, 13, '_edit_last', '1'),
(71, 29, '_edit_last', '1'),
(72, 29, '_edit_lock', '1630855542:1'),
(73, 31, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-md-10 col-12\">\n		[text* text-827 placeholder \"שם מלא\"]\n	</div>\n	    <div class=\"col-md-10 col-12\">\n				[tel* tel-82 placeholder \"טלפון\"]\n	</div>\n    <div class=\"col-md-10 col-12\">\n			[email email-256 placeholder \"מייל\"]\n	</div>\n	    <div class=\"col-md-10 col-12\">\n				[submit \"הרשמה\"]\n	</div>\n		<div class=\"col-12 acceptance-col\">\n			[acceptance acceptance-951] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\n	</div>\n</div>'),
(74, 31, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(75, 31, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(76, 31, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(77, 31, '_additional_settings', ''),
(78, 31, '_locale', 'he_IL'),
(80, 32, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-md-4 col-12\">\n		[text* text-124 placeholder \"שם מלא\"]\n	</div>\n	    <div class=\"col-md-4 col-12\">\n				[tel* tel-897 placeholder \"טלפון\"]\n	</div>\n    <div class=\"col-md-4 col-12\">\n			[email email-239 placeholder \"מייל\"]\n	</div>\n	    <div class=\"col-12\">\n				[textarea textarea-535 placeholder \"תוכן פנייה\"]\n	</div>\n	    <div class=\"col-12\">\n				[submit \"שליחת הפנייה\"]\n	</div>\n			<div class=\"col-12 acceptance-col\">\n				[acceptance acceptance-530] אני מאשר/ת קבלת דיוור פרסומי במייל [/acceptance]\n	</div>\n</div>'),
(81, 32, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(82, 32, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(83, 32, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(84, 32, '_additional_settings', ''),
(85, 32, '_locale', 'he_IL'),
(88, 33, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-xl-3 col-md-4 col-12\">\n		[text* text-496 placeholder \"שם מלא\"]\n	</div>\n	    <div class=\"col-xl-3 col-md-4 col-12\">\n				[tel* tel-687 placeholder \"טלפון\"]\n	</div>\n    <div class=\"col-xl-3 col-md-4 col-12\">\n			[email email-741 placeholder \"מייל\"]\n	</div>\n	    <div class=\"col-xl-3 col-12\">\n				[submit \"הרשמה\"]\n	</div>\n	<div class=\"col-12 acceptance-col\">\n		[acceptance acceptance-580] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\n	</div>\n</div>'),
(89, 33, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(90, 33, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(91, 33, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(92, 33, '_additional_settings', ''),
(93, 33, '_locale', 'he_IL'),
(95, 34, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-md col-12\">\n		[email* email-199 placeholder \"מייל:\"]\n	</div>\n	    <div class=\"col-md-auto col-12\">\n				[submit \"אני מעוניינ/ת להשאר מעודכנת\"]\n	</div>\n	<div class=\"col-12 acceptance-col\">\n		[acceptance acceptance-604] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\n	</div>\n</div>'),
(96, 34, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(97, 34, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(98, 34, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(99, 34, '_additional_settings', ''),
(100, 34, '_locale', 'he_IL'),
(102, 35, '_wp_attached_file', 'woocommerce-placeholder.png'),
(103, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:33:\"woocommerce-placeholder-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(104, 41, '_edit_last', '1'),
(105, 41, '_edit_lock', '1630821613:1'),
(106, 44, '_edit_last', '1'),
(107, 44, '_edit_lock', '1630600208:1'),
(108, 49, '_edit_last', '1'),
(109, 49, '_edit_lock', '1630858055:1'),
(116, 70, '_edit_lock', '1630851178:1'),
(117, 70, '_edit_last', '1'),
(118, 33, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(119, 31, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(121, 73, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-md-10 col-12\">\n		[text* text-143 placeholder \"שם מלא\"]\n	</div>\n	    <div class=\"col-md-10 col-12\">\n				[tel* tel-125 placeholder \"טלפון\"]\n	</div>\n    <div class=\"col-md-10 col-12\">\n			[email* email-971 placeholder \"מייל\"]\n	</div>\n	    <div class=\"col-md-10 col-12\">\n				[submit \"הרשמה\"]\n	</div>\n		<div class=\"col-12 acceptance-col\">\n			[acceptance acceptance-535] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\n	</div>\n</div>'),
(122, 73, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(123, 73, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(124, 73, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(125, 73, '_additional_settings', ''),
(126, 73, '_locale', 'he_IL'),
(129, 73, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(130, 74, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-12\">\n		[text* text-316 placeholder \"שם מלא\"]\n	</div>\n	    <div class=\"col-12\">\n				[tel* tel-636 placeholder \"טלפון\"]\n	</div>\n    <div class=\"col-12\">\n			[email* email-326 placeholder \"מייל\"]\n	</div>\n	    <div class=\"col-12\">\n				[submit \"הרשמה\"]\n	</div>\n		<div class=\"col-12 acceptance-col\">\n			[acceptance acceptance-858] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\n	</div>\n</div>'),
(131, 74, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(132, 74, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:30:\"[_site_title] <wordpress@anat>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(133, 74, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(134, 74, '_additional_settings', ''),
(135, 74, '_locale', 'he_IL'),
(138, 74, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(139, 116, '_menu_item_type', 'post_type'),
(140, 116, '_menu_item_menu_item_parent', '0'),
(141, 116, '_menu_item_object_id', '7'),
(142, 116, '_menu_item_object', 'page'),
(143, 116, '_menu_item_target', ''),
(144, 116, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(145, 116, '_menu_item_xfn', ''),
(146, 116, '_menu_item_url', ''),
(148, 117, '_menu_item_type', 'post_type'),
(149, 117, '_menu_item_menu_item_parent', '0'),
(150, 117, '_menu_item_object_id', '11'),
(151, 117, '_menu_item_object', 'page'),
(152, 117, '_menu_item_target', ''),
(153, 117, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(154, 117, '_menu_item_xfn', ''),
(155, 117, '_menu_item_url', ''),
(157, 118, '_menu_item_type', 'post_type'),
(158, 118, '_menu_item_menu_item_parent', '0'),
(159, 118, '_menu_item_object_id', '36'),
(160, 118, '_menu_item_object', 'page'),
(161, 118, '_menu_item_target', ''),
(162, 118, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(163, 118, '_menu_item_xfn', ''),
(164, 118, '_menu_item_url', ''),
(166, 119, '_menu_item_type', 'post_type'),
(167, 119, '_menu_item_menu_item_parent', '0'),
(168, 119, '_menu_item_object_id', '9'),
(169, 119, '_menu_item_object', 'page'),
(170, 119, '_menu_item_target', ''),
(171, 119, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(172, 119, '_menu_item_xfn', ''),
(173, 119, '_menu_item_url', ''),
(175, 120, '_menu_item_type', 'post_type'),
(176, 120, '_menu_item_menu_item_parent', '0'),
(177, 120, '_menu_item_object_id', '7'),
(178, 120, '_menu_item_object', 'page'),
(179, 120, '_menu_item_target', ''),
(180, 120, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(181, 120, '_menu_item_xfn', ''),
(182, 120, '_menu_item_url', ''),
(184, 121, '_menu_item_type', 'post_type'),
(185, 121, '_menu_item_menu_item_parent', '0'),
(186, 121, '_menu_item_object_id', '11'),
(187, 121, '_menu_item_object', 'page'),
(188, 121, '_menu_item_target', ''),
(189, 121, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(190, 121, '_menu_item_xfn', ''),
(191, 121, '_menu_item_url', ''),
(193, 122, '_menu_item_type', 'post_type'),
(194, 122, '_menu_item_menu_item_parent', '0'),
(195, 122, '_menu_item_object_id', '36'),
(196, 122, '_menu_item_object', 'page'),
(197, 122, '_menu_item_target', ''),
(198, 122, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(199, 122, '_menu_item_xfn', ''),
(200, 122, '_menu_item_url', ''),
(202, 123, '_menu_item_type', 'post_type'),
(203, 123, '_menu_item_menu_item_parent', '0'),
(204, 123, '_menu_item_object_id', '9'),
(205, 123, '_menu_item_object', 'page'),
(206, 123, '_menu_item_target', ''),
(207, 123, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(208, 123, '_menu_item_xfn', ''),
(209, 123, '_menu_item_url', ''),
(211, 124, '_menu_item_type', 'post_type'),
(212, 124, '_menu_item_menu_item_parent', '0'),
(213, 124, '_menu_item_object_id', '1'),
(214, 124, '_menu_item_object', 'post'),
(215, 124, '_menu_item_target', ''),
(216, 124, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(217, 124, '_menu_item_xfn', ''),
(218, 124, '_menu_item_url', ''),
(220, 125, '_menu_item_type', 'taxonomy'),
(221, 125, '_menu_item_menu_item_parent', '0'),
(222, 125, '_menu_item_object_id', '15'),
(223, 125, '_menu_item_object', 'product_cat'),
(224, 125, '_menu_item_target', ''),
(225, 125, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(226, 125, '_menu_item_xfn', ''),
(227, 125, '_menu_item_url', ''),
(229, 126, '_menu_item_type', 'taxonomy'),
(230, 126, '_menu_item_menu_item_parent', '0'),
(231, 126, '_menu_item_object_id', '1'),
(232, 126, '_menu_item_object', 'category'),
(233, 126, '_menu_item_target', ''),
(234, 126, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(235, 126, '_menu_item_xfn', ''),
(236, 126, '_menu_item_url', ''),
(238, 127, '_edit_last', '1'),
(239, 127, '_edit_lock', '1630797900:1'),
(240, 127, '_wp_page_template', 'views/about.php'),
(241, 127, 'single_slider_seo', ''),
(242, 127, '_single_slider_seo', 'field_5ddbde5499115'),
(243, 127, 'slider_img', ''),
(244, 127, '_slider_img', 'field_612f4e7693132'),
(245, 128, 'single_slider_seo', ''),
(246, 128, '_single_slider_seo', 'field_5ddbde5499115'),
(247, 128, 'slider_img', ''),
(248, 128, '_slider_img', 'field_612f4e7693132'),
(249, 129, '_wp_attached_file', '2021/09/footer-logo.png'),
(250, 129, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:308;s:6:\"height\";i:308;s:4:\"file\";s:23:\"2021/09/footer-logo.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"footer-logo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"footer-logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"footer-logo-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"footer-logo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"footer-logo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"footer-logo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"footer-logo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(251, 130, '_wp_attached_file', '2021/09/logo-text.png'),
(252, 130, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:195;s:6:\"height\";i:63;s:4:\"file\";s:21:\"2021/09/logo-text.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-text-150x63.png\";s:5:\"width\";i:150;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"logo-text-134x43.png\";s:5:\"width\";i:134;s:6:\"height\";i:43;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-text-100x63.png\";s:5:\"width\";i:100;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-text-100x63.png\";s:5:\"width\";i:100;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(253, 131, '_wp_attached_file', '2021/09/bran-logo.png'),
(254, 131, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:137;s:6:\"height\";i:43;s:4:\"file\";s:21:\"2021/09/bran-logo.png\";s:5:\"sizes\";a:3:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"bran-logo-134x42.png\";s:5:\"width\";i:134;s:6:\"height\";i:42;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"bran-logo-100x43.png\";s:5:\"width\";i:100;s:6:\"height\";i:43;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"bran-logo-100x43.png\";s:5:\"width\";i:100;s:6:\"height\";i:43;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(255, 132, '_wp_attached_file', '2021/09/payment-1.png'),
(256, 132, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:58;s:6:\"height\";i:39;s:4:\"file\";s:21:\"2021/09/payment-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(257, 133, '_wp_attached_file', '2021/09/payment-2.png'),
(258, 133, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:98;s:6:\"height\";i:69;s:4:\"file\";s:21:\"2021/09/payment-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(259, 134, '_wp_attached_file', '2021/09/payment-3.png'),
(260, 134, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:85;s:6:\"height\";i:48;s:4:\"file\";s:21:\"2021/09/payment-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(261, 135, '_wp_attached_file', '2021/09/payment-4.png'),
(262, 135, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:54;s:6:\"height\";i:18;s:4:\"file\";s:21:\"2021/09/payment-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(263, 136, '_wp_attached_file', '2021/09/payment-5.png'),
(264, 136, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:39;s:6:\"height\";i:24;s:4:\"file\";s:21:\"2021/09/payment-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(265, 137, '_edit_last', '1'),
(266, 137, '_edit_lock', '1630683424:1'),
(267, 9, 'contact_form_title', 'משפחה מרוויחה יותר!'),
(268, 9, '_contact_form_title', 'field_613241963648e'),
(269, 9, 'contact_form_text', 'הצטרפי למשפחת ויקטוריה סיקרט ישראל ותהני מהנחות אישיות, מידע על מוצרים חדשים וקופונים הכי שווים'),
(270, 9, '_contact_form_text', 'field_613241a13648f'),
(271, 140, 'title_tag', ''),
(272, 140, '_title_tag', 'field_5ddbe7577e0e7'),
(273, 140, 'single_slider_seo', ''),
(274, 140, '_single_slider_seo', 'field_5ddbde5499115'),
(275, 140, 'single_gallery', ''),
(276, 140, '_single_gallery', 'field_5ddbe5aea4275'),
(277, 140, 'single_video_slider', ''),
(278, 140, '_single_video_slider', 'field_5ddbe636a4276'),
(279, 140, 'contact_form_title', 'משפחה מרוויחה יותר!'),
(280, 140, '_contact_form_title', 'field_613241963648e'),
(281, 140, 'contact_form_text', 'הצטרפי למשפחת ויקטוריה סיקרט ישראל ותהני מהנחות אישיות, מידע על מוצרים חדשים וקופונים הכי שווים'),
(282, 140, '_contact_form_text', 'field_613241a13648f'),
(285, 34, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(286, 32, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(287, 141, '_edit_lock', '1630824735:1'),
(288, 141, '_edit_last', '1'),
(289, 147, '_wp_attached_file', '2021/09/category-1.png'),
(290, 147, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:423;s:6:\"height\";i:563;s:4:\"file\";s:22:\"2021/09/category-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-1-225x300.png\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"category-1-71x94.png\";s:5:\"width\";i:71;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(291, 148, '_wp_attached_file', '2021/09/category-2.png'),
(292, 148, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:364;s:6:\"height\";i:484;s:4:\"file\";s:22:\"2021/09/category-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-2-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"category-2-71x94.png\";s:5:\"width\";i:71;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(293, 149, '_wp_attached_file', '2021/09/category-3.png'),
(294, 149, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:256;s:6:\"height\";i:370;s:4:\"file\";s:22:\"2021/09/category-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-3-208x300.png\";s:5:\"width\";i:208;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"category-3-65x94.png\";s:5:\"width\";i:65;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-3-256x300.png\";s:5:\"width\";i:256;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-3-256x300.png\";s:5:\"width\";i:256;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(295, 150, '_wp_attached_file', '2021/09/category-4.png'),
(296, 150, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:290;s:6:\"height\";i:387;s:4:\"file\";s:22:\"2021/09/category-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-4-225x300.png\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"category-4-70x94.png\";s:5:\"width\";i:70;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-4-290x300.png\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-4-290x300.png\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(297, 151, '_wp_attached_file', '2021/09/category-6.png'),
(298, 151, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:295;s:4:\"file\";s:22:\"2021/09/category-6.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-6-300x291.png\";s:5:\"width\";i:300;s:6:\"height\";i:291;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"category-6-97x94.png\";s:5:\"width\";i:97;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-6-300x295.png\";s:5:\"width\";i:300;s:6:\"height\";i:295;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-6-300x295.png\";s:5:\"width\";i:300;s:6:\"height\";i:295;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(299, 152, '_wp_attached_file', '2021/09/category-5.png'),
(300, 152, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:273;s:6:\"height\";i:364;s:4:\"file\";s:22:\"2021/09/category-5.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category-5-225x300.png\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"category-5-71x94.png\";s:5:\"width\";i:71;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category-5-273x300.png\";s:5:\"width\";i:273;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category-5-273x300.png\";s:5:\"width\";i:273;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(301, 1, '_edit_lock', '1630776199:1'),
(302, 153, '_wp_attached_file', '2021/08/post-1.png'),
(303, 153, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:489;s:6:\"height\";i:333;s:4:\"file\";s:18:\"2021/08/post-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"post-1-300x204.png\";s:5:\"width\";i:300;s:6:\"height\";i:204;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"post-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:17:\"post-1-134x91.png\";s:5:\"width\";i:134;s:6:\"height\";i:91;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"post-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"post-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(304, 154, '_wp_attached_file', '2021/08/post-2.png');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(305, 154, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:492;s:6:\"height\";i:290;s:4:\"file\";s:18:\"2021/08/post-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"post-2-300x177.png\";s:5:\"width\";i:300;s:6:\"height\";i:177;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"post-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:17:\"post-2-134x79.png\";s:5:\"width\";i:134;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"post-2-300x290.png\";s:5:\"width\";i:300;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"post-2-300x290.png\";s:5:\"width\";i:300;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(306, 155, '_wp_attached_file', '2021/08/post-3.png'),
(307, 155, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:488;s:6:\"height\";i:333;s:4:\"file\";s:18:\"2021/08/post-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"post-3-300x205.png\";s:5:\"width\";i:300;s:6:\"height\";i:205;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"post-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:17:\"post-3-134x91.png\";s:5:\"width\";i:134;s:6:\"height\";i:91;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"post-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"post-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(308, 156, '_wp_attached_file', '2021/08/faq-block-img.png'),
(309, 156, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:570;s:6:\"height\";i:526;s:4:\"file\";s:25:\"2021/08/faq-block-img.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"faq-block-img-300x277.png\";s:5:\"width\";i:300;s:6:\"height\";i:277;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"faq-block-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"faq-block-img-102x94.png\";s:5:\"width\";i:102;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"faq-block-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"faq-block-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"faq-block-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"faq-block-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(310, 1, '_edit_last', '1'),
(311, 1, '_thumbnail_id', '153'),
(313, 1, 'same_title', ''),
(314, 1, '_same_title', 'field_6130fbc198e01'),
(315, 1, 'same_posts', 'a:3:{i:0;s:3:\"161\";i:1;s:3:\"171\";i:2;s:3:\"173\";}'),
(316, 1, '_same_posts', 'field_6130fbd798e02'),
(317, 1, 'same_posts_link', 'a:3:{s:5:\"title\";s:21:\"לכל המאמרים\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(318, 1, '_same_posts_link', 'field_6130fbf0282c7'),
(319, 1, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(320, 1, '_faq_title', 'field_5feb2f82db93b'),
(321, 1, 'faq_img', '156'),
(322, 1, '_faq_img', 'field_5feb2f95db93c'),
(323, 1, 'faq_item_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם 1?'),
(324, 1, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(325, 1, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(326, 1, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(327, 1, 'faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם 2?'),
(328, 1, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(329, 1, 'faq_item_1_faq_answer', 'דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(330, 1, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(331, 1, 'faq_item', '2'),
(332, 1, '_faq_item', 'field_5feb2faddb93d'),
(333, 158, 'same_title', ''),
(334, 158, '_same_title', 'field_6130fbc198e01'),
(335, 158, 'same_posts', ''),
(336, 158, '_same_posts', 'field_6130fbd798e02'),
(337, 158, 'same_posts_link', 'a:3:{s:5:\"title\";s:21:\"לכל המאמרים\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(338, 158, '_same_posts_link', 'field_6130fbf0282c7'),
(339, 158, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(340, 158, '_faq_title', 'field_5feb2f82db93b'),
(341, 158, 'faq_img', '156'),
(342, 158, '_faq_img', 'field_5feb2f95db93c'),
(343, 158, 'faq_item_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם 1?'),
(344, 158, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(345, 158, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(346, 158, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(347, 158, 'faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם 2?'),
(348, 158, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(349, 158, 'faq_item_1_faq_answer', 'דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(350, 158, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(351, 158, 'faq_item', '2'),
(352, 158, '_faq_item', 'field_5feb2faddb93d'),
(353, 159, '_edit_last', '1'),
(354, 159, '_thumbnail_id', '154'),
(356, 159, 'same_title', ''),
(357, 159, '_same_title', 'field_6130fbc198e01'),
(358, 159, 'same_posts', ''),
(359, 159, '_same_posts', 'field_6130fbd798e02'),
(360, 159, 'same_posts_link', ''),
(361, 159, '_same_posts_link', 'field_6130fbf0282c7'),
(362, 159, 'faq_title', ''),
(363, 159, '_faq_title', 'field_5feb2f82db93b'),
(364, 159, 'faq_img', ''),
(365, 159, '_faq_img', 'field_5feb2f95db93c'),
(366, 159, 'faq_item', ''),
(367, 159, '_faq_item', 'field_5feb2faddb93d'),
(368, 160, 'same_title', ''),
(369, 160, '_same_title', 'field_6130fbc198e01'),
(370, 160, 'same_posts', ''),
(371, 160, '_same_posts', 'field_6130fbd798e02'),
(372, 160, 'same_posts_link', ''),
(373, 160, '_same_posts_link', 'field_6130fbf0282c7'),
(374, 160, 'faq_title', ''),
(375, 160, '_faq_title', 'field_5feb2f82db93b'),
(376, 160, 'faq_img', ''),
(377, 160, '_faq_img', 'field_5feb2f95db93c'),
(378, 160, 'faq_item', ''),
(379, 160, '_faq_item', 'field_5feb2faddb93d'),
(380, 159, '_edit_lock', '1630740388:1'),
(381, 161, '_edit_last', '1'),
(382, 161, '_thumbnail_id', '155'),
(384, 161, 'same_title', ''),
(385, 161, '_same_title', 'field_6130fbc198e01'),
(386, 161, 'same_posts', ''),
(387, 161, '_same_posts', 'field_6130fbd798e02'),
(388, 161, 'same_posts_link', ''),
(389, 161, '_same_posts_link', 'field_6130fbf0282c7'),
(390, 161, 'faq_title', ''),
(391, 161, '_faq_title', 'field_5feb2f82db93b'),
(392, 161, 'faq_img', ''),
(393, 161, '_faq_img', 'field_5feb2f95db93c'),
(394, 161, 'faq_item', ''),
(395, 161, '_faq_item', 'field_5feb2faddb93d'),
(396, 162, 'same_title', ''),
(397, 162, '_same_title', 'field_6130fbc198e01'),
(398, 162, 'same_posts', ''),
(399, 162, '_same_posts', 'field_6130fbd798e02'),
(400, 162, 'same_posts_link', ''),
(401, 162, '_same_posts_link', 'field_6130fbf0282c7'),
(402, 162, 'faq_title', ''),
(403, 162, '_faq_title', 'field_5feb2f82db93b'),
(404, 162, 'faq_img', ''),
(405, 162, '_faq_img', 'field_5feb2f95db93c'),
(406, 162, 'faq_item', ''),
(407, 162, '_faq_item', 'field_5feb2faddb93d'),
(408, 161, '_edit_lock', '1630740413:1'),
(409, 163, '_wp_attached_file', '2021/09/home-gallery-1.png'),
(410, 163, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:509;s:6:\"height\";i:636;s:4:\"file\";s:26:\"2021/09/home-gallery-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-240x300.png\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"home-gallery-1-75x94.png\";s:5:\"width\";i:75;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(411, 164, '_wp_attached_file', '2021/09/home-gallery-2.png'),
(412, 164, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:522;s:6:\"height\";i:602;s:4:\"file\";s:26:\"2021/09/home-gallery-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-260x300.png\";s:5:\"width\";i:260;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"home-gallery-2-82x94.png\";s:5:\"width\";i:82;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(413, 165, '_wp_attached_file', '2021/09/home-gallery-3.png'),
(414, 165, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:550;s:6:\"height\";i:635;s:4:\"file\";s:26:\"2021/09/home-gallery-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-260x300.png\";s:5:\"width\";i:260;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"home-gallery-3-81x94.png\";s:5:\"width\";i:81;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(415, 166, '_wp_attached_file', '2021/09/home-gallery-4.png'),
(416, 166, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:508;s:6:\"height\";i:617;s:4:\"file\";s:26:\"2021/09/home-gallery-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-247x300.png\";s:5:\"width\";i:247;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"home-gallery-4-77x94.png\";s:5:\"width\";i:77;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(417, 167, '_wp_attached_file', '2021/09/home-gallery-5.png'),
(418, 167, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:545;s:6:\"height\";i:682;s:4:\"file\";s:26:\"2021/09/home-gallery-5.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-5-240x300.png\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"home-gallery-5-75x94.png\";s:5:\"width\";i:75;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(419, 168, '_wp_attached_file', '2021/09/home-gallery-6.png'),
(420, 168, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:531;s:6:\"height\";i:664;s:4:\"file\";s:26:\"2021/09/home-gallery-6.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-6-240x300.png\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"home-gallery-6-75x94.png\";s:5:\"width\";i:75;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(421, 169, '_wp_attached_file', '2021/09/home-gallery-7.png'),
(422, 169, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:551;s:6:\"height\";i:690;s:4:\"file\";s:26:\"2021/09/home-gallery-7.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"home-gallery-7-240x300.png\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:24:\"home-gallery-7-75x94.png\";s:5:\"width\";i:75;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"home-gallery-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"home-gallery-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"home-gallery-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(423, 7, 'main_slider', '4'),
(424, 7, '_main_slider', 'field_6131169e930c9'),
(425, 7, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(426, 7, '_home_cats_title', 'field_6131171d5ce68'),
(427, 7, 'home_cats', 'a:6:{i:0;s:2:\"24\";i:1;s:2:\"21\";i:2;s:2:\"15\";i:3;s:2:\"20\";i:4;s:2:\"23\";i:5;s:2:\"22\";}'),
(428, 7, '_home_cats', 'field_613117265ce69'),
(429, 7, 'home_about_text', '<h2>היי, אני ענת!</h2>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(430, 7, '_home_about_text', 'field_6131188d5ce6b'),
(431, 7, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(432, 7, '_home_about_link', 'field_613118a45ce6c'),
(433, 7, 'home_about_img', '195'),
(434, 7, '_home_about_img', 'field_613118e75ce6d'),
(435, 7, 'home_prods_title_1', 'קולקציית הבשמים של ויקטוריה סיקרט'),
(436, 7, '_home_prods_title_1', 'field_6131194a0b837'),
(437, 7, 'home_prods_link_1', 'a:3:{s:5:\"title\";s:51:\"בואי להכיר את כל הבשמים שלנו\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(438, 7, '_home_prods_link_1', 'field_613119550b838'),
(439, 7, 'home_prods_1', 'a:4:{i:0;s:3:\"223\";i:1;s:3:\"237\";i:2;s:3:\"242\";i:3;s:3:\"244\";}'),
(440, 7, '_home_prods_1', 'field_613119670b839'),
(441, 7, 'banner_back_img', '208'),
(442, 7, '_banner_back_img', 'field_6131199a011ea'),
(443, 7, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(444, 7, '_banner_link', 'field_613119b1011eb'),
(445, 7, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(446, 7, '_banner_title', 'field_613119c1011ec'),
(447, 7, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(448, 7, '_banner_subtitle', 'field_613119ca011ed'),
(449, 7, 'home_prods_title_2', 'קולקציית פיג’מות ונעלי בית חדשה!'),
(450, 7, '_home_prods_title_2', 'field_613119f42cfea'),
(451, 7, 'home_prods_link_2', 'a:3:{s:5:\"title\";s:51:\"בואי להכיר את כל הקרמים שלנו\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(452, 7, '_home_prods_link_2', 'field_613119f72cfeb'),
(453, 7, 'home_prods_2', 'a:4:{i:0;s:3:\"246\";i:1;s:3:\"255\";i:2;s:3:\"256\";i:3;s:3:\"243\";}'),
(454, 7, '_home_prods_2', 'field_613119f92cfec'),
(455, 7, 'reviews_image', '213'),
(456, 7, '_reviews_image', 'field_61311a2f82d85'),
(457, 7, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(458, 7, '_reviews_title', 'field_61311a4482d86'),
(459, 7, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(460, 7, '_reviews_slider', 'field_61311a5782d87'),
(461, 7, 'home_slider_seo', '2'),
(462, 7, '_home_slider_seo', 'field_61311a92d1802'),
(463, 7, 'home_slider_img', '206'),
(464, 7, '_home_slider_img', 'field_61311a9fd1804'),
(465, 7, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(466, 7, '_home_gallery_img', 'field_61311b1cb4b73'),
(467, 7, 'home_inst_title', 'באינסטגרם שלי כבר ביקרת?'),
(468, 7, '_home_inst_title', 'field_61311b3eb4b74'),
(469, 7, 'home_inst_subtitle', 'עקבי והישארי מעודכנת!'),
(470, 7, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(471, 7, 'home_video_block_title', 'צפו בסרטונים שלנו'),
(472, 7, '_home_video_block_title', 'field_61311bbc1b54b'),
(473, 7, 'home_videos', '5'),
(474, 7, '_home_videos', 'field_61311bcd1b54c'),
(475, 7, 'slider_img', '207'),
(476, 7, '_slider_img', 'field_612f4e7693132'),
(477, 7, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(478, 7, '_faq_title', 'field_5feb2f82db93b'),
(479, 7, 'faq_img', '156'),
(480, 7, '_faq_img', 'field_5feb2f95db93c'),
(481, 7, 'faq_item', '4'),
(482, 7, '_faq_item', 'field_5feb2faddb93d'),
(483, 170, 'title_tag', ''),
(484, 170, '_title_tag', 'field_5ddbe7577e0e7'),
(485, 170, 'single_slider_seo', ''),
(486, 170, '_single_slider_seo', 'field_5ddbde5499115'),
(487, 170, 'single_gallery', ''),
(488, 170, '_single_gallery', 'field_5ddbe5aea4275'),
(489, 170, 'single_video_slider', ''),
(490, 170, '_single_video_slider', 'field_5ddbe636a4276'),
(491, 170, 'main_slider', ''),
(492, 170, '_main_slider', 'field_6131169e930c9'),
(493, 170, 'home_cats_title', ''),
(494, 170, '_home_cats_title', 'field_6131171d5ce68'),
(495, 170, 'home_cats', ''),
(496, 170, '_home_cats', 'field_613117265ce69'),
(497, 170, 'home_about_text', ''),
(498, 170, '_home_about_text', 'field_6131188d5ce6b'),
(499, 170, 'home_about_link', ''),
(500, 170, '_home_about_link', 'field_613118a45ce6c'),
(501, 170, 'home_about_img', ''),
(502, 170, '_home_about_img', 'field_613118e75ce6d'),
(503, 170, 'home_prods_title_1', ''),
(504, 170, '_home_prods_title_1', 'field_6131194a0b837'),
(505, 170, 'home_prods_link_1', ''),
(506, 170, '_home_prods_link_1', 'field_613119550b838'),
(507, 170, 'home_prods_1', ''),
(508, 170, '_home_prods_1', 'field_613119670b839'),
(509, 170, 'banner_back_img', ''),
(510, 170, '_banner_back_img', 'field_6131199a011ea'),
(511, 170, 'banner_link', ''),
(512, 170, '_banner_link', 'field_613119b1011eb'),
(513, 170, 'banner_title', ''),
(514, 170, '_banner_title', 'field_613119c1011ec'),
(515, 170, 'banner_subtitle', ''),
(516, 170, '_banner_subtitle', 'field_613119ca011ed'),
(517, 170, 'home_prods_title_2', ''),
(518, 170, '_home_prods_title_2', 'field_613119f42cfea'),
(519, 170, 'home_prods_link_2', ''),
(520, 170, '_home_prods_link_2', 'field_613119f72cfeb'),
(521, 170, 'home_prods_2', ''),
(522, 170, '_home_prods_2', 'field_613119f92cfec'),
(523, 170, 'reviews_image', ''),
(524, 170, '_reviews_image', 'field_61311a2f82d85'),
(525, 170, 'reviews_title', ''),
(526, 170, '_reviews_title', 'field_61311a4482d86'),
(527, 170, 'reviews_slider', ''),
(528, 170, '_reviews_slider', 'field_61311a5782d87'),
(529, 170, 'home_slider_seo', ''),
(530, 170, '_home_slider_seo', 'field_61311a92d1802'),
(531, 170, 'home_slider_img', ''),
(532, 170, '_home_slider_img', 'field_61311a9fd1804'),
(533, 170, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(534, 170, '_home_gallery_img', 'field_61311b1cb4b73'),
(535, 170, 'home_inst_title', ''),
(536, 170, '_home_inst_title', 'field_61311b3eb4b74'),
(537, 170, 'home_inst_subtitle', ''),
(538, 170, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(539, 170, 'home_video_block_title', ''),
(540, 170, '_home_video_block_title', 'field_61311bbc1b54b'),
(541, 170, 'home_videos', ''),
(542, 170, '_home_videos', 'field_61311bcd1b54c'),
(543, 170, 'slider_img', ''),
(544, 170, '_slider_img', 'field_612f4e7693132'),
(545, 170, 'faq_title', ''),
(546, 170, '_faq_title', 'field_5feb2f82db93b'),
(547, 170, 'faq_img', ''),
(548, 170, '_faq_img', 'field_5feb2f95db93c'),
(549, 170, 'faq_item', ''),
(550, 170, '_faq_item', 'field_5feb2faddb93d'),
(551, 171, '_edit_last', '1'),
(552, 171, '_thumbnail_id', '166'),
(554, 171, 'same_title', ''),
(555, 171, '_same_title', 'field_6130fbc198e01'),
(556, 171, 'same_posts', ''),
(557, 171, '_same_posts', 'field_6130fbd798e02'),
(558, 171, 'same_posts_link', ''),
(559, 171, '_same_posts_link', 'field_6130fbf0282c7'),
(560, 171, 'faq_title', ''),
(561, 171, '_faq_title', 'field_5feb2f82db93b'),
(562, 171, 'faq_img', ''),
(563, 171, '_faq_img', 'field_5feb2f95db93c'),
(564, 171, 'faq_item', ''),
(565, 171, '_faq_item', 'field_5feb2faddb93d'),
(566, 172, 'same_title', ''),
(567, 172, '_same_title', 'field_6130fbc198e01'),
(568, 172, 'same_posts', ''),
(569, 172, '_same_posts', 'field_6130fbd798e02'),
(570, 172, 'same_posts_link', ''),
(571, 172, '_same_posts_link', 'field_6130fbf0282c7'),
(572, 172, 'faq_title', ''),
(573, 172, '_faq_title', 'field_5feb2f82db93b'),
(574, 172, 'faq_img', ''),
(575, 172, '_faq_img', 'field_5feb2f95db93c'),
(576, 172, 'faq_item', ''),
(577, 172, '_faq_item', 'field_5feb2faddb93d'),
(578, 171, '_edit_lock', '1630740480:1'),
(579, 173, '_edit_last', '1'),
(580, 173, '_edit_lock', '1630740503:1'),
(581, 173, '_thumbnail_id', '167'),
(583, 173, 'same_title', ''),
(584, 173, '_same_title', 'field_6130fbc198e01'),
(585, 173, 'same_posts', ''),
(586, 173, '_same_posts', 'field_6130fbd798e02'),
(587, 173, 'same_posts_link', ''),
(588, 173, '_same_posts_link', 'field_6130fbf0282c7'),
(589, 173, 'faq_title', ''),
(590, 173, '_faq_title', 'field_5feb2f82db93b'),
(591, 173, 'faq_img', ''),
(592, 173, '_faq_img', 'field_5feb2f95db93c'),
(593, 173, 'faq_item', ''),
(594, 173, '_faq_item', 'field_5feb2faddb93d'),
(595, 174, 'same_title', ''),
(596, 174, '_same_title', 'field_6130fbc198e01'),
(597, 174, 'same_posts', ''),
(598, 174, '_same_posts', 'field_6130fbd798e02'),
(599, 174, 'same_posts_link', ''),
(600, 174, '_same_posts_link', 'field_6130fbf0282c7'),
(601, 174, 'faq_title', ''),
(602, 174, '_faq_title', 'field_5feb2f82db93b'),
(603, 174, 'faq_img', ''),
(604, 174, '_faq_img', 'field_5feb2f95db93c'),
(605, 174, 'faq_item', ''),
(606, 174, '_faq_item', 'field_5feb2faddb93d'),
(607, 175, '_edit_last', '1'),
(608, 175, '_edit_lock', '1630740528:1'),
(609, 175, '_thumbnail_id', '164'),
(611, 175, 'same_title', ''),
(612, 175, '_same_title', 'field_6130fbc198e01'),
(613, 175, 'same_posts', ''),
(614, 175, '_same_posts', 'field_6130fbd798e02'),
(615, 175, 'same_posts_link', ''),
(616, 175, '_same_posts_link', 'field_6130fbf0282c7'),
(617, 175, 'faq_title', ''),
(618, 175, '_faq_title', 'field_5feb2f82db93b'),
(619, 175, 'faq_img', ''),
(620, 175, '_faq_img', 'field_5feb2f95db93c'),
(621, 175, 'faq_item', ''),
(622, 175, '_faq_item', 'field_5feb2faddb93d'),
(623, 176, 'same_title', ''),
(624, 176, '_same_title', 'field_6130fbc198e01'),
(625, 176, 'same_posts', ''),
(626, 176, '_same_posts', 'field_6130fbd798e02'),
(627, 176, 'same_posts_link', ''),
(628, 176, '_same_posts_link', 'field_6130fbf0282c7'),
(629, 176, 'faq_title', ''),
(630, 176, '_faq_title', 'field_5feb2f82db93b'),
(631, 176, 'faq_img', ''),
(632, 176, '_faq_img', 'field_5feb2f95db93c'),
(633, 176, 'faq_item', ''),
(634, 176, '_faq_item', 'field_5feb2faddb93d'),
(635, 177, '_edit_last', '1'),
(636, 177, '_edit_lock', '1630740553:1'),
(637, 177, '_thumbnail_id', '154'),
(639, 177, 'same_title', ''),
(640, 177, '_same_title', 'field_6130fbc198e01'),
(641, 177, 'same_posts', ''),
(642, 177, '_same_posts', 'field_6130fbd798e02'),
(643, 177, 'same_posts_link', ''),
(644, 177, '_same_posts_link', 'field_6130fbf0282c7'),
(645, 177, 'faq_title', ''),
(646, 177, '_faq_title', 'field_5feb2f82db93b'),
(647, 177, 'faq_img', ''),
(648, 177, '_faq_img', 'field_5feb2f95db93c'),
(649, 177, 'faq_item', ''),
(650, 177, '_faq_item', 'field_5feb2faddb93d'),
(651, 178, 'same_title', ''),
(652, 178, '_same_title', 'field_6130fbc198e01'),
(653, 178, 'same_posts', ''),
(654, 178, '_same_posts', 'field_6130fbd798e02'),
(655, 178, 'same_posts_link', ''),
(656, 178, '_same_posts_link', 'field_6130fbf0282c7'),
(657, 178, 'faq_title', ''),
(658, 178, '_faq_title', 'field_5feb2f82db93b'),
(659, 178, 'faq_img', ''),
(660, 178, '_faq_img', 'field_5feb2f95db93c'),
(661, 178, 'faq_item', ''),
(662, 178, '_faq_item', 'field_5feb2faddb93d'),
(663, 179, '_edit_last', '1'),
(664, 179, '_edit_lock', '1630740576:1'),
(665, 179, '_thumbnail_id', '163'),
(667, 179, 'same_title', ''),
(668, 179, '_same_title', 'field_6130fbc198e01'),
(669, 179, 'same_posts', ''),
(670, 179, '_same_posts', 'field_6130fbd798e02'),
(671, 179, 'same_posts_link', ''),
(672, 179, '_same_posts_link', 'field_6130fbf0282c7'),
(673, 179, 'faq_title', ''),
(674, 179, '_faq_title', 'field_5feb2f82db93b'),
(675, 179, 'faq_img', ''),
(676, 179, '_faq_img', 'field_5feb2f95db93c'),
(677, 179, 'faq_item', ''),
(678, 179, '_faq_item', 'field_5feb2faddb93d'),
(679, 180, 'same_title', ''),
(680, 180, '_same_title', 'field_6130fbc198e01'),
(681, 180, 'same_posts', ''),
(682, 180, '_same_posts', 'field_6130fbd798e02'),
(683, 180, 'same_posts_link', ''),
(684, 180, '_same_posts_link', 'field_6130fbf0282c7'),
(685, 180, 'faq_title', ''),
(686, 180, '_faq_title', 'field_5feb2f82db93b'),
(687, 180, 'faq_img', ''),
(688, 180, '_faq_img', 'field_5feb2f95db93c'),
(689, 180, 'faq_item', ''),
(690, 180, '_faq_item', 'field_5feb2faddb93d'),
(691, 181, '_edit_last', '1'),
(692, 181, '_edit_lock', '1630740601:1'),
(693, 181, '_thumbnail_id', '153'),
(695, 181, 'same_title', ''),
(696, 181, '_same_title', 'field_6130fbc198e01'),
(697, 181, 'same_posts', ''),
(698, 181, '_same_posts', 'field_6130fbd798e02'),
(699, 181, 'same_posts_link', ''),
(700, 181, '_same_posts_link', 'field_6130fbf0282c7'),
(701, 181, 'faq_title', ''),
(702, 181, '_faq_title', 'field_5feb2f82db93b'),
(703, 181, 'faq_img', ''),
(704, 181, '_faq_img', 'field_5feb2f95db93c'),
(705, 181, 'faq_item', ''),
(706, 181, '_faq_item', 'field_5feb2faddb93d'),
(707, 182, 'same_title', ''),
(708, 182, '_same_title', 'field_6130fbc198e01'),
(709, 182, 'same_posts', ''),
(710, 182, '_same_posts', 'field_6130fbd798e02'),
(711, 182, 'same_posts_link', ''),
(712, 182, '_same_posts_link', 'field_6130fbf0282c7'),
(713, 182, 'faq_title', ''),
(714, 182, '_faq_title', 'field_5feb2f82db93b'),
(715, 182, 'faq_img', ''),
(716, 182, '_faq_img', 'field_5feb2f95db93c'),
(717, 182, 'faq_item', ''),
(718, 182, '_faq_item', 'field_5feb2faddb93d'),
(720, 183, 'same_title', ''),
(721, 183, '_same_title', 'field_6130fbc198e01'),
(722, 183, 'same_posts', 'a:3:{i:0;s:3:\"161\";i:1;s:3:\"171\";i:2;s:3:\"173\";}'),
(723, 183, '_same_posts', 'field_6130fbd798e02'),
(724, 183, 'same_posts_link', 'a:3:{s:5:\"title\";s:21:\"לכל המאמרים\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(725, 183, '_same_posts_link', 'field_6130fbf0282c7'),
(726, 183, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(727, 183, '_faq_title', 'field_5feb2f82db93b'),
(728, 183, 'faq_img', '156'),
(729, 183, '_faq_img', 'field_5feb2f95db93c'),
(730, 183, 'faq_item_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם 1?'),
(731, 183, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(732, 183, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(733, 183, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(734, 183, 'faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם 2?'),
(735, 183, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(736, 183, 'faq_item_1_faq_answer', 'דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(737, 183, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(738, 183, 'faq_item', '2'),
(739, 183, '_faq_item', 'field_5feb2faddb93d'),
(740, 11, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(741, 11, '_faq_title', 'field_5feb2f82db93b'),
(742, 11, 'faq_img', '156'),
(743, 11, '_faq_img', 'field_5feb2f95db93c'),
(744, 11, 'faq_item_0_faq_question', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח 1?'),
(745, 11, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(746, 11, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(747, 11, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(748, 11, 'faq_item_1_faq_question', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול 2?'),
(749, 11, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(750, 11, 'faq_item_1_faq_answer', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(751, 11, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(752, 11, 'faq_item_2_faq_question', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ 3?'),
(753, 11, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(754, 11, 'faq_item_2_faq_answer', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(755, 11, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(756, 11, 'faq_item', '3'),
(757, 11, '_faq_item', 'field_5feb2faddb93d'),
(758, 187, 'title_tag', ''),
(759, 187, '_title_tag', 'field_5ddbe7577e0e7'),
(760, 187, 'single_slider_seo', ''),
(761, 187, '_single_slider_seo', 'field_5ddbde5499115'),
(762, 187, 'single_gallery', ''),
(763, 187, '_single_gallery', 'field_5ddbe5aea4275'),
(764, 187, 'single_video_slider', ''),
(765, 187, '_single_video_slider', 'field_5ddbe636a4276'),
(766, 187, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(767, 187, '_faq_title', 'field_5feb2f82db93b'),
(768, 187, 'faq_img', '156'),
(769, 187, '_faq_img', 'field_5feb2f95db93c'),
(770, 187, 'faq_item_0_faq_question', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח 1?'),
(771, 187, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(772, 187, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(773, 187, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(774, 187, 'faq_item_1_faq_question', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול 2?'),
(775, 187, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(776, 187, 'faq_item_1_faq_answer', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(777, 187, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(778, 187, 'faq_item_2_faq_question', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ 3?'),
(779, 187, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(780, 187, 'faq_item_2_faq_answer', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(781, 187, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(782, 187, 'faq_item', '3'),
(783, 187, '_faq_item', 'field_5feb2faddb93d'),
(784, 188, '_edit_last', '1'),
(785, 188, '_edit_lock', '1630784975:1'),
(786, 195, '_wp_attached_file', '2021/09/about-img.png'),
(787, 195, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:679;s:6:\"height\";i:593;s:4:\"file\";s:21:\"2021/09/about-img.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"about-img-300x262.png\";s:5:\"width\";i:300;s:6:\"height\";i:262;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"about-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"about-img-108x94.png\";s:5:\"width\";i:108;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"about-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"about-img-600x524.png\";s:5:\"width\";i:600;s:6:\"height\";i:524;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"about-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"about-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"about-img-600x524.png\";s:5:\"width\";i:600;s:6:\"height\";i:524;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"about-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(788, 127, '_thumbnail_id', '195'),
(789, 127, 'img_or_video_0_video_link', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(790, 127, '_img_or_video_0_video_link', 'field_6133b8205b3df'),
(791, 127, 'img_or_video_1_image', '164'),
(792, 127, '_img_or_video_1_image', 'field_6133b7ee5b3dd'),
(793, 127, 'img_or_video_2_image', '165'),
(794, 127, '_img_or_video_2_image', 'field_6133b7ee5b3dd'),
(795, 127, 'img_or_video_3_image', '166'),
(796, 127, '_img_or_video_3_image', 'field_6133b7ee5b3dd'),
(797, 127, 'img_or_video_4_image', '167'),
(798, 127, '_img_or_video_4_image', 'field_6133b7ee5b3dd'),
(799, 127, 'img_or_video_5_image', '168'),
(800, 127, '_img_or_video_5_image', 'field_6133b7ee5b3dd'),
(801, 127, 'img_or_video_6_video_link', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(802, 127, '_img_or_video_6_video_link', 'field_6133b8205b3df'),
(803, 127, 'img_or_video', 'a:7:{i:0;s:9:\"gal_video\";i:1;s:7:\"gal_img\";i:2;s:7:\"gal_img\";i:3;s:7:\"gal_img\";i:4;s:7:\"gal_img\";i:5;s:7:\"gal_img\";i:6;s:9:\"gal_video\";}'),
(804, 127, '_img_or_video', 'field_6133b7b35b3dc'),
(805, 127, 'about_inst_title', 'באינסטגרם שלי כבר ביקרת?'),
(806, 127, '_about_inst_title', 'field_6133b847571f7'),
(807, 127, 'about_inst_subtitle', 'עקבי והישארי מעודכנת!'),
(808, 127, '_about_inst_subtitle', 'field_6133b875571f8'),
(809, 127, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(810, 127, '_faq_title', 'field_5feb2f82db93b'),
(811, 127, 'faq_img', ''),
(812, 127, '_faq_img', 'field_5feb2f95db93c'),
(813, 127, 'faq_item_0_faq_question', 'ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? \r\n'),
(814, 127, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(815, 127, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(816, 127, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(817, 127, 'faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 2?'),
(818, 127, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(819, 127, 'faq_item_1_faq_answer', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(820, 127, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(821, 127, 'faq_item_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 3?'),
(822, 127, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(823, 127, 'faq_item_2_faq_answer', 'עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(824, 127, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(825, 127, 'faq_item', '3'),
(826, 127, '_faq_item', 'field_5feb2faddb93d'),
(827, 197, 'single_slider_seo', ''),
(828, 197, '_single_slider_seo', 'field_5ddbde5499115'),
(829, 197, 'slider_img', ''),
(830, 197, '_slider_img', 'field_612f4e7693132'),
(831, 197, 'img_or_video_0_video_link', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(832, 197, '_img_or_video_0_video_link', 'field_6133b8205b3df'),
(833, 197, 'img_or_video_1_image', '164'),
(834, 197, '_img_or_video_1_image', 'field_6133b7ee5b3dd'),
(835, 197, 'img_or_video_2_image', '165'),
(836, 197, '_img_or_video_2_image', 'field_6133b7ee5b3dd'),
(837, 197, 'img_or_video_3_image', '166'),
(838, 197, '_img_or_video_3_image', 'field_6133b7ee5b3dd'),
(839, 197, 'img_or_video_4_image', '167'),
(840, 197, '_img_or_video_4_image', 'field_6133b7ee5b3dd'),
(841, 197, 'img_or_video_5_image', '168'),
(842, 197, '_img_or_video_5_image', 'field_6133b7ee5b3dd'),
(843, 197, 'img_or_video_6_video_link', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(844, 197, '_img_or_video_6_video_link', 'field_6133b8205b3df'),
(845, 197, 'img_or_video', 'a:7:{i:0;s:9:\"gal_video\";i:1;s:7:\"gal_img\";i:2;s:7:\"gal_img\";i:3;s:7:\"gal_img\";i:4;s:7:\"gal_img\";i:5;s:7:\"gal_img\";i:6;s:9:\"gal_video\";}');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(846, 197, '_img_or_video', 'field_6133b7b35b3dc'),
(847, 197, 'about_inst_title', ''),
(848, 197, '_about_inst_title', 'field_6133b847571f7'),
(849, 197, 'about_inst_subtitle', ''),
(850, 197, '_about_inst_subtitle', 'field_6133b875571f8'),
(851, 197, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(852, 197, '_faq_title', 'field_5feb2f82db93b'),
(853, 197, 'faq_img', ''),
(854, 197, '_faq_img', 'field_5feb2f95db93c'),
(855, 197, 'faq_item_0_faq_question', 'ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? \r\n'),
(856, 197, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(857, 197, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(858, 197, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(859, 197, 'faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 2?'),
(860, 197, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(861, 197, 'faq_item_1_faq_answer', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(862, 197, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(863, 197, 'faq_item_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 3?'),
(864, 197, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(865, 197, 'faq_item_2_faq_answer', 'עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(866, 197, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(867, 197, 'faq_item', '3'),
(868, 197, '_faq_item', 'field_5feb2faddb93d'),
(869, 198, 'single_slider_seo', ''),
(870, 198, '_single_slider_seo', 'field_5ddbde5499115'),
(871, 198, 'slider_img', ''),
(872, 198, '_slider_img', 'field_612f4e7693132'),
(873, 198, 'img_or_video_0_video_link', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(874, 198, '_img_or_video_0_video_link', 'field_6133b8205b3df'),
(875, 198, 'img_or_video_1_image', '164'),
(876, 198, '_img_or_video_1_image', 'field_6133b7ee5b3dd'),
(877, 198, 'img_or_video_2_image', '165'),
(878, 198, '_img_or_video_2_image', 'field_6133b7ee5b3dd'),
(879, 198, 'img_or_video_3_image', '166'),
(880, 198, '_img_or_video_3_image', 'field_6133b7ee5b3dd'),
(881, 198, 'img_or_video_4_image', '167'),
(882, 198, '_img_or_video_4_image', 'field_6133b7ee5b3dd'),
(883, 198, 'img_or_video_5_image', '168'),
(884, 198, '_img_or_video_5_image', 'field_6133b7ee5b3dd'),
(885, 198, 'img_or_video_6_video_link', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(886, 198, '_img_or_video_6_video_link', 'field_6133b8205b3df'),
(887, 198, 'img_or_video', 'a:7:{i:0;s:9:\"gal_video\";i:1;s:7:\"gal_img\";i:2;s:7:\"gal_img\";i:3;s:7:\"gal_img\";i:4;s:7:\"gal_img\";i:5;s:7:\"gal_img\";i:6;s:9:\"gal_video\";}'),
(888, 198, '_img_or_video', 'field_6133b7b35b3dc'),
(889, 198, 'about_inst_title', ''),
(890, 198, '_about_inst_title', 'field_6133b847571f7'),
(891, 198, 'about_inst_subtitle', ''),
(892, 198, '_about_inst_subtitle', 'field_6133b875571f8'),
(893, 198, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(894, 198, '_faq_title', 'field_5feb2f82db93b'),
(895, 198, 'faq_img', ''),
(896, 198, '_faq_img', 'field_5feb2f95db93c'),
(897, 198, 'faq_item_0_faq_question', 'ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? \r\n'),
(898, 198, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(899, 198, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(900, 198, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(901, 198, 'faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 2?'),
(902, 198, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(903, 198, 'faq_item_1_faq_answer', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(904, 198, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(905, 198, 'faq_item_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 3?'),
(906, 198, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(907, 198, 'faq_item_2_faq_answer', 'עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(908, 198, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(909, 198, 'faq_item', '3'),
(910, 198, '_faq_item', 'field_5feb2faddb93d'),
(911, 199, '_menu_item_type', 'post_type'),
(912, 199, '_menu_item_menu_item_parent', '0'),
(913, 199, '_menu_item_object_id', '127'),
(914, 199, '_menu_item_object', 'page'),
(915, 199, '_menu_item_target', ''),
(916, 199, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(917, 199, '_menu_item_xfn', ''),
(918, 199, '_menu_item_url', ''),
(920, 120, '_wp_old_date', '2021-09-02'),
(921, 121, '_wp_old_date', '2021-09-02'),
(922, 122, '_wp_old_date', '2021-09-02'),
(923, 123, '_wp_old_date', '2021-09-02'),
(924, 36, '_edit_lock', '1630780170:1'),
(925, 36, '_edit_last', '1'),
(926, 36, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(927, 36, '_faq_title', 'field_5feb2f82db93b'),
(928, 36, 'faq_img', '156'),
(929, 36, '_faq_img', 'field_5feb2f95db93c'),
(930, 36, 'faq_item', '4'),
(931, 36, '_faq_item', 'field_5feb2faddb93d'),
(932, 200, 'faq_title', ''),
(933, 200, '_faq_title', 'field_5feb2f82db93b'),
(934, 200, 'faq_img', ''),
(935, 200, '_faq_img', 'field_5feb2f95db93c'),
(936, 200, 'faq_item', ''),
(937, 200, '_faq_item', 'field_5feb2faddb93d'),
(938, 36, 'faq_item_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה?'),
(939, 36, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(940, 36, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(941, 36, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(942, 36, 'faq_item_1_faq_question', 'קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי?'),
(943, 36, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(944, 36, 'faq_item_1_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(945, 36, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(946, 36, 'faq_item_2_faq_question', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח?'),
(947, 36, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(948, 36, 'faq_item_2_faq_answer', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(949, 36, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(950, 36, 'faq_item_3_faq_question', 'להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך?'),
(951, 36, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(952, 36, 'faq_item_3_faq_answer', 'ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nלהאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך.'),
(953, 36, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(954, 201, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(955, 201, '_faq_title', 'field_5feb2f82db93b'),
(956, 201, 'faq_img', '156'),
(957, 201, '_faq_img', 'field_5feb2f95db93c'),
(958, 201, 'faq_item', '4'),
(959, 201, '_faq_item', 'field_5feb2faddb93d'),
(960, 201, 'faq_item_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה?'),
(961, 201, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(962, 201, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(963, 201, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(964, 201, 'faq_item_1_faq_question', 'קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי?'),
(965, 201, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(966, 201, 'faq_item_1_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(967, 201, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(968, 201, 'faq_item_2_faq_question', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח?'),
(969, 201, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(970, 201, 'faq_item_2_faq_answer', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(971, 201, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(972, 201, 'faq_item_3_faq_question', 'להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך?'),
(973, 201, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(974, 201, 'faq_item_3_faq_answer', 'ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nלהאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך.'),
(975, 201, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(976, 127, 'about_gallery_text', '<h2>מזמינה אתכם לבקר בדף האינטסגרם שלי!</h2>\r\nבואו לעקוב אחרי ולהחשף לכל המוצרים, המבצעים וכל מה שחדש!'),
(977, 127, '_about_gallery_text', 'field_6133ce46dd6b8'),
(978, 204, 'single_slider_seo', ''),
(979, 204, '_single_slider_seo', 'field_5ddbde5499115'),
(980, 204, 'slider_img', ''),
(981, 204, '_slider_img', 'field_612f4e7693132'),
(982, 204, 'img_or_video_0_video_link', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(983, 204, '_img_or_video_0_video_link', 'field_6133b8205b3df'),
(984, 204, 'img_or_video_1_image', '164'),
(985, 204, '_img_or_video_1_image', 'field_6133b7ee5b3dd'),
(986, 204, 'img_or_video_2_image', '165'),
(987, 204, '_img_or_video_2_image', 'field_6133b7ee5b3dd'),
(988, 204, 'img_or_video_3_image', '166'),
(989, 204, '_img_or_video_3_image', 'field_6133b7ee5b3dd'),
(990, 204, 'img_or_video_4_image', '167'),
(991, 204, '_img_or_video_4_image', 'field_6133b7ee5b3dd'),
(992, 204, 'img_or_video_5_image', '168'),
(993, 204, '_img_or_video_5_image', 'field_6133b7ee5b3dd'),
(994, 204, 'img_or_video_6_video_link', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(995, 204, '_img_or_video_6_video_link', 'field_6133b8205b3df'),
(996, 204, 'img_or_video', 'a:7:{i:0;s:9:\"gal_video\";i:1;s:7:\"gal_img\";i:2;s:7:\"gal_img\";i:3;s:7:\"gal_img\";i:4;s:7:\"gal_img\";i:5;s:7:\"gal_img\";i:6;s:9:\"gal_video\";}'),
(997, 204, '_img_or_video', 'field_6133b7b35b3dc'),
(998, 204, 'about_inst_title', ''),
(999, 204, '_about_inst_title', 'field_6133b847571f7'),
(1000, 204, 'about_inst_subtitle', ''),
(1001, 204, '_about_inst_subtitle', 'field_6133b875571f8'),
(1002, 204, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(1003, 204, '_faq_title', 'field_5feb2f82db93b'),
(1004, 204, 'faq_img', ''),
(1005, 204, '_faq_img', 'field_5feb2f95db93c'),
(1006, 204, 'faq_item_0_faq_question', 'ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? \r\n'),
(1007, 204, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1008, 204, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1009, 204, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1010, 204, 'faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 2?'),
(1011, 204, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1012, 204, 'faq_item_1_faq_answer', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1013, 204, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1014, 204, 'faq_item_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 3?'),
(1015, 204, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1016, 204, 'faq_item_2_faq_answer', 'עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1017, 204, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1018, 204, 'faq_item', '3'),
(1019, 204, '_faq_item', 'field_5feb2faddb93d'),
(1020, 204, 'about_gallery_text', '<h2>מזמינה אתכם לבקר בדף האינטסגרם שלי!</h2>\r\nבואו לעקוב אחרי ולהחשף לכל המוצרים, המבצעים וכל מה שחדש!'),
(1021, 204, '_about_gallery_text', 'field_6133ce46dd6b8'),
(1022, 205, 'single_slider_seo', ''),
(1023, 205, '_single_slider_seo', 'field_5ddbde5499115'),
(1024, 205, 'slider_img', ''),
(1025, 205, '_slider_img', 'field_612f4e7693132'),
(1026, 205, 'img_or_video_0_video_link', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(1027, 205, '_img_or_video_0_video_link', 'field_6133b8205b3df'),
(1028, 205, 'img_or_video_1_image', '164'),
(1029, 205, '_img_or_video_1_image', 'field_6133b7ee5b3dd'),
(1030, 205, 'img_or_video_2_image', '165'),
(1031, 205, '_img_or_video_2_image', 'field_6133b7ee5b3dd'),
(1032, 205, 'img_or_video_3_image', '166'),
(1033, 205, '_img_or_video_3_image', 'field_6133b7ee5b3dd'),
(1034, 205, 'img_or_video_4_image', '167'),
(1035, 205, '_img_or_video_4_image', 'field_6133b7ee5b3dd'),
(1036, 205, 'img_or_video_5_image', '168'),
(1037, 205, '_img_or_video_5_image', 'field_6133b7ee5b3dd'),
(1038, 205, 'img_or_video_6_video_link', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(1039, 205, '_img_or_video_6_video_link', 'field_6133b8205b3df'),
(1040, 205, 'img_or_video', 'a:7:{i:0;s:9:\"gal_video\";i:1;s:7:\"gal_img\";i:2;s:7:\"gal_img\";i:3;s:7:\"gal_img\";i:4;s:7:\"gal_img\";i:5;s:7:\"gal_img\";i:6;s:9:\"gal_video\";}'),
(1041, 205, '_img_or_video', 'field_6133b7b35b3dc'),
(1042, 205, 'about_inst_title', 'באינסטגרם שלי כבר ביקרת?'),
(1043, 205, '_about_inst_title', 'field_6133b847571f7'),
(1044, 205, 'about_inst_subtitle', 'עקבי והישארי מעודכנת!'),
(1045, 205, '_about_inst_subtitle', 'field_6133b875571f8'),
(1046, 205, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(1047, 205, '_faq_title', 'field_5feb2f82db93b'),
(1048, 205, 'faq_img', ''),
(1049, 205, '_faq_img', 'field_5feb2f95db93c'),
(1050, 205, 'faq_item_0_faq_question', 'ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? \r\n'),
(1051, 205, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1052, 205, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1053, 205, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1054, 205, 'faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 2?'),
(1055, 205, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1056, 205, 'faq_item_1_faq_answer', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1057, 205, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1058, 205, 'faq_item_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית 3?'),
(1059, 205, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1060, 205, 'faq_item_2_faq_answer', 'עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1061, 205, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1062, 205, 'faq_item', '3'),
(1063, 205, '_faq_item', 'field_5feb2faddb93d'),
(1064, 205, 'about_gallery_text', '<h2>מזמינה אתכם לבקר בדף האינטסגרם שלי!</h2>\r\nבואו לעקוב אחרי ולהחשף לכל המוצרים, המבצעים וכל מה שחדש!'),
(1065, 205, '_about_gallery_text', 'field_6133ce46dd6b8'),
(1066, 206, '_wp_attached_file', '2021/09/slider-1.png'),
(1067, 206, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:627;s:6:\"height\";i:772;s:4:\"file\";s:20:\"2021/09/slider-1.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"slider-1-244x300.png\";s:5:\"width\";i:244;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"slider-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"slider-1-76x94.png\";s:5:\"width\";i:76;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"slider-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"slider-1-600x739.png\";s:5:\"width\";i:600;s:6:\"height\";i:739;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"slider-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"slider-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"slider-1-600x739.png\";s:5:\"width\";i:600;s:6:\"height\";i:739;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"slider-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1068, 207, '_wp_attached_file', '2021/09/slider-2.png'),
(1069, 207, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:619;s:6:\"height\";i:586;s:4:\"file\";s:20:\"2021/09/slider-2.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"slider-2-300x284.png\";s:5:\"width\";i:300;s:6:\"height\";i:284;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"slider-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"slider-2-99x94.png\";s:5:\"width\";i:99;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"slider-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"slider-2-600x568.png\";s:5:\"width\";i:600;s:6:\"height\";i:568;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"slider-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"slider-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"slider-2-600x568.png\";s:5:\"width\";i:600;s:6:\"height\";i:568;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"slider-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1070, 208, '_wp_attached_file', '2021/09/banner-e1630797529336.png'),
(1071, 208, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1035;s:6:\"height\";i:510;s:4:\"file\";s:33:\"2021/09/banner-e1630797529336.png\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"banner-e1630797529336-300x148.png\";s:5:\"width\";i:300;s:6:\"height\";i:148;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"banner-e1630797529336-1024x505.png\";s:5:\"width\";i:1024;s:6:\"height\";i:505;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"banner-e1630797529336-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"banner-e1630797529336-768x378.png\";s:5:\"width\";i:768;s:6:\"height\";i:378;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"banner-e1630797529336-134x66.png\";s:5:\"width\";i:134;s:6:\"height\";i:66;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"banner-e1630797529336-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"banner-e1630797529336-600x296.png\";s:5:\"width\";i:600;s:6:\"height\";i:296;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"banner-e1630797529336-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"banner-e1630797529336-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"banner-e1630797529336-600x296.png\";s:5:\"width\";i:600;s:6:\"height\";i:296;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"banner-e1630797529336-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1072, 208, '_wp_attachment_backup_sizes', 'a:12:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:1399;s:6:\"height\";i:513;s:4:\"file\";s:10:\"banner.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"banner-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"banner-300x110.png\";s:5:\"width\";i:300;s:6:\"height\";i:110;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"medium_large-orig\";a:4:{s:4:\"file\";s:18:\"banner-768x282.png\";s:5:\"width\";i:768;s:6:\"height\";i:282;s:9:\"mime-type\";s:9:\"image/png\";}s:10:\"large-orig\";a:4:{s:4:\"file\";s:19:\"banner-1024x375.png\";s:5:\"width\";i:1024;s:6:\"height\";i:375;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"banner-134x49.png\";s:5:\"width\";i:134;s:6:\"height\";i:49;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"banner-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"banner-600x220.png\";s:5:\"width\";i:600;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"banner-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"banner-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"banner-600x220.png\";s:5:\"width\";i:600;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"banner-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(1073, 209, '_wp_attached_file', '2021/09/slide-1.jpeg'),
(1074, 209, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:312;s:4:\"file\";s:20:\"2021/09/slide-1.jpeg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"slide-1-300x125.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-1-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"slide-1-134x56.jpeg\";s:5:\"width\";i:134;s:6:\"height\";i:56;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"slide-1-300x300.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"slide-1-600x250.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-1-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"slide-1-300x300.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"slide-1-600x250.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-1-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1075, 210, '_wp_attached_file', '2021/09/slide-2.jpeg'),
(1076, 210, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:312;s:4:\"file\";s:20:\"2021/09/slide-2.jpeg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"slide-2-300x125.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-2-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"slide-2-134x56.jpeg\";s:5:\"width\";i:134;s:6:\"height\";i:56;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"slide-2-300x300.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"slide-2-600x250.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-2-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"slide-2-300x300.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"slide-2-600x250.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-2-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1077, 211, '_wp_attached_file', '2021/09/slide-3.jpeg'),
(1078, 211, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:312;s:4:\"file\";s:20:\"2021/09/slide-3.jpeg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"slide-3-300x125.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-3-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"slide-3-134x56.jpeg\";s:5:\"width\";i:134;s:6:\"height\";i:56;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"slide-3-300x300.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"slide-3-600x250.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-3-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"slide-3-300x300.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"slide-3-600x250.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-3-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1079, 212, '_wp_attached_file', '2021/09/slide-5.jpeg'),
(1080, 212, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:312;s:4:\"file\";s:20:\"2021/09/slide-5.jpeg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"slide-5-300x125.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-5-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"slide-5-134x56.jpeg\";s:5:\"width\";i:134;s:6:\"height\";i:56;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"slide-5-300x300.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"slide-5-600x250.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-5-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"slide-5-300x300.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"slide-5-600x250.jpeg\";s:5:\"width\";i:600;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-5-100x100.jpeg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1081, 213, '_wp_attached_file', '2021/09/reviews.png'),
(1082, 213, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:379;s:6:\"height\";i:562;s:4:\"file\";s:19:\"2021/09/reviews.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"reviews-202x300.png\";s:5:\"width\";i:202;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"reviews-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:17:\"reviews-63x94.png\";s:5:\"width\";i:63;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"reviews-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"reviews-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"reviews-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"reviews-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1083, 214, '_wp_attached_file', '2021/09/review-item.png'),
(1084, 214, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:598;s:6:\"height\";i:298;s:4:\"file\";s:23:\"2021/09/review-item.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"review-item-300x149.png\";s:5:\"width\";i:300;s:6:\"height\";i:149;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"review-item-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"review-item-134x67.png\";s:5:\"width\";i:134;s:6:\"height\";i:67;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"review-item-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"review-item-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"review-item-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"review-item-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1085, 215, '_wp_attached_file', '2021/09/review-item–2.png'),
(1086, 215, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:598;s:6:\"height\";i:298;s:4:\"file\";s:27:\"2021/09/review-item–2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"review-item–2-300x149.png\";s:5:\"width\";i:300;s:6:\"height\";i:149;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"review-item–2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:26:\"review-item–2-134x67.png\";s:5:\"width\";i:134;s:6:\"height\";i:67;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"review-item–2-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"review-item–2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"review-item–2-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"review-item–2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1087, 7, 'main_slider_0_main_img', '212'),
(1088, 7, '_main_slider_0_main_img', 'field_613116b2930ca'),
(1089, 7, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(1090, 7, '_main_slider_0_main_title', 'field_613116c8930cb'),
(1091, 7, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1092, 7, '_main_slider_0_main_link', 'field_613116ee930cc'),
(1093, 7, 'main_slider_1_main_img', '209'),
(1094, 7, '_main_slider_1_main_img', 'field_613116b2930ca'),
(1095, 7, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(1096, 7, '_main_slider_1_main_title', 'field_613116c8930cb'),
(1097, 7, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1098, 7, '_main_slider_1_main_link', 'field_613116ee930cc'),
(1099, 7, 'main_slider_2_main_img', '210'),
(1100, 7, '_main_slider_2_main_img', 'field_613116b2930ca'),
(1101, 7, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(1102, 7, '_main_slider_2_main_title', 'field_613116c8930cb'),
(1103, 7, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1104, 7, '_main_slider_2_main_link', 'field_613116ee930cc'),
(1105, 7, 'main_slider_3_main_img', '211'),
(1106, 7, '_main_slider_3_main_img', 'field_613116b2930ca'),
(1107, 7, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(1108, 7, '_main_slider_3_main_title', 'field_613116c8930cb'),
(1109, 7, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1110, 7, '_main_slider_3_main_link', 'field_613116ee930cc'),
(1111, 7, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1112, 7, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(1113, 7, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1114, 7, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(1115, 7, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1116, 7, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1117, 7, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1118, 7, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1119, 7, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(1120, 7, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1121, 7, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(1122, 7, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1123, 7, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1124, 7, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1125, 7, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1126, 7, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1127, 7, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(1128, 7, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1129, 7, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1130, 7, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1131, 7, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(1132, 7, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(1133, 7, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1134, 7, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(1135, 216, 'title_tag', ''),
(1136, 216, '_title_tag', 'field_5ddbe7577e0e7'),
(1137, 216, 'single_slider_seo', '2'),
(1138, 216, '_single_slider_seo', 'field_5ddbde5499115'),
(1139, 216, 'single_gallery', ''),
(1140, 216, '_single_gallery', 'field_5ddbe5aea4275'),
(1141, 216, 'single_video_slider', ''),
(1142, 216, '_single_video_slider', 'field_5ddbe636a4276'),
(1143, 216, 'main_slider', '4'),
(1144, 216, '_main_slider', 'field_6131169e930c9'),
(1145, 216, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(1146, 216, '_home_cats_title', 'field_6131171d5ce68'),
(1147, 216, 'home_cats', 'a:5:{i:0;s:2:\"21\";i:1;s:2:\"15\";i:2;s:2:\"20\";i:3;s:2:\"23\";i:4;s:2:\"22\";}'),
(1148, 216, '_home_cats', 'field_613117265ce69'),
(1149, 216, 'home_about_text', 'ואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(1150, 216, '_home_about_text', 'field_6131188d5ce6b'),
(1151, 216, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1152, 216, '_home_about_link', 'field_613118a45ce6c'),
(1153, 216, 'home_about_img', ''),
(1154, 216, '_home_about_img', 'field_613118e75ce6d'),
(1155, 216, 'home_prods_title_1', ''),
(1156, 216, '_home_prods_title_1', 'field_6131194a0b837'),
(1157, 216, 'home_prods_link_1', ''),
(1158, 216, '_home_prods_link_1', 'field_613119550b838'),
(1159, 216, 'home_prods_1', ''),
(1160, 216, '_home_prods_1', 'field_613119670b839'),
(1161, 216, 'banner_back_img', '208'),
(1162, 216, '_banner_back_img', 'field_6131199a011ea'),
(1163, 216, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1164, 216, '_banner_link', 'field_613119b1011eb'),
(1165, 216, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(1166, 216, '_banner_title', 'field_613119c1011ec'),
(1167, 216, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(1168, 216, '_banner_subtitle', 'field_613119ca011ed'),
(1169, 216, 'home_prods_title_2', ''),
(1170, 216, '_home_prods_title_2', 'field_613119f42cfea'),
(1171, 216, 'home_prods_link_2', ''),
(1172, 216, '_home_prods_link_2', 'field_613119f72cfeb'),
(1173, 216, 'home_prods_2', ''),
(1174, 216, '_home_prods_2', 'field_613119f92cfec'),
(1175, 216, 'reviews_image', '213'),
(1176, 216, '_reviews_image', 'field_61311a2f82d85'),
(1177, 216, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(1178, 216, '_reviews_title', 'field_61311a4482d86'),
(1179, 216, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(1180, 216, '_reviews_slider', 'field_61311a5782d87'),
(1181, 216, 'home_slider_seo', '2'),
(1182, 216, '_home_slider_seo', 'field_61311a92d1802'),
(1183, 216, 'home_slider_img', '206'),
(1184, 216, '_home_slider_img', 'field_61311a9fd1804'),
(1185, 216, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(1186, 216, '_home_gallery_img', 'field_61311b1cb4b73'),
(1187, 216, 'home_inst_title', ''),
(1188, 216, '_home_inst_title', 'field_61311b3eb4b74'),
(1189, 216, 'home_inst_subtitle', ''),
(1190, 216, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(1191, 216, 'home_video_block_title', ''),
(1192, 216, '_home_video_block_title', 'field_61311bbc1b54b'),
(1193, 216, 'home_videos', ''),
(1194, 216, '_home_videos', 'field_61311bcd1b54c'),
(1195, 216, 'slider_img', '207'),
(1196, 216, '_slider_img', 'field_612f4e7693132'),
(1197, 216, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(1198, 216, '_faq_title', 'field_5feb2f82db93b'),
(1199, 216, 'faq_img', '156'),
(1200, 216, '_faq_img', 'field_5feb2f95db93c'),
(1201, 216, 'faq_item', '4'),
(1202, 216, '_faq_item', 'field_5feb2faddb93d'),
(1203, 216, 'main_slider_0_main_img', '212'),
(1204, 216, '_main_slider_0_main_img', 'field_613116b2930ca'),
(1205, 216, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(1206, 216, '_main_slider_0_main_title', 'field_613116c8930cb'),
(1207, 216, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1208, 216, '_main_slider_0_main_link', 'field_613116ee930cc'),
(1209, 216, 'main_slider_1_main_img', '209'),
(1210, 216, '_main_slider_1_main_img', 'field_613116b2930ca'),
(1211, 216, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(1212, 216, '_main_slider_1_main_title', 'field_613116c8930cb'),
(1213, 216, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1214, 216, '_main_slider_1_main_link', 'field_613116ee930cc'),
(1215, 216, 'main_slider_2_main_img', '210'),
(1216, 216, '_main_slider_2_main_img', 'field_613116b2930ca'),
(1217, 216, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(1218, 216, '_main_slider_2_main_title', 'field_613116c8930cb'),
(1219, 216, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1220, 216, '_main_slider_2_main_link', 'field_613116ee930cc'),
(1221, 216, 'main_slider_3_main_img', '211'),
(1222, 216, '_main_slider_3_main_img', 'field_613116b2930ca'),
(1223, 216, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(1224, 216, '_main_slider_3_main_title', 'field_613116c8930cb'),
(1225, 216, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1226, 216, '_main_slider_3_main_link', 'field_613116ee930cc'),
(1227, 216, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1228, 216, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(1229, 216, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1230, 216, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(1231, 216, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1232, 216, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1233, 216, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1234, 216, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1235, 216, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(1236, 216, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1237, 216, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(1238, 216, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1239, 216, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?'),
(1240, 216, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1241, 216, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1242, 216, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1243, 216, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(1244, 216, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1245, 216, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1246, 216, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1247, 216, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(1248, 216, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(1249, 216, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1250, 216, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(1251, 7, 'home_videos_0_video_item', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(1252, 7, '_home_videos_0_video_item', 'field_61311bd61b54d'),
(1253, 217, 'title_tag', ''),
(1254, 217, '_title_tag', 'field_5ddbe7577e0e7'),
(1255, 217, 'single_slider_seo', '2'),
(1256, 217, '_single_slider_seo', 'field_5ddbde5499115'),
(1257, 217, 'single_gallery', ''),
(1258, 217, '_single_gallery', 'field_5ddbe5aea4275'),
(1259, 217, 'single_video_slider', ''),
(1260, 217, '_single_video_slider', 'field_5ddbe636a4276'),
(1261, 217, 'main_slider', '4'),
(1262, 217, '_main_slider', 'field_6131169e930c9'),
(1263, 217, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(1264, 217, '_home_cats_title', 'field_6131171d5ce68'),
(1265, 217, 'home_cats', 'a:5:{i:0;s:2:\"21\";i:1;s:2:\"15\";i:2;s:2:\"20\";i:3;s:2:\"23\";i:4;s:2:\"22\";}'),
(1266, 217, '_home_cats', 'field_613117265ce69'),
(1267, 217, 'home_about_text', 'ואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(1268, 217, '_home_about_text', 'field_6131188d5ce6b'),
(1269, 217, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1270, 217, '_home_about_link', 'field_613118a45ce6c'),
(1271, 217, 'home_about_img', ''),
(1272, 217, '_home_about_img', 'field_613118e75ce6d'),
(1273, 217, 'home_prods_title_1', ''),
(1274, 217, '_home_prods_title_1', 'field_6131194a0b837'),
(1275, 217, 'home_prods_link_1', ''),
(1276, 217, '_home_prods_link_1', 'field_613119550b838'),
(1277, 217, 'home_prods_1', ''),
(1278, 217, '_home_prods_1', 'field_613119670b839'),
(1279, 217, 'banner_back_img', '208'),
(1280, 217, '_banner_back_img', 'field_6131199a011ea'),
(1281, 217, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1282, 217, '_banner_link', 'field_613119b1011eb'),
(1283, 217, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(1284, 217, '_banner_title', 'field_613119c1011ec'),
(1285, 217, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(1286, 217, '_banner_subtitle', 'field_613119ca011ed'),
(1287, 217, 'home_prods_title_2', ''),
(1288, 217, '_home_prods_title_2', 'field_613119f42cfea'),
(1289, 217, 'home_prods_link_2', ''),
(1290, 217, '_home_prods_link_2', 'field_613119f72cfeb'),
(1291, 217, 'home_prods_2', ''),
(1292, 217, '_home_prods_2', 'field_613119f92cfec'),
(1293, 217, 'reviews_image', '213'),
(1294, 217, '_reviews_image', 'field_61311a2f82d85'),
(1295, 217, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(1296, 217, '_reviews_title', 'field_61311a4482d86'),
(1297, 217, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(1298, 217, '_reviews_slider', 'field_61311a5782d87'),
(1299, 217, 'home_slider_seo', '2'),
(1300, 217, '_home_slider_seo', 'field_61311a92d1802'),
(1301, 217, 'home_slider_img', '206'),
(1302, 217, '_home_slider_img', 'field_61311a9fd1804'),
(1303, 217, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(1304, 217, '_home_gallery_img', 'field_61311b1cb4b73'),
(1305, 217, 'home_inst_title', ''),
(1306, 217, '_home_inst_title', 'field_61311b3eb4b74'),
(1307, 217, 'home_inst_subtitle', ''),
(1308, 217, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(1309, 217, 'home_video_block_title', 'צפו בסרטונים שלנו'),
(1310, 217, '_home_video_block_title', 'field_61311bbc1b54b'),
(1311, 217, 'home_videos', '1'),
(1312, 217, '_home_videos', 'field_61311bcd1b54c'),
(1313, 217, 'slider_img', '207'),
(1314, 217, '_slider_img', 'field_612f4e7693132'),
(1315, 217, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(1316, 217, '_faq_title', 'field_5feb2f82db93b'),
(1317, 217, 'faq_img', '156'),
(1318, 217, '_faq_img', 'field_5feb2f95db93c'),
(1319, 217, 'faq_item', '4'),
(1320, 217, '_faq_item', 'field_5feb2faddb93d'),
(1321, 217, 'main_slider_0_main_img', '212'),
(1322, 217, '_main_slider_0_main_img', 'field_613116b2930ca'),
(1323, 217, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(1324, 217, '_main_slider_0_main_title', 'field_613116c8930cb'),
(1325, 217, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1326, 217, '_main_slider_0_main_link', 'field_613116ee930cc'),
(1327, 217, 'main_slider_1_main_img', '209'),
(1328, 217, '_main_slider_1_main_img', 'field_613116b2930ca'),
(1329, 217, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(1330, 217, '_main_slider_1_main_title', 'field_613116c8930cb'),
(1331, 217, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1332, 217, '_main_slider_1_main_link', 'field_613116ee930cc'),
(1333, 217, 'main_slider_2_main_img', '210'),
(1334, 217, '_main_slider_2_main_img', 'field_613116b2930ca'),
(1335, 217, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(1336, 217, '_main_slider_2_main_title', 'field_613116c8930cb'),
(1337, 217, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1338, 217, '_main_slider_2_main_link', 'field_613116ee930cc'),
(1339, 217, 'main_slider_3_main_img', '211'),
(1340, 217, '_main_slider_3_main_img', 'field_613116b2930ca'),
(1341, 217, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(1342, 217, '_main_slider_3_main_title', 'field_613116c8930cb'),
(1343, 217, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1344, 217, '_main_slider_3_main_link', 'field_613116ee930cc'),
(1345, 217, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1346, 217, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(1347, 217, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1348, 217, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(1349, 217, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1350, 217, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1351, 217, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1352, 217, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1353, 217, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(1354, 217, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1355, 217, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(1356, 217, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1357, 217, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?'),
(1358, 217, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1359, 217, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1360, 217, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1361, 217, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(1362, 217, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1363, 217, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1364, 217, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1365, 217, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(1366, 217, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(1367, 217, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1368, 217, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(1369, 217, 'home_videos_0_video_item', ''),
(1370, 217, '_home_videos_0_video_item', 'field_61311bd61b54d'),
(1371, 7, 'home_videos_1_video_item', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(1372, 7, '_home_videos_1_video_item', 'field_61311bd61b54d'),
(1373, 7, 'home_videos_2_video_item', 'https://www.youtube.com/watch?v=QBSDTRfqkiQ'),
(1374, 7, '_home_videos_2_video_item', 'field_61311bd61b54d'),
(1375, 7, 'home_videos_3_video_item', 'https://www.youtube.com/watch?v=sOdJMGVFYAg'),
(1376, 7, '_home_videos_3_video_item', 'field_61311bd61b54d'),
(1377, 7, 'home_videos_4_video_item', 'https://www.youtube.com/watch?v=ne_8xYNnFJA'),
(1378, 7, '_home_videos_4_video_item', 'field_61311bd61b54d'),
(1379, 218, 'title_tag', ''),
(1380, 218, '_title_tag', 'field_5ddbe7577e0e7'),
(1381, 218, 'single_slider_seo', '2'),
(1382, 218, '_single_slider_seo', 'field_5ddbde5499115'),
(1383, 218, 'single_gallery', ''),
(1384, 218, '_single_gallery', 'field_5ddbe5aea4275'),
(1385, 218, 'single_video_slider', ''),
(1386, 218, '_single_video_slider', 'field_5ddbe636a4276'),
(1387, 218, 'main_slider', '4'),
(1388, 218, '_main_slider', 'field_6131169e930c9'),
(1389, 218, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(1390, 218, '_home_cats_title', 'field_6131171d5ce68'),
(1391, 218, 'home_cats', 'a:5:{i:0;s:2:\"21\";i:1;s:2:\"15\";i:2;s:2:\"20\";i:3;s:2:\"23\";i:4;s:2:\"22\";}'),
(1392, 218, '_home_cats', 'field_613117265ce69'),
(1393, 218, 'home_about_text', 'ואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(1394, 218, '_home_about_text', 'field_6131188d5ce6b'),
(1395, 218, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1396, 218, '_home_about_link', 'field_613118a45ce6c'),
(1397, 218, 'home_about_img', ''),
(1398, 218, '_home_about_img', 'field_613118e75ce6d'),
(1399, 218, 'home_prods_title_1', ''),
(1400, 218, '_home_prods_title_1', 'field_6131194a0b837'),
(1401, 218, 'home_prods_link_1', ''),
(1402, 218, '_home_prods_link_1', 'field_613119550b838'),
(1403, 218, 'home_prods_1', ''),
(1404, 218, '_home_prods_1', 'field_613119670b839'),
(1405, 218, 'banner_back_img', '208'),
(1406, 218, '_banner_back_img', 'field_6131199a011ea'),
(1407, 218, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1408, 218, '_banner_link', 'field_613119b1011eb'),
(1409, 218, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(1410, 218, '_banner_title', 'field_613119c1011ec'),
(1411, 218, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(1412, 218, '_banner_subtitle', 'field_613119ca011ed'),
(1413, 218, 'home_prods_title_2', ''),
(1414, 218, '_home_prods_title_2', 'field_613119f42cfea'),
(1415, 218, 'home_prods_link_2', ''),
(1416, 218, '_home_prods_link_2', 'field_613119f72cfeb'),
(1417, 218, 'home_prods_2', ''),
(1418, 218, '_home_prods_2', 'field_613119f92cfec'),
(1419, 218, 'reviews_image', '213'),
(1420, 218, '_reviews_image', 'field_61311a2f82d85'),
(1421, 218, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(1422, 218, '_reviews_title', 'field_61311a4482d86'),
(1423, 218, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(1424, 218, '_reviews_slider', 'field_61311a5782d87'),
(1425, 218, 'home_slider_seo', '2'),
(1426, 218, '_home_slider_seo', 'field_61311a92d1802'),
(1427, 218, 'home_slider_img', '206'),
(1428, 218, '_home_slider_img', 'field_61311a9fd1804'),
(1429, 218, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(1430, 218, '_home_gallery_img', 'field_61311b1cb4b73'),
(1431, 218, 'home_inst_title', ''),
(1432, 218, '_home_inst_title', 'field_61311b3eb4b74'),
(1433, 218, 'home_inst_subtitle', ''),
(1434, 218, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(1435, 218, 'home_video_block_title', 'צפו בסרטונים שלנו'),
(1436, 218, '_home_video_block_title', 'field_61311bbc1b54b'),
(1437, 218, 'home_videos', '5'),
(1438, 218, '_home_videos', 'field_61311bcd1b54c'),
(1439, 218, 'slider_img', '207'),
(1440, 218, '_slider_img', 'field_612f4e7693132'),
(1441, 218, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(1442, 218, '_faq_title', 'field_5feb2f82db93b'),
(1443, 218, 'faq_img', '156'),
(1444, 218, '_faq_img', 'field_5feb2f95db93c'),
(1445, 218, 'faq_item', '4'),
(1446, 218, '_faq_item', 'field_5feb2faddb93d'),
(1447, 218, 'main_slider_0_main_img', '212'),
(1448, 218, '_main_slider_0_main_img', 'field_613116b2930ca'),
(1449, 218, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(1450, 218, '_main_slider_0_main_title', 'field_613116c8930cb'),
(1451, 218, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1452, 218, '_main_slider_0_main_link', 'field_613116ee930cc'),
(1453, 218, 'main_slider_1_main_img', '209'),
(1454, 218, '_main_slider_1_main_img', 'field_613116b2930ca'),
(1455, 218, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(1456, 218, '_main_slider_1_main_title', 'field_613116c8930cb'),
(1457, 218, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1458, 218, '_main_slider_1_main_link', 'field_613116ee930cc'),
(1459, 218, 'main_slider_2_main_img', '210'),
(1460, 218, '_main_slider_2_main_img', 'field_613116b2930ca'),
(1461, 218, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(1462, 218, '_main_slider_2_main_title', 'field_613116c8930cb'),
(1463, 218, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1464, 218, '_main_slider_2_main_link', 'field_613116ee930cc'),
(1465, 218, 'main_slider_3_main_img', '211'),
(1466, 218, '_main_slider_3_main_img', 'field_613116b2930ca'),
(1467, 218, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(1468, 218, '_main_slider_3_main_title', 'field_613116c8930cb'),
(1469, 218, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1470, 218, '_main_slider_3_main_link', 'field_613116ee930cc'),
(1471, 218, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1472, 218, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(1473, 218, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1474, 218, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(1475, 218, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1476, 218, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1477, 218, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1478, 218, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1479, 218, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(1480, 218, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1481, 218, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(1482, 218, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1483, 218, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?'),
(1484, 218, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1485, 218, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1486, 218, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1487, 218, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(1488, 218, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1489, 218, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1490, 218, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1491, 218, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(1492, 218, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(1493, 218, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1494, 218, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(1495, 218, 'home_videos_0_video_item', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(1496, 218, '_home_videos_0_video_item', 'field_61311bd61b54d'),
(1497, 218, 'home_videos_1_video_item', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(1498, 218, '_home_videos_1_video_item', 'field_61311bd61b54d'),
(1499, 218, 'home_videos_2_video_item', 'https://www.youtube.com/watch?v=QBSDTRfqkiQ'),
(1500, 218, '_home_videos_2_video_item', 'field_61311bd61b54d'),
(1501, 218, 'home_videos_3_video_item', 'https://www.youtube.com/watch?v=sOdJMGVFYAg'),
(1502, 218, '_home_videos_3_video_item', 'field_61311bd61b54d'),
(1503, 218, 'home_videos_4_video_item', 'https://www.youtube.com/watch?v=ne_8xYNnFJA'),
(1504, 218, '_home_videos_4_video_item', 'field_61311bd61b54d'),
(1505, 219, 'title_tag', ''),
(1506, 219, '_title_tag', 'field_5ddbe7577e0e7'),
(1507, 219, 'single_slider_seo', '2'),
(1508, 219, '_single_slider_seo', 'field_5ddbde5499115'),
(1509, 219, 'single_gallery', ''),
(1510, 219, '_single_gallery', 'field_5ddbe5aea4275'),
(1511, 219, 'single_video_slider', ''),
(1512, 219, '_single_video_slider', 'field_5ddbe636a4276'),
(1513, 219, 'main_slider', '4'),
(1514, 219, '_main_slider', 'field_6131169e930c9'),
(1515, 219, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(1516, 219, '_home_cats_title', 'field_6131171d5ce68'),
(1517, 219, 'home_cats', 'a:6:{i:0;s:2:\"24\";i:1;s:2:\"21\";i:2;s:2:\"15\";i:3;s:2:\"20\";i:4;s:2:\"23\";i:5;s:2:\"22\";}'),
(1518, 219, '_home_cats', 'field_613117265ce69'),
(1519, 219, 'home_about_text', 'ואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(1520, 219, '_home_about_text', 'field_6131188d5ce6b'),
(1521, 219, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1522, 219, '_home_about_link', 'field_613118a45ce6c'),
(1523, 219, 'home_about_img', ''),
(1524, 219, '_home_about_img', 'field_613118e75ce6d'),
(1525, 219, 'home_prods_title_1', ''),
(1526, 219, '_home_prods_title_1', 'field_6131194a0b837'),
(1527, 219, 'home_prods_link_1', ''),
(1528, 219, '_home_prods_link_1', 'field_613119550b838'),
(1529, 219, 'home_prods_1', ''),
(1530, 219, '_home_prods_1', 'field_613119670b839'),
(1531, 219, 'banner_back_img', '208'),
(1532, 219, '_banner_back_img', 'field_6131199a011ea'),
(1533, 219, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1534, 219, '_banner_link', 'field_613119b1011eb'),
(1535, 219, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(1536, 219, '_banner_title', 'field_613119c1011ec'),
(1537, 219, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(1538, 219, '_banner_subtitle', 'field_613119ca011ed'),
(1539, 219, 'home_prods_title_2', ''),
(1540, 219, '_home_prods_title_2', 'field_613119f42cfea'),
(1541, 219, 'home_prods_link_2', ''),
(1542, 219, '_home_prods_link_2', 'field_613119f72cfeb'),
(1543, 219, 'home_prods_2', ''),
(1544, 219, '_home_prods_2', 'field_613119f92cfec'),
(1545, 219, 'reviews_image', '213'),
(1546, 219, '_reviews_image', 'field_61311a2f82d85'),
(1547, 219, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(1548, 219, '_reviews_title', 'field_61311a4482d86'),
(1549, 219, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(1550, 219, '_reviews_slider', 'field_61311a5782d87'),
(1551, 219, 'home_slider_seo', '2'),
(1552, 219, '_home_slider_seo', 'field_61311a92d1802'),
(1553, 219, 'home_slider_img', '206'),
(1554, 219, '_home_slider_img', 'field_61311a9fd1804'),
(1555, 219, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(1556, 219, '_home_gallery_img', 'field_61311b1cb4b73'),
(1557, 219, 'home_inst_title', ''),
(1558, 219, '_home_inst_title', 'field_61311b3eb4b74'),
(1559, 219, 'home_inst_subtitle', ''),
(1560, 219, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(1561, 219, 'home_video_block_title', 'צפו בסרטונים שלנו'),
(1562, 219, '_home_video_block_title', 'field_61311bbc1b54b'),
(1563, 219, 'home_videos', '5'),
(1564, 219, '_home_videos', 'field_61311bcd1b54c'),
(1565, 219, 'slider_img', '207'),
(1566, 219, '_slider_img', 'field_612f4e7693132'),
(1567, 219, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(1568, 219, '_faq_title', 'field_5feb2f82db93b'),
(1569, 219, 'faq_img', '156'),
(1570, 219, '_faq_img', 'field_5feb2f95db93c'),
(1571, 219, 'faq_item', '4'),
(1572, 219, '_faq_item', 'field_5feb2faddb93d'),
(1573, 219, 'main_slider_0_main_img', '212'),
(1574, 219, '_main_slider_0_main_img', 'field_613116b2930ca'),
(1575, 219, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(1576, 219, '_main_slider_0_main_title', 'field_613116c8930cb'),
(1577, 219, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1578, 219, '_main_slider_0_main_link', 'field_613116ee930cc'),
(1579, 219, 'main_slider_1_main_img', '209'),
(1580, 219, '_main_slider_1_main_img', 'field_613116b2930ca'),
(1581, 219, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(1582, 219, '_main_slider_1_main_title', 'field_613116c8930cb'),
(1583, 219, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1584, 219, '_main_slider_1_main_link', 'field_613116ee930cc'),
(1585, 219, 'main_slider_2_main_img', '210'),
(1586, 219, '_main_slider_2_main_img', 'field_613116b2930ca'),
(1587, 219, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(1588, 219, '_main_slider_2_main_title', 'field_613116c8930cb'),
(1589, 219, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1590, 219, '_main_slider_2_main_link', 'field_613116ee930cc'),
(1591, 219, 'main_slider_3_main_img', '211'),
(1592, 219, '_main_slider_3_main_img', 'field_613116b2930ca'),
(1593, 219, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(1594, 219, '_main_slider_3_main_title', 'field_613116c8930cb'),
(1595, 219, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1596, 219, '_main_slider_3_main_link', 'field_613116ee930cc'),
(1597, 219, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1598, 219, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(1599, 219, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1600, 219, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(1601, 219, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1602, 219, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1603, 219, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1604, 219, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1605, 219, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(1606, 219, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1607, 219, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(1608, 219, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1609, 219, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?'),
(1610, 219, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1611, 219, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1612, 219, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1613, 219, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(1614, 219, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1615, 219, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1616, 219, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1617, 219, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(1618, 219, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(1619, 219, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1620, 219, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(1621, 219, 'home_videos_0_video_item', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(1622, 219, '_home_videos_0_video_item', 'field_61311bd61b54d'),
(1623, 219, 'home_videos_1_video_item', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(1624, 219, '_home_videos_1_video_item', 'field_61311bd61b54d'),
(1625, 219, 'home_videos_2_video_item', 'https://www.youtube.com/watch?v=QBSDTRfqkiQ'),
(1626, 219, '_home_videos_2_video_item', 'field_61311bd61b54d'),
(1627, 219, 'home_videos_3_video_item', 'https://www.youtube.com/watch?v=sOdJMGVFYAg'),
(1628, 219, '_home_videos_3_video_item', 'field_61311bd61b54d'),
(1629, 219, 'home_videos_4_video_item', 'https://www.youtube.com/watch?v=ne_8xYNnFJA'),
(1630, 219, '_home_videos_4_video_item', 'field_61311bd61b54d'),
(1631, 220, 'title_tag', ''),
(1632, 220, '_title_tag', 'field_5ddbe7577e0e7'),
(1633, 220, 'single_slider_seo', '2'),
(1634, 220, '_single_slider_seo', 'field_5ddbde5499115'),
(1635, 220, 'single_gallery', ''),
(1636, 220, '_single_gallery', 'field_5ddbe5aea4275'),
(1637, 220, 'single_video_slider', ''),
(1638, 220, '_single_video_slider', 'field_5ddbe636a4276'),
(1639, 220, 'main_slider', '4'),
(1640, 220, '_main_slider', 'field_6131169e930c9'),
(1641, 220, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(1642, 220, '_home_cats_title', 'field_6131171d5ce68'),
(1643, 220, 'home_cats', 'a:6:{i:0;s:2:\"24\";i:1;s:2:\"21\";i:2;s:2:\"15\";i:3;s:2:\"20\";i:4;s:2:\"23\";i:5;s:2:\"22\";}'),
(1644, 220, '_home_cats', 'field_613117265ce69'),
(1645, 220, 'home_about_text', '<h2>היי, אני ענת!</h2>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(1646, 220, '_home_about_text', 'field_6131188d5ce6b');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1647, 220, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1648, 220, '_home_about_link', 'field_613118a45ce6c'),
(1649, 220, 'home_about_img', ''),
(1650, 220, '_home_about_img', 'field_613118e75ce6d'),
(1651, 220, 'home_prods_title_1', ''),
(1652, 220, '_home_prods_title_1', 'field_6131194a0b837'),
(1653, 220, 'home_prods_link_1', ''),
(1654, 220, '_home_prods_link_1', 'field_613119550b838'),
(1655, 220, 'home_prods_1', ''),
(1656, 220, '_home_prods_1', 'field_613119670b839'),
(1657, 220, 'banner_back_img', '208'),
(1658, 220, '_banner_back_img', 'field_6131199a011ea'),
(1659, 220, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1660, 220, '_banner_link', 'field_613119b1011eb'),
(1661, 220, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(1662, 220, '_banner_title', 'field_613119c1011ec'),
(1663, 220, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(1664, 220, '_banner_subtitle', 'field_613119ca011ed'),
(1665, 220, 'home_prods_title_2', ''),
(1666, 220, '_home_prods_title_2', 'field_613119f42cfea'),
(1667, 220, 'home_prods_link_2', ''),
(1668, 220, '_home_prods_link_2', 'field_613119f72cfeb'),
(1669, 220, 'home_prods_2', ''),
(1670, 220, '_home_prods_2', 'field_613119f92cfec'),
(1671, 220, 'reviews_image', '213'),
(1672, 220, '_reviews_image', 'field_61311a2f82d85'),
(1673, 220, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(1674, 220, '_reviews_title', 'field_61311a4482d86'),
(1675, 220, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(1676, 220, '_reviews_slider', 'field_61311a5782d87'),
(1677, 220, 'home_slider_seo', '2'),
(1678, 220, '_home_slider_seo', 'field_61311a92d1802'),
(1679, 220, 'home_slider_img', '206'),
(1680, 220, '_home_slider_img', 'field_61311a9fd1804'),
(1681, 220, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(1682, 220, '_home_gallery_img', 'field_61311b1cb4b73'),
(1683, 220, 'home_inst_title', ''),
(1684, 220, '_home_inst_title', 'field_61311b3eb4b74'),
(1685, 220, 'home_inst_subtitle', ''),
(1686, 220, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(1687, 220, 'home_video_block_title', 'צפו בסרטונים שלנו'),
(1688, 220, '_home_video_block_title', 'field_61311bbc1b54b'),
(1689, 220, 'home_videos', '5'),
(1690, 220, '_home_videos', 'field_61311bcd1b54c'),
(1691, 220, 'slider_img', '207'),
(1692, 220, '_slider_img', 'field_612f4e7693132'),
(1693, 220, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(1694, 220, '_faq_title', 'field_5feb2f82db93b'),
(1695, 220, 'faq_img', '156'),
(1696, 220, '_faq_img', 'field_5feb2f95db93c'),
(1697, 220, 'faq_item', '4'),
(1698, 220, '_faq_item', 'field_5feb2faddb93d'),
(1699, 220, 'main_slider_0_main_img', '212'),
(1700, 220, '_main_slider_0_main_img', 'field_613116b2930ca'),
(1701, 220, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(1702, 220, '_main_slider_0_main_title', 'field_613116c8930cb'),
(1703, 220, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1704, 220, '_main_slider_0_main_link', 'field_613116ee930cc'),
(1705, 220, 'main_slider_1_main_img', '209'),
(1706, 220, '_main_slider_1_main_img', 'field_613116b2930ca'),
(1707, 220, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(1708, 220, '_main_slider_1_main_title', 'field_613116c8930cb'),
(1709, 220, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1710, 220, '_main_slider_1_main_link', 'field_613116ee930cc'),
(1711, 220, 'main_slider_2_main_img', '210'),
(1712, 220, '_main_slider_2_main_img', 'field_613116b2930ca'),
(1713, 220, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(1714, 220, '_main_slider_2_main_title', 'field_613116c8930cb'),
(1715, 220, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1716, 220, '_main_slider_2_main_link', 'field_613116ee930cc'),
(1717, 220, 'main_slider_3_main_img', '211'),
(1718, 220, '_main_slider_3_main_img', 'field_613116b2930ca'),
(1719, 220, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(1720, 220, '_main_slider_3_main_title', 'field_613116c8930cb'),
(1721, 220, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1722, 220, '_main_slider_3_main_link', 'field_613116ee930cc'),
(1723, 220, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1724, 220, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(1725, 220, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1726, 220, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(1727, 220, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1728, 220, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1729, 220, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1730, 220, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1731, 220, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(1732, 220, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1733, 220, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(1734, 220, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1735, 220, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?'),
(1736, 220, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1737, 220, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1738, 220, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1739, 220, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(1740, 220, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1741, 220, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1742, 220, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1743, 220, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(1744, 220, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(1745, 220, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1746, 220, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(1747, 220, 'home_videos_0_video_item', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(1748, 220, '_home_videos_0_video_item', 'field_61311bd61b54d'),
(1749, 220, 'home_videos_1_video_item', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(1750, 220, '_home_videos_1_video_item', 'field_61311bd61b54d'),
(1751, 220, 'home_videos_2_video_item', 'https://www.youtube.com/watch?v=QBSDTRfqkiQ'),
(1752, 220, '_home_videos_2_video_item', 'field_61311bd61b54d'),
(1753, 220, 'home_videos_3_video_item', 'https://www.youtube.com/watch?v=sOdJMGVFYAg'),
(1754, 220, '_home_videos_3_video_item', 'field_61311bd61b54d'),
(1755, 220, 'home_videos_4_video_item', 'https://www.youtube.com/watch?v=ne_8xYNnFJA'),
(1756, 220, '_home_videos_4_video_item', 'field_61311bd61b54d'),
(1757, 221, 'title_tag', ''),
(1758, 221, '_title_tag', 'field_5ddbe7577e0e7'),
(1759, 221, 'single_slider_seo', '2'),
(1760, 221, '_single_slider_seo', 'field_5ddbde5499115'),
(1761, 221, 'single_gallery', ''),
(1762, 221, '_single_gallery', 'field_5ddbe5aea4275'),
(1763, 221, 'single_video_slider', ''),
(1764, 221, '_single_video_slider', 'field_5ddbe636a4276'),
(1765, 221, 'main_slider', '4'),
(1766, 221, '_main_slider', 'field_6131169e930c9'),
(1767, 221, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(1768, 221, '_home_cats_title', 'field_6131171d5ce68'),
(1769, 221, 'home_cats', 'a:6:{i:0;s:2:\"24\";i:1;s:2:\"21\";i:2;s:2:\"15\";i:3;s:2:\"20\";i:4;s:2:\"23\";i:5;s:2:\"22\";}'),
(1770, 221, '_home_cats', 'field_613117265ce69'),
(1771, 221, 'home_about_text', '<h2>היי, אני ענת!</h2>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(1772, 221, '_home_about_text', 'field_6131188d5ce6b'),
(1773, 221, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1774, 221, '_home_about_link', 'field_613118a45ce6c'),
(1775, 221, 'home_about_img', '195'),
(1776, 221, '_home_about_img', 'field_613118e75ce6d'),
(1777, 221, 'home_prods_title_1', ''),
(1778, 221, '_home_prods_title_1', 'field_6131194a0b837'),
(1779, 221, 'home_prods_link_1', ''),
(1780, 221, '_home_prods_link_1', 'field_613119550b838'),
(1781, 221, 'home_prods_1', ''),
(1782, 221, '_home_prods_1', 'field_613119670b839'),
(1783, 221, 'banner_back_img', '208'),
(1784, 221, '_banner_back_img', 'field_6131199a011ea'),
(1785, 221, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1786, 221, '_banner_link', 'field_613119b1011eb'),
(1787, 221, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(1788, 221, '_banner_title', 'field_613119c1011ec'),
(1789, 221, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(1790, 221, '_banner_subtitle', 'field_613119ca011ed'),
(1791, 221, 'home_prods_title_2', ''),
(1792, 221, '_home_prods_title_2', 'field_613119f42cfea'),
(1793, 221, 'home_prods_link_2', ''),
(1794, 221, '_home_prods_link_2', 'field_613119f72cfeb'),
(1795, 221, 'home_prods_2', ''),
(1796, 221, '_home_prods_2', 'field_613119f92cfec'),
(1797, 221, 'reviews_image', '213'),
(1798, 221, '_reviews_image', 'field_61311a2f82d85'),
(1799, 221, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(1800, 221, '_reviews_title', 'field_61311a4482d86'),
(1801, 221, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(1802, 221, '_reviews_slider', 'field_61311a5782d87'),
(1803, 221, 'home_slider_seo', '2'),
(1804, 221, '_home_slider_seo', 'field_61311a92d1802'),
(1805, 221, 'home_slider_img', '206'),
(1806, 221, '_home_slider_img', 'field_61311a9fd1804'),
(1807, 221, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(1808, 221, '_home_gallery_img', 'field_61311b1cb4b73'),
(1809, 221, 'home_inst_title', ''),
(1810, 221, '_home_inst_title', 'field_61311b3eb4b74'),
(1811, 221, 'home_inst_subtitle', ''),
(1812, 221, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(1813, 221, 'home_video_block_title', 'צפו בסרטונים שלנו'),
(1814, 221, '_home_video_block_title', 'field_61311bbc1b54b'),
(1815, 221, 'home_videos', '5'),
(1816, 221, '_home_videos', 'field_61311bcd1b54c'),
(1817, 221, 'slider_img', '207'),
(1818, 221, '_slider_img', 'field_612f4e7693132'),
(1819, 221, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(1820, 221, '_faq_title', 'field_5feb2f82db93b'),
(1821, 221, 'faq_img', '156'),
(1822, 221, '_faq_img', 'field_5feb2f95db93c'),
(1823, 221, 'faq_item', '4'),
(1824, 221, '_faq_item', 'field_5feb2faddb93d'),
(1825, 221, 'main_slider_0_main_img', '212'),
(1826, 221, '_main_slider_0_main_img', 'field_613116b2930ca'),
(1827, 221, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(1828, 221, '_main_slider_0_main_title', 'field_613116c8930cb'),
(1829, 221, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1830, 221, '_main_slider_0_main_link', 'field_613116ee930cc'),
(1831, 221, 'main_slider_1_main_img', '209'),
(1832, 221, '_main_slider_1_main_img', 'field_613116b2930ca'),
(1833, 221, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(1834, 221, '_main_slider_1_main_title', 'field_613116c8930cb'),
(1835, 221, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1836, 221, '_main_slider_1_main_link', 'field_613116ee930cc'),
(1837, 221, 'main_slider_2_main_img', '210'),
(1838, 221, '_main_slider_2_main_img', 'field_613116b2930ca'),
(1839, 221, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(1840, 221, '_main_slider_2_main_title', 'field_613116c8930cb'),
(1841, 221, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1842, 221, '_main_slider_2_main_link', 'field_613116ee930cc'),
(1843, 221, 'main_slider_3_main_img', '211'),
(1844, 221, '_main_slider_3_main_img', 'field_613116b2930ca'),
(1845, 221, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(1846, 221, '_main_slider_3_main_title', 'field_613116c8930cb'),
(1847, 221, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1848, 221, '_main_slider_3_main_link', 'field_613116ee930cc'),
(1849, 221, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1850, 221, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(1851, 221, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1852, 221, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(1853, 221, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1854, 221, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1855, 221, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1856, 221, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1857, 221, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(1858, 221, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1859, 221, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(1860, 221, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1861, 221, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?'),
(1862, 221, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1863, 221, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1864, 221, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1865, 221, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(1866, 221, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1867, 221, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1868, 221, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1869, 221, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(1870, 221, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(1871, 221, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1872, 221, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(1873, 221, 'home_videos_0_video_item', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(1874, 221, '_home_videos_0_video_item', 'field_61311bd61b54d'),
(1875, 221, 'home_videos_1_video_item', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(1876, 221, '_home_videos_1_video_item', 'field_61311bd61b54d'),
(1877, 221, 'home_videos_2_video_item', 'https://www.youtube.com/watch?v=QBSDTRfqkiQ'),
(1878, 221, '_home_videos_2_video_item', 'field_61311bd61b54d'),
(1879, 221, 'home_videos_3_video_item', 'https://www.youtube.com/watch?v=sOdJMGVFYAg'),
(1880, 221, '_home_videos_3_video_item', 'field_61311bd61b54d'),
(1881, 221, 'home_videos_4_video_item', 'https://www.youtube.com/watch?v=ne_8xYNnFJA'),
(1882, 221, '_home_videos_4_video_item', 'field_61311bd61b54d'),
(1883, 222, 'title_tag', ''),
(1884, 222, '_title_tag', 'field_5ddbe7577e0e7'),
(1885, 222, 'single_slider_seo', '2'),
(1886, 222, '_single_slider_seo', 'field_5ddbde5499115'),
(1887, 222, 'single_gallery', ''),
(1888, 222, '_single_gallery', 'field_5ddbe5aea4275'),
(1889, 222, 'single_video_slider', ''),
(1890, 222, '_single_video_slider', 'field_5ddbe636a4276'),
(1891, 222, 'main_slider', '4'),
(1892, 222, '_main_slider', 'field_6131169e930c9'),
(1893, 222, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(1894, 222, '_home_cats_title', 'field_6131171d5ce68'),
(1895, 222, 'home_cats', 'a:6:{i:0;s:2:\"24\";i:1;s:2:\"21\";i:2;s:2:\"15\";i:3;s:2:\"20\";i:4;s:2:\"23\";i:5;s:2:\"22\";}'),
(1896, 222, '_home_cats', 'field_613117265ce69'),
(1897, 222, 'home_about_text', '<h2>היי, אני ענת!</h2>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(1898, 222, '_home_about_text', 'field_6131188d5ce6b'),
(1899, 222, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1900, 222, '_home_about_link', 'field_613118a45ce6c'),
(1901, 222, 'home_about_img', '195'),
(1902, 222, '_home_about_img', 'field_613118e75ce6d'),
(1903, 222, 'home_prods_title_1', ''),
(1904, 222, '_home_prods_title_1', 'field_6131194a0b837'),
(1905, 222, 'home_prods_link_1', ''),
(1906, 222, '_home_prods_link_1', 'field_613119550b838'),
(1907, 222, 'home_prods_1', ''),
(1908, 222, '_home_prods_1', 'field_613119670b839'),
(1909, 222, 'banner_back_img', '208'),
(1910, 222, '_banner_back_img', 'field_6131199a011ea'),
(1911, 222, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1912, 222, '_banner_link', 'field_613119b1011eb'),
(1913, 222, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(1914, 222, '_banner_title', 'field_613119c1011ec'),
(1915, 222, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(1916, 222, '_banner_subtitle', 'field_613119ca011ed'),
(1917, 222, 'home_prods_title_2', ''),
(1918, 222, '_home_prods_title_2', 'field_613119f42cfea'),
(1919, 222, 'home_prods_link_2', ''),
(1920, 222, '_home_prods_link_2', 'field_613119f72cfeb'),
(1921, 222, 'home_prods_2', ''),
(1922, 222, '_home_prods_2', 'field_613119f92cfec'),
(1923, 222, 'reviews_image', '213'),
(1924, 222, '_reviews_image', 'field_61311a2f82d85'),
(1925, 222, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(1926, 222, '_reviews_title', 'field_61311a4482d86'),
(1927, 222, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(1928, 222, '_reviews_slider', 'field_61311a5782d87'),
(1929, 222, 'home_slider_seo', '2'),
(1930, 222, '_home_slider_seo', 'field_61311a92d1802'),
(1931, 222, 'home_slider_img', '206'),
(1932, 222, '_home_slider_img', 'field_61311a9fd1804'),
(1933, 222, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(1934, 222, '_home_gallery_img', 'field_61311b1cb4b73'),
(1935, 222, 'home_inst_title', 'באינסטגרם שלי כבר ביקרת?'),
(1936, 222, '_home_inst_title', 'field_61311b3eb4b74'),
(1937, 222, 'home_inst_subtitle', 'עקבי והישארי מעודכנת!'),
(1938, 222, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(1939, 222, 'home_video_block_title', 'צפו בסרטונים שלנו'),
(1940, 222, '_home_video_block_title', 'field_61311bbc1b54b'),
(1941, 222, 'home_videos', '5'),
(1942, 222, '_home_videos', 'field_61311bcd1b54c'),
(1943, 222, 'slider_img', '207'),
(1944, 222, '_slider_img', 'field_612f4e7693132'),
(1945, 222, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(1946, 222, '_faq_title', 'field_5feb2f82db93b'),
(1947, 222, 'faq_img', '156'),
(1948, 222, '_faq_img', 'field_5feb2f95db93c'),
(1949, 222, 'faq_item', '4'),
(1950, 222, '_faq_item', 'field_5feb2faddb93d'),
(1951, 222, 'main_slider_0_main_img', '212'),
(1952, 222, '_main_slider_0_main_img', 'field_613116b2930ca'),
(1953, 222, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(1954, 222, '_main_slider_0_main_title', 'field_613116c8930cb'),
(1955, 222, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1956, 222, '_main_slider_0_main_link', 'field_613116ee930cc'),
(1957, 222, 'main_slider_1_main_img', '209'),
(1958, 222, '_main_slider_1_main_img', 'field_613116b2930ca'),
(1959, 222, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(1960, 222, '_main_slider_1_main_title', 'field_613116c8930cb'),
(1961, 222, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1962, 222, '_main_slider_1_main_link', 'field_613116ee930cc'),
(1963, 222, 'main_slider_2_main_img', '210'),
(1964, 222, '_main_slider_2_main_img', 'field_613116b2930ca'),
(1965, 222, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(1966, 222, '_main_slider_2_main_title', 'field_613116c8930cb'),
(1967, 222, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1968, 222, '_main_slider_2_main_link', 'field_613116ee930cc'),
(1969, 222, 'main_slider_3_main_img', '211'),
(1970, 222, '_main_slider_3_main_img', 'field_613116b2930ca'),
(1971, 222, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(1972, 222, '_main_slider_3_main_title', 'field_613116c8930cb'),
(1973, 222, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1974, 222, '_main_slider_3_main_link', 'field_613116ee930cc'),
(1975, 222, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1976, 222, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(1977, 222, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1978, 222, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(1979, 222, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1980, 222, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1981, 222, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1982, 222, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1983, 222, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(1984, 222, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(1985, 222, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(1986, 222, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(1987, 222, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?'),
(1988, 222, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(1989, 222, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1990, 222, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(1991, 222, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(1992, 222, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(1993, 222, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1994, 222, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(1995, 222, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(1996, 222, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(1997, 222, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1998, 222, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(1999, 222, 'home_videos_0_video_item', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(2000, 222, '_home_videos_0_video_item', 'field_61311bd61b54d'),
(2001, 222, 'home_videos_1_video_item', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(2002, 222, '_home_videos_1_video_item', 'field_61311bd61b54d'),
(2003, 222, 'home_videos_2_video_item', 'https://www.youtube.com/watch?v=QBSDTRfqkiQ'),
(2004, 222, '_home_videos_2_video_item', 'field_61311bd61b54d'),
(2005, 222, 'home_videos_3_video_item', 'https://www.youtube.com/watch?v=sOdJMGVFYAg'),
(2006, 222, '_home_videos_3_video_item', 'field_61311bd61b54d'),
(2007, 222, 'home_videos_4_video_item', 'https://www.youtube.com/watch?v=ne_8xYNnFJA'),
(2008, 222, '_home_videos_4_video_item', 'field_61311bd61b54d'),
(2009, 223, '_edit_last', '1'),
(2010, 223, '_edit_lock', '1630836797:1'),
(2011, 224, '_wp_attached_file', '2021/09/test-prod-1.png'),
(2012, 224, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:362;s:6:\"height\";i:357;s:4:\"file\";s:23:\"2021/09/test-prod-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"test-prod-1-300x296.png\";s:5:\"width\";i:300;s:6:\"height\";i:296;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"test-prod-1-95x94.png\";s:5:\"width\";i:95;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"test-prod-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"test-prod-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2013, 225, '_wp_attached_file', '2021/09/test-prod-2.png'),
(2014, 225, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:144;s:4:\"file\";s:23:\"2021/09/test-prod-2.png\";s:5:\"sizes\";a:3:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"test-prod-2-84x94.png\";s:5:\"width\";i:84;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2015, 226, '_wp_attached_file', '2021/09/test-prod-3.png'),
(2016, 226, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:144;s:4:\"file\";s:23:\"2021/09/test-prod-3.png\";s:5:\"sizes\";a:3:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"test-prod-3-84x94.png\";s:5:\"width\";i:84;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2017, 227, '_wp_attached_file', '2021/09/test-prod-4.png'),
(2018, 227, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:129;s:6:\"height\";i:144;s:4:\"file\";s:23:\"2021/09/test-prod-4.png\";s:5:\"sizes\";a:3:{s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"test-prod-4-84x94.png\";s:5:\"width\";i:84;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2019, 223, '_thumbnail_id', '224'),
(2020, 223, 'price_per_100', ''),
(2021, 223, 'total_sales', '0'),
(2022, 223, '_tax_status', 'taxable'),
(2023, 223, '_tax_class', ''),
(2024, 223, '_manage_stock', 'no'),
(2025, 223, '_backorders', 'no'),
(2026, 223, '_sold_individually', 'no'),
(2027, 223, '_virtual', 'no'),
(2028, 223, '_downloadable', 'no'),
(2029, 223, '_download_limit', '-1'),
(2030, 223, '_download_expiry', '-1'),
(2031, 223, '_stock', NULL),
(2032, 223, '_stock_status', 'instock'),
(2033, 223, '_wc_average_rating', '0'),
(2034, 223, '_wc_review_count', '0'),
(2035, 223, '_product_version', '5.6.0'),
(2036, 223, '_product_image_gallery', '225,226,227'),
(2037, 223, 'per_100', '690'),
(2038, 223, '_per_100', 'field_6130f5e6485b5'),
(2039, 223, 'prod_delivery_info', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2040, 223, '_prod_delivery_info', 'field_6130f52c5d7ef'),
(2041, 223, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(2042, 223, '_faq_title', 'field_5feb2f82db93b'),
(2043, 223, 'faq_img', ''),
(2044, 223, '_faq_img', 'field_5feb2f95db93c'),
(2045, 223, 'faq_item_0_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם 1?'),
(2046, 223, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(2047, 223, 'faq_item_0_faq_answer', 'קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2048, 223, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(2049, 223, 'faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם 2?'),
(2050, 223, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(2051, 223, 'faq_item_1_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2052, 223, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(2053, 223, 'faq_item_2_faq_question', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם 3?'),
(2054, 223, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(2055, 223, 'faq_item_2_faq_answer', 'דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2056, 223, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(2057, 223, 'faq_item', '3'),
(2058, 223, '_faq_item', 'field_5feb2faddb93d'),
(2059, 223, '_product_attributes', 'a:1:{s:5:\"pa_ml\";a:6:{s:4:\"name\";s:5:\"pa_ml\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:1;s:11:\"is_taxonomy\";i:1;}}'),
(2060, 231, '_variation_description', ''),
(2061, 231, 'total_sales', '0'),
(2062, 231, '_tax_status', 'taxable'),
(2063, 231, '_tax_class', 'parent'),
(2064, 231, '_manage_stock', 'no'),
(2065, 231, '_backorders', 'no'),
(2066, 231, '_sold_individually', 'no'),
(2067, 231, '_virtual', 'no'),
(2068, 231, '_downloadable', 'no'),
(2069, 231, '_download_limit', '-1'),
(2070, 231, '_download_expiry', '-1'),
(2071, 231, '_stock', NULL),
(2072, 231, '_stock_status', 'instock'),
(2073, 231, '_wc_average_rating', '0'),
(2074, 231, '_wc_review_count', '0'),
(2075, 231, 'attribute_pa_ml', '100ml'),
(2076, 231, '_product_version', '5.6.0'),
(2077, 232, '_variation_description', ''),
(2078, 232, 'total_sales', '0'),
(2079, 232, '_tax_status', 'taxable'),
(2080, 232, '_tax_class', 'parent'),
(2081, 232, '_manage_stock', 'no'),
(2082, 232, '_backorders', 'no'),
(2083, 232, '_sold_individually', 'no'),
(2084, 232, '_virtual', 'no'),
(2085, 232, '_downloadable', 'no'),
(2086, 232, '_download_limit', '-1'),
(2087, 232, '_download_expiry', '-1'),
(2088, 232, '_stock', NULL),
(2089, 232, '_stock_status', 'instock'),
(2090, 232, '_wc_average_rating', '0'),
(2091, 232, '_wc_review_count', '0'),
(2092, 232, 'attribute_pa_ml', '50ml'),
(2093, 232, '_product_version', '5.6.0'),
(2094, 231, '_regular_price', '460'),
(2095, 231, '_sale_price', '350'),
(2096, 231, '_thumbnail_id', '0'),
(2097, 231, '_price', '350'),
(2099, 232, '_regular_price', '340'),
(2100, 232, '_sale_price', '290'),
(2101, 232, '_thumbnail_id', '0'),
(2102, 232, '_price', '290'),
(2103, 223, '_price', '290'),
(2104, 223, '_price', '350'),
(2105, 237, '_edit_last', '1'),
(2106, 237, '_edit_lock', '1630850968:1'),
(2107, 238, '_wp_attached_file', '2021/09/product-1.png'),
(2108, 238, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:217;s:6:\"height\";i:209;s:4:\"file\";s:21:\"2021/09/product-1.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-1-98x94.png\";s:5:\"width\";i:98;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2109, 239, '_wp_attached_file', '2021/09/product-2.png'),
(2110, 239, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:199;s:6:\"height\";i:259;s:4:\"file\";s:21:\"2021/09/product-2.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-2-72x94.png\";s:5:\"width\";i:72;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2111, 240, '_wp_attached_file', '2021/09/product-3.png'),
(2112, 240, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:210;s:6:\"height\";i:265;s:4:\"file\";s:21:\"2021/09/product-3.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-3-74x94.png\";s:5:\"width\";i:74;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2113, 241, '_wp_attached_file', '2021/09/product-4.png'),
(2114, 241, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:168;s:6:\"height\";i:201;s:4:\"file\";s:21:\"2021/09/product-4.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-4-79x94.png\";s:5:\"width\";i:79;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2115, 237, '_thumbnail_id', '239'),
(2116, 237, 'price_per_100', ''),
(2117, 237, 'total_sales', '0'),
(2118, 237, '_tax_status', 'taxable'),
(2119, 237, '_tax_class', ''),
(2120, 237, '_manage_stock', 'no'),
(2121, 237, '_backorders', 'no'),
(2122, 237, '_sold_individually', 'no'),
(2123, 237, '_virtual', 'no'),
(2124, 237, '_downloadable', 'no'),
(2125, 237, '_download_limit', '-1'),
(2126, 237, '_download_expiry', '-1'),
(2127, 237, '_stock', NULL),
(2128, 237, '_stock_status', 'instock'),
(2129, 237, '_wc_average_rating', '0'),
(2130, 237, '_wc_review_count', '0'),
(2131, 237, '_product_version', '5.6.0'),
(2132, 237, 'per_100', ''),
(2133, 237, '_per_100', 'field_6130f5e6485b5'),
(2134, 237, 'prod_delivery_info', ''),
(2135, 237, '_prod_delivery_info', 'field_6130f52c5d7ef'),
(2136, 237, 'faq_title', ''),
(2137, 237, '_faq_title', 'field_5feb2f82db93b'),
(2138, 237, 'faq_img', ''),
(2139, 237, '_faq_img', 'field_5feb2f95db93c'),
(2140, 237, 'faq_item', '');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2141, 237, '_faq_item', 'field_5feb2faddb93d'),
(2142, 242, '_edit_last', '1'),
(2143, 242, '_edit_lock', '1630850987:1'),
(2144, 242, '_thumbnail_id', '240'),
(2145, 242, 'price_per_100', ''),
(2146, 242, 'total_sales', '0'),
(2147, 242, '_tax_status', 'taxable'),
(2148, 242, '_tax_class', ''),
(2149, 242, '_manage_stock', 'no'),
(2150, 242, '_backorders', 'no'),
(2151, 242, '_sold_individually', 'no'),
(2152, 242, '_virtual', 'no'),
(2153, 242, '_downloadable', 'no'),
(2154, 242, '_download_limit', '-1'),
(2155, 242, '_download_expiry', '-1'),
(2156, 242, '_stock', NULL),
(2157, 242, '_stock_status', 'instock'),
(2158, 242, '_wc_average_rating', '0'),
(2159, 242, '_wc_review_count', '0'),
(2160, 242, '_product_version', '5.6.0'),
(2161, 242, 'per_100', ''),
(2162, 242, '_per_100', 'field_6130f5e6485b5'),
(2163, 242, 'prod_delivery_info', ''),
(2164, 242, '_prod_delivery_info', 'field_6130f52c5d7ef'),
(2165, 242, 'faq_title', ''),
(2166, 242, '_faq_title', 'field_5feb2f82db93b'),
(2167, 242, 'faq_img', ''),
(2168, 242, '_faq_img', 'field_5feb2f95db93c'),
(2169, 242, 'faq_item', ''),
(2170, 242, '_faq_item', 'field_5feb2faddb93d'),
(2171, 243, '_edit_last', '1'),
(2172, 243, 'price_per_100', ''),
(2173, 243, 'total_sales', '0'),
(2174, 243, '_tax_status', 'taxable'),
(2175, 243, '_tax_class', ''),
(2176, 243, '_manage_stock', 'no'),
(2177, 243, '_backorders', 'no'),
(2178, 243, '_sold_individually', 'no'),
(2179, 243, '_virtual', 'no'),
(2180, 243, '_downloadable', 'no'),
(2181, 243, '_download_limit', '-1'),
(2182, 243, '_download_expiry', '-1'),
(2183, 243, '_stock', NULL),
(2184, 243, '_stock_status', 'instock'),
(2185, 243, '_wc_average_rating', '0'),
(2186, 243, '_wc_review_count', '0'),
(2187, 243, '_product_version', '5.6.0'),
(2188, 243, 'per_100', ''),
(2189, 243, '_per_100', 'field_6130f5e6485b5'),
(2190, 243, 'prod_delivery_info', ''),
(2191, 243, '_prod_delivery_info', 'field_6130f52c5d7ef'),
(2192, 243, 'faq_title', ''),
(2193, 243, '_faq_title', 'field_5feb2f82db93b'),
(2194, 243, 'faq_img', ''),
(2195, 243, '_faq_img', 'field_5feb2f95db93c'),
(2196, 243, 'faq_item', ''),
(2197, 243, '_faq_item', 'field_5feb2faddb93d'),
(2198, 243, '_edit_lock', '1630851003:1'),
(2199, 244, '_edit_last', '1'),
(2200, 244, '_edit_lock', '1630852452:1'),
(2201, 244, '_thumbnail_id', '241'),
(2202, 244, 'price_per_100', ''),
(2203, 244, 'total_sales', '0'),
(2204, 244, '_tax_status', 'taxable'),
(2205, 244, '_tax_class', ''),
(2206, 244, '_manage_stock', 'no'),
(2207, 244, '_backorders', 'no'),
(2208, 244, '_sold_individually', 'no'),
(2209, 244, '_virtual', 'no'),
(2210, 244, '_downloadable', 'no'),
(2211, 244, '_download_limit', '-1'),
(2212, 244, '_download_expiry', '-1'),
(2213, 244, '_stock', NULL),
(2214, 244, '_stock_status', 'instock'),
(2215, 244, '_wc_average_rating', '0'),
(2216, 244, '_wc_review_count', '0'),
(2217, 244, '_product_version', '5.6.0'),
(2218, 244, 'per_100', ''),
(2219, 244, '_per_100', 'field_6130f5e6485b5'),
(2220, 244, 'prod_delivery_info', ''),
(2221, 244, '_prod_delivery_info', 'field_6130f52c5d7ef'),
(2222, 244, 'faq_title', ''),
(2223, 244, '_faq_title', 'field_5feb2f82db93b'),
(2224, 244, 'faq_img', ''),
(2225, 244, '_faq_img', 'field_5feb2f95db93c'),
(2226, 244, 'faq_item', ''),
(2227, 244, '_faq_item', 'field_5feb2faddb93d'),
(2228, 245, 'title_tag', ''),
(2229, 245, '_title_tag', 'field_5ddbe7577e0e7'),
(2230, 245, 'single_slider_seo', '2'),
(2231, 245, '_single_slider_seo', 'field_5ddbde5499115'),
(2232, 245, 'single_gallery', ''),
(2233, 245, '_single_gallery', 'field_5ddbe5aea4275'),
(2234, 245, 'single_video_slider', ''),
(2235, 245, '_single_video_slider', 'field_5ddbe636a4276'),
(2236, 245, 'main_slider', '4'),
(2237, 245, '_main_slider', 'field_6131169e930c9'),
(2238, 245, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(2239, 245, '_home_cats_title', 'field_6131171d5ce68'),
(2240, 245, 'home_cats', 'a:6:{i:0;s:2:\"24\";i:1;s:2:\"21\";i:2;s:2:\"15\";i:3;s:2:\"20\";i:4;s:2:\"23\";i:5;s:2:\"22\";}'),
(2241, 245, '_home_cats', 'field_613117265ce69'),
(2242, 245, 'home_about_text', '<h2>היי, אני ענת!</h2>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(2243, 245, '_home_about_text', 'field_6131188d5ce6b'),
(2244, 245, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(2245, 245, '_home_about_link', 'field_613118a45ce6c'),
(2246, 245, 'home_about_img', '195'),
(2247, 245, '_home_about_img', 'field_613118e75ce6d'),
(2248, 245, 'home_prods_title_1', 'קולקציית הבשמים של ויקטוריה סיקרט'),
(2249, 245, '_home_prods_title_1', 'field_6131194a0b837'),
(2250, 245, 'home_prods_link_1', 'a:3:{s:5:\"title\";s:51:\"בואי להכיר את כל הבשמים שלנו\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2251, 245, '_home_prods_link_1', 'field_613119550b838'),
(2252, 245, 'home_prods_1', 'a:4:{i:0;s:3:\"223\";i:1;s:3:\"237\";i:2;s:3:\"242\";i:3;s:3:\"244\";}'),
(2253, 245, '_home_prods_1', 'field_613119670b839'),
(2254, 245, 'banner_back_img', '208'),
(2255, 245, '_banner_back_img', 'field_6131199a011ea'),
(2256, 245, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2257, 245, '_banner_link', 'field_613119b1011eb'),
(2258, 245, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(2259, 245, '_banner_title', 'field_613119c1011ec'),
(2260, 245, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(2261, 245, '_banner_subtitle', 'field_613119ca011ed'),
(2262, 245, 'home_prods_title_2', ''),
(2263, 245, '_home_prods_title_2', 'field_613119f42cfea'),
(2264, 245, 'home_prods_link_2', ''),
(2265, 245, '_home_prods_link_2', 'field_613119f72cfeb'),
(2266, 245, 'home_prods_2', ''),
(2267, 245, '_home_prods_2', 'field_613119f92cfec'),
(2268, 245, 'reviews_image', '213'),
(2269, 245, '_reviews_image', 'field_61311a2f82d85'),
(2270, 245, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(2271, 245, '_reviews_title', 'field_61311a4482d86'),
(2272, 245, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(2273, 245, '_reviews_slider', 'field_61311a5782d87'),
(2274, 245, 'home_slider_seo', '2'),
(2275, 245, '_home_slider_seo', 'field_61311a92d1802'),
(2276, 245, 'home_slider_img', '206'),
(2277, 245, '_home_slider_img', 'field_61311a9fd1804'),
(2278, 245, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(2279, 245, '_home_gallery_img', 'field_61311b1cb4b73'),
(2280, 245, 'home_inst_title', 'באינסטגרם שלי כבר ביקרת?'),
(2281, 245, '_home_inst_title', 'field_61311b3eb4b74'),
(2282, 245, 'home_inst_subtitle', 'עקבי והישארי מעודכנת!'),
(2283, 245, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(2284, 245, 'home_video_block_title', 'צפו בסרטונים שלנו'),
(2285, 245, '_home_video_block_title', 'field_61311bbc1b54b'),
(2286, 245, 'home_videos', '5'),
(2287, 245, '_home_videos', 'field_61311bcd1b54c'),
(2288, 245, 'slider_img', '207'),
(2289, 245, '_slider_img', 'field_612f4e7693132'),
(2290, 245, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(2291, 245, '_faq_title', 'field_5feb2f82db93b'),
(2292, 245, 'faq_img', '156'),
(2293, 245, '_faq_img', 'field_5feb2f95db93c'),
(2294, 245, 'faq_item', '4'),
(2295, 245, '_faq_item', 'field_5feb2faddb93d'),
(2296, 245, 'main_slider_0_main_img', '212'),
(2297, 245, '_main_slider_0_main_img', 'field_613116b2930ca'),
(2298, 245, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(2299, 245, '_main_slider_0_main_title', 'field_613116c8930cb'),
(2300, 245, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2301, 245, '_main_slider_0_main_link', 'field_613116ee930cc'),
(2302, 245, 'main_slider_1_main_img', '209'),
(2303, 245, '_main_slider_1_main_img', 'field_613116b2930ca'),
(2304, 245, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(2305, 245, '_main_slider_1_main_title', 'field_613116c8930cb'),
(2306, 245, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2307, 245, '_main_slider_1_main_link', 'field_613116ee930cc'),
(2308, 245, 'main_slider_2_main_img', '210'),
(2309, 245, '_main_slider_2_main_img', 'field_613116b2930ca'),
(2310, 245, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(2311, 245, '_main_slider_2_main_title', 'field_613116c8930cb'),
(2312, 245, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2313, 245, '_main_slider_2_main_link', 'field_613116ee930cc'),
(2314, 245, 'main_slider_3_main_img', '211'),
(2315, 245, '_main_slider_3_main_img', 'field_613116b2930ca'),
(2316, 245, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(2317, 245, '_main_slider_3_main_title', 'field_613116c8930cb'),
(2318, 245, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2319, 245, '_main_slider_3_main_link', 'field_613116ee930cc'),
(2320, 245, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2321, 245, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(2322, 245, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2323, 245, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(2324, 245, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2325, 245, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(2326, 245, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2327, 245, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(2328, 245, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(2329, 245, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(2330, 245, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(2331, 245, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(2332, 245, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?'),
(2333, 245, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(2334, 245, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2335, 245, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(2336, 245, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(2337, 245, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(2338, 245, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(2339, 245, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(2340, 245, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(2341, 245, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(2342, 245, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2343, 245, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(2344, 245, 'home_videos_0_video_item', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(2345, 245, '_home_videos_0_video_item', 'field_61311bd61b54d'),
(2346, 245, 'home_videos_1_video_item', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(2347, 245, '_home_videos_1_video_item', 'field_61311bd61b54d'),
(2348, 245, 'home_videos_2_video_item', 'https://www.youtube.com/watch?v=QBSDTRfqkiQ'),
(2349, 245, '_home_videos_2_video_item', 'field_61311bd61b54d'),
(2350, 245, 'home_videos_3_video_item', 'https://www.youtube.com/watch?v=sOdJMGVFYAg'),
(2351, 245, '_home_videos_3_video_item', 'field_61311bd61b54d'),
(2352, 245, 'home_videos_4_video_item', 'https://www.youtube.com/watch?v=ne_8xYNnFJA'),
(2353, 245, '_home_videos_4_video_item', 'field_61311bd61b54d'),
(2354, 246, '_edit_last', '1'),
(2355, 246, '_edit_lock', '1630852766:1'),
(2356, 247, '_wp_attached_file', '2021/09/product-5.png'),
(2357, 247, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:309;s:6:\"height\";i:411;s:4:\"file\";s:21:\"2021/09/product-5.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-5-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-5-71x94.png\";s:5:\"width\";i:71;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2358, 248, '_wp_attached_file', '2021/09/product-6.png'),
(2359, 248, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:378;s:6:\"height\";i:503;s:4:\"file\";s:21:\"2021/09/product-6.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-6-225x300.png\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-6-71x94.png\";s:5:\"width\";i:71;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2360, 249, '_wp_attached_file', '2021/09/product-7.png'),
(2361, 249, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:258;s:6:\"height\";i:344;s:4:\"file\";s:21:\"2021/09/product-7.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-7-225x300.png\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-7-71x94.png\";s:5:\"width\";i:71;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-7-258x300.png\";s:5:\"width\";i:258;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-7-258x300.png\";s:5:\"width\";i:258;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2362, 246, 'total_sales', '0'),
(2363, 246, '_tax_status', 'taxable'),
(2364, 246, '_tax_class', ''),
(2365, 246, '_manage_stock', 'no'),
(2366, 246, '_backorders', 'no'),
(2367, 246, '_sold_individually', 'no'),
(2368, 246, '_virtual', 'no'),
(2369, 246, '_downloadable', 'no'),
(2370, 246, '_download_limit', '-1'),
(2371, 246, '_download_expiry', '-1'),
(2372, 246, '_stock', NULL),
(2373, 246, '_stock_status', 'instock'),
(2374, 246, '_wc_average_rating', '0'),
(2375, 246, '_wc_review_count', '0'),
(2376, 246, '_product_version', '5.6.0'),
(2377, 246, '_product_attributes', 'a:1:{s:7:\"pa_size\";a:6:{s:4:\"name\";s:7:\"pa_size\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:1;s:11:\"is_taxonomy\";i:1;}}'),
(2413, 246, '_default_attributes', 'a:1:{s:7:\"pa_size\";s:1:\"l\";}'),
(2415, 252, '_variation_description', ''),
(2416, 252, 'total_sales', '0'),
(2417, 252, '_tax_status', 'taxable'),
(2418, 252, '_tax_class', 'parent'),
(2419, 252, '_manage_stock', 'no'),
(2420, 252, '_backorders', 'no'),
(2421, 252, '_sold_individually', 'no'),
(2422, 252, '_virtual', 'no'),
(2423, 252, '_downloadable', 'no'),
(2424, 252, '_download_limit', '-1'),
(2425, 252, '_download_expiry', '-1'),
(2426, 252, '_stock', NULL),
(2427, 252, '_stock_status', 'instock'),
(2428, 252, '_wc_average_rating', '0'),
(2429, 252, '_wc_review_count', '0'),
(2430, 252, 'attribute_pa_size', 'l'),
(2431, 252, '_product_version', '5.6.0'),
(2432, 253, '_variation_description', ''),
(2433, 253, 'total_sales', '0'),
(2434, 253, '_tax_status', 'taxable'),
(2435, 253, '_tax_class', 'parent'),
(2436, 253, '_manage_stock', 'no'),
(2437, 253, '_backorders', 'no'),
(2438, 253, '_sold_individually', 'no'),
(2439, 253, '_virtual', 'no'),
(2440, 253, '_downloadable', 'no'),
(2441, 253, '_download_limit', '-1'),
(2442, 253, '_download_expiry', '-1'),
(2443, 253, '_stock', NULL),
(2444, 253, '_stock_status', 'instock'),
(2445, 253, '_wc_average_rating', '0'),
(2446, 253, '_wc_review_count', '0'),
(2447, 253, 'attribute_pa_size', 'm'),
(2448, 253, '_product_version', '5.6.0'),
(2449, 254, '_variation_description', ''),
(2450, 254, 'total_sales', '0'),
(2451, 254, '_tax_status', 'taxable'),
(2452, 254, '_tax_class', 'parent'),
(2453, 254, '_manage_stock', 'no'),
(2454, 254, '_backorders', 'no'),
(2455, 254, '_sold_individually', 'no'),
(2456, 254, '_virtual', 'no'),
(2457, 254, '_downloadable', 'no'),
(2458, 254, '_download_limit', '-1'),
(2459, 254, '_download_expiry', '-1'),
(2460, 254, '_stock', NULL),
(2461, 254, '_stock_status', 'instock'),
(2462, 254, '_wc_average_rating', '0'),
(2463, 254, '_wc_review_count', '0'),
(2464, 254, 'attribute_pa_size', 's'),
(2465, 254, '_product_version', '5.6.0'),
(2466, 252, '_regular_price', '140'),
(2467, 252, '_sale_price', '120'),
(2468, 252, '_thumbnail_id', '0'),
(2469, 252, '_price', '120'),
(2470, 253, '_regular_price', '150'),
(2471, 253, '_sale_price', '130'),
(2472, 253, '_thumbnail_id', '0'),
(2473, 253, '_price', '130'),
(2474, 246, '_price', '120'),
(2475, 246, '_price', '130'),
(2476, 246, '_thumbnail_id', '247'),
(2477, 246, 'per_100', ''),
(2478, 246, '_per_100', 'field_6130f5e6485b5'),
(2479, 246, 'prod_delivery_info', ''),
(2480, 246, '_prod_delivery_info', 'field_6130f52c5d7ef'),
(2481, 246, 'faq_title', ''),
(2482, 246, '_faq_title', 'field_5feb2f82db93b'),
(2483, 246, 'faq_img', ''),
(2484, 246, '_faq_img', 'field_5feb2f95db93c'),
(2485, 246, 'faq_item', ''),
(2486, 246, '_faq_item', 'field_5feb2faddb93d'),
(2487, 255, '_edit_last', '1'),
(2488, 255, '_edit_lock', '1630852789:1'),
(2489, 255, '_thumbnail_id', '248'),
(2490, 255, 'total_sales', '0'),
(2491, 255, '_tax_status', 'taxable'),
(2492, 255, '_tax_class', ''),
(2493, 255, '_manage_stock', 'no'),
(2494, 255, '_backorders', 'no'),
(2495, 255, '_sold_individually', 'no'),
(2496, 255, '_virtual', 'no'),
(2497, 255, '_downloadable', 'no'),
(2498, 255, '_download_limit', '-1'),
(2499, 255, '_download_expiry', '-1'),
(2500, 255, '_stock', NULL),
(2501, 255, '_stock_status', 'instock'),
(2502, 255, '_wc_average_rating', '0'),
(2503, 255, '_wc_review_count', '0'),
(2504, 255, '_product_version', '5.6.0'),
(2505, 255, 'per_100', ''),
(2506, 255, '_per_100', 'field_6130f5e6485b5'),
(2507, 255, 'prod_delivery_info', ''),
(2508, 255, '_prod_delivery_info', 'field_6130f52c5d7ef'),
(2509, 255, 'faq_title', ''),
(2510, 255, '_faq_title', 'field_5feb2f82db93b'),
(2511, 255, 'faq_img', ''),
(2512, 255, '_faq_img', 'field_5feb2f95db93c'),
(2513, 255, 'faq_item', ''),
(2514, 255, '_faq_item', 'field_5feb2faddb93d'),
(2515, 256, '_edit_last', '1'),
(2516, 256, '_edit_lock', '1630855296:1'),
(2517, 256, '_thumbnail_id', '249'),
(2518, 256, 'total_sales', '0'),
(2519, 256, '_tax_status', 'taxable'),
(2520, 256, '_tax_class', ''),
(2521, 256, '_manage_stock', 'no'),
(2522, 256, '_backorders', 'no'),
(2523, 256, '_sold_individually', 'no'),
(2524, 256, '_virtual', 'no'),
(2525, 256, '_downloadable', 'no'),
(2526, 256, '_download_limit', '-1'),
(2527, 256, '_download_expiry', '-1'),
(2528, 256, '_stock', NULL),
(2529, 256, '_stock_status', 'instock'),
(2530, 256, '_wc_average_rating', '0'),
(2531, 256, '_wc_review_count', '0'),
(2532, 256, '_product_version', '5.6.0'),
(2533, 256, 'per_100', ''),
(2534, 256, '_per_100', 'field_6130f5e6485b5'),
(2535, 256, 'prod_delivery_info', ''),
(2536, 256, '_prod_delivery_info', 'field_6130f52c5d7ef'),
(2537, 256, 'faq_title', ''),
(2538, 256, '_faq_title', 'field_5feb2f82db93b'),
(2539, 256, 'faq_img', ''),
(2540, 256, '_faq_img', 'field_5feb2f95db93c'),
(2541, 256, 'faq_item', ''),
(2542, 256, '_faq_item', 'field_5feb2faddb93d'),
(2543, 257, 'title_tag', ''),
(2544, 257, '_title_tag', 'field_5ddbe7577e0e7'),
(2545, 257, 'single_slider_seo', '2'),
(2546, 257, '_single_slider_seo', 'field_5ddbde5499115'),
(2547, 257, 'single_gallery', ''),
(2548, 257, '_single_gallery', 'field_5ddbe5aea4275'),
(2549, 257, 'single_video_slider', ''),
(2550, 257, '_single_video_slider', 'field_5ddbe636a4276'),
(2551, 257, 'main_slider', '4'),
(2552, 257, '_main_slider', 'field_6131169e930c9'),
(2553, 257, 'home_cats_title', 'הכירו את המוצרים שלנו:'),
(2554, 257, '_home_cats_title', 'field_6131171d5ce68'),
(2555, 257, 'home_cats', 'a:6:{i:0;s:2:\"24\";i:1;s:2:\"21\";i:2;s:2:\"15\";i:3;s:2:\"20\";i:4;s:2:\"23\";i:5;s:2:\"22\";}'),
(2556, 257, '_home_cats', 'field_613117265ce69'),
(2557, 257, 'home_about_text', '<h2>היי, אני ענת!</h2>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך!\r\n\r\nהחלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם. מוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות.\r\n\r\nהבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך? עדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.'),
(2558, 257, '_home_about_text', 'field_6131188d5ce6b'),
(2559, 257, 'home_about_link', 'a:3:{s:5:\"title\";s:21:\"להמשך קריאה\";s:3:\"url\";s:32:\"/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(2560, 257, '_home_about_link', 'field_613118a45ce6c'),
(2561, 257, 'home_about_img', '195'),
(2562, 257, '_home_about_img', 'field_613118e75ce6d'),
(2563, 257, 'home_prods_title_1', 'קולקציית הבשמים של ויקטוריה סיקרט'),
(2564, 257, '_home_prods_title_1', 'field_6131194a0b837'),
(2565, 257, 'home_prods_link_1', 'a:3:{s:5:\"title\";s:51:\"בואי להכיר את כל הבשמים שלנו\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2566, 257, '_home_prods_link_1', 'field_613119550b838'),
(2567, 257, 'home_prods_1', 'a:4:{i:0;s:3:\"223\";i:1;s:3:\"237\";i:2;s:3:\"242\";i:3;s:3:\"244\";}'),
(2568, 257, '_home_prods_1', 'field_613119670b839'),
(2569, 257, 'banner_back_img', '208'),
(2570, 257, '_banner_back_img', 'field_6131199a011ea'),
(2571, 257, 'banner_link', 'a:3:{s:5:\"title\";s:26:\"התחילי בקנייה!\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2572, 257, '_banner_link', 'field_613119b1011eb'),
(2573, 257, 'banner_title', 'ביננו, מגיע לך פינוק אמיתי!'),
(2574, 257, '_banner_title', 'field_613119c1011ec'),
(2575, 257, 'banner_subtitle', 'בואי להתחדש ולהתפנק...'),
(2576, 257, '_banner_subtitle', 'field_613119ca011ed'),
(2577, 257, 'home_prods_title_2', 'קולקציית פיג’מות ונעלי בית חדשה!'),
(2578, 257, '_home_prods_title_2', 'field_613119f42cfea'),
(2579, 257, 'home_prods_link_2', 'a:3:{s:5:\"title\";s:51:\"בואי להכיר את כל הקרמים שלנו\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2580, 257, '_home_prods_link_2', 'field_613119f72cfeb'),
(2581, 257, 'home_prods_2', 'a:4:{i:0;s:3:\"246\";i:1;s:3:\"255\";i:2;s:3:\"256\";i:3;s:3:\"243\";}'),
(2582, 257, '_home_prods_2', 'field_613119f92cfec'),
(2583, 257, 'reviews_image', '213'),
(2584, 257, '_reviews_image', 'field_61311a2f82d85'),
(2585, 257, 'reviews_title', 'הלקוחות האהובות שלי מספרות...'),
(2586, 257, '_reviews_title', 'field_61311a4482d86'),
(2587, 257, 'reviews_slider', 'a:2:{i:0;s:3:\"214\";i:1;s:3:\"215\";}'),
(2588, 257, '_reviews_slider', 'field_61311a5782d87'),
(2589, 257, 'home_slider_seo', '2'),
(2590, 257, '_home_slider_seo', 'field_61311a92d1802'),
(2591, 257, 'home_slider_img', '206'),
(2592, 257, '_home_slider_img', 'field_61311a9fd1804'),
(2593, 257, 'home_gallery_img', 'a:7:{i:0;s:3:\"163\";i:1;s:3:\"164\";i:2;s:3:\"165\";i:3;s:3:\"166\";i:4;s:3:\"167\";i:5;s:3:\"168\";i:6;s:3:\"169\";}'),
(2594, 257, '_home_gallery_img', 'field_61311b1cb4b73'),
(2595, 257, 'home_inst_title', 'באינסטגרם שלי כבר ביקרת?'),
(2596, 257, '_home_inst_title', 'field_61311b3eb4b74'),
(2597, 257, 'home_inst_subtitle', 'עקבי והישארי מעודכנת!'),
(2598, 257, '_home_inst_subtitle', 'field_61311b7eb4b75'),
(2599, 257, 'home_video_block_title', 'צפו בסרטונים שלנו'),
(2600, 257, '_home_video_block_title', 'field_61311bbc1b54b'),
(2601, 257, 'home_videos', '5'),
(2602, 257, '_home_videos', 'field_61311bcd1b54c'),
(2603, 257, 'slider_img', '207'),
(2604, 257, '_slider_img', 'field_612f4e7693132'),
(2605, 257, 'faq_title', 'כל מה שרציתן לדעת -אני עונה'),
(2606, 257, '_faq_title', 'field_5feb2f82db93b'),
(2607, 257, 'faq_img', '156'),
(2608, 257, '_faq_img', 'field_5feb2f95db93c'),
(2609, 257, 'faq_item', '4'),
(2610, 257, '_faq_item', 'field_5feb2faddb93d'),
(2611, 257, 'main_slider_0_main_img', '212'),
(2612, 257, '_main_slider_0_main_img', 'field_613116b2930ca'),
(2613, 257, 'main_slider_0_main_title', 'עכשיו אפשר להגיד לילה טוב'),
(2614, 257, '_main_slider_0_main_title', 'field_613116c8930cb'),
(2615, 257, 'main_slider_0_main_link', 'a:3:{s:5:\"title\";s:46:\"הכירו את הפיג’מות החדשות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2616, 257, '_main_slider_0_main_link', 'field_613116ee930cc'),
(2617, 257, 'main_slider_1_main_img', '209'),
(2618, 257, '_main_slider_1_main_img', 'field_613116b2930ca'),
(2619, 257, 'main_slider_1_main_title', 'לורם איפסום דולור סיט אמט 1'),
(2620, 257, '_main_slider_1_main_title', 'field_613116c8930cb'),
(2621, 257, 'main_slider_1_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2622, 257, '_main_slider_1_main_link', 'field_613116ee930cc'),
(2623, 257, 'main_slider_2_main_img', '210'),
(2624, 257, '_main_slider_2_main_img', 'field_613116b2930ca'),
(2625, 257, 'main_slider_2_main_title', 'לורם איפסום דולור סיט אמט 2'),
(2626, 257, '_main_slider_2_main_title', 'field_613116c8930cb'),
(2627, 257, 'main_slider_2_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2628, 257, '_main_slider_2_main_link', 'field_613116ee930cc'),
(2629, 257, 'main_slider_3_main_img', '211'),
(2630, 257, '_main_slider_3_main_img', 'field_613116b2930ca'),
(2631, 257, 'main_slider_3_main_title', 'לורם איפסום דולור סיט אמט 3'),
(2632, 257, '_main_slider_3_main_title', 'field_613116c8930cb'),
(2633, 257, 'main_slider_3_main_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(2634, 257, '_main_slider_3_main_link', 'field_613116ee930cc'),
(2635, 257, 'home_slider_seo_0_content', '<h2>לורם איפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2636, 257, '_home_slider_seo_0_content', 'field_61311a92d1803'),
(2637, 257, 'home_slider_seo_1_content', '<h2>לורם איפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2638, 257, '_home_slider_seo_1_content', 'field_61311a92d1803'),
(2639, 257, 'single_slider_seo_0_content', '<h2>לורם איפסום 11</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2640, 257, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(2641, 257, 'single_slider_seo_1_content', '<h2>לורם איפסום 22</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.\r\n\r\nתצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2642, 257, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(2643, 257, 'faq_item_0_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 1?'),
(2644, 257, '_faq_item_0_faq_question', 'field_5feb2fc0db93e'),
(2645, 257, 'faq_item_0_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק.'),
(2646, 257, '_faq_item_0_faq_answer', 'field_5feb2fd3db93f'),
(2647, 257, 'faq_item_1_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 2?'),
(2648, 257, '_faq_item_1_faq_question', 'field_5feb2fc0db93e'),
(2649, 257, 'faq_item_1_faq_answer', 'תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2650, 257, '_faq_item_1_faq_answer', 'field_5feb2fd3db93f'),
(2651, 257, 'faq_item_2_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 3?'),
(2652, 257, '_faq_item_2_faq_question', 'field_5feb2fc0db93e'),
(2653, 257, 'faq_item_2_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(2654, 257, '_faq_item_2_faq_answer', 'field_5feb2fd3db93f'),
(2655, 257, 'faq_item_3_faq_question', 'לורם סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק 4?'),
(2656, 257, '_faq_item_3_faq_question', 'field_5feb2fc0db93e'),
(2657, 257, 'faq_item_3_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(2658, 257, '_faq_item_3_faq_answer', 'field_5feb2fd3db93f'),
(2659, 257, 'home_videos_0_video_item', 'https://www.youtube.com/watch?v=9N2_iEA1UkE'),
(2660, 257, '_home_videos_0_video_item', 'field_61311bd61b54d'),
(2661, 257, 'home_videos_1_video_item', 'https://www.youtube.com/watch?v=Ui-PRtEYd0Y'),
(2662, 257, '_home_videos_1_video_item', 'field_61311bd61b54d'),
(2663, 257, 'home_videos_2_video_item', 'https://www.youtube.com/watch?v=QBSDTRfqkiQ'),
(2664, 257, '_home_videos_2_video_item', 'field_61311bd61b54d'),
(2665, 257, 'home_videos_3_video_item', 'https://www.youtube.com/watch?v=sOdJMGVFYAg'),
(2666, 257, '_home_videos_3_video_item', 'field_61311bd61b54d'),
(2667, 257, 'home_videos_4_video_item', 'https://www.youtube.com/watch?v=ne_8xYNnFJA'),
(2668, 257, '_home_videos_4_video_item', 'field_61311bd61b54d'),
(2669, 262, '_wp_attached_file', '2021/09/menu-icon-1.png'),
(2670, 262, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:38;s:6:\"height\";i:53;s:4:\"file\";s:23:\"2021/09/menu-icon-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2671, 263, '_wp_attached_file', '2021/09/menu-icon-2.png'),
(2672, 263, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:49;s:6:\"height\";i:61;s:4:\"file\";s:23:\"2021/09/menu-icon-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2673, 264, '_wp_attached_file', '2021/09/menu-icon-3.png'),
(2674, 264, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:45;s:6:\"height\";i:59;s:4:\"file\";s:23:\"2021/09/menu-icon-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2675, 265, '_wp_attached_file', '2021/09/menu-icon-4.png'),
(2676, 265, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:70;s:6:\"height\";i:56;s:4:\"file\";s:23:\"2021/09/menu-icon-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2677, 266, '_wp_attached_file', '2021/09/menu-icon-5.png'),
(2678, 266, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:45;s:6:\"height\";i:47;s:4:\"file\";s:23:\"2021/09/menu-icon-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2679, 267, '_wp_attached_file', '2021/09/menu-icon-6.png'),
(2680, 267, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:38;s:6:\"height\";i:65;s:4:\"file\";s:23:\"2021/09/menu-icon-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2681, 268, '_wp_attached_file', '2021/09/menu-icon-7.png'),
(2682, 268, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:56;s:6:\"height\";i:53;s:4:\"file\";s:23:\"2021/09/menu-icon-7.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2683, 269, '_wp_attached_file', '2021/09/menu-icon-8.png'),
(2684, 269, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:59;s:6:\"height\";i:51;s:4:\"file\";s:23:\"2021/09/menu-icon-8.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_posts`
--

CREATE TABLE `fmn_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_posts`
--

INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-08-30 20:39:27', '2021-08-30 17:39:27', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש.</p>\r\n<p>קוויז דומור ליאמום בלינך רוגצה. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק.</p>\r\n<p>בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. </p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d', '', '', '2021-09-04 10:32:39', '2021-09-04 07:32:39', '', 0, 'http://anat:8888/?p=1', 0, 'post', '', 1),
(2, 1, '2021-08-30 20:39:27', '2021-08-30 17:39:27', '<!-- wp:paragraph -->\n<p>זהו עמוד לדוגמה. הוא שונה מפוסט רגיל בבלוג מכיוון שהוא יישאר במקום אחד ויופיע בתפריט הניווט באתר (ברוב התבניות). רוב האנשים מתחילים עם עמוד אודות המציג אותם למבקרים חדשים באתר. הנה תוכן לדוגמה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>אהלן! אני סטודנט לכלכלה ביום, אך מבלה את הלילות בהגשמת החלום שלי להיות שחקן תאטרון. אני גר ביפו, יש לי כלב נהדר בשם שוקו, אני אוהב ערק אשכוליות בשישי בצהריים (במיוחד תוך כדי משחק שש-בש על חוף הים). ברוכים הבאים לאתר שלי!</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>... או משהו כזה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>החברה א.א. שומדברים נוסדה בשנת 1971, והיא מספקת שומדברים איכותיים לציבור מאז. א.א. שומדברים ממוקמת בעיר תקוות-ים, מעסיקה מעל 2,000 אנשים ועושה כל מיני דברים מדהים עבור הקהילה התקוות-ימית.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>כמשתמש וורדפרס חדש, כדאי לבקר <a href=\"http://anat:8888/wp-admin/\">בלוח הבקרה שלך</a> כדי למחוק את העמוד הזה, וליצור עמודים חדשים עבור התוכן שלך. שיהיה בכיף!</p>\n<!-- /wp:paragraph -->', 'עמוד לדוגמא', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2021-08-30 20:39:27', '2021-08-30 17:39:27', '', 0, 'http://anat:8888/?page_id=2', 0, 'page', '', 0),
(3, 1, '2021-08-30 20:39:27', '2021-08-30 17:39:27', '<!-- wp:heading --><h2>מי אנחנו</h2><!-- /wp:heading --><!-- wp:paragraph --><p>כתובת האתר שלנו היא: http://anat:8888.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>מה המידע האישי שאנו אוספים ומדוע אנחנו אוספים אותו</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>תגובות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כאשר המבקרים משאירים תגובות באתר אנו אוספים את הנתונים המוצגים בטופס התגובה, ובנוסף גם את כתובת ה-IP של המבקר, ואת מחרוזת ה-user agent של הדפדפן שלו כדי לסייע בזיהוי תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>יתכן ונעביר מחרוזת אנונימית שנוצרה מכתובת הדואר האלקטרוני שלך (הנקראת גם hash) לשירות Gravatar כדי לראות אם הנך חבר/ה בשירות. מדיניות הפרטיות של שירות Gravatar זמינה כאן: https://automattic.com/privacy/. לאחר אישור התגובה שלך, תמונת הפרופיל שלך גלויה לציבור בהקשר של התגובה שלך.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>מדיה</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בהעלאה של תמונות לאתר, מומלץ להימנע מהעלאת תמונות עם נתוני מיקום מוטבעים (EXIF GPS). המבקרים באתר יכולים להוריד ולחלץ את כל נתוני מיקום מהתמונות באתר.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>טפסי יצירת קשר</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>עוגיות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בכתיבת תגובה באתר שלנו, באפשרותך להחליט אם לאפשר לנו לשמור את השם שלך, כתובת האימייל שלך וכתובת האתר שלך בקבצי עוגיות (cookies). השמירה תתבצע לנוחיותך, על מנת שלא יהיה צורך למלא את הפרטים שלך שוב בכתיבת תגובה נוספת. קבצי העוגיות ישמרו לשנה.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה מבקר בעמוד ההתחברות של האתר, נגדיר קובץ עוגיה זמני על מנת לקבוע האם הדפדפן שלך מקבל קבצי עוגיות. קובץ עוגיה זה אינו מכיל נתונים אישיים והוא נמחק בעת סגירת הדפדפן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>כאשר תתחבר, אנחנו גם נגדיר מספר \'עוגיות\' על מנת לשמור את פרטי ההתחברות שלך ואת בחירות התצוגה שלך. עוגיות התחברות תקפות ליומיים, ועוגיות אפשרויות מסך תקפות לשנה. אם תבחר באפשרות &quot;זכור אותי&quot;, פרטי ההתחברות שלך יהיו תקפים למשך שבועיים. אם תתנתק מהחשבון שלך, עוגיות ההתחברות יימחקו.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה עורך או מפרסם מאמר, קובץ \'עוגיה\' נוסף יישמר בדפדפן שלך. קובץ \'עוגיה\' זה אינו כולל נתונים אישיים ופשוט מציין את מזהה הפוסט של המאמר. הוא יפוג לאחר יום אחד.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>תוכן מוטמע מאתרים אחרים</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כתבות או פוסטים באתר זה עשויים לכלול תוכן מוטבע (לדוגמה, קטעי וידאו, תמונות, מאמרים, וכו\'). תוכן מוטבע מאתרי אינטרנט אחרים דינו כביקור הקורא באתרי האינטרנט מהם מוטבע התוכן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אתרים אלו עשויים לאסוף נתונים אודותיך, להשתמש בקבצי \'עוגיות\', להטמיע מעקב של צד שלישי נוסף, ולנטר את האינטראקציה שלך עם תוכן מוטמע זה, לרבות מעקב אחר האינטראקציה שלך עם התוכן המוטמע, אם יש לך חשבון ואתה מחובר לאתר זה.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>אנליטיקה</h3><!-- /wp:heading --><!-- wp:heading --><h2>עם מי אנו חולקים את המידע שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>משך הזמן בו נשמור את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>במידה ותגיב/י על תוכן באתר, התגובה והנתונים אודותיה יישמרו ללא הגבלת זמן, כדי שנוכל לזהות ולאשר את כל התגובות העוקבות באופן אוטומטי.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>עבור משתמשים רשומים באתר (במידה ויש כאלה) אנו מאחסנים גם את המידע האישי שהם מספקים בפרופיל המשתמש שלהם. כל המשתמשים יכולים לראות, לערוך או למחוק את המידע האישי שלהם בכל עת (פרט לשם המשתמש אותו לא ניתן לשנות). גם מנהלי האתר יכולים לראות ולערוך מידע זה.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>אילו זכויות יש לך על המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>אם יש לך חשבון באתר זה, או שהשארת תגובות באתר, באפשרותך לבקש לקבל קובץ של הנתונים האישיים שאנו מחזיקים לגביך, כולל כל הנתונים שסיפקת לנו. באפשרותך גם לבקש שנמחק כל מידע אישי שאנו מחזיקים לגביך. הדבר אינו כולל נתונים שאנו מחויבים לשמור למטרות מנהליות, משפטיות או ביטחוניות.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>להיכן אנו שולחים את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>תגובות מבקרים עלולות להיבדק על ידי שירות אוטומטי למניעת תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>פרטי ההתקשרות שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>מידע נוסף</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>כיצד אנו מגינים על המידע שלך</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>אילו הליכי פרצת נתונים יש לנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>מאילו גופי צד-שלישי אנו מקבלים מידע</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>קבלת החלטות אוטומטיות ו/או יצירת פרופילים שאנו עושים עם נתוני המשתמשים שלנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>דרישות גילוי רגולטוריות של התעשייה </h3><!-- /wp:heading -->', 'מדיניות פרטיות', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-08-30 20:39:27', '2021-08-30 17:39:27', '', 0, 'http://anat:8888/?page_id=3', 0, 'page', '', 0),
(4, 1, '2021-08-30 20:39:41', '0000-00-00 00:00:00', '', 'טיוטה משמירה אוטומטית', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-08-30 20:39:41', '0000-00-00 00:00:00', '', 0, 'http://anat:8888/?p=4', 0, 'post', '', 0),
(5, 1, '2021-08-31 22:26:30', '0000-00-00 00:00:00', '', 'טיוטה אוטומטית', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2021-08-31 22:26:30', '0000-00-00 00:00:00', '', 0, 'http://anat:8888/?page_id=5', 0, 'page', '', 0),
(7, 1, '2021-08-31 22:27:17', '2021-08-31 19:27:17', '', 'דף הבית', '', 'publish', 'closed', 'closed', '', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', '', '', '2021-09-05 17:43:24', '2021-09-05 14:43:24', '', 0, 'http://anat:8888/?page_id=7', 0, 'page', '', 0),
(8, 1, '2021-08-31 22:27:17', '2021-08-31 19:27:17', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-08-31 22:27:17', '2021-08-31 19:27:17', '', 7, 'http://anat:8888/?p=8', 0, 'revision', '', 0),
(9, 1, '2021-08-31 22:27:27', '2021-08-31 19:27:27', '', 'צור קשר', '', 'publish', 'closed', 'closed', '', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', '', '', '2021-09-03 18:39:44', '2021-09-03 15:39:44', '', 0, 'http://anat:8888/?page_id=9', 0, 'page', '', 0),
(10, 1, '2021-08-31 22:27:27', '2021-08-31 19:27:27', '', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2021-08-31 22:27:27', '2021-08-31 19:27:27', '', 9, 'http://anat:8888/?p=10', 0, 'revision', '', 0),
(11, 1, '2021-09-01 11:39:56', '2021-09-01 08:39:56', '', 'מאמרים', '', 'publish', 'closed', 'closed', '', '%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d', '', '', '2021-09-04 21:00:18', '2021-09-04 18:00:18', '', 0, 'http://anat:8888/?page_id=11', 0, 'page', '', 0),
(12, 1, '2021-09-01 11:39:56', '2021-09-01 08:39:56', '', 'מאמרים', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2021-09-01 11:39:56', '2021-09-01 08:39:56', '', 11, 'http://anat:8888/?p=12', 0, 'revision', '', 0),
(13, 1, '2021-09-01 12:56:53', '2021-09-01 09:56:53', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'פרטי קשר', '%d7%a4%d7%a8%d7%98%d7%99-%d7%a7%d7%a9%d7%a8', 'publish', 'closed', 'closed', '', 'group_5dd3a32b38e68', '', '', '2021-09-01 14:01:24', '2021-09-01 11:01:24', '', 0, 'http://anat:8888/?p=13', 0, 'acf-field-group', '', 0),
(14, 1, '2021-09-01 12:56:53', '2021-09-01 09:56:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'טלפון', 'tel', 'publish', 'closed', 'closed', '', 'field_5dd3a33f42e0c', '', '', '2021-09-01 12:56:53', '2021-09-01 09:56:53', '', 13, 'http://anat:8888/?post_type=acf-field&p=14', 0, 'acf-field', '', 0),
(15, 1, '2021-09-01 12:56:53', '2021-09-01 09:56:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'מייל', 'mail', 'publish', 'closed', 'closed', '', 'field_5dd3a35642e0d', '', '', '2021-09-01 12:56:53', '2021-09-01 09:56:53', '', 13, 'http://anat:8888/?post_type=acf-field&p=15', 1, 'acf-field', '', 0),
(16, 1, '2021-09-01 12:56:53', '2021-09-01 09:56:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'פקס', 'fax', 'publish', 'closed', 'closed', '', 'field_5dd3a36942e0e', '', '', '2021-09-01 12:56:53', '2021-09-01 09:56:53', '', 13, 'http://anat:8888/?post_type=acf-field&p=16', 2, 'acf-field', '', 0),
(17, 1, '2021-09-01 12:56:53', '2021-09-01 09:56:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כתובת', 'address', 'publish', 'closed', 'closed', '', 'field_5dd3a4d142e0f', '', '', '2021-09-01 12:56:53', '2021-09-01 09:56:53', '', 13, 'http://anat:8888/?post_type=acf-field&p=17', 3, 'acf-field', '', 0),
(18, 1, '2021-09-01 12:56:53', '2021-09-01 09:56:53', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'מפה (תמונה)', 'map_image', 'publish', 'closed', 'closed', '', 'field_5ddbd67734310', '', '', '2021-09-01 12:56:53', '2021-09-01 09:56:53', '', 13, 'http://anat:8888/?post_type=acf-field&p=18', 4, 'acf-field', '', 0),
(19, 1, '2021-09-01 12:56:53', '2021-09-01 09:56:53', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'פייסבוק', 'facebook', 'publish', 'closed', 'closed', '', 'field_5ddbd6b3cc0cd', '', '', '2021-09-01 12:56:53', '2021-09-01 09:56:53', '', 13, 'http://anat:8888/?post_type=acf-field&p=19', 5, 'acf-field', '', 0),
(20, 1, '2021-09-01 12:56:53', '2021-09-01 09:56:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Whatsapp', 'whatsapp', 'publish', 'closed', 'closed', '', 'field_5ddbd6cdcc0ce', '', '', '2021-09-01 12:56:53', '2021-09-01 09:56:53', '', 13, 'http://anat:8888/?post_type=acf-field&p=20', 6, 'acf-field', '', 0),
(21, 1, '2021-09-01 12:56:58', '2021-09-01 09:56:58', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"views/home.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תוספות תוכן', '%d7%aa%d7%95%d7%a1%d7%a4%d7%95%d7%aa-%d7%aa%d7%95%d7%9b%d7%9f', 'publish', 'closed', 'closed', '', 'group_5ddbde4d59412', '', '', '2021-09-02 23:19:55', '2021-09-02 20:19:55', '', 0, 'http://anat:8888/?p=21', 0, 'acf-field-group', '', 0),
(22, 1, '2021-09-01 12:56:58', '2021-09-01 09:56:58', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"70\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר SEO', 'single_slider_seo', 'publish', 'closed', 'closed', '', 'field_5ddbde5499115', '', '', '2021-09-01 12:57:34', '2021-09-01 09:57:34', '', 21, 'http://anat:8888/?post_type=acf-field&#038;p=22', 0, 'acf-field', '', 0),
(23, 1, '2021-09-01 12:56:58', '2021-09-01 09:56:58', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תוכן', 'content', 'publish', 'closed', 'closed', '', 'field_5ddbde7399116', '', '', '2021-09-01 12:56:58', '2021-09-01 09:56:58', '', 22, 'http://anat:8888/?post_type=acf-field&p=23', 0, 'acf-field', '', 0),
(27, 1, '2021-09-01 12:57:34', '2021-09-01 09:57:34', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'slider_img', 'publish', 'closed', 'closed', '', 'field_612f4e7693132', '', '', '2021-09-02 21:41:06', '2021-09-02 18:41:06', '', 21, 'http://anat:8888/?post_type=acf-field&#038;p=27', 1, 'acf-field', '', 0),
(28, 1, '2021-09-01 14:01:24', '2021-09-01 11:01:24', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Instagram', 'instagram', 'publish', 'closed', 'closed', '', 'field_612f5d7b40554', '', '', '2021-09-01 14:01:24', '2021-09-01 11:01:24', '', 13, 'http://anat:8888/?post_type=acf-field&p=28', 7, 'acf-field', '', 0),
(29, 1, '2021-09-01 15:18:37', '2021-09-01 12:18:37', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"views/home.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'דף הבית', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', 'publish', 'closed', 'closed', '', 'group_612f6e5b87cd9', '', '', '2021-09-05 02:31:18', '2021-09-04 23:31:18', '', 0, 'http://anat:8888/?post_type=acf-field-group&#038;p=29', 0, 'acf-field-group', '', 0),
(30, 1, '2021-09-01 15:18:37', '2021-09-01 12:18:37', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'סליידר ראשי', 'סליידר_ראשי', 'publish', 'closed', 'closed', '', 'field_612f6e66fcac1', '', '', '2021-09-01 15:18:37', '2021-09-01 12:18:37', '', 29, 'http://anat:8888/?post_type=acf-field&p=30', 0, 'acf-field', '', 0),
(31, 1, '2021-09-01 15:20:24', '2021-09-01 12:20:24', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-md-10 col-12\">\r\n		[text* text-827 placeholder \"שם מלא\"]\r\n	</div>\r\n	    <div class=\"col-md-10 col-12\">\r\n				[tel* tel-82 placeholder \"טלפון\"]\r\n	</div>\r\n    <div class=\"col-md-10 col-12\">\r\n			[email email-256 placeholder \"מייל\"]\r\n	</div>\r\n	    <div class=\"col-md-10 col-12\">\r\n				[submit \"הרשמה\"]\r\n	</div>\r\n		<div class=\"col-12 acceptance-col\">\r\n			[acceptance acceptance-951] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'פופאפ', '', 'publish', 'closed', 'closed', '', '%d7%a4%d7%95%d7%a4%d7%90%d7%a4', '', '', '2021-09-02 20:29:06', '2021-09-02 17:29:06', '', 0, 'http://anat:8888/?post_type=wpcf7_contact_form&#038;p=31', 0, 'wpcf7_contact_form', '', 0),
(32, 1, '2021-09-01 15:22:37', '2021-09-01 12:22:37', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-md-4 col-12\">\r\n		[text* text-124 placeholder \"שם מלא\"]\r\n	</div>\r\n	    <div class=\"col-md-4 col-12\">\r\n				[tel* tel-897 placeholder \"טלפון\"]\r\n	</div>\r\n    <div class=\"col-md-4 col-12\">\r\n			[email email-239 placeholder \"מייל\"]\r\n	</div>\r\n	    <div class=\"col-12\">\r\n				[textarea textarea-535 placeholder \"תוכן פנייה\"]\r\n	</div>\r\n	    <div class=\"col-12\">\r\n				[submit \"שליחת הפנייה\"]\r\n	</div>\r\n			<div class=\"col-12 acceptance-col\">\r\n				[acceptance acceptance-530] אני מאשר/ת קבלת דיוור פרסומי במייל [/acceptance]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'צור קשר', '', 'publish', 'closed', 'closed', '', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', '', '', '2021-09-03 18:46:22', '2021-09-03 15:46:22', '', 0, 'http://anat:8888/?post_type=wpcf7_contact_form&#038;p=32', 0, 'wpcf7_contact_form', '', 0),
(33, 1, '2021-09-01 15:25:12', '2021-09-01 12:25:12', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-xl-3 col-md-4 col-12\">\r\n		[text* text-496 placeholder \"שם מלא\"]\r\n	</div>\r\n	    <div class=\"col-xl-3 col-md-4 col-12\">\r\n				[tel* tel-687 placeholder \"טלפון\"]\r\n	</div>\r\n    <div class=\"col-xl-3 col-md-4 col-12\">\r\n			[email email-741 placeholder \"מייל\"]\r\n	</div>\r\n	    <div class=\"col-xl-3 col-12\">\r\n				[submit \"הרשמה\"]\r\n	</div>\r\n	<div class=\"col-12 acceptance-col\">\r\n		[acceptance acceptance-580] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'טופס חוזר', '', 'publish', 'closed', 'closed', '', '%d7%98%d7%95%d7%a4%d7%a1-%d7%97%d7%95%d7%96%d7%a8', '', '', '2021-09-02 20:27:27', '2021-09-02 17:27:27', '', 0, 'http://anat:8888/?post_type=wpcf7_contact_form&#038;p=33', 0, 'wpcf7_contact_form', '', 0),
(34, 1, '2021-09-01 15:26:57', '2021-09-01 12:26:57', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-md col-12\">\r\n		[email* email-199 placeholder \"מייל:\"]\r\n	</div>\r\n	    <div class=\"col-md-auto col-12\">\r\n				[submit \"אני מעוניינ/ת להשאר מעודכנת\"]\r\n	</div>\r\n	<div class=\"col-12 acceptance-col\">\r\n		[acceptance acceptance-604] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'טופס בפוטר', '', 'publish', 'closed', 'closed', '', '%d7%98%d7%95%d7%a4%d7%a1-%d7%91%d7%a4%d7%95%d7%98%d7%a8', '', '', '2021-09-03 18:46:07', '2021-09-03 15:46:07', '', 0, 'http://anat:8888/?post_type=wpcf7_contact_form&#038;p=34', 0, 'wpcf7_contact_form', '', 0),
(35, 1, '2021-09-02 18:53:55', '2021-09-02 15:53:55', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2021-09-02 18:53:55', '2021-09-02 15:53:55', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2021-09-02 18:53:56', '2021-09-02 15:53:56', '', 'מוצרים', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2021-09-04 21:26:56', '2021-09-04 18:26:56', '', 0, 'http://anat:8888/shop/', 0, 'page', '', 0),
(37, 1, '2021-09-02 18:53:56', '2021-09-02 15:53:56', '<!-- wp:shortcode -->[woocommerce_cart]<!-- /wp:shortcode -->', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2021-09-02 18:53:56', '2021-09-02 15:53:56', '', 0, 'http://anat:8888/cart/', 0, 'page', '', 0),
(38, 1, '2021-09-02 18:53:56', '2021-09-02 15:53:56', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2021-09-02 18:53:56', '2021-09-02 15:53:56', '', 0, 'http://anat:8888/checkout/', 0, 'page', '', 0),
(39, 1, '2021-09-02 18:53:56', '2021-09-02 15:53:56', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2021-09-02 18:53:56', '2021-09-02 15:53:56', '', 0, 'http://anat:8888/my-account/', 0, 'page', '', 0),
(40, 1, '2021-09-02 18:53:56', '0000-00-00 00:00:00', '<!-- wp:paragraph -->\n<p><b>This is a sample page.</b></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h3>Overview</h3>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our refund and returns policy lasts 30 days. If 30 days have passed since your purchase, we can’t offer you a full refund or exchange.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Several types of goods are exempt from being returned. Perishable goods such as food, flowers, newspapers or magazines cannot be returned. We also do not accept products that are intimate or sanitary goods, hazardous materials, or flammable liquids or gases.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Additional non-returnable items:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul>\n<li>Gift cards</li>\n<li>Downloadable software products</li>\n<li>Some health and personal care items</li>\n</ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>To complete your return, we require a receipt or proof of purchase.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Please do not send your purchase back to the manufacturer.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>There are certain situations where only partial refunds are granted:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul>\n<li>Book with obvious signs of use</li>\n<li>CD, DVD, VHS tape, software, video game, cassette tape, or vinyl record that has been opened.</li>\n<li>Any item not in its original condition, is damaged or missing parts for reasons not due to our error.</li>\n<li>Any item that is returned more than 30 days after delivery</li>\n</ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<h2>Refunds</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you are approved, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within a certain amount of days.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<b>Late or missing refunds</b>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you haven’t received a refund yet, first check your bank account again.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Then contact your credit card company, it may take some time before your refund is officially posted.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Next contact your bank. There is often some processing time before a refund is posted.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you’ve done all of this and you still have not received your refund yet, please contact us at {email address}.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<b>Sale items</b>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Only regular priced items may be refunded. Sale items cannot be refunded.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h2>Exchanges</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We only replace items if they are defective or damaged. If you need to exchange it for the same item, send us an email at {email address} and send your item to: {physical address}.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h2>Gifts</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If the item was marked as a gift when purchased and shipped directly to you, you’ll receive a gift credit for the value of your return. Once the returned item is received, a gift certificate will be mailed to you.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If the item wasn’t marked as a gift when purchased, or the gift giver had the order shipped to themselves to give to you later, we will send a refund to the gift giver and they will find out about your return.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h2>Shipping returns</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To return your product, you should mail your product to: {physical address}.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You will be responsible for paying for your own shipping costs for returning your item. Shipping costs are non-refundable. If you receive a refund, the cost of return shipping will be deducted from your refund.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Depending on where you live, the time it may take for your exchanged product to reach you may vary.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you are returning more expensive items, you may consider using a trackable shipping service or purchasing shipping insurance. We don’t guarantee that we will receive your returned item.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h2>Need help?</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Contact us at {email} for questions related to refunds and returns.</p>\n<!-- /wp:paragraph -->', 'Refund and Returns Policy', '', 'draft', 'closed', 'closed', '', 'refund_returns', '', '', '2021-09-02 18:53:56', '0000-00-00 00:00:00', '', 0, 'http://anat:8888/?page_id=40', 0, 'page', '', 0),
(41, 1, '2021-09-02 19:03:27', '2021-09-02 16:03:27', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"product\";}}}s:8:\"position\";s:15:\"acf_after_title\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'מוצר', '%d7%9e%d7%95%d7%a6%d7%a8', 'publish', 'closed', 'closed', '', 'group_6130f524ca7e3', '', '', '2021-09-02 19:05:29', '2021-09-02 16:05:29', '', 0, 'http://anat:8888/?post_type=acf-field-group&#038;p=41', 0, 'acf-field-group', '', 0),
(42, 1, '2021-09-02 19:03:28', '2021-09-02 16:03:28', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;}', 'משלוחים והחזרות', 'prod_delivery_info', 'publish', 'closed', 'closed', '', 'field_6130f52c5d7ef', '', '', '2021-09-02 19:05:22', '2021-09-02 16:05:22', '', 41, 'http://anat:8888/?post_type=acf-field&#038;p=42', 1, 'acf-field', '', 0),
(43, 1, '2021-09-02 19:05:22', '2021-09-02 16:05:22', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'מחיר ל- 100ml', 'per_100', 'publish', 'closed', 'closed', '', 'field_6130f5e6485b5', '', '', '2021-09-02 19:05:22', '2021-09-02 16:05:22', '', 41, 'http://anat:8888/?post_type=acf-field&p=43', 0, 'acf-field', '', 0),
(44, 1, '2021-09-02 19:29:31', '2021-09-02 16:29:31', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'מאמר', '%d7%9e%d7%90%d7%9e%d7%a8', 'publish', 'closed', 'closed', '', 'group_6130f6a0686e5', '', '', '2021-09-02 19:31:59', '2021-09-02 16:31:59', '', 0, 'http://anat:8888/?post_type=acf-field-group&#038;p=44', 0, 'acf-field-group', '', 0),
(45, 1, '2021-09-02 19:29:31', '2021-09-02 16:29:31', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'מאמרים נוספים', 'מאמרים_נוספים', 'publish', 'closed', 'closed', '', 'field_6130fba398e00', '', '', '2021-09-02 19:29:31', '2021-09-02 16:29:31', '', 44, 'http://anat:8888/?post_type=acf-field&p=45', 0, 'acf-field', '', 0),
(46, 1, '2021-09-02 19:29:31', '2021-09-02 16:29:31', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'כותרת', 'same_title', 'publish', 'closed', 'closed', '', 'field_6130fbc198e01', '', '', '2021-09-02 19:31:59', '2021-09-02 16:31:59', '', 44, 'http://anat:8888/?post_type=acf-field&#038;p=46', 1, 'acf-field', '', 0),
(47, 1, '2021-09-02 19:29:31', '2021-09-02 16:29:31', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:4:\"post\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מאמרים נוספים', 'same_posts', 'publish', 'closed', 'closed', '', 'field_6130fbd798e02', '', '', '2021-09-02 19:29:31', '2021-09-02 16:29:31', '', 44, 'http://anat:8888/?post_type=acf-field&p=47', 2, 'acf-field', '', 0),
(48, 1, '2021-09-02 19:29:57', '2021-09-02 16:29:57', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לכל המאמרים', 'same_posts_link', 'publish', 'closed', 'closed', '', 'field_6130fbf0282c7', '', '', '2021-09-02 19:29:57', '2021-09-02 16:29:57', '', 44, 'http://anat:8888/?post_type=acf-field&p=48', 3, 'acf-field', '', 0),
(49, 1, '2021-09-02 19:31:35', '2021-09-02 16:31:35', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תוספות', '%d7%aa%d7%95%d7%a1%d7%a4%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_6130fc1191eb6', '', '', '2021-09-05 18:25:51', '2021-09-05 15:25:51', '', 0, 'http://anat:8888/?post_type=acf-field-group&#038;p=49', 0, 'acf-field-group', '', 0),
(50, 1, '2021-09-02 19:31:35', '2021-09-02 16:31:35', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'פוסט', 'פוסט', 'publish', 'closed', 'closed', '', 'field_6130fc1a33f76', '', '', '2021-09-02 19:31:35', '2021-09-02 16:31:35', '', 49, 'http://anat:8888/?post_type=acf-field&p=50', 0, 'acf-field', '', 0),
(51, 1, '2021-09-02 19:31:35', '2021-09-02 16:31:35', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת של טופס', 'post_form_title', 'publish', 'closed', 'closed', '', 'field_6130fc2233f77', '', '', '2021-09-02 19:31:35', '2021-09-02 16:31:35', '', 49, 'http://anat:8888/?post_type=acf-field&p=51', 1, 'acf-field', '', 0),
(52, 1, '2021-09-02 19:31:35', '2021-09-02 16:31:35', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'post_form_text', 'publish', 'closed', 'closed', '', 'field_6130fc3133f78', '', '', '2021-09-02 19:31:35', '2021-09-02 16:31:35', '', 49, 'http://anat:8888/?post_type=acf-field&p=52', 2, 'acf-field', '', 0),
(53, 1, '2021-09-02 19:31:35', '2021-09-02 16:31:35', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לכל המאמרים', 'to_blog_link', 'publish', 'closed', 'closed', '', 'field_6130fc4933f79', '', '', '2021-09-02 19:31:35', '2021-09-02 16:31:35', '', 49, 'http://anat:8888/?post_type=acf-field&p=53', 3, 'acf-field', '', 0),
(54, 1, '2021-09-02 19:35:11', '2021-09-02 16:35:11', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'חדשות', 'חדשות', 'publish', 'closed', 'closed', '', 'field_6130fca12f191', '', '', '2021-09-02 19:35:11', '2021-09-02 16:35:11', '', 49, 'http://anat:8888/?post_type=acf-field&p=54', 4, 'acf-field', '', 0),
(55, 1, '2021-09-02 19:35:11', '2021-09-02 16:35:11', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'לוגו של מותג', 'header_brand_logo', 'publish', 'closed', 'closed', '', 'field_6130fcce2f192', '', '', '2021-09-02 19:35:11', '2021-09-02 16:35:11', '', 49, 'http://anat:8888/?post_type=acf-field&p=55', 5, 'acf-field', '', 0),
(56, 1, '2021-09-02 19:35:11', '2021-09-02 16:35:11', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'קישור', 'header_brand_link', 'publish', 'closed', 'closed', '', 'field_6130fcf92f193', '', '', '2021-09-02 19:35:11', '2021-09-02 16:35:11', '', 49, 'http://anat:8888/?post_type=acf-field&p=56', 6, 'acf-field', '', 0),
(57, 1, '2021-09-02 19:35:11', '2021-09-02 16:35:11', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר עם חדשות', 'news_slider', 'publish', 'closed', 'closed', '', 'field_6130fd0e2f194', '', '', '2021-09-02 19:35:11', '2021-09-02 16:35:11', '', 49, 'http://anat:8888/?post_type=acf-field&p=57', 7, 'acf-field', '', 0),
(58, 1, '2021-09-02 19:35:11', '2021-09-02 16:35:11', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'טקסט', 'news_text', 'publish', 'closed', 'closed', '', 'field_6130fd242f195', '', '', '2021-09-02 19:35:11', '2021-09-02 16:35:11', '', 57, 'http://anat:8888/?post_type=acf-field&p=58', 0, 'acf-field', '', 0),
(59, 1, '2021-09-02 19:35:11', '2021-09-02 16:35:11', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'news_link', 'publish', 'closed', 'closed', '', 'field_6130fd2f2f196', '', '', '2021-09-02 19:35:11', '2021-09-02 16:35:11', '', 57, 'http://anat:8888/?post_type=acf-field&p=59', 1, 'acf-field', '', 0),
(60, 1, '2021-09-02 19:37:42', '2021-09-02 16:37:42', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'פופאפ', 'פופאפ', 'publish', 'closed', 'closed', '', 'field_6130fd4b974ad', '', '', '2021-09-02 19:37:42', '2021-09-02 16:37:42', '', 49, 'http://anat:8888/?post_type=acf-field&p=60', 8, 'acf-field', '', 0),
(61, 1, '2021-09-02 19:37:42', '2021-09-02 16:37:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת טופס', 'pop_form_title', 'publish', 'closed', 'closed', '', 'field_6130fd67974ae', '', '', '2021-09-02 19:37:42', '2021-09-02 16:37:42', '', 49, 'http://anat:8888/?post_type=acf-field&p=61', 9, 'acf-field', '', 0),
(62, 1, '2021-09-02 19:37:42', '2021-09-02 16:37:42', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'pop_form_text', 'publish', 'closed', 'closed', '', 'field_6130fd78974af', '', '', '2021-09-02 19:37:42', '2021-09-02 16:37:42', '', 49, 'http://anat:8888/?post_type=acf-field&p=62', 10, 'acf-field', '', 0),
(63, 1, '2021-09-02 19:37:42', '2021-09-02 16:37:42', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'פוטר', 'פוטר', 'publish', 'closed', 'closed', '', 'field_6130fd88974b0', '', '', '2021-09-02 19:37:42', '2021-09-02 16:37:42', '', 49, 'http://anat:8888/?post_type=acf-field&p=63', 11, 'acf-field', '', 0),
(64, 1, '2021-09-02 19:37:42', '2021-09-02 16:37:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת טופס', 'foo_form_title', 'publish', 'closed', 'closed', '', 'field_6130fd9a974b1', '', '', '2021-09-02 19:37:42', '2021-09-02 16:37:42', '', 49, 'http://anat:8888/?post_type=acf-field&p=64', 12, 'acf-field', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(65, 1, '2021-09-02 19:37:42', '2021-09-02 16:37:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת משנה', 'foo_form_subtitle', 'publish', 'closed', 'closed', '', 'field_6130fdad974b2', '', '', '2021-09-02 19:37:42', '2021-09-02 16:37:42', '', 49, 'http://anat:8888/?post_type=acf-field&p=65', 13, 'acf-field', '', 0),
(66, 1, '2021-09-02 19:37:42', '2021-09-02 16:37:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת תפריט עם קישורים 1', 'menu_title_1', 'publish', 'closed', 'closed', '', 'field_6130fdbb974b3', '', '', '2021-09-02 19:37:42', '2021-09-02 16:37:42', '', 49, 'http://anat:8888/?post_type=acf-field&p=66', 14, 'acf-field', '', 0),
(67, 1, '2021-09-02 19:37:52', '2021-09-02 16:37:52', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת תפריט עם קישורים 2', 'menu_title_2', 'publish', 'closed', 'closed', '', 'field_6130fddd2e53d', '', '', '2021-09-02 19:37:52', '2021-09-02 16:37:52', '', 49, 'http://anat:8888/?post_type=acf-field&p=67', 15, 'acf-field', '', 0),
(70, 1, '2021-09-02 19:38:30', '2021-09-02 16:38:30', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תמונות', '%d7%aa%d7%9e%d7%95%d7%a0%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_5dd3a22d0e311', '', '', '2021-09-02 19:39:15', '2021-09-02 16:39:15', '', 0, 'http://anat:8888/?p=70', 0, 'acf-field-group', '', 0),
(71, 1, '2021-09-02 19:38:30', '2021-09-02 16:38:30', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'לוגו', 'logo', 'publish', 'closed', 'closed', '', 'field_5dd3a310760b9', '', '', '2021-09-02 19:38:30', '2021-09-02 16:38:30', '', 70, 'http://anat:8888/?post_type=acf-field&p=71', 0, 'acf-field', '', 0),
(72, 1, '2021-09-02 19:39:15', '2021-09-02 16:39:15', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'לוגו בפוטר', 'logo_foo', 'publish', 'closed', 'closed', '', 'field_6130fe1a32ded', '', '', '2021-09-02 19:39:15', '2021-09-02 16:39:15', '', 70, 'http://anat:8888/?post_type=acf-field&p=72', 1, 'acf-field', '', 0),
(73, 1, '2021-09-02 20:32:21', '2021-09-02 17:32:21', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-md-10 col-12\">\r\n		[text* text-143 placeholder \"שם מלא\"]\r\n	</div>\r\n	    <div class=\"col-md-10 col-12\">\r\n				[tel* tel-125 placeholder \"טלפון\"]\r\n	</div>\r\n    <div class=\"col-md-10 col-12\">\r\n			[email* email-971 placeholder \"מייל\"]\r\n	</div>\r\n	    <div class=\"col-md-10 col-12\">\r\n				[submit \"הרשמה\"]\r\n	</div>\r\n		<div class=\"col-12 acceptance-col\">\r\n			[acceptance acceptance-535] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'פוסט, חנות/קטגוריה', '', 'publish', 'closed', 'closed', '', '%d7%a4%d7%95%d7%a1%d7%98', '', '', '2021-09-02 20:42:21', '2021-09-02 17:42:21', '', 0, 'http://anat:8888/?post_type=wpcf7_contact_form&#038;p=73', 0, 'wpcf7_contact_form', '', 0),
(74, 1, '2021-09-02 20:43:46', '2021-09-02 17:43:46', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-12\">\r\n		[text* text-316 placeholder \"שם מלא\"]\r\n	</div>\r\n	    <div class=\"col-12\">\r\n				[tel* tel-636 placeholder \"טלפון\"]\r\n	</div>\r\n    <div class=\"col-12\">\r\n			[email* email-326 placeholder \"מייל\"]\r\n	</div>\r\n	    <div class=\"col-12\">\r\n				[submit \"הרשמה\"]\r\n	</div>\r\n		<div class=\"col-12 acceptance-col\">\r\n			[acceptance acceptance-858] אני מאשרת קבלת דיוור פרסומי במייל [/acceptance]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@anat>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'חנות/קטגוריה', '', 'publish', 'closed', 'closed', '', '%d7%9c%d7%9c%d7%90-%d7%9b%d7%95%d7%aa%d7%a8%d7%aa', '', '', '2021-09-02 20:44:12', '2021-09-02 17:44:12', '', 0, 'http://anat:8888/?post_type=wpcf7_contact_form&#038;p=74', 0, 'wpcf7_contact_form', '', 0),
(75, 1, '2021-09-02 21:25:12', '2021-09-02 18:25:12', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר ראשי', 'main_slider', 'publish', 'closed', 'closed', '', 'field_6131169e930c9', '', '', '2021-09-02 21:25:12', '2021-09-02 18:25:12', '', 29, 'http://anat:8888/?post_type=acf-field&p=75', 1, 'acf-field', '', 0),
(76, 1, '2021-09-02 21:25:12', '2021-09-02 18:25:12', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"2048x2048\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'main_img', 'publish', 'closed', 'closed', '', 'field_613116b2930ca', '', '', '2021-09-02 21:25:12', '2021-09-02 18:25:12', '', 75, 'http://anat:8888/?post_type=acf-field&p=76', 0, 'acf-field', '', 0),
(77, 1, '2021-09-02 21:25:12', '2021-09-02 18:25:12', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'main_title', 'publish', 'closed', 'closed', '', 'field_613116c8930cb', '', '', '2021-09-02 21:25:12', '2021-09-02 18:25:12', '', 75, 'http://anat:8888/?post_type=acf-field&p=77', 1, 'acf-field', '', 0),
(78, 1, '2021-09-02 21:25:12', '2021-09-02 18:25:12', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'main_link', 'publish', 'closed', 'closed', '', 'field_613116ee930cc', '', '', '2021-09-02 21:25:12', '2021-09-02 18:25:12', '', 75, 'http://anat:8888/?post_type=acf-field&p=78', 2, 'acf-field', '', 0),
(79, 1, '2021-09-02 21:33:29', '2021-09-02 18:33:29', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'קטגוריות', 'קטגוריות', 'publish', 'closed', 'closed', '', 'field_6131170c5ce67', '', '', '2021-09-02 21:41:33', '2021-09-02 18:41:33', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=79', 2, 'acf-field', '', 0),
(80, 1, '2021-09-02 21:33:29', '2021-09-02 18:33:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_cats_title', 'publish', 'closed', 'closed', '', 'field_6131171d5ce68', '', '', '2021-09-02 21:41:33', '2021-09-02 18:41:33', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=80', 3, 'acf-field', '', 0),
(81, 1, '2021-09-02 21:33:29', '2021-09-02 18:33:29', 'a:13:{s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:8:\"taxonomy\";s:11:\"product_cat\";s:10:\"field_type\";s:8:\"checkbox\";s:8:\"add_term\";i:1;s:10:\"save_terms\";i:0;s:10:\"load_terms\";i:0;s:13:\"return_format\";s:6:\"object\";s:8:\"multiple\";i:0;s:10:\"allow_null\";i:0;}', 'קטגוריות', 'home_cats', 'publish', 'closed', 'closed', '', 'field_613117265ce69', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=81', 4, 'acf-field', '', 0),
(82, 1, '2021-09-02 21:33:29', '2021-09-02 18:33:29', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'אודות', 'אודות', 'publish', 'closed', 'closed', '', 'field_6131173b5ce6a', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=82', 5, 'acf-field', '', 0),
(83, 1, '2021-09-02 21:33:29', '2021-09-02 18:33:29', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'home_about_text', 'publish', 'closed', 'closed', '', 'field_6131188d5ce6b', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=83', 6, 'acf-field', '', 0),
(84, 1, '2021-09-02 21:33:29', '2021-09-02 18:33:29', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'home_about_link', 'publish', 'closed', 'closed', '', 'field_613118a45ce6c', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=84', 7, 'acf-field', '', 0),
(85, 1, '2021-09-02 21:33:29', '2021-09-02 18:33:29', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'home_about_img', 'publish', 'closed', 'closed', '', 'field_613118e75ce6d', '', '', '2021-09-05 02:31:18', '2021-09-04 23:31:18', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=85', 8, 'acf-field', '', 0),
(86, 1, '2021-09-02 21:35:52', '2021-09-02 18:35:52', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם מוצרים 1', 'בלוק_עם_מוצרים_1', 'publish', 'closed', 'closed', '', 'field_613119390b836', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=86', 9, 'acf-field', '', 0),
(87, 1, '2021-09-02 21:35:52', '2021-09-02 18:35:52', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_prods_title_1', 'publish', 'closed', 'closed', '', 'field_6131194a0b837', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=87', 10, 'acf-field', '', 0),
(88, 1, '2021-09-02 21:35:52', '2021-09-02 18:35:52', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'home_prods_link_1', 'publish', 'closed', 'closed', '', 'field_613119550b838', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=88', 11, 'acf-field', '', 0),
(89, 1, '2021-09-02 21:35:52', '2021-09-02 18:35:52', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מוצרים', 'home_prods_1', 'publish', 'closed', 'closed', '', 'field_613119670b839', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=89', 12, 'acf-field', '', 0),
(90, 1, '2021-09-02 21:37:16', '2021-09-02 18:37:16', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'באנר', 'באנר', 'publish', 'closed', 'closed', '', 'field_6131198f011e9', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=90', 13, 'acf-field', '', 0),
(91, 1, '2021-09-02 21:37:16', '2021-09-02 18:37:16', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"2048x2048\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונת רקע', 'banner_back_img', 'publish', 'closed', 'closed', '', 'field_6131199a011ea', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=91', 14, 'acf-field', '', 0),
(92, 1, '2021-09-02 21:37:16', '2021-09-02 18:37:16', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'banner_link', 'publish', 'closed', 'closed', '', 'field_613119b1011eb', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=92', 15, 'acf-field', '', 0),
(93, 1, '2021-09-02 21:37:16', '2021-09-02 18:37:16', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'banner_title', 'publish', 'closed', 'closed', '', 'field_613119c1011ec', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=93', 16, 'acf-field', '', 0),
(94, 1, '2021-09-02 21:37:16', '2021-09-02 18:37:16', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת משנה', 'banner_subtitle', 'publish', 'closed', 'closed', '', 'field_613119ca011ed', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=94', 17, 'acf-field', '', 0),
(95, 1, '2021-09-02 21:37:48', '2021-09-02 18:37:48', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם מוצרים 2', '_העתק', 'publish', 'closed', 'closed', '', 'field_613119ed2cfe9', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=95', 18, 'acf-field', '', 0),
(96, 1, '2021-09-02 21:37:48', '2021-09-02 18:37:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_prods_title_2', 'publish', 'closed', 'closed', '', 'field_613119f42cfea', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=96', 19, 'acf-field', '', 0),
(97, 1, '2021-09-02 21:37:48', '2021-09-02 18:37:48', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'home_prods_link_2', 'publish', 'closed', 'closed', '', 'field_613119f72cfeb', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=97', 20, 'acf-field', '', 0),
(98, 1, '2021-09-02 21:37:48', '2021-09-02 18:37:48', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מוצרים', 'home_prods_2', 'publish', 'closed', 'closed', '', 'field_613119f92cfec', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=98', 21, 'acf-field', '', 0),
(99, 1, '2021-09-02 21:39:47', '2021-09-02 18:39:47', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'המלצות', 'המלצות', 'publish', 'closed', 'closed', '', 'field_61311a1f82d84', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=99', 22, 'acf-field', '', 0),
(100, 1, '2021-09-02 21:39:47', '2021-09-02 18:39:47', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'reviews_image', 'publish', 'closed', 'closed', '', 'field_61311a2f82d85', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=100', 23, 'acf-field', '', 0),
(101, 1, '2021-09-02 21:39:47', '2021-09-02 18:39:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת בלוק', 'reviews_title', 'publish', 'closed', 'closed', '', 'field_61311a4482d86', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=101', 24, 'acf-field', '', 0),
(102, 1, '2021-09-02 21:39:47', '2021-09-02 18:39:47', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'המלצות', 'reviews_slider', 'publish', 'closed', 'closed', '', 'field_61311a5782d87', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=102', 25, 'acf-field', '', 0),
(103, 1, '2021-09-02 21:40:48', '2021-09-02 18:40:48', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"70\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר SEO', 'home_slider_seo', 'publish', 'closed', 'closed', '', 'field_61311a92d1802', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=103', 27, 'acf-field', '', 0),
(104, 1, '2021-09-02 21:40:48', '2021-09-02 18:40:48', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תוכן', 'content', 'publish', 'closed', 'closed', '', 'field_61311a92d1803', '', '', '2021-09-02 21:40:48', '2021-09-02 18:40:48', '', 103, 'http://anat:8888/?post_type=acf-field&p=104', 0, 'acf-field', '', 0),
(105, 1, '2021-09-02 21:40:48', '2021-09-02 18:40:48', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'home_slider_img', 'publish', 'closed', 'closed', '', 'field_61311a9fd1804', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=105', 28, 'acf-field', '', 0),
(106, 1, '2021-09-02 21:41:33', '2021-09-02 18:41:33', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'סליידר SEO 1', 'סליידר_seo_1', 'publish', 'closed', 'closed', '', 'field_61311ad213d94', '', '', '2021-09-02 21:41:48', '2021-09-02 18:41:48', '', 29, 'http://anat:8888/?post_type=acf-field&#038;p=106', 26, 'acf-field', '', 0),
(107, 1, '2021-09-02 21:44:50', '2021-09-02 18:44:50', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'גלריית תמונות', 'גלריית_תמונות', 'publish', 'closed', 'closed', '', 'field_61311af5b4b72', '', '', '2021-09-02 21:44:50', '2021-09-02 18:44:50', '', 29, 'http://anat:8888/?post_type=acf-field&p=107', 29, 'acf-field', '', 0),
(108, 1, '2021-09-02 21:44:50', '2021-09-02 18:44:50', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'גלריית תמונות', 'home_gallery_img', 'publish', 'closed', 'closed', '', 'field_61311b1cb4b73', '', '', '2021-09-02 21:44:50', '2021-09-02 18:44:50', '', 29, 'http://anat:8888/?post_type=acf-field&p=108', 30, 'acf-field', '', 0),
(109, 1, '2021-09-02 21:44:50', '2021-09-02 18:44:50', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת של פריט עם קישור לאינסטגרם', 'home_inst_title', 'publish', 'closed', 'closed', '', 'field_61311b3eb4b74', '', '', '2021-09-02 21:44:50', '2021-09-02 18:44:50', '', 29, 'http://anat:8888/?post_type=acf-field&p=109', 31, 'acf-field', '', 0),
(110, 1, '2021-09-02 21:44:50', '2021-09-02 18:44:50', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת משנה', 'home_inst_subtitle', 'publish', 'closed', 'closed', '', 'field_61311b7eb4b75', '', '', '2021-09-02 21:44:50', '2021-09-02 18:44:50', '', 29, 'http://anat:8888/?post_type=acf-field&p=110', 32, 'acf-field', '', 0),
(111, 1, '2021-09-02 21:45:59', '2021-09-02 18:45:59', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'סליידר עם סרטונים', 'סליידר_עם_סרטונים', 'publish', 'closed', 'closed', '', 'field_61311ba71b54a', '', '', '2021-09-02 21:45:59', '2021-09-02 18:45:59', '', 29, 'http://anat:8888/?post_type=acf-field&p=111', 33, 'acf-field', '', 0),
(112, 1, '2021-09-02 21:45:59', '2021-09-02 18:45:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_video_block_title', 'publish', 'closed', 'closed', '', 'field_61311bbc1b54b', '', '', '2021-09-02 21:45:59', '2021-09-02 18:45:59', '', 29, 'http://anat:8888/?post_type=acf-field&p=112', 34, 'acf-field', '', 0),
(113, 1, '2021-09-02 21:45:59', '2021-09-02 18:45:59', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סרטונים', 'home_videos', 'publish', 'closed', 'closed', '', 'field_61311bcd1b54c', '', '', '2021-09-02 21:45:59', '2021-09-02 18:45:59', '', 29, 'http://anat:8888/?post_type=acf-field&p=113', 35, 'acf-field', '', 0),
(114, 1, '2021-09-02 21:45:59', '2021-09-02 18:45:59', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'קישור לוידאו', 'video_item', 'publish', 'closed', 'closed', '', 'field_61311bd61b54d', '', '', '2021-09-02 21:45:59', '2021-09-02 18:45:59', '', 113, 'http://anat:8888/?post_type=acf-field&p=114', 0, 'acf-field', '', 0),
(115, 1, '2021-09-02 22:41:43', '2021-09-02 19:41:43', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Payments', 'payments', 'publish', 'closed', 'closed', '', 'field_613128dbc0b8c', '', '', '2021-09-02 22:41:43', '2021-09-02 19:41:43', '', 49, 'http://anat:8888/?post_type=acf-field&p=115', 16, 'acf-field', '', 0),
(116, 1, '2021-09-02 23:13:55', '2021-09-02 20:13:55', ' ', '', '', 'publish', 'closed', 'closed', '', '116', '', '', '2021-09-02 23:13:55', '2021-09-02 20:13:55', '', 0, 'http://anat:8888/?p=116', 1, 'nav_menu_item', '', 0),
(117, 1, '2021-09-02 23:13:55', '2021-09-02 20:13:55', ' ', '', '', 'publish', 'closed', 'closed', '', '117', '', '', '2021-09-02 23:13:55', '2021-09-02 20:13:55', '', 0, 'http://anat:8888/?p=117', 2, 'nav_menu_item', '', 0),
(118, 1, '2021-09-02 23:13:55', '2021-09-02 20:13:55', ' ', '', '', 'publish', 'closed', 'closed', '', '118', '', '', '2021-09-02 23:13:55', '2021-09-02 20:13:55', '', 0, 'http://anat:8888/?p=118', 3, 'nav_menu_item', '', 0),
(119, 1, '2021-09-02 23:13:55', '2021-09-02 20:13:55', ' ', '', '', 'publish', 'closed', 'closed', '', '119', '', '', '2021-09-02 23:13:55', '2021-09-02 20:13:55', '', 0, 'http://anat:8888/?p=119', 4, 'nav_menu_item', '', 0),
(120, 1, '2021-09-04 21:25:14', '2021-09-02 20:14:55', ' ', '', '', 'publish', 'closed', 'closed', '', '120', '', '', '2021-09-04 21:25:14', '2021-09-04 18:25:14', '', 0, 'http://anat:8888/?p=120', 1, 'nav_menu_item', '', 0),
(121, 1, '2021-09-04 21:25:14', '2021-09-02 20:14:55', ' ', '', '', 'publish', 'closed', 'closed', '', '121', '', '', '2021-09-04 21:25:14', '2021-09-04 18:25:14', '', 0, 'http://anat:8888/?p=121', 2, 'nav_menu_item', '', 0),
(122, 1, '2021-09-04 21:25:14', '2021-09-02 20:14:55', ' ', '', '', 'publish', 'closed', 'closed', '', '122', '', '', '2021-09-04 21:25:14', '2021-09-04 18:25:14', '', 0, 'http://anat:8888/?p=122', 4, 'nav_menu_item', '', 0),
(123, 1, '2021-09-04 21:25:14', '2021-09-02 20:14:55', ' ', '', '', 'publish', 'closed', 'closed', '', '123', '', '', '2021-09-04 21:25:14', '2021-09-04 18:25:14', '', 0, 'http://anat:8888/?p=123', 5, 'nav_menu_item', '', 0),
(124, 1, '2021-09-02 23:15:18', '2021-09-02 20:15:18', ' ', '', '', 'publish', 'closed', 'closed', '', '124', '', '', '2021-09-02 23:15:18', '2021-09-02 20:15:18', '', 0, 'http://anat:8888/?p=124', 1, 'nav_menu_item', '', 0),
(125, 1, '2021-09-02 23:16:06', '2021-09-02 20:16:06', ' ', '', '', 'publish', 'closed', 'closed', '', '125', '', '', '2021-09-02 23:16:06', '2021-09-02 20:16:06', '', 0, 'http://anat:8888/?p=125', 1, 'nav_menu_item', '', 0),
(126, 1, '2021-09-02 23:16:06', '2021-09-02 20:16:06', ' ', '', '', 'publish', 'closed', 'closed', '', '126', '', '', '2021-09-02 23:16:06', '2021-09-02 20:16:06', '', 0, 'http://anat:8888/?p=126', 2, 'nav_menu_item', '', 0),
(127, 1, '2021-09-02 23:16:53', '2021-09-02 20:16:53', '<h1>היי, אני ענת!</h1>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך! החלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם.\r\n\r\nמוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות. הבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך?\r\n\r\nעדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.', 'אודות', '', 'publish', 'closed', 'closed', '', '%d7%90%d7%95%d7%93%d7%95%d7%aa', '', '', '2021-09-04 22:58:43', '2021-09-04 19:58:43', '', 0, 'http://anat:8888/?page_id=127', 0, 'page', '', 0),
(128, 1, '2021-09-02 23:16:53', '2021-09-02 20:16:53', '', 'אודות', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2021-09-02 23:16:53', '2021-09-02 20:16:53', '', 127, 'http://anat:8888/?p=128', 0, 'revision', '', 0),
(129, 1, '2021-09-03 18:13:31', '2021-09-03 15:13:31', '', 'footer-logo', '', 'inherit', 'open', 'closed', '', 'footer-logo', '', '', '2021-09-03 18:13:31', '2021-09-03 15:13:31', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/footer-logo.png', 0, 'attachment', 'image/png', 0),
(130, 1, '2021-09-03 18:13:42', '2021-09-03 15:13:42', '', 'logo-text', '', 'inherit', 'open', 'closed', '', 'logo-text', '', '', '2021-09-03 18:13:42', '2021-09-03 15:13:42', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/logo-text.png', 0, 'attachment', 'image/png', 0),
(131, 1, '2021-09-03 18:16:23', '2021-09-03 15:16:23', '', 'bran-logo', '', 'inherit', 'open', 'closed', '', 'bran-logo', '', '', '2021-09-03 18:16:23', '2021-09-03 15:16:23', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/bran-logo.png', 0, 'attachment', 'image/png', 0),
(132, 1, '2021-09-03 18:19:57', '2021-09-03 15:19:57', '', 'payment-1', '', 'inherit', 'open', 'closed', '', 'payment-1', '', '', '2021-09-03 18:19:57', '2021-09-03 15:19:57', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/payment-1.png', 0, 'attachment', 'image/png', 0),
(133, 1, '2021-09-03 18:19:58', '2021-09-03 15:19:58', '', 'payment-2', '', 'inherit', 'open', 'closed', '', 'payment-2', '', '', '2021-09-03 18:19:58', '2021-09-03 15:19:58', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/payment-2.png', 0, 'attachment', 'image/png', 0),
(134, 1, '2021-09-03 18:19:59', '2021-09-03 15:19:59', '', 'payment-3', '', 'inherit', 'open', 'closed', '', 'payment-3', '', '', '2021-09-03 18:19:59', '2021-09-03 15:19:59', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/payment-3.png', 0, 'attachment', 'image/png', 0),
(135, 1, '2021-09-03 18:19:59', '2021-09-03 15:19:59', '', 'payment-4', '', 'inherit', 'open', 'closed', '', 'payment-4', '', '', '2021-09-03 18:19:59', '2021-09-03 15:19:59', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/payment-4.png', 0, 'attachment', 'image/png', 0),
(136, 1, '2021-09-03 18:20:00', '2021-09-03 15:20:00', '', 'payment-5', '', 'inherit', 'open', 'closed', '', 'payment-5', '', '', '2021-09-03 18:20:00', '2021-09-03 15:20:00', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/payment-5.png', 0, 'attachment', 'image/png', 0),
(137, 1, '2021-09-03 18:39:24', '2021-09-03 15:39:24', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:17:\"views/contact.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'צור קשר', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', 'publish', 'closed', 'closed', '', 'group_61324179e5456', '', '', '2021-09-03 18:39:24', '2021-09-03 15:39:24', '', 0, 'http://anat:8888/?post_type=acf-field-group&#038;p=137', 0, 'acf-field-group', '', 0),
(138, 1, '2021-09-03 18:39:24', '2021-09-03 15:39:24', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'contact_form_title', 'publish', 'closed', 'closed', '', 'field_613241963648e', '', '', '2021-09-03 18:39:24', '2021-09-03 15:39:24', '', 137, 'http://anat:8888/?post_type=acf-field&p=138', 0, 'acf-field', '', 0),
(139, 1, '2021-09-03 18:39:24', '2021-09-03 15:39:24', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:4:\"rows\";s:0:\"\";}', 'טקסט', 'contact_form_text', 'publish', 'closed', 'closed', '', 'field_613241a13648f', '', '', '2021-09-03 18:39:24', '2021-09-03 15:39:24', '', 137, 'http://anat:8888/?post_type=acf-field&p=139', 1, 'acf-field', '', 0),
(140, 1, '2021-09-03 18:39:44', '2021-09-03 15:39:44', '', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2021-09-03 18:39:44', '2021-09-03 15:39:44', '', 9, 'http://anat:8888/?p=140', 0, 'revision', '', 0),
(141, 1, '2021-09-04 09:57:44', '2021-09-04 06:57:44', 'a:7:{s:8:\"location\";a:4:{i:0;a:2:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}i:1;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:17:\"views/contact.php\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"all\";}}i:3;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"product\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'שאלות ותשובות', '%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_5feb2f4e471ee', '', '', '2021-09-05 09:04:37', '2021-09-05 06:04:37', '', 0, 'http://anat:8888/?p=141', 10, 'acf-field-group', '', 0),
(142, 1, '2021-09-04 09:57:44', '2021-09-04 06:57:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'faq_title', 'publish', 'closed', 'closed', '', 'field_5feb2f82db93b', '', '', '2021-09-04 09:57:44', '2021-09-04 06:57:44', '', 141, 'http://anat:8888/?post_type=acf-field&p=142', 0, 'acf-field', '', 0),
(143, 1, '2021-09-04 09:57:44', '2021-09-04 06:57:44', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'faq_img', 'publish', 'closed', 'closed', '', 'field_5feb2f95db93c', '', '', '2021-09-04 09:58:35', '2021-09-04 06:58:35', '', 141, 'http://anat:8888/?post_type=acf-field&#038;p=143', 1, 'acf-field', '', 0),
(144, 1, '2021-09-04 09:57:44', '2021-09-04 06:57:44', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'שאלות ותשובות', 'faq_item', 'publish', 'closed', 'closed', '', 'field_5feb2faddb93d', '', '', '2021-09-04 09:57:44', '2021-09-04 06:57:44', '', 141, 'http://anat:8888/?post_type=acf-field&p=144', 2, 'acf-field', '', 0),
(145, 1, '2021-09-04 09:57:44', '2021-09-04 06:57:44', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'שאלה', 'faq_question', 'publish', 'closed', 'closed', '', 'field_5feb2fc0db93e', '', '', '2021-09-04 09:57:44', '2021-09-04 06:57:44', '', 144, 'http://anat:8888/?post_type=acf-field&p=145', 0, 'acf-field', '', 0),
(146, 1, '2021-09-04 09:57:44', '2021-09-04 06:57:44', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תשובה', 'faq_answer', 'publish', 'closed', 'closed', '', 'field_5feb2fd3db93f', '', '', '2021-09-04 09:57:44', '2021-09-04 06:57:44', '', 144, 'http://anat:8888/?post_type=acf-field&p=146', 1, 'acf-field', '', 0),
(147, 1, '2021-09-04 10:00:26', '2021-09-04 07:00:26', '', 'category-1', '', 'inherit', 'open', 'closed', '', 'category-1', '', '', '2021-09-04 10:00:26', '2021-09-04 07:00:26', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/category-1.png', 0, 'attachment', 'image/png', 0),
(148, 1, '2021-09-04 10:00:27', '2021-09-04 07:00:27', '', 'category-2', '', 'inherit', 'open', 'closed', '', 'category-2', '', '', '2021-09-04 10:00:27', '2021-09-04 07:00:27', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/category-2.png', 0, 'attachment', 'image/png', 0),
(149, 1, '2021-09-04 10:00:28', '2021-09-04 07:00:28', '', 'category-3', '', 'inherit', 'open', 'closed', '', 'category-3', '', '', '2021-09-04 10:00:28', '2021-09-04 07:00:28', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/category-3.png', 0, 'attachment', 'image/png', 0),
(150, 1, '2021-09-04 10:00:29', '2021-09-04 07:00:29', '', 'category-4', '', 'inherit', 'open', 'closed', '', 'category-4', '', '', '2021-09-04 10:00:29', '2021-09-04 07:00:29', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/category-4.png', 0, 'attachment', 'image/png', 0),
(151, 1, '2021-09-04 10:00:30', '2021-09-04 07:00:30', '', 'category-6', '', 'inherit', 'open', 'closed', '', 'category-6', '', '', '2021-09-04 10:00:30', '2021-09-04 07:00:30', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/category-6.png', 0, 'attachment', 'image/png', 0),
(152, 1, '2021-09-04 10:00:31', '2021-09-04 07:00:31', '', 'category-5', '', 'inherit', 'open', 'closed', '', 'category-5', '', '', '2021-09-04 10:00:31', '2021-09-04 07:00:31', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/category-5.png', 0, 'attachment', 'image/png', 0),
(153, 1, '2021-09-04 10:26:27', '2021-09-04 07:26:27', '', 'post-1', '', 'inherit', 'open', 'closed', '', 'post-1', '', '', '2021-09-04 10:26:27', '2021-09-04 07:26:27', '', 1, 'http://anat:8888/wp-content/uploads/2021/08/post-1.png', 0, 'attachment', 'image/png', 0),
(154, 1, '2021-09-04 10:26:28', '2021-09-04 07:26:28', '', 'post-2', '', 'inherit', 'open', 'closed', '', 'post-2', '', '', '2021-09-04 10:26:28', '2021-09-04 07:26:28', '', 1, 'http://anat:8888/wp-content/uploads/2021/08/post-2.png', 0, 'attachment', 'image/png', 0),
(155, 1, '2021-09-04 10:26:30', '2021-09-04 07:26:30', '', 'post-3', '', 'inherit', 'open', 'closed', '', 'post-3', '', '', '2021-09-04 10:26:30', '2021-09-04 07:26:30', '', 1, 'http://anat:8888/wp-content/uploads/2021/08/post-3.png', 0, 'attachment', 'image/png', 0),
(156, 1, '2021-09-04 10:26:46', '2021-09-04 07:26:46', '', 'faq-block-img', '', 'inherit', 'open', 'closed', '', 'faq-block-img', '', '', '2021-09-04 10:26:46', '2021-09-04 07:26:46', '', 1, 'http://anat:8888/wp-content/uploads/2021/08/faq-block-img.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(157, 1, '2021-09-04 10:27:00', '2021-09-04 07:27:00', '<!-- wp:paragraph -->\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש.</p>\n<p>קוויז דומור ליאמום בלינך רוגצה. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק.</p>\n<p>בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. </p>\n<!-- /wp:paragraph -->', 'שלום עולם! TEST', '', 'inherit', 'closed', 'closed', '', '1-autosave-v1', '', '', '2021-09-04 10:27:00', '2021-09-04 07:27:00', '', 1, 'http://anat:8888/?p=157', 0, 'revision', '', 0),
(158, 1, '2021-09-04 10:27:48', '2021-09-04 07:27:48', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש.</p>\r\n<p>קוויז דומור ליאמום בלינך רוגצה. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק.</p>\r\n<p>בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. </p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-09-04 10:27:48', '2021-09-04 07:27:48', '', 1, 'http://anat:8888/?p=158', 0, 'revision', '', 0),
(159, 1, '2021-09-04 10:28:46', '2021-09-04 07:28:46', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'כותרת מאמר לורם איפסום 1', '', 'publish', 'open', 'open', '', '%d7%9b%d7%95%d7%aa%d7%a8%d7%aa-%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', '', '', '2021-09-04 10:28:46', '2021-09-04 07:28:46', '', 0, 'http://anat:8888/?p=159', 0, 'post', '', 0),
(160, 1, '2021-09-04 10:28:46', '2021-09-04 07:28:46', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'כותרת מאמר לורם איפסום 1', '', 'inherit', 'closed', 'closed', '', '159-revision-v1', '', '', '2021-09-04 10:28:46', '2021-09-04 07:28:46', '', 159, 'http://anat:8888/?p=160', 0, 'revision', '', 0),
(161, 1, '2021-09-04 10:29:12', '2021-09-04 07:29:12', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'כותרת מאמר לורם איפסום 2', '', 'publish', 'open', 'open', '', '%d7%9b%d7%95%d7%aa%d7%a8%d7%aa-%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', '', '', '2021-09-04 10:29:12', '2021-09-04 07:29:12', '', 0, 'http://anat:8888/?p=161', 0, 'post', '', 0),
(162, 1, '2021-09-04 10:29:12', '2021-09-04 07:29:12', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'כותרת מאמר לורם איפסום 2', '', 'inherit', 'closed', 'closed', '', '161-revision-v1', '', '', '2021-09-04 10:29:12', '2021-09-04 07:29:12', '', 161, 'http://anat:8888/?p=162', 0, 'revision', '', 0),
(163, 1, '2021-09-04 10:29:42', '2021-09-04 07:29:42', '', 'home-gallery-1', '', 'inherit', 'open', 'closed', '', 'home-gallery-1', '', '', '2021-09-04 10:29:42', '2021-09-04 07:29:42', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/home-gallery-1.png', 0, 'attachment', 'image/png', 0),
(164, 1, '2021-09-04 10:29:43', '2021-09-04 07:29:43', '', 'home-gallery-2', '', 'inherit', 'open', 'closed', '', 'home-gallery-2', '', '', '2021-09-04 10:29:43', '2021-09-04 07:29:43', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/home-gallery-2.png', 0, 'attachment', 'image/png', 0),
(165, 1, '2021-09-04 10:29:45', '2021-09-04 07:29:45', '', 'home-gallery-3', '', 'inherit', 'open', 'closed', '', 'home-gallery-3', '', '', '2021-09-04 10:29:45', '2021-09-04 07:29:45', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/home-gallery-3.png', 0, 'attachment', 'image/png', 0),
(166, 1, '2021-09-04 10:29:46', '2021-09-04 07:29:46', '', 'home-gallery-4', '', 'inherit', 'open', 'closed', '', 'home-gallery-4', '', '', '2021-09-04 10:29:46', '2021-09-04 07:29:46', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/home-gallery-4.png', 0, 'attachment', 'image/png', 0),
(167, 1, '2021-09-04 10:29:47', '2021-09-04 07:29:47', '', 'home-gallery-5', '', 'inherit', 'open', 'closed', '', 'home-gallery-5', '', '', '2021-09-04 10:29:47', '2021-09-04 07:29:47', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/home-gallery-5.png', 0, 'attachment', 'image/png', 0),
(168, 1, '2021-09-04 10:29:48', '2021-09-04 07:29:48', '', 'home-gallery-6', '', 'inherit', 'open', 'closed', '', 'home-gallery-6', '', '', '2021-09-04 10:29:48', '2021-09-04 07:29:48', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/home-gallery-6.png', 0, 'attachment', 'image/png', 0),
(169, 1, '2021-09-04 10:29:50', '2021-09-04 07:29:50', '', 'home-gallery-7', '', 'inherit', 'open', 'closed', '', 'home-gallery-7', '', '', '2021-09-04 10:29:50', '2021-09-04 07:29:50', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/home-gallery-7.png', 0, 'attachment', 'image/png', 0),
(170, 1, '2021-09-04 10:29:57', '2021-09-04 07:29:57', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-04 10:29:57', '2021-09-04 07:29:57', '', 7, 'http://anat:8888/?p=170', 0, 'revision', '', 0),
(171, 1, '2021-09-04 10:30:20', '2021-09-04 07:30:20', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'כותרת מאמר לורם איפסום 3', '', 'publish', 'open', 'open', '', '%d7%9b%d7%95%d7%aa%d7%a8%d7%aa-%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', '', '', '2021-09-04 10:30:20', '2021-09-04 07:30:20', '', 0, 'http://anat:8888/?p=171', 0, 'post', '', 0),
(172, 1, '2021-09-04 10:30:20', '2021-09-04 07:30:20', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'כותרת מאמר לורם איפסום 3', '', 'inherit', 'closed', 'closed', '', '171-revision-v1', '', '', '2021-09-04 10:30:20', '2021-09-04 07:30:20', '', 171, 'http://anat:8888/?p=172', 0, 'revision', '', 0),
(173, 1, '2021-09-04 10:30:43', '2021-09-04 07:30:43', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'כותרת מאמר לורם איפסום 4', '', 'publish', 'open', 'open', '', '%d7%9b%d7%95%d7%aa%d7%a8%d7%aa-%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', '', '', '2021-09-04 10:30:43', '2021-09-04 07:30:43', '', 0, 'http://anat:8888/?p=173', 0, 'post', '', 0),
(174, 1, '2021-09-04 10:30:43', '2021-09-04 07:30:43', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'כותרת מאמר לורם איפסום 4', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2021-09-04 10:30:43', '2021-09-04 07:30:43', '', 173, 'http://anat:8888/?p=174', 0, 'revision', '', 0),
(175, 1, '2021-09-04 10:31:08', '2021-09-04 07:31:08', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'כותרת מאמר לורם איפסום 5', '', 'publish', 'open', 'open', '', '%d7%9b%d7%95%d7%aa%d7%a8%d7%aa-%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5', '', '', '2021-09-04 10:31:08', '2021-09-04 07:31:08', '', 0, 'http://anat:8888/?p=175', 0, 'post', '', 0),
(176, 1, '2021-09-04 10:31:08', '2021-09-04 07:31:08', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'כותרת מאמר לורם איפסום 5', '', 'inherit', 'closed', 'closed', '', '175-revision-v1', '', '', '2021-09-04 10:31:08', '2021-09-04 07:31:08', '', 175, 'http://anat:8888/?p=176', 0, 'revision', '', 0),
(177, 1, '2021-09-04 10:31:33', '2021-09-04 07:31:33', 'ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'כותרת מאמר לורם איפסום 6', '', 'publish', 'open', 'open', '', '%d7%9b%d7%95%d7%aa%d7%a8%d7%aa-%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6', '', '', '2021-09-04 10:31:33', '2021-09-04 07:31:33', '', 0, 'http://anat:8888/?p=177', 0, 'post', '', 0),
(178, 1, '2021-09-04 10:31:33', '2021-09-04 07:31:33', 'ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'כותרת מאמר לורם איפסום 6', '', 'inherit', 'closed', 'closed', '', '177-revision-v1', '', '', '2021-09-04 10:31:33', '2021-09-04 07:31:33', '', 177, 'http://anat:8888/?p=178', 0, 'revision', '', 0),
(179, 1, '2021-09-04 10:31:56', '2021-09-04 07:31:56', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'כותרת מאמר לורם איפסום 7', '', 'publish', 'open', 'open', '', '%d7%9b%d7%95%d7%aa%d7%a8%d7%aa-%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-7', '', '', '2021-09-04 10:31:56', '2021-09-04 07:31:56', '', 0, 'http://anat:8888/?p=179', 0, 'post', '', 0),
(180, 1, '2021-09-04 10:31:56', '2021-09-04 07:31:56', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'כותרת מאמר לורם איפסום 7', '', 'inherit', 'closed', 'closed', '', '179-revision-v1', '', '', '2021-09-04 10:31:56', '2021-09-04 07:31:56', '', 179, 'http://anat:8888/?p=180', 0, 'revision', '', 0),
(181, 1, '2021-09-04 10:32:21', '2021-09-04 07:32:21', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'כותרת מאמר לורם איפסום 8', '', 'publish', 'open', 'open', '', '%d7%9b%d7%95%d7%aa%d7%a8%d7%aa-%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-8', '', '', '2021-09-04 10:32:21', '2021-09-04 07:32:21', '', 0, 'http://anat:8888/?p=181', 0, 'post', '', 0),
(182, 1, '2021-09-04 10:32:21', '2021-09-04 07:32:21', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'כותרת מאמר לורם איפסום 8', '', 'inherit', 'closed', 'closed', '', '181-revision-v1', '', '', '2021-09-04 10:32:21', '2021-09-04 07:32:21', '', 181, 'http://anat:8888/?p=182', 0, 'revision', '', 0),
(183, 1, '2021-09-04 10:32:39', '2021-09-04 07:32:39', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש.</p>\r\n<p>קוויז דומור ליאמום בלינך רוגצה. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק.</p>\r\n<p>בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. </p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-09-04 10:32:39', '2021-09-04 07:32:39', '', 1, 'http://anat:8888/?p=183', 0, 'revision', '', 0),
(184, 1, '2021-09-04 20:27:03', '2021-09-04 17:27:03', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'טופס חוזר', 'טופס_חוזר', 'publish', 'closed', 'closed', '', 'field_6133ac20910fb', '', '', '2021-09-04 20:27:03', '2021-09-04 17:27:03', '', 49, 'http://anat:8888/?post_type=acf-field&p=184', 17, 'acf-field', '', 0),
(185, 1, '2021-09-04 20:27:03', '2021-09-04 17:27:03', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'base_form_title', 'publish', 'closed', 'closed', '', 'field_6133ac34910fc', '', '', '2021-09-04 20:27:03', '2021-09-04 17:27:03', '', 49, 'http://anat:8888/?post_type=acf-field&p=185', 18, 'acf-field', '', 0),
(186, 1, '2021-09-04 20:27:03', '2021-09-04 17:27:03', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'base_form_text', 'publish', 'closed', 'closed', '', 'field_6133ac38910fd', '', '', '2021-09-04 20:27:03', '2021-09-04 17:27:03', '', 49, 'http://anat:8888/?post_type=acf-field&p=186', 19, 'acf-field', '', 0),
(187, 1, '2021-09-04 21:00:18', '2021-09-04 18:00:18', '', 'מאמרים', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2021-09-04 21:00:18', '2021-09-04 18:00:18', '', 11, 'http://anat:8888/?p=187', 0, 'revision', '', 0),
(188, 1, '2021-09-04 21:17:36', '2021-09-04 18:17:36', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:15:\"views/about.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'אודות', '%d7%90%d7%95%d7%93%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_6133b7a0c4937', '', '', '2021-09-04 22:51:56', '2021-09-04 19:51:56', '', 0, 'http://anat:8888/?post_type=acf-field-group&#038;p=188', 0, 'acf-field-group', '', 0),
(189, 1, '2021-09-04 21:17:36', '2021-09-04 18:17:36', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'גלריה', 'גלריה', 'publish', 'closed', 'closed', '', 'field_6133b7a85b3db', '', '', '2021-09-04 21:17:36', '2021-09-04 18:17:36', '', 188, 'http://anat:8888/?post_type=acf-field&p=189', 0, 'acf-field', '', 0),
(190, 1, '2021-09-04 21:17:36', '2021-09-04 18:17:36', 'a:9:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"layouts\";a:2:{s:20:\"layout_6133b7e1804f9\";a:6:{s:3:\"key\";s:20:\"layout_6133b7e1804f9\";s:5:\"label\";s:10:\"תמונה\";s:4:\"name\";s:7:\"gal_img\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:20:\"layout_6133b8055b3de\";a:6:{s:3:\"key\";s:20:\"layout_6133b8055b3de\";s:5:\"label\";s:10:\"וידאו\";s:4:\"name\";s:9:\"gal_video\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:28:\"הוספת שורה חדשה\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'תמונה או סרטון', 'img_or_video', 'publish', 'closed', 'closed', '', 'field_6133b7b35b3dc', '', '', '2021-09-04 22:51:56', '2021-09-04 19:51:56', '', 188, 'http://anat:8888/?post_type=acf-field&#038;p=190', 2, 'acf-field', '', 0),
(191, 1, '2021-09-04 21:17:36', '2021-09-04 18:17:36', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_6133b7e1804f9\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'image', 'publish', 'closed', 'closed', '', 'field_6133b7ee5b3dd', '', '', '2021-09-04 21:17:36', '2021-09-04 18:17:36', '', 190, 'http://anat:8888/?post_type=acf-field&p=191', 0, 'acf-field', '', 0),
(192, 1, '2021-09-04 21:17:36', '2021-09-04 18:17:36', 'a:8:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_6133b8055b3de\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'קישור לוידאו', 'video_link', 'publish', 'closed', 'closed', '', 'field_6133b8205b3df', '', '', '2021-09-04 21:17:36', '2021-09-04 18:17:36', '', 190, 'http://anat:8888/?post_type=acf-field&p=192', 0, 'acf-field', '', 0),
(193, 1, '2021-09-04 21:18:57', '2021-09-04 18:18:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת של פריט עם קישור לאינסטגרם', 'about_inst_title', 'publish', 'closed', 'closed', '', 'field_6133b847571f7', '', '', '2021-09-04 22:51:56', '2021-09-04 19:51:56', '', 188, 'http://anat:8888/?post_type=acf-field&#038;p=193', 3, 'acf-field', '', 0),
(194, 1, '2021-09-04 21:18:57', '2021-09-04 18:18:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת משנה של פריט עם קישור לאינסטגרם', 'about_inst_subtitle', 'publish', 'closed', 'closed', '', 'field_6133b875571f8', '', '', '2021-09-04 22:51:56', '2021-09-04 19:51:56', '', 188, 'http://anat:8888/?post_type=acf-field&#038;p=194', 4, 'acf-field', '', 0),
(195, 1, '2021-09-04 21:19:31', '2021-09-04 18:19:31', '', 'about-img', '', 'inherit', 'open', 'closed', '', 'about-img', '', '', '2021-09-04 21:19:31', '2021-09-04 18:19:31', '', 127, 'http://anat:8888/wp-content/uploads/2021/09/about-img.png', 0, 'attachment', 'image/png', 0),
(196, 1, '2021-09-04 21:21:09', '2021-09-04 18:21:09', 'ואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך! החלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם.\n\nמוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות. הבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך?\n\nעדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.', 'אודות', '', 'inherit', 'closed', 'closed', '', '127-autosave-v1', '', '', '2021-09-04 21:21:09', '2021-09-04 18:21:09', '', 127, 'http://anat:8888/?p=196', 0, 'revision', '', 0),
(197, 1, '2021-09-04 21:24:38', '2021-09-04 18:24:38', 'ואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך! החלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם.\r\n\r\nמוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות. הבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך?\r\n\r\nעדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.', 'אודות', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2021-09-04 21:24:38', '2021-09-04 18:24:38', '', 127, 'http://anat:8888/?p=197', 0, 'revision', '', 0),
(198, 1, '2021-09-04 21:24:51', '2021-09-04 18:24:51', '<h1>היי, אני ענת!</h1>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך! החלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם.\r\n\r\nמוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות. הבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך?\r\n\r\nעדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.', 'אודות', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2021-09-04 21:24:51', '2021-09-04 18:24:51', '', 127, 'http://anat:8888/?p=198', 0, 'revision', '', 0),
(199, 1, '2021-09-04 21:25:14', '2021-09-04 18:25:14', ' ', '', '', 'publish', 'closed', 'closed', '', '199', '', '', '2021-09-04 21:25:14', '2021-09-04 18:25:14', '', 0, 'http://anat:8888/?p=199', 3, 'nav_menu_item', '', 0),
(200, 1, '2021-09-04 21:25:30', '2021-09-04 18:25:30', '', 'מוצרים', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2021-09-04 21:25:30', '2021-09-04 18:25:30', '', 36, 'http://anat:8888/?p=200', 0, 'revision', '', 0),
(201, 1, '2021-09-04 21:26:56', '2021-09-04 18:26:56', '', 'מוצרים', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2021-09-04 21:26:56', '2021-09-04 18:26:56', '', 36, 'http://anat:8888/?p=201', 0, 'revision', '', 0),
(203, 1, '2021-09-04 22:51:56', '2021-09-04 19:51:56', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'about_gallery_text', 'publish', 'closed', 'closed', '', 'field_6133ce46dd6b8', '', '', '2021-09-04 22:51:56', '2021-09-04 19:51:56', '', 188, 'http://anat:8888/?post_type=acf-field&p=203', 1, 'acf-field', '', 0),
(204, 1, '2021-09-04 22:52:17', '2021-09-04 19:52:17', '<h1>היי, אני ענת!</h1>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך! החלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם.\r\n\r\nמוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות. הבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך?\r\n\r\nעדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.', 'אודות', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2021-09-04 22:52:17', '2021-09-04 19:52:17', '', 127, 'http://anat:8888/?p=204', 0, 'revision', '', 0),
(205, 1, '2021-09-04 22:58:43', '2021-09-04 19:58:43', '<h1>היי, אני ענת!</h1>\r\nואם גם את מכורה כמוני למותג Victoria’s Secret, הבוטיק הוא המקום בשבילך! החלטתי לפתוח הבוטיק כדי להגשים חלום ולהביא לארץ את הפריטים הכי שווים של ויקטוריה סיקרט. אחרי אין סוף חיפושים של הקולקציות השונות בארץ הבנתי שאין לי סיכוי למצוא אותן פה, ואני לגמרי בטוחה שיש עוד המון כמוני... כי בנינו, אין כמו המוצרים שלהם.\r\n\r\nמוצרי הטיפוח המפנקים, הארנקים הייחודיים ושלא נדבר על קולקציית התיקים ההיסטרית ושאר המוצרים כמו נעלי הבית המושלמות. הבוטיק הוא חנות אונליין, שבתוכו ניתן למצוא את כל מה שדמיינתן עליו מהקולקציות העדכניות ביותר של המותג ויקטוריה סיקרט. המוצרים מתחדשים תמיד, ובמחירים שווים במיוחד בלי לקרוע את הכיס.. אין יותר כיף מזה! ומה איתך?\r\n\r\nעדיין לא הזמנת? זה הזמן לחפש את ההתמכרות הבאה שלך.', 'אודות', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2021-09-04 22:58:43', '2021-09-04 19:58:43', '', 127, 'http://anat:8888/?p=205', 0, 'revision', '', 0),
(206, 1, '2021-09-05 02:14:41', '2021-09-04 23:14:41', '', 'slider-1', '', 'inherit', 'open', 'closed', '', 'slider-1', '', '', '2021-09-05 02:14:41', '2021-09-04 23:14:41', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/slider-1.png', 0, 'attachment', 'image/png', 0),
(207, 1, '2021-09-05 02:14:43', '2021-09-04 23:14:43', '', 'slider-2', '', 'inherit', 'open', 'closed', '', 'slider-2', '', '', '2021-09-05 02:14:43', '2021-09-04 23:14:43', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/slider-2.png', 0, 'attachment', 'image/png', 0),
(208, 1, '2021-09-05 02:14:45', '2021-09-04 23:14:45', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2021-09-05 02:14:45', '2021-09-04 23:14:45', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/banner.png', 0, 'attachment', 'image/png', 0),
(209, 1, '2021-09-05 02:19:26', '2021-09-04 23:19:26', '', 'slide-1', '', 'inherit', 'open', 'closed', '', 'slide-1', '', '', '2021-09-05 02:19:26', '2021-09-04 23:19:26', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/slide-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(210, 1, '2021-09-05 02:19:27', '2021-09-04 23:19:27', '', 'slide-2', '', 'inherit', 'open', 'closed', '', 'slide-2', '', '', '2021-09-05 02:19:27', '2021-09-04 23:19:27', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/slide-2.jpeg', 0, 'attachment', 'image/jpeg', 0),
(211, 1, '2021-09-05 02:19:29', '2021-09-04 23:19:29', '', 'slide-3', '', 'inherit', 'open', 'closed', '', 'slide-3', '', '', '2021-09-05 02:19:29', '2021-09-04 23:19:29', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/slide-3.jpeg', 0, 'attachment', 'image/jpeg', 0),
(212, 1, '2021-09-05 02:19:31', '2021-09-04 23:19:31', '', 'slide-5', '', 'inherit', 'open', 'closed', '', 'slide-5', '', '', '2021-09-05 02:19:31', '2021-09-04 23:19:31', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/slide-5.jpeg', 0, 'attachment', 'image/jpeg', 0),
(213, 1, '2021-09-05 02:22:07', '2021-09-04 23:22:07', '', 'reviews', '', 'inherit', 'open', 'closed', '', 'reviews', '', '', '2021-09-05 02:22:07', '2021-09-04 23:22:07', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/reviews.png', 0, 'attachment', 'image/png', 0),
(214, 1, '2021-09-05 02:22:19', '2021-09-04 23:22:19', '', 'review-item', '', 'inherit', 'open', 'closed', '', 'review-item', '', '', '2021-09-05 02:22:19', '2021-09-04 23:22:19', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/review-item.png', 0, 'attachment', 'image/png', 0),
(215, 1, '2021-09-05 02:22:47', '2021-09-04 23:22:47', '', 'review-item–2', '', 'inherit', 'open', 'closed', '', 'review-item-2', '', '', '2021-09-05 02:22:47', '2021-09-04 23:22:47', '', 7, 'http://anat:8888/wp-content/uploads/2021/09/review-item–2.png', 0, 'attachment', 'image/png', 0),
(216, 1, '2021-09-05 02:22:54', '2021-09-04 23:22:54', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-05 02:22:54', '2021-09-04 23:22:54', '', 7, 'http://anat:8888/?p=216', 0, 'revision', '', 0),
(217, 1, '2021-09-05 02:23:21', '2021-09-04 23:23:21', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-05 02:23:21', '2021-09-04 23:23:21', '', 7, 'http://anat:8888/?p=217', 0, 'revision', '', 0),
(218, 1, '2021-09-05 02:24:33', '2021-09-04 23:24:33', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-05 02:24:33', '2021-09-04 23:24:33', '', 7, 'http://anat:8888/?p=218', 0, 'revision', '', 0),
(219, 1, '2021-09-05 02:29:55', '2021-09-04 23:29:55', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-05 02:29:55', '2021-09-04 23:29:55', '', 7, 'http://anat:8888/?p=219', 0, 'revision', '', 0),
(220, 1, '2021-09-05 02:30:24', '2021-09-04 23:30:24', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-05 02:30:24', '2021-09-04 23:30:24', '', 7, 'http://anat:8888/?p=220', 0, 'revision', '', 0),
(221, 1, '2021-09-05 02:32:18', '2021-09-04 23:32:18', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-05 02:32:18', '2021-09-04 23:32:18', '', 7, 'http://anat:8888/?p=221', 0, 'revision', '', 0),
(222, 1, '2021-09-05 02:34:17', '2021-09-04 23:34:17', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-05 02:34:17', '2021-09-04 23:34:17', '', 7, 'http://anat:8888/?p=222', 0, 'revision', '', 0),
(223, 1, '2021-09-05 08:51:42', '2021-09-05 05:51:42', 'בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'Bombshell Seduction 50ml', 'Bombshell Seduction, מתוך קולקציית הבשמים של המותג העולמי Victoria’s Secret.\r\nבניחוח פרחוני מתקתק בשילוב ניחוח לימוני עדין .', 'publish', 'open', 'closed', '', 'bombshell-seduction-50ml', '', '', '2021-09-05 09:58:24', '2021-09-05 06:58:24', '', 0, 'http://anat:8888/?post_type=product&#038;p=223', 0, 'product', '', 0),
(224, 1, '2021-09-05 08:47:22', '2021-09-05 05:47:22', '', 'test-prod-1', '', 'inherit', 'open', 'closed', '', 'test-prod-1', '', '', '2021-09-05 08:47:22', '2021-09-05 05:47:22', '', 223, 'http://anat:8888/wp-content/uploads/2021/09/test-prod-1.png', 0, 'attachment', 'image/png', 0),
(225, 1, '2021-09-05 08:47:23', '2021-09-05 05:47:23', '', 'test-prod-2', '', 'inherit', 'open', 'closed', '', 'test-prod-2', '', '', '2021-09-05 08:47:23', '2021-09-05 05:47:23', '', 223, 'http://anat:8888/wp-content/uploads/2021/09/test-prod-2.png', 0, 'attachment', 'image/png', 0),
(226, 1, '2021-09-05 08:47:24', '2021-09-05 05:47:24', '', 'test-prod-3', '', 'inherit', 'open', 'closed', '', 'test-prod-3', '', '', '2021-09-05 08:47:24', '2021-09-05 05:47:24', '', 223, 'http://anat:8888/wp-content/uploads/2021/09/test-prod-3.png', 0, 'attachment', 'image/png', 0),
(227, 1, '2021-09-05 08:47:25', '2021-09-05 05:47:25', '', 'test-prod-4', '', 'inherit', 'open', 'closed', '', 'test-prod-4', '', '', '2021-09-05 08:47:25', '2021-09-05 05:47:25', '', 223, 'http://anat:8888/wp-content/uploads/2021/09/test-prod-4.png', 0, 'attachment', 'image/png', 0),
(228, 1, '2021-09-05 09:05:52', '2021-09-05 06:05:52', 'בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'Bombshell Seduction 50ml', '', 'inherit', 'closed', 'closed', '', '223-autosave-v1', '', '', '2021-09-05 09:05:52', '2021-09-05 06:05:52', '', 223, 'http://anat:8888/?p=228', 0, 'revision', '', 0),
(229, 1, '2021-09-05 09:03:23', '2021-09-05 06:03:23', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'מוצר', 'מוצר', 'publish', 'closed', 'closed', '', 'field_61345d8a12b93', '', '', '2021-09-05 09:03:23', '2021-09-05 06:03:23', '', 49, 'http://anat:8888/?post_type=acf-field&p=229', 20, 'acf-field', '', 0),
(230, 1, '2021-09-05 09:03:23', '2021-09-05 06:03:23', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'משלוחים והחזרות', 'prod_delivery', 'publish', 'closed', 'closed', '', 'field_61345d9912b94', '', '', '2021-09-05 09:03:23', '2021-09-05 06:03:23', '', 49, 'http://anat:8888/?post_type=acf-field&p=230', 21, 'acf-field', '', 0),
(231, 1, '2021-09-05 09:57:24', '2021-09-05 06:57:24', '', 'Bombshell Seduction 50ml - 100ml', 'גודל: 100ml', 'publish', 'closed', 'closed', '', 'bombshell-seduction-50ml-100ml', '', '', '2021-09-05 09:57:52', '2021-09-05 06:57:52', '', 223, 'http://anat:8888/?post_type=product_variation&p=231', 1, 'product_variation', '', 0),
(232, 1, '2021-09-05 09:57:24', '2021-09-05 06:57:24', '', 'Bombshell Seduction 50ml - 50ml', 'גודל: 50ml', 'publish', 'closed', 'closed', '', 'bombshell-seduction-50ml-50ml', '', '', '2021-09-05 09:58:17', '2021-09-05 06:58:17', '', 223, 'http://anat:8888/?post_type=product_variation&p=232', 2, 'product_variation', '', 0),
(233, 1, '2021-09-05 09:59:39', '2021-09-05 06:59:39', '<!-- wp:shortcode -->[yith_wcwl_wishlist]<!-- /wp:shortcode -->', 'רשימת משאלות', '', 'publish', 'closed', 'closed', '', 'wishlist', '', '', '2021-09-05 09:59:39', '2021-09-05 06:59:39', '', 0, 'http://anat:8888/wishlist/', 0, 'page', '', 0),
(234, 1, '2021-09-05 13:46:27', '2021-09-05 10:46:27', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'טופס בחנות', 'טופס_בחנות', 'publish', 'closed', 'closed', '', 'field_61349fde43a8b', '', '', '2021-09-05 13:46:27', '2021-09-05 10:46:27', '', 49, 'http://anat:8888/?post_type=acf-field&p=234', 22, 'acf-field', '', 0),
(235, 1, '2021-09-05 13:46:27', '2021-09-05 10:46:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'shop_form_title', 'publish', 'closed', 'closed', '', 'field_61349fee43a8c', '', '', '2021-09-05 13:46:27', '2021-09-05 10:46:27', '', 49, 'http://anat:8888/?post_type=acf-field&p=235', 23, 'acf-field', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(236, 1, '2021-09-05 13:46:27', '2021-09-05 10:46:27', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'shop_form_text', 'publish', 'closed', 'closed', '', 'field_61349ff443a8d', '', '', '2021-09-05 13:46:27', '2021-09-05 10:46:27', '', 49, 'http://anat:8888/?post_type=acf-field&p=236', 24, 'acf-field', '', 0),
(237, 1, '2021-09-05 17:11:46', '2021-09-05 14:11:46', '', 'מוצר לורם איפסום 1', '', 'publish', 'open', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', '', '', '2021-09-05 17:11:47', '2021-09-05 14:11:47', '', 0, 'http://anat:8888/?post_type=product&#038;p=237', 0, 'product', '', 0),
(238, 1, '2021-09-05 17:11:38', '2021-09-05 14:11:38', '', 'product-1', '', 'inherit', 'open', 'closed', '', 'product-1', '', '', '2021-09-05 17:11:38', '2021-09-05 14:11:38', '', 237, 'http://anat:8888/wp-content/uploads/2021/09/product-1.png', 0, 'attachment', 'image/png', 0),
(239, 1, '2021-09-05 17:11:39', '2021-09-05 14:11:39', '', 'product-2', '', 'inherit', 'open', 'closed', '', 'product-2', '', '', '2021-09-05 17:11:39', '2021-09-05 14:11:39', '', 237, 'http://anat:8888/wp-content/uploads/2021/09/product-2.png', 0, 'attachment', 'image/png', 0),
(240, 1, '2021-09-05 17:11:40', '2021-09-05 14:11:40', '', 'product-3', '', 'inherit', 'open', 'closed', '', 'product-3', '', '', '2021-09-05 17:11:40', '2021-09-05 14:11:40', '', 237, 'http://anat:8888/wp-content/uploads/2021/09/product-3.png', 0, 'attachment', 'image/png', 0),
(241, 1, '2021-09-05 17:11:41', '2021-09-05 14:11:41', '', 'product-4', '', 'inherit', 'open', 'closed', '', 'product-4', '', '', '2021-09-05 17:11:41', '2021-09-05 14:11:41', '', 237, 'http://anat:8888/wp-content/uploads/2021/09/product-4.png', 0, 'attachment', 'image/png', 0),
(242, 1, '2021-09-05 17:12:05', '2021-09-05 14:12:05', '', 'מוצר לורם איפסום 2', '', 'publish', 'open', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', '', '', '2021-09-05 17:12:06', '2021-09-05 14:12:06', '', 0, 'http://anat:8888/?post_type=product&#038;p=242', 0, 'product', '', 0),
(243, 1, '2021-09-05 17:12:18', '2021-09-05 14:12:18', '', 'מוצר לורם איפסום 3', '', 'publish', 'open', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', '', '', '2021-09-05 17:12:18', '2021-09-05 14:12:18', '', 0, 'http://anat:8888/?post_type=product&#038;p=243', 0, 'product', '', 0),
(244, 1, '2021-09-05 17:12:40', '2021-09-05 14:12:40', '', 'מוצר לורם איפסום 4', '', 'publish', 'open', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', '', '', '2021-09-05 17:12:40', '2021-09-05 14:12:40', '', 0, 'http://anat:8888/?post_type=product&#038;p=244', 0, 'product', '', 0),
(245, 1, '2021-09-05 17:37:00', '2021-09-05 14:37:00', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-05 17:37:00', '2021-09-05 14:37:00', '', 7, 'http://anat:8888/?p=245', 0, 'revision', '', 0),
(246, 1, '2021-09-05 17:41:45', '2021-09-05 14:41:45', '', 'מוצר לורם איפסום 5', '', 'publish', 'open', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5', '', '', '2021-09-05 17:41:45', '2021-09-05 14:41:45', '', 0, 'http://anat:8888/?post_type=product&#038;p=246', 0, 'product', '', 0),
(247, 1, '2021-09-05 17:39:22', '2021-09-05 14:39:22', '', 'product-5', '', 'inherit', 'open', 'closed', '', 'product-5', '', '', '2021-09-05 17:39:22', '2021-09-05 14:39:22', '', 246, 'http://anat:8888/wp-content/uploads/2021/09/product-5.png', 0, 'attachment', 'image/png', 0),
(248, 1, '2021-09-05 17:39:23', '2021-09-05 14:39:23', '', 'product-6', '', 'inherit', 'open', 'closed', '', 'product-6', '', '', '2021-09-05 17:39:23', '2021-09-05 14:39:23', '', 246, 'http://anat:8888/wp-content/uploads/2021/09/product-6.png', 0, 'attachment', 'image/png', 0),
(249, 1, '2021-09-05 17:39:25', '2021-09-05 14:39:25', '', 'product-7', '', 'inherit', 'open', 'closed', '', 'product-7', '', '', '2021-09-05 17:39:25', '2021-09-05 14:39:25', '', 246, 'http://anat:8888/wp-content/uploads/2021/09/product-7.png', 0, 'attachment', 'image/png', 0),
(252, 1, '2021-09-05 17:41:10', '2021-09-05 14:41:10', '', 'מוצר לורם איפסום 5 - L', 'מידה: L', 'publish', 'closed', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5-l', '', '', '2021-09-05 17:41:33', '2021-09-05 14:41:33', '', 246, 'http://anat:8888/?post_type=product_variation&p=252', 1, 'product_variation', '', 0),
(253, 1, '2021-09-05 17:41:10', '2021-09-05 14:41:10', '', 'מוצר לורם איפסום 5 - M', 'מידה: M', 'publish', 'closed', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5-m', '', '', '2021-09-05 17:41:33', '2021-09-05 14:41:33', '', 246, 'http://anat:8888/?post_type=product_variation&p=253', 2, 'product_variation', '', 0),
(254, 1, '2021-09-05 17:41:10', '2021-09-05 14:41:10', '', 'מוצר לורם איפסום 5 - S', 'מידה: S', 'publish', 'closed', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5-s', '', '', '2021-09-05 17:41:10', '2021-09-05 14:41:10', '', 246, 'http://anat:8888/?post_type=product_variation&p=254', 3, 'product_variation', '', 0),
(255, 1, '2021-09-05 17:42:08', '2021-09-05 14:42:08', '', 'מוצר לורם איפסום 6', '', 'publish', 'open', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6', '', '', '2021-09-05 17:42:08', '2021-09-05 14:42:08', '', 0, 'http://anat:8888/?post_type=product&#038;p=255', 0, 'product', '', 0),
(256, 1, '2021-09-05 17:42:31', '2021-09-05 14:42:31', '', 'מוצר לורם איפסום 7', '', 'publish', 'open', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-7', '', '', '2021-09-05 17:42:31', '2021-09-05 14:42:31', '', 0, 'http://anat:8888/?post_type=product&#038;p=256', 0, 'product', '', 0),
(257, 1, '2021-09-05 17:43:24', '2021-09-05 14:43:24', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-09-05 17:43:24', '2021-09-05 14:43:24', '', 7, 'http://anat:8888/?p=257', 0, 'revision', '', 0),
(258, 1, '2021-09-05 18:25:51', '2021-09-05 15:25:51', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'תפריט עם אייקונים', 'תפריט_עם_אייקונים', 'publish', 'closed', 'closed', '', 'field_6134e1192ede6', '', '', '2021-09-05 18:25:51', '2021-09-05 15:25:51', '', 49, 'http://anat:8888/?post_type=acf-field&p=258', 25, 'acf-field', '', 0),
(259, 1, '2021-09-05 18:25:51', '2021-09-05 15:25:51', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'תפריט עם אייקונים', 'menu_item', 'publish', 'closed', 'closed', '', 'field_6134e13b2ede7', '', '', '2021-09-05 18:25:51', '2021-09-05 15:25:51', '', 49, 'http://anat:8888/?post_type=acf-field&p=259', 26, 'acf-field', '', 0),
(260, 1, '2021-09-05 18:25:51', '2021-09-05 15:25:51', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'אייקון', 'icon', 'publish', 'closed', 'closed', '', 'field_6134e15a2ede8', '', '', '2021-09-05 18:25:51', '2021-09-05 15:25:51', '', 259, 'http://anat:8888/?post_type=acf-field&p=260', 0, 'acf-field', '', 0),
(261, 1, '2021-09-05 18:25:51', '2021-09-05 15:25:51', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'link', 'publish', 'closed', 'closed', '', 'field_6134e16c2ede9', '', '', '2021-09-05 18:25:51', '2021-09-05 15:25:51', '', 259, 'http://anat:8888/?post_type=acf-field&p=261', 1, 'acf-field', '', 0),
(262, 1, '2021-09-05 18:27:10', '2021-09-05 15:27:10', '', 'menu-icon-1', '', 'inherit', 'open', 'closed', '', 'menu-icon-1', '', '', '2021-09-05 18:27:10', '2021-09-05 15:27:10', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/menu-icon-1.png', 0, 'attachment', 'image/png', 0),
(263, 1, '2021-09-05 18:27:11', '2021-09-05 15:27:11', '', 'menu-icon-2', '', 'inherit', 'open', 'closed', '', 'menu-icon-2', '', '', '2021-09-05 18:27:11', '2021-09-05 15:27:11', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/menu-icon-2.png', 0, 'attachment', 'image/png', 0),
(264, 1, '2021-09-05 18:27:12', '2021-09-05 15:27:12', '', 'menu-icon-3', '', 'inherit', 'open', 'closed', '', 'menu-icon-3', '', '', '2021-09-05 18:27:12', '2021-09-05 15:27:12', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/menu-icon-3.png', 0, 'attachment', 'image/png', 0),
(265, 1, '2021-09-05 18:27:13', '2021-09-05 15:27:13', '', 'menu-icon-4', '', 'inherit', 'open', 'closed', '', 'menu-icon-4', '', '', '2021-09-05 18:27:13', '2021-09-05 15:27:13', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/menu-icon-4.png', 0, 'attachment', 'image/png', 0),
(266, 1, '2021-09-05 18:27:13', '2021-09-05 15:27:13', '', 'menu-icon-5', '', 'inherit', 'open', 'closed', '', 'menu-icon-5', '', '', '2021-09-05 18:27:13', '2021-09-05 15:27:13', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/menu-icon-5.png', 0, 'attachment', 'image/png', 0),
(267, 1, '2021-09-05 18:27:14', '2021-09-05 15:27:14', '', 'menu-icon-6', '', 'inherit', 'open', 'closed', '', 'menu-icon-6', '', '', '2021-09-05 18:27:14', '2021-09-05 15:27:14', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/menu-icon-6.png', 0, 'attachment', 'image/png', 0),
(268, 1, '2021-09-05 18:27:15', '2021-09-05 15:27:15', '', 'menu-icon-7', '', 'inherit', 'open', 'closed', '', 'menu-icon-7', '', '', '2021-09-05 18:27:15', '2021-09-05 15:27:15', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/menu-icon-7.png', 0, 'attachment', 'image/png', 0),
(269, 1, '2021-09-05 18:27:16', '2021-09-05 15:27:16', '', 'menu-icon-8', '', 'inherit', 'open', 'closed', '', 'menu-icon-8', '', '', '2021-09-05 18:27:16', '2021-09-05 15:27:16', '', 0, 'http://anat:8888/wp-content/uploads/2021/09/menu-icon-8.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_termmeta`
--

CREATE TABLE `fmn_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_termmeta`
--

INSERT INTO `fmn_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 20, 'order', '0'),
(2, 20, 'faq_title', ''),
(3, 20, '_faq_title', 'field_5feb2f82db93b'),
(4, 20, 'faq_img', ''),
(5, 20, '_faq_img', 'field_5feb2f95db93c'),
(6, 20, 'faq_item', ''),
(7, 20, '_faq_item', 'field_5feb2faddb93d'),
(8, 20, 'display_type', ''),
(9, 20, 'thumbnail_id', '151'),
(10, 15, 'faq_title', ''),
(11, 15, '_faq_title', 'field_5feb2f82db93b'),
(12, 15, 'faq_img', ''),
(13, 15, '_faq_img', 'field_5feb2f95db93c'),
(14, 15, 'faq_item', ''),
(15, 15, '_faq_item', 'field_5feb2faddb93d'),
(16, 15, 'display_type', ''),
(17, 15, 'thumbnail_id', '149'),
(18, 21, 'order', '0'),
(19, 21, 'faq_title', ''),
(20, 21, '_faq_title', 'field_5feb2f82db93b'),
(21, 21, 'faq_img', ''),
(22, 21, '_faq_img', 'field_5feb2f95db93c'),
(23, 21, 'faq_item', ''),
(24, 21, '_faq_item', 'field_5feb2faddb93d'),
(25, 21, 'display_type', ''),
(26, 21, 'thumbnail_id', '150'),
(27, 22, 'order', '0'),
(28, 22, 'faq_title', ''),
(29, 22, '_faq_title', 'field_5feb2f82db93b'),
(30, 22, 'faq_img', ''),
(31, 22, '_faq_img', 'field_5feb2f95db93c'),
(32, 22, 'faq_item', ''),
(33, 22, '_faq_item', 'field_5feb2faddb93d'),
(34, 22, 'display_type', ''),
(35, 22, 'thumbnail_id', '147'),
(36, 23, 'order', '0'),
(37, 23, 'faq_title', ''),
(38, 23, '_faq_title', 'field_5feb2f82db93b'),
(39, 23, 'faq_img', ''),
(40, 23, '_faq_img', 'field_5feb2f95db93c'),
(41, 23, 'faq_item', ''),
(42, 23, '_faq_item', 'field_5feb2faddb93d'),
(43, 23, 'display_type', ''),
(44, 23, 'thumbnail_id', '148'),
(45, 1, 'faq_title', ''),
(46, 1, '_faq_title', 'field_5feb2f82db93b'),
(47, 1, 'faq_img', ''),
(48, 1, '_faq_img', 'field_5feb2f95db93c'),
(49, 1, 'faq_item', ''),
(50, 1, '_faq_item', 'field_5feb2faddb93d'),
(51, 24, 'order', '0'),
(52, 24, 'faq_title', ''),
(53, 24, '_faq_title', 'field_5feb2f82db93b'),
(54, 24, 'faq_img', ''),
(55, 24, '_faq_img', 'field_5feb2f95db93c'),
(56, 24, 'faq_item', ''),
(57, 24, '_faq_item', 'field_5feb2faddb93d'),
(58, 24, 'display_type', ''),
(59, 24, 'thumbnail_id', '152'),
(60, 15, 'product_count_product_cat', '5'),
(61, 25, 'order_pa_גודל', '0'),
(62, 25, 'faq_title', ''),
(63, 25, '_faq_title', 'field_5feb2f82db93b'),
(64, 25, 'faq_img', ''),
(65, 25, '_faq_img', 'field_5feb2f95db93c'),
(66, 25, 'faq_item', ''),
(67, 25, '_faq_item', 'field_5feb2faddb93d'),
(68, 26, 'order_pa_גודל', '0'),
(69, 26, 'faq_title', ''),
(70, 26, '_faq_title', 'field_5feb2f82db93b'),
(71, 26, 'faq_img', ''),
(72, 26, '_faq_img', 'field_5feb2f95db93c'),
(73, 26, 'faq_item', ''),
(74, 26, '_faq_item', 'field_5feb2faddb93d'),
(75, 27, 'order_pa_מידה', '0'),
(76, 27, 'faq_title', ''),
(77, 27, '_faq_title', 'field_5feb2f82db93b'),
(78, 27, 'faq_img', ''),
(79, 27, '_faq_img', 'field_5feb2f95db93c'),
(80, 27, 'faq_item', ''),
(81, 27, '_faq_item', 'field_5feb2faddb93d'),
(82, 28, 'order_pa_מידה', '0'),
(83, 28, 'faq_title', ''),
(84, 28, '_faq_title', 'field_5feb2f82db93b'),
(85, 28, 'faq_img', ''),
(86, 28, '_faq_img', 'field_5feb2f95db93c'),
(87, 28, 'faq_item', ''),
(88, 28, '_faq_item', 'field_5feb2faddb93d'),
(89, 29, 'order_pa_מידה', '0'),
(90, 29, 'faq_title', ''),
(91, 29, '_faq_title', 'field_5feb2f82db93b'),
(92, 29, 'faq_img', ''),
(93, 29, '_faq_img', 'field_5feb2f95db93c'),
(94, 29, 'faq_item', ''),
(95, 29, '_faq_item', 'field_5feb2faddb93d'),
(96, 30, 'order', '0'),
(97, 30, 'faq_title', ''),
(98, 30, '_faq_title', 'field_5feb2f82db93b'),
(99, 30, 'faq_img', ''),
(100, 30, '_faq_img', 'field_5feb2f95db93c'),
(101, 30, 'faq_item', ''),
(102, 30, '_faq_item', 'field_5feb2faddb93d'),
(103, 30, 'display_type', ''),
(104, 30, 'thumbnail_id', '0'),
(105, 31, 'order', '0'),
(106, 31, 'faq_title', ''),
(107, 31, '_faq_title', 'field_5feb2f82db93b'),
(108, 31, 'faq_img', ''),
(109, 31, '_faq_img', 'field_5feb2f95db93c'),
(110, 31, 'faq_item', ''),
(111, 31, '_faq_item', 'field_5feb2faddb93d'),
(112, 31, 'display_type', ''),
(113, 31, 'thumbnail_id', '0'),
(114, 23, 'product_count_product_cat', '2'),
(115, 30, 'product_count_product_cat', '1'),
(116, 22, 'product_count_product_cat', '1');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_terms`
--

CREATE TABLE `fmn_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_terms`
--

INSERT INTO `fmn_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'כללי', '%d7%9b%d7%9c%d7%9c%d7%99', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'הבשמים שלנו', 'parfums', 0),
(16, 'Footer menu', 'footer-menu', 0),
(17, 'Header menu', 'header-menu', 0),
(18, 'תפריט עם קישורים 1', '%d7%aa%d7%a4%d7%a8%d7%99%d7%98-%d7%a2%d7%9d-%d7%a7%d7%99%d7%a9%d7%95%d7%a8%d7%99%d7%9d-1', 0),
(19, 'תפריט עם קישורים 2', '%d7%aa%d7%a4%d7%a8%d7%99%d7%98-%d7%a2%d7%9d-%d7%a7%d7%99%d7%a9%d7%95%d7%a8%d7%99%d7%9d-2', 0),
(20, 'מארזים', '%d7%9e%d7%90%d7%a8%d7%96%d7%99%d7%9d', 0),
(21, 'ארנקים | תיקים | קלמרים', '%d7%90%d7%a8%d7%a0%d7%a7%d7%99%d7%9d-%d7%aa%d7%99%d7%a7%d7%99%d7%9d-%d7%a7%d7%9c%d7%9e%d7%a8%d7%99%d7%9d', 0),
(22, 'פיג’מות והלבשה', '%d7%a4%d7%99%d7%92%d7%9e%d7%95%d7%aa-%d7%95%d7%94%d7%9c%d7%91%d7%a9%d7%94', 0),
(23, 'מבשמי הגוף שלנו', '%d7%9e%d7%91%d7%a9%d7%9e%d7%99-%d7%94%d7%92%d7%95%d7%a3-%d7%a9%d7%9c%d7%a0%d7%95', 0),
(24, 'אקססוריז', '%d7%90%d7%a7%d7%a1%d7%a1%d7%95%d7%a8%d7%99%d7%96', 0),
(25, '50ml', '50ml', 0),
(26, '100ml', '100ml', 0),
(27, 'S', 's', 0),
(28, 'M', 'm', 0),
(29, 'L', 'l', 0),
(30, 'תת קטגוריה לורם איפסום 1', '%d7%aa%d7%aa-%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', 0),
(31, 'תת קטגוריה לורם איפסום 2', '%d7%aa%d7%aa-%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_relationships`
--

CREATE TABLE `fmn_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_relationships`
--

INSERT INTO `fmn_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(13, 1, 0),
(21, 1, 0),
(68, 1, 0),
(70, 1, 0),
(116, 16, 0),
(117, 16, 0),
(118, 16, 0),
(119, 16, 0),
(120, 17, 0),
(121, 17, 0),
(122, 17, 0),
(123, 17, 0),
(124, 18, 0),
(125, 19, 0),
(126, 19, 0),
(141, 1, 0),
(159, 1, 0),
(161, 1, 0),
(171, 1, 0),
(173, 1, 0),
(175, 1, 0),
(177, 1, 0),
(179, 1, 0),
(181, 1, 0),
(199, 17, 0),
(223, 4, 0),
(223, 15, 0),
(223, 25, 0),
(223, 26, 0),
(237, 2, 0),
(237, 23, 0),
(237, 30, 0),
(242, 2, 0),
(242, 15, 0),
(243, 2, 0),
(243, 15, 0),
(244, 2, 0),
(244, 15, 0),
(246, 4, 0),
(246, 23, 0),
(246, 27, 0),
(246, 28, 0),
(246, 29, 0),
(255, 2, 0),
(255, 15, 0),
(256, 2, 0),
(256, 22, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_taxonomy`
--

CREATE TABLE `fmn_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_taxonomy`
--

INSERT INTO `fmn_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 9),
(2, 2, 'product_type', '', 0, 6),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 2),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 5),
(16, 16, 'nav_menu', '', 0, 4),
(17, 17, 'nav_menu', '', 0, 5),
(18, 18, 'nav_menu', '', 0, 1),
(19, 19, 'nav_menu', '', 0, 2),
(20, 20, 'product_cat', '', 0, 0),
(21, 21, 'product_cat', '', 0, 0),
(22, 22, 'product_cat', '', 0, 1),
(23, 23, 'product_cat', '', 0, 2),
(24, 24, 'product_cat', '', 0, 0),
(25, 25, 'pa_ml', '', 0, 1),
(26, 26, 'pa_ml', '', 0, 1),
(27, 27, 'pa_size', '', 0, 1),
(28, 28, 'pa_size', '', 0, 1),
(29, 29, 'pa_size', '', 0, 1),
(30, 30, 'product_cat', '', 23, 1),
(31, 31, 'product_cat', '', 23, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_usermeta`
--

CREATE TABLE `fmn_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_usermeta`
--

INSERT INTO `fmn_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'dev_admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'fmn_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'fmn_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:2:{s:64:\"992e9f21eb1d60785038679497e2fd41f17a64eec25095d3c915d4dad8f0b272\";a:4:{s:10:\"expiration\";i:1631554779;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36\";s:5:\"login\";i:1630345179;}s:64:\"3c44ff16f16ab238bdc3154aabcf84c1fce93ec8385f4886695501bd78b41c40\";a:4:{s:10:\"expiration\";i:1631554779;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36\";s:5:\"login\";i:1630345179;}}'),
(17, 1, 'fmn_dashboard_quick_press_last_post_id', '4'),
(18, 1, '_woocommerce_tracks_anon_id', 'woo:DAq7iVvoJdpD+iP1ZEyRr4jq'),
(19, 1, 'last_update', '1630598272'),
(20, 1, 'woocommerce_admin_activity_panel_inbox_last_read', '1630598271225'),
(21, 1, 'wc_last_active', '1630800000'),
(22, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(23, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-product_tag\";}'),
(24, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(25, 1, '_order_count', '0'),
(26, 1, 'fmn_user-settings', 'libraryContent=browse'),
(27, 1, 'fmn_user-settings-time', '1630682411'),
(28, 1, 'nav_menu_recently_edited', '17'),
(29, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"115f89503138416a242f40fb7d7f338e\";a:6:{s:3:\"key\";s:32:\"115f89503138416a242f40fb7d7f338e\";s:10:\"product_id\";i:223;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"36f76fb62ac326633be66211f53a026c\";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_users`
--

CREATE TABLE `fmn_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_users`
--

INSERT INTO `fmn_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'dev_admin', '$P$B8DyRuQAGrYRfGsX0Vr1uTjnxkkswG0', 'dev_admin', 'maxf@leos.co.il', '', '2021-08-30 17:39:27', '', 0, 'dev_admin');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_admin_notes`
--

CREATE TABLE `fmn_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  `layout` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'info'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_admin_notes`
--

INSERT INTO `fmn_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`, `layout`, `image`, `is_deleted`, `icon`) VALUES
(1, 'mercadopago_q3_2021_EN', 'marketing', 'en_US', 'Get paid with Mercado Pago Checkout', 'Latin America\'s leading payment processor is now available for WooCommerce stores. Securely accept debit and credit cards, cash, bank transfers, and installment payments – backed by exclusive fraud prevention tools.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(2, 'wayflyer_q3_2021', 'marketing', 'en_US', 'Grow your revenue with Wayflyer financing and analytics', 'Flexible financing tailored to your needs by <a href=\"https://woocommerce.com/products/wayflyer/\">Wayflyer</a> – one fee, no interest rates, penalties, equity, or personal guarantees. Based on your store\'s performance, Wayflyer can provide the financing you need to grow and the analytical insights to help you spend it.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(3, 'eu_vat_changes_2021', 'marketing', 'en_US', 'Get your business ready for the new EU tax regulations', 'On July 1, 2021, new taxation rules will come into play when the <a href=\"https://ec.europa.eu/taxation_customs/business/vat/modernising-vat-cross-border-ecommerce_en\">European Union (EU) Value-Added Tax (VAT) eCommerce package</a> takes effect.<br /><br />The new regulations will impact virtually every B2C business involved in cross-border eCommerce trade with the EU.<br /><br />We therefore recommend <a href=\"https://woocommerce.com/posts/new-eu-vat-regulations\">familiarizing yourself with the new updates</a>, and consult with a tax professional to ensure your business is following regulations and best practice.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(4, 'paypal_ppcp_gtm_2021', 'marketing', 'en_US', 'Offer more options with the new PayPal', 'Get the latest PayPal extension for a full suite of payment methods with extensive currency and country coverage.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(5, 'facebook_pixel_api_2021', 'marketing', 'en_US', 'Improve the performance of your Facebook ads', 'Enable Facebook Pixel and Conversions API through the latest version of Facebook for WooCommerce for improved measurement and ad targeting capabilities.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(6, 'facebook_ec_2021', 'marketing', 'en_US', 'Sync your product catalog with Facebook to help boost sales', 'A single click adds all products to your Facebook Business Page shop. Product changes are automatically synced, with the flexibility to control which products are listed.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(7, 'ecomm-need-help-setting-up-your-store', 'info', 'en_US', 'Need help setting up your Store?', 'Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(8, 'woocommerce-services', 'info', 'en_US', 'WooCommerce Shipping & Tax', 'WooCommerce Shipping &amp; Tax helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(9, 'ecomm-unique-shopping-experience', 'info', 'en_US', 'For a shopping experience as unique as your customers', 'Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(10, 'wc-admin-getting-started-in-ecommerce', 'info', 'en_US', 'Getting Started in eCommerce - webinar', 'We want to make eCommerce and this process of getting started as easy as possible for you. Watch this webinar to get tips on how to have our store up and running in a breeze.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(11, 'your-first-product', 'info', 'en_US', 'Your first product', 'That\'s huge! You\'re well on your way to building a successful online store — now it’s time to think about how you\'ll fulfill your orders.<br /><br />Read our shipping guide to learn best practices and options for putting together your shipping strategy. And for WooCommerce stores in the United States, you can print discounted shipping labels via USPS with <a href=\"https://href.li/?https://woocommerce.com/shipping\" target=\"_blank\">WooCommerce Shipping</a>.', '{}', 'unactioned', 'woocommerce.com', '2021-09-05 06:59:38', NULL, 0, 'plain', '', 0, 'info'),
(12, 'wc-square-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with Square you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(13, 'wc-square-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with Square and Apple Pay ', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(14, 'wcpay-apple-pay-is-now-available', 'marketing', 'en_US', 'Apple Pay is now available with WooCommerce Payments!', 'Increase your conversion rate by offering a fast and secure checkout with <a href=\"https://woocommerce.com/apple-pay/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">Apple Pay</a>®. It’s free to get started with <a href=\"https://woocommerce.com/payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">WooCommerce Payments</a>.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(15, 'wcpay-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with WooCommerce Payments you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(16, 'wcpay-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with WooCommerce Payments and Apple Pay', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(17, 'wc-admin-optimizing-the-checkout-flow', 'info', 'en_US', 'Optimizing the checkout flow', 'It\'s crucial to get your store\'s checkout as smooth as possible to avoid losing sales. Let\'s take a look at how you can optimize the checkout experience for your shoppers.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(18, 'wc-admin-first-five-things-to-customize', 'info', 'en_US', 'The first 5 things to customize in your store', 'Deciding what to start with first is tricky. To help you properly prioritize, we\'ve put together this short list of the first few things you should customize in WooCommerce.', '{}', 'unactioned', 'woocommerce.com', '2021-09-04 16:57:56', NULL, 0, 'plain', '', 0, 'info'),
(19, 'wc-payments-qualitative-feedback', 'info', 'en_US', 'WooCommerce Payments setup - let us know what you think', 'Congrats on enabling WooCommerce Payments for your store. Please share your feedback in this 2 minute survey to help us improve the setup process.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(20, 'share-your-feedback-on-paypal', 'info', 'en_US', 'Share your feedback on PayPal', 'Share your feedback in this 2 minute survey about how we can make the process of accepting payments more useful for your store.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(21, 'wcpay_instant_deposits_gtm_2021', 'marketing', 'en_US', 'Get paid within minutes – Instant Deposits for WooCommerce Payments', 'Stay flexible with immediate access to your funds when you need them – including nights, weekends, and holidays. With <a href=\"https://woocommerce.com/products/woocommerce-payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_instant_deposits\">WooCommerce Payments\'</a> new Instant Deposits feature, you’re able to transfer your earnings to a debit card within minutes.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(22, 'google_listings_and_ads_install', 'marketing', 'en_US', 'Drive traffic and sales with Google', 'Reach online shoppers to drive traffic and sales for your store by showcasing products across Google, for free or with ads.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(23, 'wc-subscriptions-security-update-3-0-15', 'info', 'en_US', 'WooCommerce Subscriptions security update!', 'We recently released an important security update to WooCommerce Subscriptions. To ensure your site\'s data is protected, please upgrade <strong>WooCommerce Subscriptions to version 3.0.15</strong> or later.<br /><br />Click the button below to view and update to the latest Subscriptions version, or log in to <a href=\"https://woocommerce.com/my-dashboard\">WooCommerce.com Dashboard</a> and navigate to your <strong>Downloads</strong> page.<br /><br />We recommend always using the latest version of WooCommerce Subscriptions, and other software running on your site, to ensure maximum security.<br /><br />If you have any questions we are here to help — just <a href=\"https://woocommerce.com/my-account/create-a-ticket/\">open a ticket</a>.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(24, 'woocommerce-core-update-5-4-0', 'info', 'en_US', 'Update to WooCommerce 5.4.1 now', 'WooCommerce 5.4.1 addresses a checkout issue discovered in WooCommerce 5.4. We recommend upgrading to WooCommerce 5.4.1 as soon as possible.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(25, 'wcpay-promo-2020-11', 'marketing', 'en_US', 'wcpay-promo-2020-11', 'wcpay-promo-2020-11', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(26, 'wcpay-promo-2020-12', 'marketing', 'en_US', 'wcpay-promo-2020-12', 'wcpay-promo-2020-12', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(27, 'wcpay-promo-2021-6-incentive-1', 'marketing', 'en_US', 'Simplify the payments process for you and your customers with WooCommerce Payments', 'With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies.\n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br /><br />\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">Terms of Service</a>\n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(28, 'wcpay-promo-2021-6-incentive-2', 'marketing', 'en_US', 'Simplify the payments process for you and your customers with WooCommerce Payments', 'With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies.\n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br /><br />\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">Terms of Service</a>\n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(29, 'ppxo-pps-upgrade-paypal-payments-1', 'info', 'en_US', 'Get the latest PayPal extension for WooCommerce', 'Heads up! There\'s a new PayPal on the block!<br /><br />Now is a great time to upgrade to our latest <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension</a> to continue to receive support and updates with PayPal.<br /><br />Get access to a full suite of PayPal payment methods, extensive currency and country coverage, and pay later options with the all-new PayPal extension for WooCommerce.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(30, 'ppxo-pps-upgrade-paypal-payments-2', 'info', 'en_US', 'Upgrade your PayPal experience!', 'We\'ve developed a whole new <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension for WooCommerce</a> that combines the best features of our many PayPal extensions into just one extension.<br /><br />Get access to a full suite of PayPal payment methods, extensive currency and country coverage, offer subscription and recurring payments, and the new PayPal pay later options.<br /><br />Start using our latest PayPal today to continue to receive support and updates.', '{}', 'unactioned', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(31, 'woocommerce-core-sqli-july-2021-need-to-update', 'update', 'en_US', 'Action required: Critical vulnerabilities in WooCommerce', 'In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br />Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br /><br />For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(32, 'woocommerce-blocks-sqli-july-2021-need-to-update', 'update', 'en_US', 'Action required: Critical vulnerabilities in WooCommerce Blocks', 'In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br />Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br /><br />For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(33, 'woocommerce-core-sqli-july-2021-store-patched', 'update', 'en_US', 'Solved: Critical vulnerabilities patched in WooCommerce', 'In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br /><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(34, 'woocommerce-blocks-sqli-july-2021-store-patched', 'update', 'en_US', 'Solved: Critical vulnerabilities patched in WooCommerce Blocks', 'In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br /><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-09-02 15:53:57', NULL, 0, 'plain', '', 0, 'info'),
(35, 'wc-admin-onboarding-email-marketing', 'info', 'en_US', 'Sign up for tips, product updates, and inspiration', 'We\'re here for you - get tips, product updates and inspiration straight to your email box', '{}', 'unactioned', 'woocommerce-admin', '2021-09-02 15:53:58', NULL, 0, 'plain', '', 0, 'info'),
(36, 'wc-refund-returns-page', 'info', 'en_US', 'Setup a Refund and Returns Policy page to boost your store\'s credibility.', 'We have created a sample draft Refund and Returns Policy page for you. Please have a look and update it to fit your store.', '{}', 'unactioned', 'woocommerce-core', '2021-09-02 15:53:58', NULL, 0, 'plain', '', 0, 'info'),
(37, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', '{}', 'unactioned', 'woocommerce-admin', '2021-09-02 15:53:58', NULL, 0, 'plain', '', 0, 'info'),
(38, 'wc-admin-choosing-a-theme', 'marketing', 'en_US', 'רוצה לבחור ערכת עיצוב?', 'כדאי לעיין בערכות העיצוב שתואמות ל-WooCommerce ולבחור ערכת עיצוב שמתאימה לצרכים של המותג והעסק שלך.', '{}', 'unactioned', 'woocommerce-admin', '2021-09-04 06:39:15', NULL, 0, 'plain', '', 0, 'info'),
(39, 'wc-admin-insight-first-product-and-payment', 'survey', 'en_US', 'תובנות', 'מעל 80% מהסוחרים החדשים מוסיפים את המוצר הראשון שלהם ומגדירים לפחות אמצעי תשלום אחד במהלך השבוע הראשון.<br><br>האם התובנות האלה מועילות לך?', '{}', 'unactioned', 'woocommerce-admin', '2021-09-04 06:39:15', NULL, 0, 'plain', '', 0, 'info'),
(40, 'wc-admin-mobile-app', 'info', 'en_US', 'להתקין את האפליקציה של Woo לנייד', 'כדאי להתקין את האפליקציה של WooCommerce לנייד כדי לנהל הזמנות, לקבל הודעות על מכירות ולהציג מדדים חשובים – בכל מקום.', '{}', 'unactioned', 'woocommerce-admin', '2021-09-04 16:57:55', NULL, 0, 'plain', '', 0, 'info'),
(41, 'wc-admin-add-first-product-note', 'email', 'en_US', 'להוסיף את המוצר הראשון שלך', '{greetings}<br /><br />Nice one; יצרת חנות ב-WooCommerce! כעת אפשר להוסיף את המוצר הראשון שלך ולהתכונן למכירה הראשונה.<br /><br />קיימות שלוש דרכים להוסיף מוצרים: אפשר <strong>ליצור את המוצרים באופן ידני, לייבא מספר מוצרים במקביל דרך קובץ CSV‏</strong> או <strong>להעביר אותם משירות אחר</strong>.<br /><br /><a href=\"https://docs.woocommerce.com/document/managing-products/?utm_source=help_panel\">כדאי לעיין במסמכים שלנו</a> למידע נוסף, או פשוט להתחיל כעת!', '{\"role\":\"administrator\"}', 'unactioned', 'woocommerce-admin', '2021-09-04 16:57:56', NULL, 0, 'plain', 'http://anat:8888/wp-content/plugins/woocommerce/packages/woocommerce-admin/images/admin_notes/dashboard-widget-setup.png', 0, 'info'),
(42, 'wc-admin-learn-more-about-variable-products', 'info', 'en_US', 'למידע נוסף בנושא מוצרים עם סוגים', 'מוצרים עם סוגים הם מוצרים עוצמתיים שמאפשרים לך להציע ללקוחות מגוון של סוגים בכל מוצר ולשלוט במחיר, במלאי, בתמונה ובמאפיינים נוספים של כל סוג. אפשר להשתמש באפשרות זאת במוצרים כגון חולצות, ולהציע ללקוחות מידה גדולה, בינונית או קטנה וצבעים שונים.', '{}', 'unactioned', 'woocommerce-admin', '2021-09-05 05:51:42', NULL, 0, 'plain', '', 0, 'info'),
(43, 'wc-admin-filter-by-product-variations-in-reports', 'info', 'en_US', 'חדש – ניתן לסנן לפי סוגי מוצרים בדוחות של הזמנות ושל מוצרים', 'אחת האפשרויות הכי מבוקשות שלנו הושקה לשימוש! אפשר להציג תובנות של כל סוגי המוצרים בדוחות של הזמנות ושל מוצרים.', '{}', 'unactioned', 'woocommerce-admin', '2021-09-05 15:54:00', NULL, 0, 'banner', 'http://anat:8888/wp-content/plugins/woocommerce/packages/woocommerce-admin/images/admin_notes/filter-by-product-variations-note.svg', 0, 'info');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_admin_note_actions`
--

CREATE TABLE `fmn_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `actioned_text` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonce_action` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `nonce_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_admin_note_actions`
--

INSERT INTO `fmn_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`, `actioned_text`, `nonce_action`, `nonce_name`) VALUES
(38, 35, 'yes-please', 'Yes please!', 'https://woocommerce.us8.list-manage.com/subscribe/post?u=2c1434dc56f9506bf3c3ecd21&amp;id=13860df971&amp;SIGNUPPAGE=plugin', 'actioned', 0, '', NULL, NULL),
(76, 36, 'notify-refund-returns-page', 'Edit page', 'http://anat:8888/wp-admin/post.php?post=40&action=edit', 'actioned', 0, '', NULL, NULL),
(77, 37, 'connect', 'Connect', '?page=wc-addons&section=helper', 'unactioned', 0, '', NULL, NULL),
(152, 38, 'visit-the-theme-marketplace', 'מעבר לחנות של ערכות העיצוב', 'https://woocommerce.com/product-category/themes/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(153, 39, 'affirm-insight-first-product-and-payment', 'כן', '', 'actioned', 0, 'תודה על המשוב', NULL, NULL),
(154, 39, 'affirm-insight-first-product-and-payment', 'לא', '', 'actioned', 0, 'תודה על המשוב', NULL, NULL),
(192, 40, 'learn-more', 'מידע נוסף', 'https://woocommerce.com/mobile/', 'actioned', 0, '', NULL, NULL),
(193, 41, 'add-first-product', 'להוסיף מוצר', 'http://anat:8888/wp-admin/admin.php?page=wc-admin&task=products', 'actioned', 0, '', NULL, NULL),
(231, 42, 'learn-more', 'מידע נוסף', 'https://docs.woocommerce.com/document/variable-product/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(343, 43, 'learn-more', 'מידע נוסף', 'https://docs.woocommerce.com/document/woocommerce-analytics/#variations-report', 'actioned', 0, '', NULL, NULL),
(344, 1, 'mercadopago_q3_2021_EN', 'Free download', 'https://woocommerce.com/products/mercado-pago-checkout/?utm_source=inbox&utm_medium=product&utm_campaign=mercadopago_q3_2021_EN', 'actioned', 1, '', NULL, NULL),
(345, 2, 'wayflyer_q3_2021', 'Get funded', 'https://woocommerce.com/products/wayflyer/', 'actioned', 1, '', NULL, NULL),
(346, 3, 'eu_vat_changes_2021', 'Learn more about the EU tax regulations', 'https://woocommerce.com/posts/new-eu-vat-regulations', 'actioned', 1, '', NULL, NULL),
(347, 4, 'open_wc_paypal_payments_product_page', 'Learn more', 'https://woocommerce.com/products/woocommerce-paypal-payments/', 'actioned', 1, '', NULL, NULL),
(348, 5, 'upgrade_now_facebook_pixel_api', 'Upgrade now', 'plugin-install.php?tab=plugin-information&plugin=&section=changelog', 'actioned', 1, '', NULL, NULL),
(349, 6, 'learn_more_facebook_ec', 'Learn more', 'https://woocommerce.com/products/facebook/', 'unactioned', 1, '', NULL, NULL),
(350, 7, 'set-up-concierge', 'Schedule free session', 'https://wordpress.com/me/concierge', 'actioned', 1, '', NULL, NULL),
(351, 8, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/woocommerce-shipping-and-tax/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(352, 9, 'learn-more-ecomm-unique-shopping-experience', 'Learn more', 'https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(353, 10, 'watch-the-webinar', 'Watch the webinar', 'https://youtu.be/V_2XtCOyZ7o', 'actioned', 1, '', NULL, NULL),
(354, 11, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/ecommerce-shipping-solutions-guide/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(355, 12, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-boost-sales', 'actioned', 1, '', NULL, NULL),
(356, 13, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-grow-your-business', 'actioned', 1, '', NULL, NULL),
(357, 14, 'add-apple-pay', 'Add Apple Pay', '/admin.php?page=wc-settings&tab=checkout&section=woocommerce_payments', 'actioned', 1, '', NULL, NULL),
(358, 14, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/payments/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay', 'actioned', 1, '', NULL, NULL),
(359, 15, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-boost-sales', 'actioned', 1, '', NULL, NULL),
(360, 16, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-grow-your-business', 'actioned', 1, '', NULL, NULL),
(361, 17, 'optimizing-the-checkout-flow', 'Learn more', 'https://woocommerce.com/posts/optimizing-woocommerce-checkout?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(362, 18, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/first-things-customize-woocommerce/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(363, 19, 'qualitative-feedback-from-new-users', 'Share feedback', 'https://automattic.survey.fm/wc-pay-new', 'actioned', 1, '', NULL, NULL),
(364, 20, 'share-feedback', 'Share feedback', 'http://automattic.survey.fm/paypal-feedback', 'unactioned', 1, '', NULL, NULL),
(365, 21, 'learn-more', 'Learn about Instant Deposits eligibility', 'https://docs.woocommerce.com/document/payments/instant-deposits/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits', 'actioned', 1, '', NULL, NULL),
(366, 22, 'get-started', 'Get started', 'https://woocommerce.com/products/google-listings-and-ads', 'actioned', 1, '', NULL, NULL),
(367, 23, 'update-wc-subscriptions-3-0-15', 'View latest version', 'http://anat:8888/wp-admin/admin.php?page=wc-admin&page=wc-addons&section=helper', 'actioned', 1, '', NULL, NULL),
(368, 24, 'update-wc-core-5-4-0', 'How to update WooCommerce', 'https://docs.woocommerce.com/document/how-to-update-woocommerce/', 'actioned', 1, '', NULL, NULL),
(369, 27, 'get-woo-commerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL),
(370, 28, 'get-woocommerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL),
(371, 29, 'ppxo-pps-install-paypal-payments-1', 'View upgrade guide', 'https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/', 'actioned', 1, '', NULL, NULL),
(372, 30, 'ppxo-pps-install-paypal-payments-2', 'View upgrade guide', 'https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/', 'actioned', 1, '', NULL, NULL),
(373, 31, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(374, 31, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(375, 32, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(376, 32, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(377, 33, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(378, 33, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(379, 34, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(380, 34, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_category_lookup`
--

CREATE TABLE `fmn_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_category_lookup`
--

INSERT INTO `fmn_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(15, 15),
(20, 20),
(21, 21),
(22, 22),
(23, 23),
(23, 30),
(23, 31),
(24, 24),
(30, 30),
(31, 31);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_customer_lookup`
--

CREATE TABLE `fmn_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_download_log`
--

CREATE TABLE `fmn_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_coupon_lookup`
--

CREATE TABLE `fmn_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_product_lookup`
--

CREATE TABLE `fmn_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_stats`
--

CREATE TABLE `fmn_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_tax_lookup`
--

CREATE TABLE `fmn_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_product_meta_lookup`
--

CREATE TABLE `fmn_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(19,4) DEFAULT NULL,
  `max_price` decimal(19,4) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  `tax_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'taxable',
  `tax_class` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_product_meta_lookup`
--

INSERT INTO `fmn_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`, `tax_status`, `tax_class`) VALUES
(223, '', 0, 0, '290.0000', '350.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(231, '', 0, 0, '350.0000', '350.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', 'parent'),
(232, '', 0, 0, '290.0000', '290.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', 'parent'),
(237, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(242, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(243, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(244, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(246, '', 0, 0, '120.0000', '130.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(252, '', 0, 0, '120.0000', '120.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', 'parent'),
(253, '', 0, 0, '130.0000', '130.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', 'parent'),
(254, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', 'parent'),
(255, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(256, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', '');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_reserved_stock`
--

CREATE TABLE `fmn_wc_reserved_stock` (
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `stock_quantity` double NOT NULL DEFAULT '0',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_tax_rate_classes`
--

CREATE TABLE `fmn_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_tax_rate_classes`
--

INSERT INTO `fmn_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_webhooks`
--

CREATE TABLE `fmn_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_api_keys`
--

CREATE TABLE `fmn_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_attribute_taxonomies`
--

CREATE TABLE `fmn_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_woocommerce_attribute_taxonomies`
--

INSERT INTO `fmn_woocommerce_attribute_taxonomies` (`attribute_id`, `attribute_name`, `attribute_label`, `attribute_type`, `attribute_orderby`, `attribute_public`) VALUES
(1, 'ml', 'גודל', 'select', 'menu_order', 0),
(2, 'size', 'מידה', 'select', 'menu_order', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `fmn_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_log`
--

CREATE TABLE `fmn_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_order_itemmeta`
--

CREATE TABLE `fmn_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_order_items`
--

CREATE TABLE `fmn_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_payment_tokenmeta`
--

CREATE TABLE `fmn_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_payment_tokens`
--

CREATE TABLE `fmn_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_sessions`
--

CREATE TABLE `fmn_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_woocommerce_sessions`
--

INSERT INTO `fmn_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(1, '1', 'a:8:{s:4:\"cart\";s:413:\"a:1:{s:32:\"115f89503138416a242f40fb7d7f338e\";a:11:{s:3:\"key\";s:32:\"115f89503138416a242f40fb7d7f338e\";s:10:\"product_id\";i:223;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"36f76fb62ac326633be66211f53a026c\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:290;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:290;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:393:\"a:15:{s:8:\"subtotal\";s:3:\"290\";s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";s:1:\"0\";s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";s:3:\"290\";s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:1:\"0\";s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:3:\"290\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:413:\"a:1:{s:32:\"115f89503138416a242f40fb7d7f338e\";a:11:{s:3:\"key\";s:32:\"115f89503138416a242f40fb7d7f338e\";s:10:\"product_id\";i:223;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:9:\"data_hash\";s:32:\"36f76fb62ac326633be66211f53a026c\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:580;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:580;s:8:\"line_tax\";i:0;}}\";s:8:\"customer\";s:758:\"a:27:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2021-09-02T18:57:52+03:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"IL\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"IL\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:15:\"maxf@leos.co.il\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";s:14:\"shipping_phone\";s:0:\"\";}\";s:10:\"wc_notices\";N;}', 1631008809);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zones`
--

CREATE TABLE `fmn_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zone_locations`
--

CREATE TABLE `fmn_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zone_methods`
--

CREATE TABLE `fmn_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_tax_rates`
--

CREATE TABLE `fmn_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_tax_rate_locations`
--

CREATE TABLE `fmn_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yith_wcwl`
--

CREATE TABLE `fmn_yith_wcwl` (
  `ID` bigint(20) NOT NULL,
  `prod_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `wishlist_id` bigint(20) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `original_price` decimal(9,3) DEFAULT NULL,
  `original_currency` char(3) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `on_sale` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fmn_yith_wcwl`
--

INSERT INTO `fmn_yith_wcwl` (`ID`, `prod_id`, `quantity`, `user_id`, `wishlist_id`, `position`, `original_price`, `original_currency`, `dateadded`, `on_sale`) VALUES
(1, 223, 1, 1, 1, 0, '290.000', 'ILS', '2021-09-05 07:14:25', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yith_wcwl_lists`
--

CREATE TABLE `fmn_yith_wcwl_lists` (
  `ID` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `wishlist_slug` varchar(200) NOT NULL,
  `wishlist_name` text,
  `wishlist_token` varchar(64) NOT NULL,
  `wishlist_privacy` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fmn_yith_wcwl_lists`
--

INSERT INTO `fmn_yith_wcwl_lists` (`ID`, `user_id`, `session_id`, `wishlist_slug`, `wishlist_name`, `wishlist_token`, `wishlist_privacy`, `is_default`, `dateadded`, `expiration`) VALUES
(1, 1, NULL, '', '', 'O3ZAISZ9XB17', 0, 1, '2021-09-05 04:14:25', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fmn_actionscheduler_actions`
--
ALTER TABLE `fmn_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`),
  ADD KEY `claim_id_status_scheduled_date_gmt` (`claim_id`,`status`,`scheduled_date_gmt`);

--
-- Indexes for table `fmn_actionscheduler_claims`
--
ALTER TABLE `fmn_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `fmn_actionscheduler_groups`
--
ALTER TABLE `fmn_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `fmn_actionscheduler_logs`
--
ALTER TABLE `fmn_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `fmn_links`
--
ALTER TABLE `fmn_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `fmn_options`
--
ALTER TABLE `fmn_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `fmn_term_relationships`
--
ALTER TABLE `fmn_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_users`
--
ALTER TABLE `fmn_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `fmn_wc_admin_notes`
--
ALTER TABLE `fmn_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `fmn_wc_admin_note_actions`
--
ALTER TABLE `fmn_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `fmn_wc_category_lookup`
--
ALTER TABLE `fmn_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `fmn_wc_customer_lookup`
--
ALTER TABLE `fmn_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `fmn_wc_order_coupon_lookup`
--
ALTER TABLE `fmn_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_order_product_lookup`
--
ALTER TABLE `fmn_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_order_stats`
--
ALTER TABLE `fmn_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`(191));

--
-- Indexes for table `fmn_wc_order_tax_lookup`
--
ALTER TABLE `fmn_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_product_meta_lookup`
--
ALTER TABLE `fmn_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `fmn_wc_reserved_stock`
--
ALTER TABLE `fmn_wc_reserved_stock`
  ADD PRIMARY KEY (`order_id`,`product_id`);

--
-- Indexes for table `fmn_wc_tax_rate_classes`
--
ALTER TABLE `fmn_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `fmn_wc_webhooks`
--
ALTER TABLE `fmn_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `fmn_woocommerce_api_keys`
--
ALTER TABLE `fmn_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `fmn_woocommerce_attribute_taxonomies`
--
ALTER TABLE `fmn_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `fmn_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `fmn_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `fmn_woocommerce_log`
--
ALTER TABLE `fmn_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `fmn_woocommerce_order_itemmeta`
--
ALTER TABLE `fmn_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `fmn_woocommerce_order_items`
--
ALTER TABLE `fmn_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `fmn_woocommerce_payment_tokenmeta`
--
ALTER TABLE `fmn_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `fmn_woocommerce_payment_tokens`
--
ALTER TABLE `fmn_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `fmn_woocommerce_sessions`
--
ALTER TABLE `fmn_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `fmn_woocommerce_shipping_zones`
--
ALTER TABLE `fmn_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `fmn_woocommerce_shipping_zone_locations`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `fmn_woocommerce_shipping_zone_methods`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `fmn_woocommerce_tax_rates`
--
ALTER TABLE `fmn_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `fmn_woocommerce_tax_rate_locations`
--
ALTER TABLE `fmn_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `fmn_yith_wcwl`
--
ALTER TABLE `fmn_yith_wcwl`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Indexes for table `fmn_yith_wcwl_lists`
--
ALTER TABLE `fmn_yith_wcwl_lists`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `wishlist_token` (`wishlist_token`),
  ADD KEY `wishlist_slug` (`wishlist_slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_actions`
--
ALTER TABLE `fmn_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_claims`
--
ALTER TABLE `fmn_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_groups`
--
ALTER TABLE `fmn_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_logs`
--
ALTER TABLE `fmn_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_links`
--
ALTER TABLE `fmn_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_options`
--
ALTER TABLE `fmn_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1341;

--
-- AUTO_INCREMENT for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2685;

--
-- AUTO_INCREMENT for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=270;

--
-- AUTO_INCREMENT for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `fmn_users`
--
ALTER TABLE `fmn_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_wc_admin_notes`
--
ALTER TABLE `fmn_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `fmn_wc_admin_note_actions`
--
ALTER TABLE `fmn_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;

--
-- AUTO_INCREMENT for table `fmn_wc_customer_lookup`
--
ALTER TABLE `fmn_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_wc_tax_rate_classes`
--
ALTER TABLE `fmn_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fmn_wc_webhooks`
--
ALTER TABLE `fmn_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_api_keys`
--
ALTER TABLE `fmn_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_attribute_taxonomies`
--
ALTER TABLE `fmn_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `fmn_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_log`
--
ALTER TABLE `fmn_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_order_itemmeta`
--
ALTER TABLE `fmn_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_order_items`
--
ALTER TABLE `fmn_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_payment_tokenmeta`
--
ALTER TABLE `fmn_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_payment_tokens`
--
ALTER TABLE `fmn_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_sessions`
--
ALTER TABLE `fmn_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zones`
--
ALTER TABLE `fmn_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zone_locations`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zone_methods`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_tax_rates`
--
ALTER TABLE `fmn_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_tax_rate_locations`
--
ALTER TABLE `fmn_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_yith_wcwl`
--
ALTER TABLE `fmn_yith_wcwl`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_yith_wcwl_lists`
--
ALTER TABLE `fmn_yith_wcwl_lists`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  ADD CONSTRAINT `fk_fmn_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `fmn_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
