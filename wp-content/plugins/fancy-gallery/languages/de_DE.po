msgid ""
msgstr ""
"Project-Id-Version: Gallery Manager\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-31 20:10+0200\n"
"PO-Revision-Date: \n"
"Last-Translator: Dennis Hoppe <Mail@DennisHoppe.de>\n"
"Language-Team: Dennis Hoppe <Mail@DennisHoppe.de>\n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-KeywordsList: t;t:1,2c;__;_e;_x:1,2c;_ex:1,2c\n"
"X-Generator: Poedit 2.0.6\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../classes/lightbox.php:22
msgid "Previous image"
msgstr "Vorheriges Bild"

#: ../classes/lightbox.php:23
msgid "Next image"
msgstr "Nächstes Bild"

#: ../classes/lightbox.php:26
msgid "Close"
msgstr "Schließen"

#: ../classes/mocking-bird.php:17 ../classes/mocking-bird.php:20
msgid "Upgrade to Pro"
msgstr "Upgrade auf Pro"

#: ../classes/mocking-bird.php:19
#, php-format
msgid ""
"This feature is available in the <a href=\"%s\" target=\"_blank\">premium "
"version</a>."
msgstr ""
"Diese Funktion ist in der <a href=\"%s\" target=\"_blank\">Pro-Version</a> "
"verfügbar."

#: ../classes/mocking-bird.php:20
msgid "Unlock this feature"
msgstr "Feature freischalten"

#: ../classes/mocking-bird.php:23
msgctxt "Link to the authors website"
msgid "https://dennishoppe.de/en/wordpress-plugins/gallery-manager"
msgstr "https://dennishoppe.de/wordpress-plugins/gallery-manager"

#: ../classes/mocking-bird.php:56
msgid ""
"If you like this free version of the lightbox, you will love "
"<u><strong>Gallery Manager Pro</strong></u>!"
msgstr ""
"Wenn dir diese kostenfreie Version der Lightbox gefällt, wirst du "
"<u><strong>Gallery Manager Pro</strong></u> lieben!"

#: ../classes/mocking-bird.php:71
msgid "Free Version"
msgstr "Kostenfreie Version"

#: ../classes/mocking-bird.php:85
msgid "Enjoy all features of the Pro Version"
msgstr "Nutze alle Funktionen der Pro-Version"

#: ../classes/mocking-bird.php:86
msgid ""
"If you like the free version, you will love <u><strong>Gallery Manager Pro</"
"strong></u>!"
msgstr ""
"Wenn dir die kostenfreie Version gefällt, wirst du <u><strong>Gallery "
"Manager Pro</strong></u> lieben!"

#: ../classes/options.php:23
msgid "Gallery Options"
msgstr "Einstellungen &rsaquo; Galerien"

#: ../classes/options.php:24 ../classes/post-type.php:78
#: ../widgets/galleries.php:11 ../widgets/galleries.php:26
msgid "Galleries"
msgstr "Galerien"

#: ../classes/options.php:31
msgid "Settings"
msgstr "Einstellungen"

#: ../classes/options.php:37
msgid "Gallery Management"
msgstr "Galerieverwaltung"

#: ../classes/options.php:38 ../options-page/lightbox.php:6
msgid "Lightbox"
msgstr "Lightbox"

#: ../classes/options.php:39
msgid "Gallery Previews"
msgstr "Galerievorschau"

#: ../classes/options.php:40
msgid "Taxonomies"
msgstr "Klassifizierungen"

#: ../classes/options.php:41
msgid "Gallery Archive"
msgstr "Galerie-Archiv"

#: ../classes/options.php:75
msgid "Gallery Settings"
msgstr "Einstellungen &rsaquo; Galerien"

#: ../classes/options.php:79
msgid "Settings saved."
msgstr "Einstellungen gespeichert."

#: ../classes/options.php:107
msgid "Save Changes"
msgstr "Änderungen speichern"

#: ../classes/post-type.php:79
msgid "Gallery"
msgstr "Galerie"

#: ../classes/post-type.php:80
msgid "Add Gallery"
msgstr "Galerie hinzufügen"

#: ../classes/post-type.php:81
msgid "New Gallery"
msgstr "Neue Galerie"

#: ../classes/post-type.php:82
msgid "Edit Gallery"
msgstr "Galerie bearbeiten"

#: ../classes/post-type.php:83
msgid "View Gallery"
msgstr "Galerie ansehen"

#: ../classes/post-type.php:84
msgid "Search Galleries"
msgstr "Galerien suchen"

#: ../classes/post-type.php:85
msgid "No Galleries found"
msgstr "Keine Galerien gefunden"

#: ../classes/post-type.php:86
msgid "No Galleries found in Trash"
msgstr "Keine Galerien im Papierkorb gefunden"

#: ../classes/post-type.php:87
msgid "All Galleries"
msgstr "Alle Galerien"

#: ../classes/post-type.php:88
msgid "Gallery Index Page"
msgstr "Galerie-Index-Seite"

#: ../classes/post-type.php:104 ../classes/taxonomies.php:40
#: ../classes/taxonomies.php:71
msgctxt "URL slug"
msgid "galleries"
msgstr "galerien"

#: ../classes/post-type.php:124
#, php-format
msgid "Gallery updated. (<a href=\"%s\">View gallery</a>)"
msgstr "Galerie aktualisiert. (<a href=\"%s\">Galerie ansehen</a>)"

#: ../classes/post-type.php:125
msgid "Custom field updated."
msgstr "Benutzerdefiniertes Feld aktualisiert."

#: ../classes/post-type.php:126
msgid "Custom field deleted."
msgstr "Benutzerdefiniertes Feld gelöscht."

#: ../classes/post-type.php:127
msgid "Gallery updated."
msgstr "Galerie aktualisiert."

#: ../classes/post-type.php:128
#, php-format
msgid "Gallery restored to revision from %s"
msgstr "Galerie auf den Stand von %s zurückgesetzt"

#: ../classes/post-type.php:129
#, php-format
msgid "Gallery published. (<a href=\"%s\">View gallery</a>)"
msgstr "Galerie veröffentlicht. (<a href=\"%s\">Galerie ansehen</a>)"

#: ../classes/post-type.php:130
msgid "Gallery saved."
msgstr "Galerie gespeichert."

#: ../classes/post-type.php:131
msgid "Gallery submitted."
msgstr "Galerie eingereicht."

#: ../classes/post-type.php:132
#, php-format
msgid "Gallery scheduled. (<a target=\"_blank\" href=\"%s\">View gallery</a>)"
msgstr ""
"Galerie angelegt. (<a target=\"_blank\" href=\"%s\">Galerie ansehen</a>)"

#: ../classes/post-type.php:133
#, php-format
msgid ""
"Gallery draft updated. (<a target=\"_blank\" href=\"%s\">Preview gallery</a>)"
msgstr ""
"Entwurf gespeichert. (<a target=\"_blank\" href=\"%s\">Galerievorschau</a>)"

#: ../classes/post-type.php:145
msgid "Images"
msgstr "Bilder"

#: ../classes/post-type.php:147
msgid "Appearance"
msgstr "Design"

#: ../classes/post-type.php:151 ../meta-boxes/owner.php:3
msgid "Owner"
msgstr "Eigentümer"

#: ../classes/post-type.php:154 ../classes/post-type.php:193
msgid "Shortcode"
msgstr "Shortcode"

#: ../classes/post-type.php:157
msgid "Hash"
msgstr "Hash"

#: ../classes/scripts.php:25
msgid ""
"Do you want to remove this image from the gallery? It will not be deleted "
"from your media library."
msgstr ""
"Möchtest du dieses Bild aus der Galerie entfernen? Es wird trotzdem noch in "
"der Mediathek bleiben bis du es endgültig löschst."

#: ../classes/taxonomies.php:14
msgid "Gallery Categories"
msgstr "Galeriekategorien"

#: ../classes/taxonomies.php:16
msgid "Categories"
msgstr "Kategorien"

#: ../classes/taxonomies.php:17
msgid "Category"
msgstr "Kategorie"

#: ../classes/taxonomies.php:18
msgid "All Categories"
msgstr "Alle Kategorien"

#: ../classes/taxonomies.php:19
msgid "Edit Category"
msgstr "Kategorie bearbeiten"

#: ../classes/taxonomies.php:20
msgid "View Category"
msgstr "Kategorie ansehen"

#: ../classes/taxonomies.php:21
msgid "Update Category"
msgstr "Kategorie aktualisieren"

#: ../classes/taxonomies.php:22
msgid "Add New Category"
msgstr "Neue Kategorie hinzufügen"

#: ../classes/taxonomies.php:23
msgid "New Category"
msgstr "Neue Kategorie"

#: ../classes/taxonomies.php:24
msgid "Parent Category"
msgstr "Übergeordnete Kategorie"

#: ../classes/taxonomies.php:25
msgid "Parent Category:"
msgstr "Übergeordnete Kategorie:"

#: ../classes/taxonomies.php:26
msgid "Search Categories"
msgstr "Kategorien durchsuchen"

#: ../classes/taxonomies.php:27
msgid "Popular Categories"
msgstr "Häufig verwendete Kategorien"

#: ../classes/taxonomies.php:28
msgid "Separate Categories with commas"
msgstr "Kategorien durch Komma getrennt"

#: ../classes/taxonomies.php:29
msgid "Add or remove Categories"
msgstr "Kategorien hinzufügen oder entfernen"

#: ../classes/taxonomies.php:30
msgid "Choose from the most used Categories"
msgstr "Wähle aus den am meisten verwendeten Kategorien"

#: ../classes/taxonomies.php:31
msgid "No Categories found."
msgstr "Keine Galerien gefunden."

#: ../classes/taxonomies.php:40
#, php-format
msgctxt "URL slug"
msgid "%s/category"
msgstr "%s/kategorie"

#: ../classes/taxonomies.php:45
msgid "Gallery Tags"
msgstr "Schlagworte der Galerie"

#: ../classes/taxonomies.php:47
msgid "Tags"
msgstr "Schlagworte"

#: ../classes/taxonomies.php:48
msgid "Tag"
msgstr "Schlagwort"

#: ../classes/taxonomies.php:49
msgid "All Tags"
msgstr "Alle Schlagworte"

#: ../classes/taxonomies.php:50
msgid "Edit Tag"
msgstr "Schlagwort bearbeiten"

#: ../classes/taxonomies.php:51
msgid "View Tag"
msgstr "Schlagwort ansehen"

#: ../classes/taxonomies.php:52
msgid "Update Tag"
msgstr "Schlagwort aktualisieren"

#: ../classes/taxonomies.php:53
msgid "Add New Tag"
msgstr "Neues Schlagwort hinzufügen"

#: ../classes/taxonomies.php:54
msgid "New Tag"
msgstr "Neues Schlagwort"

#: ../classes/taxonomies.php:55
msgid "Parent Tag"
msgstr "Übergeordnetes Schlagwort"

#: ../classes/taxonomies.php:56
msgid "Parent Tag:"
msgstr "Übergeordnetes Schlagwort:"

#: ../classes/taxonomies.php:57
msgid "Search Tags"
msgstr "Schlagworte durchsuchen"

#: ../classes/taxonomies.php:58
msgid "Popular Tags"
msgstr "Häufig verwendete Schlagworte"

#: ../classes/taxonomies.php:59
msgid "Separate Tags with commas"
msgstr "Schlagworte durch Komma getrennt"

#: ../classes/taxonomies.php:60
msgid "Add or remove Tags"
msgstr "Schlagworte hinzufügen oder entfernen"

#: ../classes/taxonomies.php:61
msgid "Choose from the most used Tags"
msgstr "Wähle aus den am meisten verwendeten Schlagworten"

#: ../classes/taxonomies.php:62
msgid "No Tags found."
msgstr "Keine Schlagworte gefunden."

#: ../classes/taxonomies.php:71
#, php-format
msgctxt "URL slug"
msgid "%s/tag"
msgstr "%s/schlagwort"

#: ../classes/taxonomies.php:111
msgid "Archive Url"
msgstr "Archiv Url"

#: ../classes/taxonomies.php:114
#, php-format
msgid "This is the URL to the archive of this %s."
msgstr "Das ist die URL des %s-Archives."

#: ../classes/taxonomies.php:118
msgid "Archive Feed"
msgstr "Archiv Feed"

#: ../classes/taxonomies.php:121
#, php-format
msgid "This is the URL to the feed of the archive of this %s."
msgstr "Das ist die URL zum RSS-Feed des %s-Archives."

#: ../classes/template-tags-fallbacks.php:48
msgid "Uncategorized"
msgstr "Unkategorisiert"

#: ../classes/thumbnails.php:31
msgid "Thumbnail"
msgstr "Vorschaubild"

#: ../classes/thumbnails.php:32
msgid "Medium"
msgstr "Mittel"

#: ../classes/thumbnails.php:33
msgid "Large"
msgstr "Groß"

#: ../classes/thumbnails.php:34
msgid "Full Size"
msgstr "Volle Größe"

#: ../meta-boxes/appearance.php:5 ../options-page/previews.php:39
msgid "Columns"
msgstr "Spalten"

#: ../meta-boxes/appearance.php:15 ../options-page/previews.php:48
msgid "Size"
msgstr "Größe"

#: ../meta-boxes/images.php:19
msgid "Remove image"
msgstr "Bild entfernen"

#: ../meta-boxes/images.php:24
msgid "Add images"
msgstr "Bilder hinzufügen"

#: ../meta-boxes/owner.php:14
msgid "Changes the owner of this gallery."
msgstr "Verändert den Eigentümer dieser Galerie."

#: ../meta-boxes/show-code.php:3
msgid ""
"To embed this gallery in another posts content you can use this "
"<em>[gallery]</em> shortcode:"
msgstr ""
"Um diese Galerie in einen Inhalt einzubinden, verwendest du einfach diesen "
"<em>[gallery]</em> Shortcode:"

#: ../meta-boxes/show-code.php:5
msgid "Just copy this code to all places where this gallery should appear."
msgstr ""
"Kopiere diesen Code an alle Stellen, an denen diese Galerie erscheinen soll."

#: ../meta-boxes/show-hash.php:3
msgid ""
"To start this gallery in a lightbox by clicking a link you can link to this "
"<em>#hash</em>:"
msgstr ""
"Um die Galerie in der Lightbox beim Klick zu öffnen, kannst du einen Link "
"auf diesen <em>#hash</em> setzen:"

#: ../meta-boxes/show-hash.php:5
msgid "Just use this hash as link target (href)."
msgstr "Setze diesen Hash einfach als Linkziel (href-Attribut)."

#: ../options-page/archive.php:6
msgid "Enable the gallery archive."
msgstr "Galerie-Archiv aktivieren."

#: ../options-page/archive.php:10
#, php-format
msgid ""
"The archive link for your galleries is: <a href=\"%1$s\" target=\"_blank\">"
"%1$s</a>"
msgstr ""
"Der Archiv-Link für deine Galerien lautet: <a href=\"%1$s\" target=\"_blank"
"\">%1$s</a>"

#: ../options-page/archive.php:14
#, php-format
msgid ""
"The archive feed for your galleries is: <a href=\"%1$s\" target=\"_blank\">"
"%1$s</a>"
msgstr ""
"Der Archiv-Link zum RSS-Feed deiner Galerien lautet: <a href=\"%1$s\" target="
"\"_blank\">%1$s</a>"

#: ../options-page/gallery-management.php:5
msgid "Text Editor"
msgstr "Texteditor"

#: ../options-page/gallery-management.php:8
#: ../options-page/gallery-management.php:49
#: ../options-page/gallery-management.php:60 ../options-page/lightbox.php:9
#: ../options-page/lightbox.php:30 ../options-page/lightbox.php:43
#: ../options-page/lightbox.php:53 ../options-page/lightbox.php:63
msgid "On"
msgstr "An"

#: ../options-page/gallery-management.php:9
#: ../options-page/gallery-management.php:19
#: ../options-page/gallery-management.php:29
#: ../options-page/gallery-management.php:39
#: ../options-page/gallery-management.php:50
#: ../options-page/gallery-management.php:61 ../options-page/lightbox.php:19
msgid "Off"
msgstr "Aus"

#: ../options-page/gallery-management.php:11
msgid "Enables or disables text editor for galleries."
msgstr "Aktiviert oder deaktiviert den Texteditor für Galerien."

#: ../options-page/gallery-management.php:16
msgid "Excerpts"
msgstr "Auszüge"

#: ../options-page/gallery-management.php:21
msgid "Enables or disables text excerpts for galleries."
msgstr "Aktiviert oder deaktiviert Textauszüge für Galerien."

#: ../options-page/gallery-management.php:26
msgid "Revisions"
msgstr "Revisionen"

#: ../options-page/gallery-management.php:31
msgid "Enables or disables revisions for galleries."
msgstr "Aktiviert oder deaktiviert Revisionen für Galerien."

#: ../options-page/gallery-management.php:36
msgid "Comments"
msgstr "Kommentare"

#: ../options-page/gallery-management.php:41
msgid "Enables or disables comments and trackbacks for galleries."
msgstr "Aktiviert oder deaktiviert Kommentare und Trackbacks für Galerien."

#: ../options-page/gallery-management.php:46
msgid "Featured Image"
msgstr "Beitragsbild"

#: ../options-page/gallery-management.php:52
msgid "Enables or disables the \"Featured Image\" for galleries."
msgstr "Aktiviert oder deaktiviert die \"Beitragsbild\"-Funktion für Galerien."

#: ../options-page/gallery-management.php:57
msgid "Custom Fields"
msgstr "Benutzerdefinierte Felder"

#: ../options-page/gallery-management.php:63
msgid "Enables or disables the \"Custom Fields\" for galleries."
msgstr "Aktiviert oder deaktiviert benutzerdefinierte Felder für Galerien."

#: ../options-page/lightbox.php:11
msgid "Turn this off if you do not want to use the included lightbox."
msgstr ""
"Schalte die Lightbox aus, wenn dir die mitgelieferte Version nicht zusagt."

#: ../options-page/lightbox.php:16
msgid "Loop mode"
msgstr "Schleifenmodus"

#: ../options-page/lightbox.php:21
msgid ""
"Enables the user to get from the last image to the first one with the \"Next "
"&raquo;\" button."
msgstr ""
"Ermöglicht es dem Benutzer vom letzten zum ersten Bild durch den \"Weiter "
"&raquo;\" Button zu gelangen."

#: ../options-page/lightbox.php:26
msgid "Title &amp; Description"
msgstr "Titel &amp; Beschreibung"

#: ../options-page/lightbox.php:32
msgid ""
"Turn this off if you do not want to display the image title and description "
"in your lightbox."
msgstr ""
"Schalte diese Option ab, wenn du Bildtitel und Beschreibung nicht in der "
"Lightbox anzeigen möchtest."

#: ../options-page/lightbox.php:34
msgid ""
"This feature won't work because the <a href=\"https://secure.php.net/manual/"
"en/book.libxml.php\" target=\"_blank\">LibXML PHP extension</a> is not "
"available on your webserver or it is too old."
msgstr ""
"Dieses Feature wird nicht funktionieren weil die <a href=\"https://secure."
"php.net/manual/en/book.libxml.php\" target=\"_blank\">LibXML PHP "
"Erweiterung</a> auf deinem Webserver nicht verfügbar oder zu alt ist."

#: ../options-page/lightbox.php:40
msgid "Close button"
msgstr "Schließen-Knopf"

#: ../options-page/lightbox.php:45
msgid ""
"Turn this off if you do not want to display a close button in your lightbox."
msgstr ""
"Schalte diese Option ab, wenn du keinen Schließen-Knopf in der Lightbox "
"anzeigen möchtest."

#: ../options-page/lightbox.php:50
msgid "Indicator thumbnails"
msgstr "Navigationsvorschaubilder"

#: ../options-page/lightbox.php:55
msgid ""
"Turn this off if you do not want to display small preview thumbnails below "
"the lightbox image."
msgstr ""
"Schalte diese Option ab, wenn du keine Navigationsvorschaubilder in der "
"Lightbox anzeigen möchtest."

#: ../options-page/lightbox.php:60
msgid "Slideshow play/pause button"
msgstr "Slideshow-Start/Stop-Knopf"

#: ../options-page/lightbox.php:65
msgid ""
"Turn this off if you do not want to provide a slideshow function in the "
"lightbox."
msgstr ""
"Schalte diese Option ab, wenn du keinen Slideshow-Knopf in der Lightbox "
"anzeigen möchtest."

#: ../options-page/lightbox.php:70
msgid "Slideshow speed"
msgstr "Geschwindigkeit der Slideshow"

#: ../options-page/lightbox.php:73 ../options-page/lightbox.php:90
msgid "ms"
msgstr "ms"

#: ../options-page/lightbox.php:74
msgid "The delay between two images in the slideshow."
msgstr "Die Verzögerung zwischen zwei Bildwechseln in der Slideshow."

#: ../options-page/lightbox.php:79
msgid "Preload images"
msgstr "Bilder vorladen"

#: ../options-page/lightbox.php:82
msgid "The number of images which should be preloaded around the current one."
msgstr ""
"Die Anzahl der Bilder, die vor und nach dem  aktuellen Bild im Hintergrund "
"vorgeladen werden."

#: ../options-page/lightbox.php:87
msgid "Animation speed"
msgstr "Animationsgeschwindigkeit"

#: ../options-page/lightbox.php:91
msgid "The speed of the image change animation."
msgstr ""
"Die Geschwindigkeit in der die Bildwechsel-Animation durchgeführt wird."

#: ../options-page/lightbox.php:96
msgid "Stretch images"
msgstr "Bilder vergrößern"

#: ../options-page/lightbox.php:99
msgid "No stretching"
msgstr "Nicht vergrößern"

#: ../options-page/lightbox.php:100
msgid "Contain"
msgstr "Einpassen"

#: ../options-page/lightbox.php:101
msgid "Cover"
msgstr "Aufspannen"

#: ../options-page/lightbox.php:103
msgid ""
"\"Contain\" means to scale the image to the largest size such that both its "
"width and its height can fit the screen."
msgstr ""
"\"Einpassen\" bedeutet, dass das Bild soweit vergrößert wird, dass die Höhe "
"und Breite des Bildes nicht größer als der Viewport sind. Vom Bild wird "
"nichts abgeschnitten."

#: ../options-page/lightbox.php:104
msgid ""
"\"Cover\" means to scale the image to be as large as possible so that the "
"screen is completely covered by the image. Some parts of the image may be "
"cropped and invisible."
msgstr ""
"\"Aufspannen\" bedeutet, dass das Bild soweit vergrößert wird bis es den "
"gesamten Viewport einnimmt. Überstehende Teile werden dabei abgeschnitten."

#: ../options-page/lightbox.php:109
msgid "Script position"
msgstr "Scriptposition"

#: ../options-page/lightbox.php:112
msgid "Footer of the website"
msgstr "Fußbereich der Webseite"

#: ../options-page/lightbox.php:113
msgid "Header of the website"
msgstr "Kopfbereich der Webseite"

#: ../options-page/lightbox.php:115
msgid ""
"Please choose the position of the javascript. \"Footer\" is recommended. Use "
"\"Header\" if you have trouble to make the lightbox work."
msgstr ""
"Bitte wähle die Position des Javascripts aus. Der \"Fußbereich\" wird "
"empfohlen. Wähle \"Kopfbereich\" aus, wenn die Lightbox nicht richtig "
"funktionieren sollte."

#: ../options-page/previews.php:4
msgid "Gallery previews are randomly chosen images from a gallery."
msgstr "Galerievorschauen bestehen aus zufälligen Bildern einer Galerie."

#: ../options-page/previews.php:5
msgid ""
"They will be shown where your theme would display a text excerpt for regular "
"posts usually."
msgstr ""
"Diese werden dort angezeigt, wo dein Theme normalerweise einen Textauszug "
"für einen regulären Beitrag anzeigen würde."

#: ../options-page/previews.php:28
msgid "Enable previews for auto generated excerpts."
msgstr "Galerievorschauen für automatisch generierte Auszüge aktivieren."

#: ../options-page/previews.php:34
msgid "Number of images"
msgstr "Anzahl der Bilder"

#: ../options-page/previews.php:60
msgid ""
"Enable previews for custom excerpts too. (Do not activate this option if "
"your theme displays the excerpt and the content on the same page.)"
msgstr ""
"Vorschau auch für manuell verfasste Textauszüge aktivieren. (Aktiviere diese "
"Option nicht, wenn dein Theme den Textauszug und den Inhaltstext auf der "
"selben Seiten anzeigt.)"

#: ../options-page/taxonomies.php:3
msgid ""
"Please select the taxonomies you want to use to classify your galleries."
msgstr ""
"Bitte wähle die Klassifizierungen aus, die du zur Gruppierung deiner "
"Galerien nutzen möchtest."

#: ../options-page/taxonomies.php:14 ../options-page/taxonomies.php:24
msgid "hierarchical"
msgstr "hierarchisch"

#: ../options-page/taxonomies.php:19
msgid "Events"
msgstr "Ereignisse"

#: ../options-page/taxonomies.php:19
msgid "Places"
msgstr "Orte"

#: ../options-page/taxonomies.php:19
msgid "Dates"
msgstr "Zeiträume"

#: ../options-page/taxonomies.php:19
msgid "Persons"
msgstr "Personen"

#: ../options-page/taxonomies.php:19
msgid "Photographers"
msgstr "Fotografen"

#: ../widgets/galleries.php:12
msgid "Displays some of your galleries."
msgstr "Zeigt eine Auswahl deiner Galerien an."

#: ../widgets/galleries.php:44 ../widgets/random-images.php:70
#: ../widgets/taxonomies.php:49 ../widgets/taxonomy-cloud.php:49
msgid "Title:"
msgstr "Titel:"

#: ../widgets/galleries.php:46 ../widgets/random-images.php:72
#: ../widgets/taxonomies.php:51 ../widgets/taxonomy-cloud.php:51
msgid "Leave blank to use the widget default title."
msgstr "Leer lassen, um Standardtitel zu verwenden."

#: ../widgets/galleries.php:50
msgid "Number of galleries:"
msgstr "Galerienanzahl:"

#: ../widgets/galleries.php:55 ../widgets/taxonomies.php:84
#: ../widgets/taxonomy-cloud.php:79
msgid "Order by:"
msgstr "Sortieren nach:"

#: ../widgets/galleries.php:57
msgid "Title"
msgstr "Titel"

#: ../widgets/galleries.php:58
msgid "Date"
msgstr "Zeitraum"

#: ../widgets/galleries.php:59
msgid "Modified"
msgstr "Bearbeitungsdatum"

#: ../widgets/galleries.php:60
msgid "Randomly"
msgstr "Zufällig"

#: ../widgets/galleries.php:61
msgid "Number of comments"
msgstr "Anzahl der Kommentare"

#: ../widgets/galleries.php:66 ../widgets/taxonomies.php:94
#: ../widgets/taxonomy-cloud.php:89
msgid "Order:"
msgstr "Reihenfolge:"

#: ../widgets/galleries.php:68 ../widgets/taxonomies.php:96
#: ../widgets/taxonomy-cloud.php:91
msgid "Ascending"
msgstr "Aufsteigend"

#: ../widgets/galleries.php:69 ../widgets/taxonomies.php:97
#: ../widgets/taxonomy-cloud.php:92
msgid "Descending"
msgstr "Absteigend"

#: ../widgets/random-images.php:11 ../widgets/random-images.php:26
msgid "Random Images"
msgstr "Zufällige Bilder"

#: ../widgets/random-images.php:12
msgid "Displays some random images from your galleries."
msgstr "Zeigt zufällige Bilder aus deinen Galerien an."

#: ../widgets/random-images.php:76
msgid "Number of images:"
msgstr "Anzahl der Bilder:"

#: ../widgets/random-images.php:81
msgid "Columns:"
msgstr "Spalten:"

#: ../widgets/random-images.php:90
msgid "Thumbnail size:"
msgstr "Vorschaubildgröße:"

#: ../widgets/taxonomies.php:11 ../widgets/taxonomies.php:26
#: ../widgets/taxonomy-cloud.php:26
msgid "Gallery Taxonomies"
msgstr "Galerieklassifizierungen"

#: ../widgets/taxonomies.php:12
msgid ""
"Displays your gallery taxonomies like categories, tags, events, "
"photographers, etc."
msgstr ""
"Zeigt deine Galerieklassifizierungen wie Kategorien, Schlagworte, "
"Ereignisse, Fotografen, etc. an."

#: ../widgets/taxonomies.php:55 ../widgets/taxonomy-cloud.php:55
msgid "Taxonomy:"
msgstr "Klassifizierung:"

#: ../widgets/taxonomies.php:61 ../widgets/taxonomy-cloud.php:61
msgid "Please choose the taxonomy the widget should display."
msgstr "Bitte wähle die Klassifizierung aus, die das Widget anzeigen soll."

#: ../widgets/taxonomies.php:65 ../widgets/taxonomy-cloud.php:65
msgid "Number of terms:"
msgstr "Anzahl der Elemente:"

#: ../widgets/taxonomies.php:67 ../widgets/taxonomy-cloud.php:67
msgid "Leave blank to show all."
msgstr "Leer lassen, um alle anzuzeigen."

#: ../widgets/taxonomies.php:80
msgid "Show gallery counts."
msgstr "Galerienanzahl einblenden."

#: ../widgets/taxonomies.php:86 ../widgets/taxonomy-cloud.php:81
msgid "Name"
msgstr "Name"

#: ../widgets/taxonomies.php:87 ../widgets/taxonomy-cloud.php:82
msgid "Gallery count"
msgstr "Galerienanzahl"

#: ../widgets/taxonomies.php:89 ../widgets/taxonomy-cloud.php:84
msgid "Slug"
msgstr "Slug"

#: ../widgets/taxonomy-cloud.php:11
msgid "Gallery Taxonomy Cloud"
msgstr "Galerieklassifizierungen (Wolke)"

#: ../widgets/taxonomy-cloud.php:12
msgid "Displays your gallery taxonomies as tag cloud."
msgstr "Zeigt deine Galerieklassifizierungen als  \"Wortwolke\" an."

#: ../widgets/taxonomy-cloud.php:93
msgid "Random"
msgstr "Zufällig"

#~ msgid "Remove this ad? Upgrade to Pro!"
#~ msgstr "Die Werbung nervt? Jetzt zu Pro wechseln!"

#~ msgid ""
#~ "If you like this gallery management plugin please consider upgrading to "
#~ "<u>Gallery Manager Pro</u>!"
#~ msgstr ""
#~ "Wenn dir der Gallery-Manager zusagt, upgrade doch auf <u>Gallery Manager "
#~ "Pro</u>!"

#~ msgid ""
#~ "In the <a href=\"%s\" target=\"_blank\">premium version of Gallery "
#~ "Manager</a> you can take advantage of the gallery management without any "
#~ "limitations."
#~ msgstr ""
#~ "In der <a href=\"%s\" target=\"_blank\">Premium Version vom Gallery "
#~ "Manager</a> kannst du alle Vorteile des Galerie-Managements ohne "
#~ "Einschränkungen nutzen."

#~ msgid "&laquo; Back to your galleries"
#~ msgstr "&laquo; Zurück zu deinen Galerien"

#~ msgid "Gallery Shortcode"
#~ msgstr "Galerie-Shortcode"

#~ msgid "Gallery Hash"
#~ msgstr "Galerie-Hash"
