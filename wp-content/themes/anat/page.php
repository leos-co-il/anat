<?php

the_post();
get_header();
$fields = get_fields();

?>
<article class="page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
