<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<div class="row justify-content-center align-items-start mb-3">
						<div class="col-xl col-12 breadcrumbs-custom">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container py-5">
		<div class="row justify-content-between">
			<div class="col-lg-7 col-12 post-content-col">
				<div class="base-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-lg-5 col-12 post-form-col">
				<?php if (has_post_thumbnail()) : ?>
					<img src="<?= postThumb(); ?>" alt="post-image" class="post-page-img w-100 mb-5">
				<?php endif;
				get_template_part('views/partials/repeat', 'form_pop',
					[
						'title' => opt('post_form_title'),
						'text' => opt('post_form_text'),
						'id' => '73',
					]); ?>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 3,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="same-posts">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col-auto">
					<div class="base-title mb-4">
						<?= $fields['same_title'] ? $fields['same_title'] : 'מאמרים נוספים שעשויים לעניין אתכם'; ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $post) : ?>
					<a class="col-lg-4 col-sm-11 col-12 card-post"  href="<?= get_the_permalink($post); ?>">
						<span class="post-img"<?php if (has_post_thumbnail($post)) : ?>
							style="background-image: url('<?= postThumb($post); ?>')"<?php endif; ?>>
							<span class="card-post-overlay"></span>
						</span>
						<span class="post-card-content">
							<span class="post-card-title"><?= $post->post_title; ?></span>
							<span class="post-card-text">
								<?= text_preview($post->post_content, 15); ?>
							</span>
						</span>
					</a>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
			'img' => $fields['faq_img'],
		]);
endif;
get_footer(); ?>
