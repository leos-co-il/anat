<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
		'posts_per_page' => 6,
		'post_type' => 'post',
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				]
		]
]);
$published_posts = get_posts([
		'numberposts' => -1,
		'post_type' => 'post',
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				]
		]
]);
?>

<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<div class="row justify-content-center align-items-start mb-3">
						<div class="col-xl col-12 breadcrumbs-custom">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="base-title text-center">
					<?= $query->name; ?>
				</h1>
				<div class="base-output text-center">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="posts-page">
		<?php if ($posts->have_posts()) : ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch put-here-posts">
					<?php foreach ($posts->posts as $i => $post) : ?>
						<?php get_template_part('views/partials/card', 'post', [
							'post' => $post,
						]); ?>
					<?php endforeach; ?>
				</div>
				<?php if ($published_posts && $published_posts > 6) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<div class="more-link load-more-posts" data-term="<?= $query->term_id; ?>">
								טענו עוד מאמרים
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="blog-form">
	<?php get_template_part('views/partials/repeat', 'form_block'); ?>
</div>
<?php if ($faq = get_field('faq_item', $query)) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => get_field('faq_title', $query),
			'faq' => $faq,
			'img' => get_field('faq_img', $faq),
		]);
endif;
get_footer(); ?>
