<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$whatsapp = opt('whatsapp');
$mail = opt('mail');
$facebook = opt('facebook');
$address = opt('address');
?>

<article class="page-body">
	<div class="container-fluid">
		<div class="row justify-content-center align-items-start mb-3">
			<div class="col-xl-11 col-12 breadcrumbs-custom">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				} ?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-12 d-flex align-items-center">
				<div class="row justify-content-center align-items-start w-100">
					<?php if ($address) : ?>
						<a href="https://www.waze.com/ul?q=<?= $address; ?>"
						   class="contact-item-link contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.1s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-geo.png">
							</div>
							<h3 class="contact-info-title">
								הכתובת שלנו
							</h3>
							<p class="small-text text-center">
								<?= $address; ?>
							</p>
						</a>
					<?php endif;
					if ($facebook) : ?>
						<a href="<?= $facebook; ?>"
						   class="contact-item-link contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.3s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-facebook.png">
							</div>
							<h3 class="contact-info-title">
								בואו נהיה חברים!
							</h3>
							<p class="small-text text-center">
								עשו לנו לייק
							</p>
						</a>
					<?php endif;
					if ($whatsapp) : ?>
						<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="col-lg-3 col-sm-6 col-11 contact-item contact-item-link wow zoomIn"
						   data-wow-delay="0.5s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-whatsapp.png">
							</div>
							<h3 class="contact-info-title">
								טלפון:
							</h3>
							<p class="small-text text-center">
								<?= $whatsapp; ?>
							</p>
						</a>
					<?php endif;
					if ($mail) : ?>
						<a href="mailto:<?= $mail; ?>" class="contact-item col-lg-3 col-sm-6 col-11 contact-item-link wow zoomIn"
						   data-wow-delay="0.7s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-mail.png">
							</div>
							<h3 class="contact-info-title">
								מייל:
							</h3>
							<p class="small-text text-center">
								<?= $mail; ?>
							</p>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-xl-3 col-sm-8 col-12 faq-img-col">
				<?php $img = has_post_thumbnail() ? postThumb() : IMG.'faq-block-img.png'; ?>
				<img src="<?= $img; ?>" alt="contact-page-img">
			</div>
		</div>
		<div class="row justify-content-center mb-5">
			<div class="col-12">
				<div class="contact-page-form">
					<?php if ($fields['contact_form_title']) : ?>
						<h3 class="form-title"><?= $fields['contact_form_title']; ?></h3>
					<?php endif;
					if ($fields['contact_form_text']) : ?>
						<p class="form-subtitle"><?= $fields['contact_form_text']; ?></p>
					<?php endif;
					getForm('32'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
