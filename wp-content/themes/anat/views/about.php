<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>

<article class="article-page-body page-body pb-5">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<div class="row justify-content-center align-items-start mb-3">
						<div class="col-xl col-12 breadcrumbs-custom">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container pt-5 mb-5">
		<div class="row justify-content-between">
			<div class="<?= has_post_thumbnail() ? 'col-xl-6 col-lg-7 col-12' : 'col-12'; ?>">
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
			<?php if (has_post_thumbnail()) : ?>
				<div class="col-xl-5 col-lg-5 col-12 d-flex justify-content-center align-items-center">
					<img src="<?= postThumb(); ?>" alt="about-page-image" class="w-100">
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form_block');
if ($fields['img_or_video'] || $fields['about_gallery_text']) : ?>
	<section class="gallery-img-video">
		<?php if ($fields['about_gallery_text']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto mb-4">
						<div class="base-output about-gallery-output text-center">
							<?= $fields['about_gallery_text']; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; if ($fields['img_or_video']) : ?>
			<div class="gallery-line-flex">
				<?php foreach ($fields['img_or_video'] as $item) : ?>
					<div class="gallery-item">
						<?php if (($item['acf_fc_layout'] === 'gal_video') && $item['video_link']) : ?>
							<div class="gallery-video" style="background-image: url('<?= getYoutubeThumb($item['video_link']); ?>')">
								<span class="gallery-video-overlay"></span>
								<span class="play-button" data-video="<?= getYoutubeId($item['video_link']); ?>">
									<img src="<?= ICONS ?>play.png">
								</span>
							</div>
						<?php elseif (($item['acf_fc_layout'] === 'gal_img') && $item['image']) : ?>
							<a href="<?= $item['image']['url']; ?>" class="gallery-image" data-lightbox="gallery"
							   style="background-image: url('<?= $item['image']['url']; ?>')"></a>
						<?php endif; ?>
					</div>
				<?php endforeach;
				if ($inst = opt('instagram')) : ?>
					<a class="insta-item" href="<?= $inst; ?>">
						<span class="insta-item-wrap">
							<?php if ($fields['about_inst_title']) : ?>
								<span class="insta-title">
									<?= $fields['about_inst_title']; ?>
								</span>
							<?php endif; ?>
							<img src="<?= ICONS ?>instagram.png" alt="instagram">
							<?php if ($fields['about_inst_subtitle']) : ?>
								<span class="insta-subtitle">
							<?= $fields['about_inst_title']; ?>
						</span>
							<?php endif; ?>
						</span>
					</a>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</section>
<?php
	get_template_part('views/partials/repeat', 'video_modal');
endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
			'img' => $fields['faq_img'],
		]);
endif;
get_footer(); ?>
