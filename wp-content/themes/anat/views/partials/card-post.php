<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<a class="col-lg-4 col-md-6 col-sm-10 col-12 card-post more-card" data-id="<?= $args['post']->ID; ?>" href="<?= $link; ?>">
		<span class="post-img"<?php if (has_post_thumbnail($args['post'])) : ?>
			style="background-image: url('<?= postThumb($args['post']); ?>')"<?php endif; ?>>
					<span class="card-post-overlay"></span>
			</span>
		<span class="post-card-content">
			<span class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></span>
			<span class="post-card-text">
				<?= text_preview($args['post']->post_content, 15); ?>
			</span>
		</span>
	</a>
<?php endif; ?>
