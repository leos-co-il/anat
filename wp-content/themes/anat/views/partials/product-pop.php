<?php if (!isset($args['product'])) {
	return;
}
$product = wc_get_product($args['product']);
$post_id = $product->get_id();
$post_link = get_the_permalink($post_id);
$product_thumb = wp_get_attachment_image_url( get_post_thumbnail_id($post_id), 'full' );;
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = wp_get_attachment_image_url($_item, 'large', '');
	}
}
$fields = get_fields($post_id);
$info_del = get_field('prod_delivery_info', $post_id) ? $fields['prod_delivery_info'] : opt('prod_delivery');
$info_content = $product->get_description();
?>
<div id="product-<?= $post_id; ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="container">
		<div class="row">
			<div class="col-xl-6">
				<h2 class="product-pop-title">
					<?= $product->get_name(); ?>
				</h2>
			</div>
		</div>
	</div>
	<section class="product-summary mb-5">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-xl-5 col-lg-6 col-12 d-flex flex-column align-items-start">
					<div class="put-price-here">
						<span class="price-title ml-2">מחיר:</span>
						<div class="put-price-here-place">
							<?= $product->get_price_html(); ?>
						</div>
					</div>
					<?php
//					/**
//					 * Hook: woocommerce_single_product_summary.
//					 *
//					 * @hooked woocommerce_template_single_title - 5
//					 * @hooked woocommerce_template_single_rating - 10
//					 * @hooked woocommerce_template_single_price - 10
//					 * @hooked woocommerce_template_single_excerpt - 20
//					 * @hooked woocommerce_template_single_add_to_cart - 30
//					 * @hooked woocommerce_template_single_meta - 40
//					 * @hooked woocommerce_template_single_sharing - 50
//					 * @hooked WC_Structured_Data::generate_product_data() - 60
//					 */
//					do_action( 'woocommerce_single_product_summary' );
					?>

					<?php do_action('display_excerpt_custom');
					if ($info_del || $info_content): ?>
						<div class="product-item-info mt-4 w-100">
							<div id="accordion-product">
								<?php if ($info_content) : $c = 1; ?>
									<div class="card">
										<div class="card-header" id="heading<?= $c ?>">
											<h5 class="mb-0 accordion-button-wrap">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $c ?>" aria-expanded="true" aria-controls="collapse<?= $c ?>">
													מידע נוסף
												</button>
											</h5>
										</div>
										<div id="collapse<?= $c ?>" class="collapse" aria-labelledby="heading<?= $c ?>" data-parent="#accordion-product">
											<div class="card-body base-output slider-output">
												<?= $info_content; ?>
											</div>
										</div>
									</div>
								<?php endif;
								if ($info_del) : $x = 2; ?>
									<div class="card">
										<div class="card-header" id="heading<?= $x ?>">
											<h5 class="mb-0 accordion-button-wrap">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $x ?>" aria-expanded="true" aria-controls="collapse<?= $x ?>">
													משלוחים
												</button>
											</h5>
										</div>
										<div id="collapse<?= $x ?>" class="collapse" aria-labelledby="heading<?= $x ?>" data-parent="#accordion-product">
											<div class="card-body base-output slider-output">
												<?= $info_del; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
					<div class="socials-share">
					<span class="share-text">
						שתפו מוצר זה
					</span>
						<!--	WHATSAPP-->
						<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
						   class="social-share-link">
							<i class="fab fa-whatsapp"></i>
						</a>
						<!--	MAIL-->
						<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
						   class="social-share-link">
							<i class="far fa-envelope"></i>
						</a>
						<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
						   class="social-share-link">
							<i class="fab fa-facebook-f"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-6 gallery-slider-wrap">
					<div class="gallery-slider" dir="rtl">
						<?php if($product_thumb): ?>
							<div class="p-2">
								<a class="big-slider-item" style="background-image: url('<?= $product_thumb; ?>')"
								   href="<?= $product_thumb; ?>" data-lightbox="images"></a>
							</div>
						<?php endif;
						foreach ($product_gallery_images as $img): ?>
							<div class="p-2">
								<a class="big-slider-item" style="background-image: url('<?= $img; ?>')"
								   href="<?= $img; ?>" data-lightbox="images">
								</a>
							</div>
						<?php endforeach; ?>
					</div>
					<div class="thumbs" dir="rtl">
						<?php if($product_thumb): ?>
							<div class="p-2">
								<a class="thumb-item" style="background-image: url('<?= $product_thumb; ?>')"
								   href="<?= $product_thumb; ?>" data-lightbox="images-small"></a>
							</div>
						<?php endif;
						foreach ($product_gallery_images as $img): ?>
							<div class="p-2">
								<a class="thumb-item" style="background-image: url('<?= $img; ?>')"
								   href="<?= $img; ?>" data-lightbox="images-small">
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
