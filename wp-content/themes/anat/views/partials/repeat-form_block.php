<div class="repeat-form-block">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php if ($f_title = opt('base_form_title')) : ?>
					<h2 class="form-title"><?= $f_title; ?></h2>
				<?php endif;
				if ($f_subtitle = opt('base_form_text')) : ?>
					<p class="form-subtitle"><?= $f_subtitle; ?></p>
				<?php endif;
				getForm('33'); ?>
			</div>
		</div>
	</div>
</div>
