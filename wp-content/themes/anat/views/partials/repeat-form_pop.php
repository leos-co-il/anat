<?php
$id = (isset($args['id']) && $args['id']) ? $args['id'] : '31';
?>
<div class="form-wrapper pop-wrapper">
	<span class="close-form">
		<img src="<?= ICONS ?>close.png">
	</span>
	<?php if (isset($args['title']) && $args['title']) : ?>
		<h2 class="form-title"><?= $args['title']; ?></h2>
	<?php endif;
	if (isset($args['text']) && $args['text']) : ?>
		<p class="form-subtitle"><?= $args['text']; ?></p>
	<?php endif;
	getForm($id); ?>
</div>
