<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col-auto">
					<h2 class="base-title">
						<?= (isset($args['title']) && $args['title']) ? $args['title'] : 'כל מה שרציתן לדעת -אני עונה'; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-8 col-lg-9 col-12 faq-content-col">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span class="faq-icon">
											?
										</span>
										<span class="faq-body-title"><?= $item['faq_question']; ?></span>
										<img src="<?= ICONS ?>arrow-bottom.txt" class="arrow-top" alt="arrow-top">
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="base-output small-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-xl-4 col-lg-3 faq-img-col">
					<?php $img = (isset($args['img']) && $args['img']) ? $args['img']['url'] : IMG.'faq-block-img.png'; ?>
					<img src="<?= $img; ?>" alt="faq-block-img">
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
