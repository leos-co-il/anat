<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 6,
	'post_type' => 'post',
	'suppress_filters' => false,
]);
$published_posts = count(get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
]))
?>

<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<div class="row justify-content-center align-items-start mb-3">
						<div class="col-xl col-12 breadcrumbs-custom">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="base-title text-center">
					<?php the_title(); ?>
				</h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="posts-page">
		<?php if ($posts->have_posts()) : ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch put-here-posts">
					<?php foreach ($posts->posts as $i => $post) : ?>
						<?php get_template_part('views/partials/card', 'post', [
								'post' => $post,
						]); ?>
					<?php endforeach; ?>
				</div>
				<?php if ($published_posts && $published_posts > 6) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<div class="more-link load-more-posts">
								טענו עוד מאמרים
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="blog-form">
	<?php get_template_part('views/partials/repeat', 'form_block'); ?>
</div>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
					'img' => $fields['faq_img'],
			]);
endif;
get_footer(); ?>
