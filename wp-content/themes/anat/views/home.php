<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<?php if ($fields['main_slider']) : ?>
	<section class="main-block-home">
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['main_slider'] as $slide) : ?>
				<div class="main-slide" <?php if ($slide['main_img']) : ?>
					style="background-image: url('<?= $slide['main_img']['url']; ?>')"<?php endif; ?>>
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-12 d-flex flex-column justify-content-center align-items-center">
								<h2 class="main-title"><?= $slide['main_title']; ?></h2>
								<?php if ($slide['main_link']) : ?>
									<a href="<?= $slide['main_link']['url']; ?>" class="main-link">
										<?= (isset($slide['main_link']['title']) && $slide['main_link']['title'])
												? $slide['main_link']['title'] : 'לחנות'; ?>
									</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif;
if ($fields['home_cats']) : ?>
	<section class="home-cats-block mb-4">
		<?php if ($fields['home_cats_title']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="base-title text-center"><?= $fields['home_cats_title']; ?></h2>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="cats-line gallery-line-flex">
			<?php foreach ($fields['home_cats'] as $i => $cat_item) :
				$thumbnail_id = get_term_meta($cat_item->term_id, 'thumbnail_id', true );
				$cat_image = wp_get_attachment_url( $thumbnail_id ); ?>
				<a class="home-category wow zoomIn" data-wow-delay="0.<?= $i * 2; ?>s" href="<?= get_category_link($cat_item); ?>">
					<span class="home-cat-img">
						<?php if ($cat_image) : ?>
							<img src="<?= $cat_image; ?>" alt="category-img">
						<?php endif; ?>
					</span>
					<span class="home-cat-title-wrap">
						<span class="base-link"><?= $cat_item->name; ?></span>
					</span>
				</a>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif;
if ($fields['home_about_text']) : ?>
	<section class="home-about-block my-5">
		<div class="container">
			<div class="row justify-content-between">
				<div class="<?= $fields['home_about_img'] ? 'col-xl-6 col-lg-7 col-12' : 'col-12'; ?>
				d-flex flex-column align-items-start">
					<div class="base-output">
						<?= $fields['home_about_text']; ?>
					</div>
					<?php if ($fields['home_about_link']) : ?>
						<a href="<?= $fields['home_about_link']['url']; ?>" class="base-link">
							<?= (isset($fields['home_about_link']['title']) && $fields['home_about_link']['title'])
										? $fields['home_about_link']['title'] : 'להמשך קריאה'; ?>
						</a>
					<?php endif; ?>
				</div>
				<?php if ($fields['home_about_img']) : ?>
					<div class="col-xl-5 col-lg-5 col-12 d-flex justify-content-center align-items-center h-about-col-img">
						<img src="<?= $fields['home_about_img']['url']; ?>" alt="about-page-image" class="w-100">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
<!--Products 1-->
<?php if ($fields['home_prods_1']) {
	get_template_part('views/partials/content', 'products_output',
			[
					'block_title' => $fields['home_prods_title_1'],
					'products' => $fields['home_prods_1'],
					'block_link' => $fields['home_prods_link_1']
			]);
}
if ($fields['banner_title'] || $fields['banner_link'] || $fields['banner_subtitle']) : ?>
	<section class="banner-block" <?php if ($fields['banner_back_img']) : ?>
		style="background-image: url('<?= $fields['banner_back_img']['url']; ?>')"
	<?php endif; ?>>
		<div class="container h-100">
			<div class="row justify-content-center h-100">
				<div class="col-auto h-100 d-flex flex-column justify-content-between align-items-center">
					<div class="titles-wrap">
						<?php if ($fields['banner_title']) : ?>
							<h2 class="main-title"><?= $fields['banner_title']; ?></h2>
						<?php endif;
						if ($fields['banner_subtitle']) : ?>
							<h3 class="main-subtitle"><?= $fields['banner_subtitle']; ?></h3>
						<?php endif; ?>
					</div>
					<?php if ($fields['banner_link']) : ?>
						<a href="<?= $slide['banner_link']['url']; ?>" class="main-link">
							<?= (isset($slide['banner_link']['title']) && $slide['banner_link']['title'])
									? $slide['banner_link']['title'] : 'התחילי בקנייה!'; ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_prods_2']) : ?>
	<div class="card-has-white">
		<?php get_template_part('views/partials/content', 'products_output',
			[
					'block_title' => $fields['home_prods_title_2'],
					'products' => $fields['home_prods_2'],
					'block_link' => $fields['home_prods_link_2']
			]); ?>
	</div>
<?php endif;
if ($fields['reviews_slider']) : ?>
	<section class="reviews-block arrows-slider">
		<?php if ($fields['reviews_image']) : ?>
			<img src="<?= $fields['reviews_image']['url']; ?>" alt="reviews_image" class="reviews-image-girl">
		<?php endif; ?>
		<div class="container">
			<?php if ($fields['reviews_title']) : ?>
				<div class="row justify-content-end">
					<div class="col-xl-8 col-lg-10 col-12">
						<h2 class="main-title"><?= $fields['reviews_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-8 col-md-9 col-12">
					<div class="base-slider" dir="rtl">
						<?php foreach ($fields['reviews_slider'] as $review) : ?>
							<div class="review-slide">
								<img src="<?= $review['url']; ?>" alt="review-img" class="review-img">
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['home_slider_img'],
					'content' => $fields['home_slider_seo'],
			]); ?>
	</div>
<?php endif;
if ($fields['home_gallery_img']) : ?>
	<div class="gallery-line-flex">
		<?php foreach ($fields['home_gallery_img'] as $item) : ?>
			<div class="gallery-item">
				<a href="<?= $item['url']; ?>" class="gallery-image" data-lightbox="gallery"
				   style="background-image: url('<?= $item['url']; ?>')"></a>
			</div>
		<?php endforeach;
		if ($inst = opt('instagram')) : ?>
			<a class="insta-item" href="<?= $inst; ?>">
						<span class="insta-item-wrap">
							<?php if ($fields['home_inst_title']) : ?>
								<span class="insta-title">
									<?= $fields['home_inst_title']; ?>
								</span>
							<?php endif; ?>
							<img src="<?= ICONS ?>instagram.png" alt="instagram">
							<?php if ($fields['home_inst_subtitle']) : ?>
								<span class="insta-subtitle">
							<?= $fields['home_inst_subtitle']; ?>
						</span>
							<?php endif; ?>
						</span>
			</a>
		<?php endif; ?>
	</div>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
			]);
}
if ($fields['home_videos']) : ?>
	<section class="home-video-block">
		<?php if ($fields['home_video_block_title']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="base-title text-center"><?= $fields['home_video_block_title']; ?></h2>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="arrows-slider video-slider-block mb-5">
			<div class="gallery-home" dir="rtl">
				<?php foreach ($fields['home_videos'] as $video) : if ($video['video_item']) : ?>
					<div>
						<div class="home-gallery-video" style="background-image: url('<?= getYoutubeThumb($video['video_item']); ?>')">
							<span class="gallery-video-overlay"></span>
							<span class="play-button" data-video="<?= getYoutubeId($video['video_item']); ?>">
									<img src="<?= ICONS ?>play.png">
								</span>
						</div>
					</div>
				<?php endif; endforeach; ?>
			</div>
		</div>
	</section>
<?php get_template_part('views/partials/repeat', 'video_modal');
endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
					'img' => $fields['faq_img'],
			]);
endif;
get_footer(); ?>
