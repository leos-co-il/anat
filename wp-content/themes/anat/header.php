<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header class="sticky">
	<div class="header-top">
		<div class="container-fluid">
			<div class="row justify-content-center align-items-center">
				<div class="col-xl-11 col-12">
					<div class="row justify-content-center align-items-center">
						<div class="col col-none"></div>
						<?php if ($flash_msg = opt('news_slider')) : ?>
							<div class="col-md-8 col-11">
								<div class="news-slider" dir="rtl">
									<?php foreach ($flash_msg as $msg): ?>
										<div class="single-msg d-flex centered">
											<?php if($msg['news_link']): ?>
											<a href="<?= (isset($msg['news_link']['url']) && $msg['news_link']['url']) ? $msg['news_link']['url'] : ''; ?>"
											   title="<?= $msg['news_text'] ?>" class="msg-link">
												<?php endif; ?>
												<span class="msg-text"><?= $msg['news_text'] ?></span>
												<?php if(isset($msg['news_link']['url']) && $msg['news_link']['url']): ?>
											</a>
										<?php endif; ?>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
						<div class="col-md col-none d-flex justify-content-end align-items-center">
							<?php if ($brand_img = opt('header_brand_logo')) : ?>
								<div class="brand-link-wrap">
									<?php if ($brand_link = opt('header_brand_link')) : ?>
									<a href="<?= $brand_link; ?>" class="header-brand-link">
										<?php endif; ?>
											<img src="<?= $brand_img['url']; ?>" alt="brand" class="header-brand">
										<?php if ($brand_link) : ?>
									</a>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12 position-relative">
					<?php if ($menu = opt('menu_item')) : ?>
						<div class="drop-menu">
							<div class="menu-line row justify-content-start align-items-stretch">
								<?php foreach ($menu as $slide) : if (isset($slide['link']['url'])) : ?>
									<a class="col-sm-3 col-6 menu-item-drop" href="<?= $slide['link']['url']; ?>">
								<span class="menu-icon-wrap">
									<span class="menu-icon-inside">
										<?php if ($slide['icon']) : ?>
											<img src="<?= $slide['icon']['url']; ?>" alt="menu-item">
										<?php endif; ?>
									</span>
								</span>
										<span class="menu-drop-title">
									<?= (isset($slide['link']['title']) && $slide['link']['title'])
											? $slide['link']['title'] : ''; ?>
								</span>
									</a>
								<?php endif; endforeach; ?>
							</div>
						</div>
					<?php endif; ?>

					<div class="row align-items-center justify-content-between">
						<div class="col-lg-auto col-sm-1 col-2">
							<button class="hamburger hamburger--spin menu-trigger" type="button">
								<span class="hamburger-box">
									<span class="hamburger-inner"></span>
								</span>
							</button>
						</div>
						<div class="col-lg col-md-11 col-none">
							<nav id="MainNav" class="h-100">
								<div id="MobNavBtn">
									<span></span>
									<span></span>
									<span></span>
								</div>
								<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
							</nav>
						</div>
						<div class="col-auto">
							<div class="log-button d-flex align-items-center">
								<a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="base-text font-weight-bold">
									התחבר
								</a>
								<a href="<?= wp_registration_url(); ?>" class="base-text font-weight-bold">
									| הרשם
								</a>
							</div>
						</div>
						<div class="col-lg-auto col">
							<div class="row justify-content-end align-items-center row-header-custom">
								<?php if(defined( 'YITH_WCWL' )): ?>
									<div class="col-auto">
										<a href="<?= get_permalink(get_option('yith_wcwl_wishlist_page_id')); ?>" class="header-btn">
											<img src="<?= ICONS ?>heart.png" alt="header-wishlist">
										</a>
									</div>
								<?php endif; ?>
								<div class="col-auto">
									<a href="<?= wc_get_cart_url(); ?>" class="header-btn" id="mini-cart">
										<img src="<?= ICONS ?>basket.png" alt="shopping-cart">
										<span class="count-circle" id="cart-count">
										<?= WC()->cart->get_cart_contents_count(); ?>
									</span>
									</a>
								</div>
								<?php if ($logo = opt('logo')) : ?>
									<div class="col-auto">
										<a href="/" class="logo">
											<img src="<?= $logo['url'] ?>" alt="logo">
										</a>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9 col-12 d-flex justify-content-center position-relative">
				<img src="<?= IMG ?>pop-flower.png" alt="flower" class="pop-flower">
				<div class="float-form d-flex flex-column align-items-center">
					<?php get_template_part('views/partials/repeat', 'form_pop',
							[
								'title' => opt('pop_form_title'),
								'text' => opt('pop_form_text'),
								'id' => '31',
							]); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pop-form-product">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-12 d-flex justify-content-center position-relative">
				<div class="float-product d-flex flex-column align-items-center position-relative">
					<div class="pop-product-wrapper">
						<span class="close-pop">
							<img src="<?= ICONS ?>close-pop.png" alt="close-popup">
						</span>
						<div class="put-here-pop">
							<div class="preloader">
								<?= svg_simple(ICONS.'loading.svg'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="triggers-col">
	<?php if ($tel = opt('tel')) : ?>
		<a href="tel:<?= $tel; ?>" class="trigger-item">
			<img src="<?= ICONS ?>fix-tel.png" alt="phone">
		</a>
	<?php endif;
	if ($facebook = opt('facebook')) : ?>
		<a href="<?= $facebook; ?>" class="trigger-item">
			<img src="<?= ICONS ?>fix-facebook.png" alt="facebook">
		</a>
	<?php endif;
	if ($inst = opt('instagram')) : ?>
		<a href="<?= $inst; ?>" class="trigger-item">
			<img src="<?= ICONS ?>fix-instagram.png" alt="instagram">
		</a>
	<?php endif; ?>
	<div class="trigger-item pop-trigger">
		<img src="<?= ICONS ?>fix-pop.png" alt="pop-trigger">
	</div>
</div>
