<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;


if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$post_id = $product->get_id();
$category = get_the_terms($post_id, 'product_cat');
$post_link = get_the_permalink();
$product_thumb = wp_get_attachment_image_url( get_post_thumbnail_id($post_id), 'full' );;
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = wp_get_attachment_image_url($_item, 'large', '');
	}
}
$fields = get_fields();
$info_del = $fields['prod_delivery_info'] ? $fields['prod_delivery_info'] : opt('prod_delivery');
$info_content = get_the_content();
//$related_products = $fields['relates_products'];
?>

<?php
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="container-fluid mt-3 mb-5">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="single-product-breadcrumbs">
					<a href="/">דף הבית</a>>
					<?php if($category): ?>
						<a href="<?= get_term_link($category[0]) ?>">
							<?= $category[0]->name ?>
						</a>>
					<?php endif; ?>
					<span><?php the_title(); ?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-6">
				<?php do_action('woocommerce_single_title_custom'); ?>
			</div>
		</div>
	</div>
	<section class="product-summary mb-5">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-xl-5 col-lg-6 col-12 d-flex flex-column align-items-start">
					<div class="put-price-here">
						<span class="price-title ml-2">מחיר:</span>
						<div class="put-price-here-place">
							<?= $product->get_price_html(); ?>
						</div>
					</div>
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
					?>

					<?php do_action('display_excerpt_custom');
					if ($info_del || $info_content): ?>
						<div class="product-item-info mt-4 w-100">
							<div id="accordion-product">
								<?php if ($info_content) : $c = 1; ?>
									<div class="card">
										<div class="card-header" id="heading<?= $c ?>">
											<h5 class="mb-0 accordion-button-wrap">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $c ?>" aria-expanded="true" aria-controls="collapse<?= $c ?>">
													תיאור
												</button>
											</h5>
										</div>
										<div id="collapse<?= $c ?>" class="collapse" aria-labelledby="heading<?= $c ?>" data-parent="#accordion-product">
											<div class="card-body base-output slider-output">
												<?= $info_content; ?>
											</div>
										</div>
									</div>
								<?php endif;
								if ($info_del) : $x = 2; ?>
									<div class="card">
										<div class="card-header" id="heading<?= $x ?>">
											<h5 class="mb-0 accordion-button-wrap">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $x ?>" aria-expanded="true" aria-controls="collapse<?= $x ?>">
													משלוחים
												</button>
											</h5>
										</div>
										<div id="collapse<?= $x ?>" class="collapse" aria-labelledby="heading<?= $x ?>" data-parent="#accordion-product">
											<div class="card-body base-output slider-output">
												<?= $info_del; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
					<div class="socials-share">
					<span class="share-text">
						שתפו מוצר זה
					</span>
						<!--	WHATSAPP-->
						<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
						   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
							<i class="fab fa-whatsapp"></i>
						</a>
						<!--	MAIL-->
						<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
						   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
							<i class="far fa-envelope"></i>
						</a>
						<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
						   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
							<i class="fab fa-facebook-f"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-6 gallery-slider-wrap">
					<div class="gallery-slider" dir="rtl">
						<?php if($product_thumb): ?>
							<div class="p-2">
								<a class="big-slider-item" style="background-image: url('<?= $product_thumb; ?>')"
								   href="<?= $product_thumb; ?>" data-lightbox="images"></a>
							</div>
						<?php endif;
						foreach ($product_gallery_images as $img): ?>
							<div class="p-2">
								<a class="big-slider-item" style="background-image: url('<?= $img; ?>')"
								   href="<?= $img; ?>" data-lightbox="images">
								</a>
							</div>
						<?php endforeach; ?>
					</div>
					<div class="thumbs" dir="rtl">
						<?php if($product_thumb): ?>
							<div class="p-2">
								<a class="thumb-item" style="background-image: url('<?= $product_thumb; ?>')"
								   href="<?= $product_thumb; ?>" data-lightbox="images-small"></a>
							</div>
						<?php endif;
						foreach ($product_gallery_images as $img): ?>
							<div class="p-2">
								<a class="thumb-item" style="background-image: url('<?= $img; ?>')"
								   href="<?= $img; ?>" data-lightbox="images-small">
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
//	if($related_products): ?>
<!--		<div class="related-pad">-->
<!--			<div class="container mt-5">-->
<!--				<div class="row">-->
<!--					<div class="col-12">-->
<!--						<h2 class="related-title">-->
<!---->
<!--						</h2>-->
<!--					</div>-->
<!--					<div class="col-12 related-products-col mb-5 arrows-slider slider-prods">-->
<!--						<div class="related-slider">-->
<!--							--><?php
//
//							echo get_template_part('views/partials/product', 'slider', [
//									'products' => $related_products,
//							]);
//
//							?>
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	--><?php //endif; ?>
</div>
<?php get_template_part('views/partials/repeat', 'form_block');
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
					'img' => $fields['faq_img'],
			]);
endif;
do_action( 'woocommerce_after_single_product' ); ?>
