<?php
/**
 * Sidebar
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/sidebar.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$cats = get_terms([
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'parent' => 0,
]);
?>
	<div class="accordion" id="accordion-cats">
		<div class="sidebar-cats">
			<?php foreach($cats as $number => $parent_term) : ?>
				<?php $term_children = get_term_children($parent_term->term_id, 'product_cat'); ?>
				<div class="card">
					<div class="card-header" >
						<a class="prod-cat-title" href="<?= get_category_link($parent_term); ?>">
							<?= $parent_term->name; ?>
						</a>
						<?php if (!empty($term_children)) : ?>
							<button class="btn btn-link accordion-trigger" type="button">
								<span class="arrow-top"><img src="<?= ICONS ?>arrow-bottom.txt" alt="arrow"></span>
							</button>
						<?php endif; ?>
					</div>
					<div id="collapse-<?= $number; ?>" class="collapse body-acc" >
						<div class="card-body">
							<?php if ($term_children) :
								foreach ($term_children as $x => $child) : $term = get_term_by( 'id', $child, 'product_cat' ); ?>
									<div class="subcat-item">
										<a href="<?= get_term_link($term); ?>">
											<?= $term->name; ?>
										</a>
									</div>
								<?php endforeach;
							endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="post-form-col shop-form-col my-5">
		<?php get_template_part('views/partials/repeat', 'form_pop',
				[
						'title' => opt('shop_form_title'),
						'text' => opt('shop_form_text'),
						'id' => '73',
				]); ?>
	</div>
<?php get_sidebar( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
