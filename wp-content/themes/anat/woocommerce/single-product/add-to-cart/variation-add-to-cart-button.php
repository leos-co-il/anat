<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$in_wishlist = null;

if ( function_exists( 'YITH_WCWL' ) ) {
	$in_wishlist = YITH_WCWL()->is_product_in_wishlist( $product->get_id() );
}
?>
<div class="add-to-cart-single-item d-flex woocommerce-variation-add-to-cart variations_button">
	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart d-flex" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
		<div class="qty-wrap">
			<div class="plus" data-id="<?= $product->get_id() ?>"> + </div>
			<?php

			do_action( 'woocommerce_before_add_to_cart_quantity' );

			woocommerce_quantity_input(
					array(
							'classes' => 'qty-for-' .  $product->get_id(),
							'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
							'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
							'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
					)
			);

			do_action( 'woocommerce_after_add_to_cart_quantity' );

			?>
			<div class="minus" data-id="<?= $product->get_id() ?>"> - </div>
		</div>
		<div class="wrapper-product-wish">
			<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt add-custom">
				<span>הוספה לסל</span>
			</button>
			<?php if($in_wishlist !== null): ?>
				<div class="wishlist-btn">
					<?php if($in_wishlist): ?>
						<span class="item-wishlist has-tooltip">
<!--							  <span class="tooltiptext">המוצר כבר נמצא ברשימת משאלות</span>-->
								<?= svg_simple(THEMEPATH . '/assets/iconsall/like_full.svg') ?>
							</span>
					<?php elseif ($in_wishlist === false): ?>
						<?= do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
		<input type="hidden" name="attribute_pa_size" class="put-size" value="0" />
		<input type="hidden" name="attribute_pa_ml" class="put-ml" value="0" />
		<input type="hidden" name="add-to-cart" class="put-prod-id" value="<?php echo absint( $product->get_id() ); ?>" />
		<input type="hidden" name="product_id" class="put-prod-id" value="<?php echo absint( $product->get_id() ); ?>" />
		<input type="hidden" name="variation_id" class="put-var-id variation_id" value="0" />
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' );
	do_action('yith_wcwl_selectively_hide_add_to_wishlist');?>
</div>

<!--<div class="woocommerce-variation-add-to-cart variations_button">-->
<!--	--><?php //do_action( 'woocommerce_before_add_to_cart_button' ); ?>
<!---->
<!--	--><?php
//	do_action( 'woocommerce_before_add_to_cart_quantity' );
//
//	woocommerce_quantity_input(
//		array(
//			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
//			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
//			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
//		)
//	);
//
//	do_action( 'woocommerce_after_add_to_cart_quantity' );
//	?>
<!---->
<!--	<button type="submit" class="single_add_to_cart_button button alt">--><?php //echo esc_html( $product->single_add_to_cart_text() ); ?><!--</button>-->
<!---->
<!--	--><?php //do_action( 'woocommerce_after_add_to_cart_button' ); ?>
<!---->
<!--	<input type="hidden" name="add-to-cart" value="--><?php //echo absint( $product->get_id() ); ?><!--" />-->
<!--	<input type="hidden" name="product_id" value="--><?php //echo absint( $product->get_id() ); ?><!--" />-->
<!--	<input type="hidden" name="variation_id" class="variation_id" value="0" />-->
<!--</div>-->
